_id: TdoM8rKDS3FtmY81
_key: '!items!TdoM8rKDS3FtmY81'
_stats:
  coreVersion: '12.331'
folder: aFRC2yEnNJ6BUx24
img: systems/pf1/icons/spells/link-blue-2.jpg
name: Telekinetic Blast
system:
  abilityType: sp
  actions:
    - _id: ZxAx9U9IWLmBC8v2
      ability:
        attack: dex
        damage: con
        damageMult: 1
      actionType: rsak
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      attackBonus: >-
        (min(@resources.classFeat_burn.value, floor(@class.level /
        3)))[Elemental Overflow]
      damage:
        parts:
          - formula: >-
              (ceil(@class.level / 2))d6 + (ceil(@class.level / 2))[Level Bonus]
              + (min(@resources.classFeat_burn.value, floor(@class.level / 3)) *
              2)[Elemental Overflow]
            types:
              - bludgeoning
      duration:
        units: inst
      name: Bludgeoning
      range:
        units: ft
        value: '30'
      uses:
        autoDeductChargesCost: '0'
    - _id: EaJIJo8oozr57hFM
      ability:
        attack: dex
        damage: con
        damageMult: 1
      actionType: rsak
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      attackBonus: >-
        (min(@resources.classFeat_burn.value, floor(@class.level /
        3)))[Elemental Overflow]
      damage:
        parts:
          - formula: >-
              (ceil(@class.level / 2))d6 + (ceil(@class.level / 2))[Level Bonus]
              + (min(@resources.classFeat_burn.value, floor(@class.level / 3)) *
              2)[Elemental Overflow]
            types:
              - piercing
      duration:
        units: inst
      name: Piercing
      range:
        units: ft
        value: '30'
      uses:
        autoDeductChargesCost: '0'
    - _id: w1nIG6OckNL5g6x6
      ability:
        attack: dex
        damage: con
        damageMult: 1
      actionType: rsak
      activation:
        type: standard
        unchained:
          cost: 2
          type: action
      attackBonus: >-
        (min(@resources.classFeat_burn.value, floor(@class.level /
        3)))[Elemental Overflow]
      damage:
        parts:
          - formula: >-
              (ceil(@class.level / 2))d6 + (ceil(@class.level / 2))[Level Bonus]
              + (min(@resources.classFeat_burn.value, floor(@class.level / 3)) *
              2)[Elemental Overflow]
            types:
              - slashing
      duration:
        units: inst
      name: Slashing
      range:
        units: ft
        value: '30'
      uses:
        autoDeductChargesCost: '0'
  associations:
    classes:
      - Kineticist
  description:
    value: >-
      <p><strong>Element</strong> aether; <strong>Type</strong> simple blast
      (Sp); <strong>Level</strong> -; <strong>Burn</strong>
      0</p><p><strong>Blast Type</strong> physical; <strong>Damage</strong>
      bludgeoning, piercing, or slashing</p><hr /><p>You throw a nearby
      unattended object at a single foe as a ranged attack. The object must
      weigh no more than 5 pounds per kineticist level you possess. If the
      attack hits, the target and the thrown object each take the blast's
      damage. Since the object is enfolded in strands of aether, even if you use
      this power on a magic weapon or other unusual object, the attack doesn't
      use any of the magic weapon's bonuses or effects; it simply deals your
      blast damage. Alternatively, you can loosen the strands of aether in order
      to deal damage to both the object and the target as though you had thrown
      the object yourself (instead of dealing your normal blast damage). You
      substitute your Constitution modifier for your Strength modifier if
      throwing the object would have added your Strength modifier on the damage
      roll, and you don't take the -4 penalty on the attack roll for throwing an
      object that wasn't designed to be thrown. In this case, the object's
      special effects apply (including effects from its materials), and if the
      object is a weapon, you must be proficient with it and able to wield it
      with one hand; otherwise, the item deals damage as a one-handed improvised
      weapon for a creature of your size.</p>
  sources:
    - id: PZO1132
      pages: '15'
  subType: classFeat
  tags:
    - Simple Blast
    - Aether Element
type: feat
