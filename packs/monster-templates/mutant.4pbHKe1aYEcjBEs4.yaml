_id: 4pbHKe1aYEcjBEs4
_key: '!items!4pbHKe1aYEcjBEs4'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/violet_07.jpg
name: Mutant
system:
  changes:
    - _id: 596pdv81
      formula: '4'
      target: str
      type: untyped
    - _id: f2760g6m
      formula: '4'
      target: str
      type: untyped
    - _id: u0rlwpzv
      formula: '-2'
      target: str
      type: untyped
  crOffset: '1'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>When long-term exposure to radiation
      or bizarre magical fields doesn’t result in a creature’s death, it might
      mutate the creature into a twisted version of itself. Some of these
      mutations can be advantageous, while others are unquestionably a
      hindrance. Mutants often band together into roving bands of loosely
      affiliated marauders, traveling the land in search of food, shelter, or
      whatever else motivates their fractured minds.<p>"Mutant" is an acquired
      template that can be added to any living, corporeal creature. A mutant
      retains the base creature’s statistics and special abilities except as
      noted here.<p><b>Challenge Rating:</b> Base creature’s CR +
      1.<p><b>Type:</b> The creature’s type changes to aberration (augmented).
      Do not recalculate its Hit Dice, base attack bonus, or
      saves.<p><b>Attacks:</b> A mutant retains all the natural weapons,
      manufactured weapon attacks, and weapon and armor proficiencies of the
      base creature.<p><b>Special Abilities:</b> A mutant retains any
      extraordinary and supernatural abilities of the base
      creature.<p><b>Abilities:</b> A mutant gains a +4 bonus to two ability
      scores of its choice and takes a –2 penalty to two ability scores of its
      choice (These can be selected under the "Changes" tab of this
      window).<p><b>Skills:</b> A mutant gains Climb, Intimidate, Knowledge (any
      one), Perception, Sense Motive, Survival, and Swim as class
      skills.<p><b>Deformities:</b> Each mutant has one of the following
      deformities. It can take a second deformity to gain a mutation as detailed
      in Mutations. A deformity can’t be taken if it wouldn’t disadvantage the
      mutant.<p><em>Blind (Ex):</em> The mutant can’t see, and gains the blinded
      condition unless it possesses a means of seeing other than normal vision,
      darkvision, or low-light vision. This blindness can’t be magically
      removed.<p><em>Deaf (Ex):</em> The mutant can’t hear, and gains the
      deafened condition. This deafness can’t be magically
      removed.<p><em>Fragile (Ex):</em> When the mutant fails a Fortitude save,
      it is staggered for 1 round.<p><em>Fractured Mind (Ex):</em> When the
      mutant fails a Will save, it is confused for 1 round.<p><em>Lame (Ex):
      </em>The mutant’s stunted legs reduce its base speed by 10 feet. This
      deformity can’t be taken if the mutant’s base speed is already slower than
      20 feet.<p><em>Light Blindness (Ex):</em> The mutant has the <a
      href="https://aonprd.com/UMR.aspx?ItemName=Light%20Blindness">light
      blindness</a> special ability. This deformity can’t be taken in
      conjunction with the blind deformity.<p><em>Mindless (Ex): </em>The mutant
      gains the mindless trait. A mindless mutant has no Intelligence score,
      loses all feats and skills, and is immune to mind-affecting effects. A
      mutant with class levels retains its hit points, base attack bonus, and
      base saves from its class levels, but loses all weapon and armor
      proficiencies and other class abilities. This deformity could prevent the
      mutant from being eligible to take certain special abilities, at the GM’s
      discretion.<p><em>Misshapen (Ex): </em>Humanoid mutants only. The mutant
      can’t wear armor (including magic armor) fashioned for humanoid creatures.
      Armor made to fit the mutant costs twice as much.<p><em>Poor Ability
      (Ex):</em> The mutant takes a –4 penalty to one ability score in addition
      to the normal ability score penalties applied by the
      template.<p><em>Spasms (Ex):</em> When the mutant fails a Reflex save, for
      1 round it can’t take attacks of opportunity or immediate actions and
      loses its Dexterity bonus to AC, on ranged attack rolls and attack rolls
      made using finesse weapons, and on ability checks and skill
      checks.<p><em>Useless Arm (Ex):</em> One of the mutant’s arms is malformed
      and useless.<p><em>Vulnerability (Ex):</em> The mutant is vulnerable to
      one energy type. If the base creature has innate resistance or immunity to
      that energy type, it loses those abilities.<p><b>Mutations:</b> A mutant
      gains one of the beneficial mutations below when it acquires this
      template, plus an additional mutation for every 4 Hit Dice it possesses.
      By taking an extra deformity (see above), a mutant can add an additional
      mutation. Only the first extra deformity provides this benefit. A mutant
      that gains additional Hit Dice after acquiring this template does not gain
      additional mutations.<p><em>Armored (Ex):</em> The mutant’s natural armor
      bonus to its AC increases by 2. This ability can be taken multiple
      times.<p><em>Bulbous Eyes (Ex):</em> The mutant has darkvision with a
      range of 60 feet and low-light vision.<p><em>Celerity (Ex): </em>As a
      swift action, the mutant gains the benefits of <a
      href="https://aonprd.com/DeityDisplay.aspx?ItemName=haste">haste</a> for 1
      round. This ability can be used once every 1d4 rounds. The mutant gains a
      +2 bonus on Initiative checks.<p><em>Echolocation (Ex): </em>The mutant
      has blindsense with a range of 30 feet and gains <a
      href="https://aonprd.com/FeatDisplay.aspx?ItemName=Blind-Fight">Blind-Fight</a>
      as a bonus feat.<p><em>Extra Arm (Ex):</em> The mutant has an extra arm
      and gains <a
      href="https://aonprd.com/FeatDisplay.aspx?ItemName=Multiweapon%20Fighting">Multiweapon
      Fighting</a> as a bonus feat if this mutation brings its total number of
      arms above two. This ability can be taken multiple times, adding an arm
      each time.<p><em>Fast Healing (Ex): </em>The mutant gains fast healing
      5.<p><em>Feral (Ex):</em> The mutant gains a bite attack and gains one
      claw attack for each arm or forelimb. These natural weapons deal damage
      based on the mutant’s size. If it already has these attacks, their damage
      improves by one size category.<p><em>Gills (Ex):</em> The mutant has the
      aquatic subtype, the amphibious ability, and a swim speed equal to its
      base speed.<p><em>Increased Speed (Ex):</em> One of the mutant’s speeds
      increases by 10 feet. If this mutation is taken multiple times, apply it
      to a different speed each time.<p><em>Leaping (Ex):</em> The mutant gains
      Acrobatics as a class skill and a +10 bonus on Acrobatics checks to jump.
      The mutant always counts as having a running start when
      jumping.<p><em>Mental Armor (Su):</em> The mutant generates a protective
      field (as <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=mage%20armor">mage
      armor</a>) while conscious. If its mental armor is removed, the mutant can
      restore it as a swift action.<p><em>Rage (Ex):</em> The mutant gains the
      ability to enter a manic rage, as per the <a
      href="https://aonprd.com/ClassDisplay.aspx?ItemName=Barbarian">barbarian
      rage class feature</a>. The mutant uses its Hit Dice as its barbarian
      level. If it also has levels in barbarian, it adds its racial Hit Dice to
      its barbarian levels to determine its number of rounds of rage per
      day.<p><em>Resistance (Ex):</em> The mutant has resistance 10 to a single
      energy type. This special ability can be selected multiple times, for the
      same or different types. Selecting it twice for one energy type grants
      resistance 20; taking it three times for the same energy type provides
      immunity.<p><em>Rugged (Ex): </em>The mutant has DR 5/—.<p><em>Sealed Mind
      (Ex):</em> The mutant is immune to mind-affecting effects.<p><em>Slam
      (Ex):</em> The mutant gains a slam attack for each arm or forelimb. These
      attacks deal damage based on the mutant’s size. If the mutant already has
      a slam attack, its slam damage improves by one size
      category.<p><em>Spell-Like Ability (Sp)</em>: The mutant has one of the
      following spell-like abilities, usable at will unless noted otherwise: <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=charm%20monster">charm
      monster</a>, <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=charm%20person">charm
      person</a>, <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=chill%20metal">chill
      metal</a>, <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=deep%20slumber">deep
      slumber</a>, <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=dimension%20door">dimension
      door</a> (3/day), <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=dominate%20person">dominate
      person</a> (dominating a new character frees any previously dominated
      creature), <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=heat%20metal">heat
      metal</a>, <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=mirror%20image">mirror
      image</a>, <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=modify%20memory">modify
      memory</a>, <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=rage">rage</a>, <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=shocking%20grasp">shocking
      grasp</a>, <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=shout">shout</a>, <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=sleep">sleep</a>, <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=suggestion">suggestion</a>,
      <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=telekinesis">telekinesis</a>.
      The mutant’s caster level is equal to its Hit Dice. The mutant can choose
      only spells with a level no higher than half its Hit Dice. This ability
      can be taken multiple times, choosing a different spell each
      time.<p><em>Stench (Ex):</em> The mutant has the stench ability, with a
      duration of 1 minute.<p><em>Telepathy (Su, Sp):</em> The mutant has
      telepathy with a range of 100 feet as a supernatural ability and <a
      href="https://aonprd.com/SpellDisplay.aspx?ItemName=detect%20thoughts">detect
      thoughts</a> as a spell-like ability, usable at will.<p><em>Wings (Ex):
      </em>The mutant grows wings, gaining a fly speed of 40 feet with average
      maneuverability.</p>
  subType: template
type: feat
