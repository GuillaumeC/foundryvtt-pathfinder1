_id: pftwaky9o6qa4Q1q
_key: '!items!pftwaky9o6qa4Q1q'
_stats:
  coreVersion: '12.331'
img: systems/pf1/icons/skills/violet_07.jpg
name: Petitioner
system:
  crOffset: '-(@attribute.hd.total - (@attribute.hd.total -1))'
  description:
    value: >-
      <p><b>Acquired/Inherited Template</b> Acquired<br><b>Simple Template</b>
      No<br><b>Usable with Summons</b> No<p>Petitioners are the souls of mortals
      brought to the Outer Planes after death in order to experience their
      ultimate punishment, reward, or fate. A petitioner retains fragments of
      its memories from life, and its appearance depends not only upon the shape
      it held in life but also upon the nature of the Outer Plane to which it
      has come. The stat block detailed above presents a typical petitioner
      formed from the soul of an average human—it does not include any of the
      plane-specific abilities or features a petitioner gains, and should be
      modified as appropriate depending on the plane to which the petitioner is
      assigned.<p>Creatures who die, become petitioners, and then return to life
      retain no memories of the time they spent as petitioners in the afterlife.
      A petitioner who dies is gone forever—its "life force" has either returned
      to the Positive Energy Plane or, in some cases, provided the energy to
      trigger the creation of another outsider. Petitioners who please a deity
      or another powerful outsider can be granted rewards—the most common such
      reward manifests as a transformation into a different outsider, such as an
      archon, azata, demon, or devil, depending upon the petitioner’s alignment.
      In rare cases, a creature can retain its personality from life all the way
      through its existence as a petitioner and into its third "life" as an
      outsider, although such events are rare indeed.<p>"Petitioner" is an
      acquired template that can be added to any creature whose soul migrates to
      one of the Outer Planes following its death (henceforth referred to as the
      base creature). The petitioner uses all of the base creature’s statistics
      and abilities except as noted below.<p><b>CR:</b> A petitioner’s CR is 1.
      In some cases, at the GM’s discretion, particularly large or unusual
      petitioners with higher than normal ability scores may begin with a higher
      CR; compare the petitioner’s statistics to the values on Table 1–1 on page
      293 to help determine an unusual petitioner’s starting CR.</p>

      <p><b>Alignment:</b> A petitioner’s alignment is identical to that of its
      home plane.</p>

      <p><b>Size and Type:</b> The creature’s type changes to outsider. It loses
      all subtypes. Its size does not change.</p>

      <p><b>Senses:</b> Petitioners lose any unusual senses they had, but gain
      darkvision 60 feet.</p>

      <p><b>Armor Class:</b> The petitioner loses all racial bonuses to its
      Armor Class.</p>

      <p><b>Hit Dice:</b> Petitioners lose all racial and class-based Hit Dice
      and gain 2d10 racial Hit Dice as outsiders.</p>

      <p><b>Saves:</b> Petitioners have good Fortitude and Reflex saves; a
      petitioner’s base saves are Fort +3, Ref +3, Will +0.</p>

      <p><b>Defensive Abilities:</b> Petitioners lose all the defensive
      abilities of the base creature. Petitioners are immune to mind-affecting
      effects.</p>

      <p><b>Attacks:</b> The creature’s BAB is +2, subject to modification for
      size and Strength. It loses all natural attacks and gains a slam attack as
      appropriate for a creature of its size.</p>

      <p><b>Special Attacks:</b> Petitioners lose all special attacks.</p>

      <p><b>Abilities:</b> Same as the base creature.</p>

      <p><b>Feats:</b> Petitioners lose all feats. As a 2 HD outsider, a
      petitioner gains one feat—typically Toughness.</p>

      <p><b>Skills:</b> Petitioners lose all skill ranks they possessed as
      mortals. As a 2 HD outsider, a petitioner has 12 skill ranks it can spend
      on skills (with a maximum of 2 ranks in any one skill), and gains bonus
      skill ranks as appropriate for its Intelligence. Unlike most outsiders,
      petitioners do not gain an additional 4 class skills beyond those
      available to all outsiders.</p>

      <p><b>Special Qualities:</b> Petitioners lose all special qualities, along
      with all abilities granted by class levels (including increases on saving
      throws and to HD and BAB).</p>

      <h2>Petitioner Traits</h2>

      <p>A petitioner gains additional traits based on its home
      plane.<p><b>Abaddon (Neutral Evil):</b> The "hunted" have bodies that are
      identical to what they had in life—these petitioners are doomed to be
      stalked and eventually consumed by the daemons that lust for souls. A
      hunted that survives long enough eventually warps and twists into a
      daemon. The hunted gain DR 5/— and fast healing 1 so that they provide a
      slightly more robust hunt for their daemonic predators.</p>

      <p><b>Abyss (Chaotic Evil):</b> "Larvae" are perhaps the most hideous of
      petitioners—they appear as pallid, maggot-like creatures with heads
      similar to those they possessed in life. Larvae that feed long enough on
      Abyssal filth eventually transform into demons. They have cold,
      electricity, and fire resistance 10, and instead of a slam attack gain a
      bite attack as appropriate for their size.</p>

      <p><b>Elysium (Chaotic Good):</b> The "chosen" have idealized versions of
      their mortal bodies. In time, after experiencing the pleasures Elysium has
      to offer, the chosen become azatas. The chosen gain resistance to cold and
      fire 10 and a +2 bonus to Charisma.</p>

      <p><b>Heaven (Lawful Good):</b> The "elect" appear similar to their mortal
      forms, save that they possess a golden halo and feathered wings. After
      spending enough time aiding heavenly tasks, the elect become archons. They
      gain a fly speed equal to their base speed (average mobility).</p>

      <p><b>Hell (Lawful Evil):</b> The "damned" retain their mortal forms, but
      are heavily scarred by various tortures. Those who endure the torments of
      Hell long enough may eventually be approved for transformation into
      devils. The damned gain immunity to fire (but not immunity to the pain
      caused by fire—whenever one of the damned takes fire damage, it must make
      a DC 15 Fortitude save to resist being stunned by the pain for 1d4
      rounds).</p>

      <p><b>Limbo (Chaotic Neutral):</b> The "shapeless" retain their basic
      forms, but these forms constantly waver and shimmer, as if they were
      ghosts in peril of dissolving away. After wallowing in the chaos of Limbo
      for long enough, they can transform into proteans. The shapeless have the
      incorporeal subtype, an incorporeal touch attack, and all advantages
      granted by that defensive ability.</p>

      <p><b>Nirvana (Neutral Good):</b> The "cleansed" take on the forms of
      animals that closely approximate their personalities. Upon achieving true
      enlightenment, they transform into agathions. The cleansed gain cold and
      sonic resistance 10 and a +2 bonus to Wisdom.</p>

      <p><b>Purgatory (Neutral):</b> The "dead" appear as animated skeletons but
      are not undead—in time, they can earn the right to become aeons. They gain
      DR 10/bludgeoning and immunity to cold.</p>

      <p><b>Utopia (Lawful Neutral):</b> The "remade" retain the same body shape
      but have milky white skin covered in dense black script, as if some
      strange scribe had used them for parchment. Upon deciphering the riddles
      posed by these complex lines of script, one of the remade can enter an
      axiomite forge to be transformed into an inevitable. The remade are immune
      to hostile transmutation effects and gain a +2 bonus to Intelligence.</p>
  subType: template
type: feat
