_id: g0x2fnktg1553wv1
_key: '!items!g0x2fnktg1553wv1'
_stats:
  coreVersion: '12.331'
folder: 9hBqwSiT8GIHl0Ht
img: icons/magic/death/skull-horned-worn-fire-blue.webp
name: Symbol of Death
system:
  actions:
    - _id: 3ghytfcvn8jy223f
      actionType: spellsave
      activation:
        cost: 10
        type: minute
        unchained:
          cost: 10
          type: minute
      duration:
        units: seeText
      effect: one symbol
      name: Use
      range:
        units: ft
        value: '0'
      save:
        description: Fortitude negates
        type: fort
  components:
    material: true
    somatic: true
    verbal: true
  description:
    value: >-
      <p>This spell allows you to scribe a potent rune of power upon a
      surface.</p><p>When triggered, a <i>symbol of death</i> kills one or more
      creatures within 60 feet of the <i>symbol</i> (treat as a burst) whose
      combined total current hit points do not exceed 150. The <i>symbol of
      death</i> affects the closest creatures first, skipping creatures with too
      many hit points to affect.</p><p>Once triggered, the <i>symbol</i> becomes
      active and glows, lasting for 10 minutes per caster level or until it has
      affected 150 hit points' worth of creatures, whichever comes first. A
      creature that enters the area while the <i>symbol of death</i> is active
      is subject to its effect, whether or not that creature was in the area
      when it was triggered. A creature need save against the <i>symbol</i> only
      once as long as it remains within the area, though if it leaves the area
      and returns while the <i>symbol</i> is still active, it must save
      again.</p><p>Until it is triggered, the <i>symbol of death</i> is inactive
      (though visible and legible at a distance of 60 feet). To be effective, a
      <i>symbol of death</i> must always be placed in plain sight and in a
      prominent location. Covering or hiding the rune renders the <i>symbol of
      death</i> ineffective, unless a creature removes the covering, in which
      case the <i>symbol of death</i> works normally.</p><p>As a default, a
      <i>symbol of death</i> is triggered whenever a creature does one or more
      of the following, as you select: looks at the rune; reads the rune;
      touches the rune; passes over the rune; or passes through a portal bearing
      the rune. Regardless of the trigger method or methods chosen, a creature
      more than 60 feet from a <i>symbol of death</i> can't trigger it (even if
      it meets one or more of the triggering conditions, such as reading the
      rune). Once the spell is cast, a <i>symbol of death</i>'s triggering
      conditions cannot be changed.</p><p>In this case, "reading" the rune means
      any attempt to study it, identify it, or fathom its meaning. Throwing a
      cover over a <i>symbol of death</i> to render it inoperative triggers it
      if the <i>symbol</i> reacts to touch. You can't use a <i>symbol of
      death</i> offensively; for instance, a touch-triggered <i>symbol of
      death</i> remains untriggered if an item bearing the <i>symbol of
      death</i> is used to touch a creature. Likewise, a <i>symbol of death</i>
      cannot be placed on a weapon and set to activate when the weapon strikes a
      foe.</p><p>You can also set special triggering limitations of your own.
      These can be as simple or elaborate as you desire. Special conditions for
      triggering a <i>symbol of death</i> can be based on a creature's name,
      identity, or alignment, but otherwise must be based on observable actions
      or qualities. Intangibles such as level, class, HD, and hit points don't
      qualify.</p><p>When scribing a <i>symbol of death</i>, you can specify a
      password or phrase that prevents a creature using it from triggering the
      <i>symbol</i>'s effect. Anyone using the password remains immune to that
      particular rune's effects so long as the creature remains within 60 feet
      of the rune. If the creature leaves the radius and returns later, it must
      use the password again.</p><p>You also can attune any number of creatures
      to the <i>symbol of death</i>, but doing this can extend the casting time.
      Attuning one or two creatures takes negligible time, and attuning a small
      group (as many as 10 creatures) extends the casting time to 1 hour.
      Attuning a large group (as many as 25 creatures) takes 24 hours. Attuning
      larger groups takes an additional 24 hours per 25 creatures. Any creature
      attuned to a <i>symbol of death</i> cannot trigger it and is immune to its
      effects, even if within its radius when it is triggered. You are
      automatically considered attuned to your own <i>symbol</i>s of
      <i>death,</i> and thus always ignore the effects and cannot inadvertently
      trigger them.</p><p><i>Read magic</i> allows you to identify a
      <i>symbol</i> with a Spellcraft check (DC 10 + the <i>symbol</i>'s spell
      level). Of course, if the <i>symbol</i> is set to be triggered by reading
      it, this will trigger the <i>symbol</i>.</p><p>A <i>symbol of death</i>
      can be removed by a successful <i>dispel magic</i> targeted solely on the
      rune. An <i>erase</i> spell has no effect on a <i>symbol of death</i>.
      Destruction of the surface where a <i>symbol of death</i> is inscribed
      destroys the <i>symbol</i> but also triggers it.</p><p><i>Symbol of
      death</i> can be made permanent with a <i>permanency</i> spell.</p><p>A
      permanent <i>symbol of death</i> that is disabled or has affected its
      maximum number of hit points becomes inactive for 10 minutes, but then can
      be triggered again as normal.</p><p><i>Note:</i> Magic traps such as
      <i>symbol of death</i> are hard to detect and disable. A rogue (only) can
      use the Perception skill to find a <i>symbol of death</i> and Disable
      Device to thwart it. The DC in each case is 25 + spell level, or 33 for
      <i>symbol of death</i>.</p>
  descriptors:
    - death
  learnedAt:
    class:
      arcanist: 8
      cleric: 8
      occultist: 6
      oracle: 8
      sorcerer: 8
      witch: 8
      wizard: 8
    domain:
      Rune: 8
  level: 8
  materials:
    gpValue: 10000
    value: mercury and phosphorus, plus powdered diamond and opal worth 5,000 gp each
  school: nec
  sources:
    - id: PZO1110
      pages: '355'
type: spell
