_id: rZv6gghP92T1cEDy
_key: '!journal!rZv6gghP92T1cEDy'
_stats:
  coreVersion: '12.331'
name: Technology
pages:
  - _id: mTkj156p3HIRVBEd
    _key: '!journal.pages!rZv6gghP92T1cEDy.mTkj156p3HIRVBEd'
    _stats:
      coreVersion: '12.331'
    name: Technological Hazards
    sort: 100000
    text:
      content: >-
        <p>@Source[PZO9272;pages=54-56]</p><h2>Radiation</h2><p>Radiation is a
        very real threat to those who explore technological ruins. Radiation is
        a poison effect whose initial effect causes Constitution drain and
        secondary effect causes Strength damage. Radiation dangers are organized
        into four categories: low, medium, high, and severe.</p><p><strong>Area
        of Effect</strong>: Radiation suffuses a spherical area of effect that
        can extend into solid objects. The closer one gets to the center of an
        area of radiation, the stronger the radiation effect becomes. Radiation
        entries list the maximum level of radiation in an area, as well as the
        radius out to which this radiation level applies. Each increment up to
        an equal length beyond that radius degrades the radiation strength by
        one level. For example, a spherical area of high radiation with a radius
        of 20 feet creates a zone of medium radiation 21 feet to 40 feet from
        the center in all directions, and a similar zone of low radiation from
        41 to 60 feet.</p><p><strong>Initial Effect</strong>: Radiation
        initially deals Constitution drain unless the affected character
        succeeds at a Fortitude save. A new saving throw must be attempted to
        resist radiation's initial damage each round a victim remains exposed to
        it.</p><p><strong>Secondary Effect</strong>: Secondary effects from
        radiation deal Strength damage at a much slower rate than most poisons.
        This secondary effect ends only after a character succeeds at two
        consecutive Fortitude saving throws to resist secondary radiation
        damage. If a character has Strength damage equal to his current Strength
        score, further damage dealt by a secondary effect is instead
        Constitution damage.</p><p><strong>Removing Radiation Effects</strong>:
        All radiation damage is a poison effect, and as such it can be removed
        with any effect that neutralizes poison. Ability damage and drain caused
        by radiation damage can be healed normally.</p><table><thead><tr><th
        colspan="4">Radiation Damage</th></tr><tr><td>Radiation
        Level</td><td>Fort DC</td><td>Initial Effect</td><td>Secondary
        Effect</td></tr></thead><tbody><tr><td>Low</td><td>13</td><td>1 Con
        drain</td><td>1 Str
        damage/day</td></tr><tr><td>Medium</td><td>17</td><td>1d4 Con
        drain</td><td>1d4 Str
        damage/day</td></tr><tr><td>High</td><td>22</td><td>2d4 Con
        drain</td><td>1d6 Str
        damage/day</td></tr><tr><td>Severe</td><td>30</td><td>4d6 Con
        drain</td><td>2d6 Str damage/day</td></tr></tbody></table><h2>Timeworn
        Technology</h2><p>Between languishing in forgotten ruins open to the
        elements, being used by those ignorant of the nature of this technology,
        and having no one skilled at building, maintaining, or repairing such
        devices, most technological items are "timeworn"—damaged and
        malfunctioning (when not completely nonfunctional). These malfunctions
        manifest in two ways: limited charges and glitches.</p><p>Only
        technological items that consume charges (including nanite canisters) or
        are pharmaceutical items can be affected by these timeworn rules, though
        any technological item can still become broken or nonfunctional just as
        any other item.</p><p>A piece of timeworn technology may have additional
        aesthetic and functional differences from a new piece of the same
        equipment. Many of these effects are purely cosmetic, such as cracks in
        the casing of an arc grenade or primitive etchings on a suit of
        technological armor placed there by a barbarian millennia ago. Pieces of
        timeworn technology may also have minor mechanical effects beyond
        glitches (at the GM's discretion). A timeworn laser pistol might
        constantly hum at a low but noticeable frequency, imparting a -1 penalty
        on Stealth checks. A timeworn plasma grenade could be caked in a strange
        viscous fluid that has a pungent odor, making its wielder more easily
        tracked via scent. Timeworn technological items should clearly evoke a
        sense of age and danger, and even the most standard piece of Androffan
        gear can be made unique based on individual deteriorations.</p><p>Note
        that not every technological item is timeworn, but most technology that
        PCs encounter outside of the deepest and most remote of ruins will be.
        These items function as presented in the previous chapter, can be
        recharged, and do not suffer glitches.</p><p>A timeworn technological
        item that is still somewhat functioning is worth half of its normal
        listed price, though one drained of its charges is worth 1% of its
        normal value, as a curiosity to collectors. Timeworn technology also has
        the following properties.</p><h3>Non-Rechargable</h3><p>Timeworn
        technological items can't be recharged. When a timeworn technological
        item is properly identified or first used, roll randomly to determine
        how many charges it has left before it becomes
        useless.</p><h3>Glitches</h3><p>Timeworn technology sometimes doesn't
        work the way it was originally intended to. When an item glitches, its
        effect is hampered or enhanced, as determined by a d% roll. See the
        rolltables for Timeworn Glitches for a list of glitch effects for armor,
        weapons, pharmaceuticals, and other technological equipment. Not all
        glitches are catastrophic; they represent unpredictable effects, for
        good and ill. When a timeworn technological item is first used after a
        month or more of inactivity, there's a 50% chance that it will glitch.
        Additionally, when using an item in a way that would drain its last
        charge, there's a 50% chance it will glitch. If an item requires a d20
        roll (such as a skill check or an attack roll) to activate or use, it
        has a 50% chance to glitch on a natural 1..</p><p>Timeworn technology
        doesn't always work as intended. There's a 50% chance that timeworn
        items glitch under the following conditions.</p><ul><li><p>When an item
        is first used after a month or more of
        inactivity.</p></li><li><p>Anytime a single-use consumable is
        used.</p></li><li><p>When using an item in a way that would drain its
        last charge.</p></li><li><p>Whenan item requires a d20 roll (such as a
        skill check or an attack roll) to activate or use, and that roll results
        in a natural 1.</p></li><li><p>When a critical hit is confirmed against
        the wearer of an active defensive item, such as armor or a force
        field.</p></li></ul><p>Not all glitches are catastrophic. When an item
        glitches, its effect is hampered or enhanced, as determined by a d%
        roll. For items that can consume a variable number of charges, these
        additional charges do not affect the item's performance; if such an item
        must consume twice as many charges, the amount is based on how many
        charges the user intended to use. When a glitch would cause an item to
        consume more charges than it currently holds, the item is drained of all
        charges and fails to function, but any negative effects still occur.
        Items that fail to function simply shut down if activated, and cannot be
        activated again for 1 round.</p><h2>Traps and Technology</h2><p>Mundane
        and magical traps can incorporate technological elements in their
        construction. Computers and AIs can also be connected to traps, allowing
        them to selectively trigger the traps remotely. Traps that incorporate
        technology gain the technological type, in addition to their normal type
        (see the following examples). The following technological triggers are
        available to traps.</p><p><strong>Electric Eyes</strong>: Similar to a
        visual trigger, the trap incorporates a camera or other visual and audio
        recording device. Electric eyes typically have darkvision to a range of
        120 feet, low-light vision, and a Perception bonus of +15. This trigger
        adds +1 to the trap's CR and +5 to its crafting
        DC.</p><p><strong>Genetic</strong>: A genetic trigger works in a fashion
        similar to scent. It can be set to target or not target living creatures
        within 30 feet (adjusted by wind as for scent) based on an individual,
        close family, or species relationship. For example, a trap could be set
        to target orcs, or to not target an NPC and his close relations. Genetic
        triggers are used in conjunction with other triggers and limit the
        maximum range of those triggers. A trap can incorporate up to half its
        CR in different genetic samples and conditions. This trigger adds +1 to
        the trap's CR and +5 to its crafting DC.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: 605z6x691P00ERMn
    _key: '!journal.pages!rZv6gghP92T1cEDy.605z6x691P00ERMn'
    _stats:
      coreVersion: '12.331'
    name: Technological Equipment
    sort: 400000
    text:
      content: >-
        <p>@Source[PZO9272;pages=18-19]</p><p>To set a technological campaign
        apart from a standard fantasy adventure, you need a variety of unusual
        futuristic items. But be it a laser gun in the hands of a terrible enemy
        or a set of strange gravity armor found in the treasure trove of an
        oddly uniform metal dungeon, technology from the future (or even the
        present-day real world) in a fantasy setting should be handled in a
        manner similar to magic items elsewhere in the Pathfinder Roleplaying
        Game.</p><p>Many technological items replicate specific spells or
        magical effects. However, they do not use magic in any way, and thus
        function normally in areas of antimagic or primal magic, and are
        otherwise unaffected by any effects that target or affect magic
        items.</p><h2>Color Code</h2><p>Many technological items follow a color
        code that organizes similar items (such as nanite hypoguns, force
        fields, or gravity clips) according to their overall power. The power
        level for each color is listed below. Note that there are nine colors in
        the scale—the effects of an item of any individual color should roughly
        correspond to the power level of a spell of the associated level. Seven
        of these colors are associated with the seven skymetals. The least of
        the colors, brown, is associated with base ores, while the greatest of
        the colors, prismatic, is associated with all of the skymetals. In some
        technological items, actual skymetals of the appropriate color are used
        in the creation of the object, but in most, synthetic plastics and
        metals are used in place of the more valuable
        skymetals.</p><ul><li><p><strong>Brown</strong>: Roughly equivalent to a
        1st-level spell. This color is not associated with
        skymetal.</p></li><li><p><strong>Black</strong>: Roughly equivalent to a
        2nd-level spell. This color is associated with
        adamantine.</p></li><li><p><strong>White</strong>: Roughly equivalent to
        a 3rd-level spell. This color is associated with
        siccatite.</p></li><li><p><strong>Gray</strong>: Roughly equivalent to a
        4th-level spell. This color is associated with
        inubrix.</p></li><li><p><strong>Green</strong>: Roughly equivalent to a
        5th-level spell. This color is associated with
        noqual.</p></li><li><p><strong>Red</strong>: Roughly equivalent to a
        6th-level spell. This color is associated with
        djezet.</p></li><li><p><strong>Blue</strong>: Roughly equivalent to a
        7th-level spell. This color is associated with
        abysium.</p></li><li><p><strong>Orange</strong>: Roughly equivalent to
        an 8th-level spell. This color is associated with
        horacalcum.</p></li><li><p><strong>Prismatic</strong>: Roughly
        equivalent to a 9th-level spell. This color is associated with all
        skymetals.</p></li></ul><h2>Power Sources</h2><p>Most of the
        technological wonders presented here require energy to function. These
        items each have a capacity score, which indicates the maximum number of
        charges the item can store at any one time. The number of charges an
        item consumes when it is used varies from item to item. An item's
        capacity can be filled from any power source—like a battery or a
        generator—as a standard action. When an item is charged, it always takes
        as many charges from the attached power source as it can hold, filling
        as close to its capacity as possible. Note that charging an item from a
        generator is more efficient, as any charges drained from a battery in
        excess of the number of charges an item can store are
        lost.</p><h2>Signals</h2><p>@Source[PZO9272;pages=43]</p><p>Some items,
        like
        @UUID[Compendium.pf1.technology.Item.vkX8IP7mqJ1WzYy1]{chipfinders},
        @UUID[Compendium.pf1.technology.Item.vkZiORtGaqdCBT2R]{commsets}, and
        @UUID[Compendium.pf1.technology.Item.LHf8L2FLFkiqYmSB]{detonators}, can
        remotely interact with other objects via signals. Signals have a maximum
        range set by the device. Furthermore, a signal cannot penetrate solid
        barriers well. A signal is blocked by 1 foot of metal, 5 feet of stone,
        or 20 feet of organic matter. Force fields do not block signals at all.
        @UUID[Compendium.pf1.technology.Item.PwlXM1Qw4bX1JfRC]{Signal boosters}
        and @UUID[Compendium.pf1.technology.Item.uZf4VqssxJFH6Yov]{signal
        jammers} can also affect signal range.</p><h2>Crafting High-Tech
        Items</h2><p>@Source[PZO9272;pages=16-17]</p><p>The process of building
        technological items has much in common with magic item creation, though
        it uses different feats, skills, and facilities. As with magic items,
        the creator invests time and money in the creation process and at the
        end attempts a single skill check to complete construction. Since
        technological items do not have caster levels, the DC of this check is
        defined in the description of each technological item. Failing this
        check means that the item does not function and the materials are
        wasted. Failing this check by 5 or more may result in a catastrophic
        failure, such as electrocution or an explosion, at the GM's
        discretion.</p><p>Unlike magic items, which often require spells as
        prerequisites for construction, high-tech items require a specialized
        laboratory with the necessary tools for fabrication. Using a crafting
        lab to build a high-tech item consumes an amount of power each day. Days
        when the crafting lab is without power effectively delay continued
        construction of a high-tech item, but time already spent building the
        item is not lost. In addition, crafting an item requires an expenditure
        of time (from a character with the appropriate crafting feat) and an
        expenditure of money used to secure the technological components and
        expendable resources needed for the work.</p><p>Creating a technological
        item requires 8 hours of work per 1,000 gp in the item's base price (or
        fraction thereof). The creator must spend the gold at the beginning of
        the construction process. The process can be accelerated to 4 hours of
        work per 1,000 gp by increasing the DC to create the item by 5. When
        determining the required time, ignore any fixed costs such as the weapon
        portion of implanted weaponry.</p><p>The creator can work for a maximum
        of 8 hours per day, even if she doesn't require sleep or rest. These
        days need not be consecutive. Ideally, the creator can work for at least
        4 hours at a time uninterrupted, but if this is not possible (such as
        while adventuring), the creator can devote 4 hours of work broken up
        over the day, accomplishing a net of 2 hours of progress. Work under
        distracting or dangerous conditions nets only half the progress as well.
        If the creator can't dedicate at least 4 hours of work during a day
        (even if broken up or under distracting conditions), any work performed
        that day is wasted.</p><p>A character can work on multiple technological
        items at a time, or even in the same day as long as at least 2 hours net
        labor can be spent on each item. This doesn't let a creator exceed the
        limits on work accomplished in a single day, but does require separate
        power expenditures for each item (working on multiple projects at a time
        is not particularly energy efficient).</p><p>Technological items can be
        repaired using the appropriate crafting feats in the same way magical
        items can be repaired, but such methods cannot repair the more
        fundamental ravages of time that afflict timeworn technological
        items.</p><h3>Crafting Laboratories</h3><p>Although there is a wide
        range of technological items, the types of laboratories needed to craft
        objects are relatively limited. Crafting laboratories are,
        unfortunately, incredibly rare, and those whose locations are currently
        known and found in relatively safe regions are firmly controlled. A
        crafting laboratory is similar to a technological artifact, in that it
        cannot be assembled or built with current resources. In order to craft a
        technological item, one must secure a laboratory for use. (This allows
        GMs to limit the role high-tech crafting plays in any one game—make sure
        to inform your players of the limited availability of crafting
        laboratories at the start of your game so they know whether selecting
        high-tech crafting feats is a useful option for their PCs!)</p><p>The
        six types of laboratories are listed below. Each lab also lists the
        number of charges required for a day's work on a single project—these
        numbers are generally rather high, and laboratories that don't draw
        power from a generator can consume staggering amounts of battery power.
        A laboratory that uses power from a generator applies the listed charges
        to that generator's dedicated yield for as long as work on the item
        continues.</p><p><strong>Cybernetics Lab (100 charges)</strong>: A
        cybernetics lab is used to craft cybernetic equipment and devices that
        interface directly with a living creature's
        biology.</p><p><strong>Graviton Lab (250 charges)</strong>: A graviton
        lab is used to craft items that utilize graviton technology, such as
        gravity rifles, force fields, and magboots.</p><p><strong>Medical Lab
        (20 charges)</strong>: This lab is used to craft medical items like
        trauma packs and medlances and pharmaceuticals.</p><p><strong>Military
        Lab (100 charges)</strong>: A military lab is used to craft weapons that
        don't require more specialized laboratories.</p><p><strong>Nanotech Lab
        (150 charges)</strong>: This lab is used to craft devices that utilize
        nanotechnology, such as id rifles and k-lances.</p><p><strong>Production
        Lab (50 charges)</strong>: A production lab is used to craft objects
        that don't require more specialized laboratories.</p><h3>Hybrid
        Items</h3><p>Items with both magical and technological components, such
        as the null blade, use a special crafting process. The creator must
        first succeed at a skill check at the listed DC for crafting the
        technological portion of the item, and then must succeed at a check
        based on the item's caster level for crafting the magical portion. Any
        spell or level-based prerequisites not met increase this crafting DC, as
        described for magic item creation. The skill used for each check is
        based on the item creation feats required by the item. Failure on either
        check ruins the item. Use the item's listed price as normal for
        determining crafting time, and the item's cost for raw materials. If the
        creator has feats or abilities that accelerate item creation, only the
        least favorable bonus applies. In other words, to create a hybrid item
        faster, the creator needs to be able to create both magical and
        technological items faster.</p><p>It is also possible to enhance
        high-tech armor and high-tech weaponry with armor special abilities or
        weapon special abilities, including magical enhancement bonuses. One
        could build a +2 laser rifle, a +4 dancing humanoid bane chainsaw, or a
        +1 ghost touch spacesuit. In theory, a magic item creator could even
        infuse a technological item with magical intelligence. To create a
        magical high-tech item like this, one must first secure the high-tech
        item itself, either via purchase, discovery, or crafting. All high-tech
        weapons and armor are considered masterwork for the purposes of adding
        magical enhancements to them (though they do not gain the other typical
        benefits for masterwork items). At the GM's discretion, some magical
        special abilities might simply not be appropriate for application to
        certain technological items. When a character crafts an existing
        technological item into a magic item in this manner, he does not need to
        meet the base item's crafting requirements—a wizard with Craft Magic
        Arms and Armor can create a +1 arc pistol from a normal arc pistol
        without having Craft Technological Arms and Armor and without having
        access to a military laboratory. In a situation where a character wishes
        to craft the entire item from scratch, the nonmagical technological item
        must be fully crafted and completed before work on magically enhancing
        it can begin.</p><h3>Pricing and Creating New Technological
        Items</h3><p>New technological items can and should be created, using
        existing items for inspiration. A new item may resemble an existing
        magic item, such as how jet packs function like winged boots, but there
        should be differences beyond just battery power to keep technology
        distinct. When pricing a new technological item, use the existing
        guidelines for estimating magic item value. There's no extra cost
        associated with technological items since they have extra weaknesses to
        go along with their advantages. Items that use charges should be priced
        as if they were use-activated, not as if they were charged in the way a
        wand or ring of the ram is charged, unless the item is disposable and
        has 50 or fewer charges, as the assumption is that a newly crafted
        technological item can be recharged with relative ease.</p>
      format: 1
    title:
      level: 1
      show: true
    type: text
  - _id: rBQLN2IKx8quRrOw
    _key: '!journal.pages!rZv6gghP92T1cEDy.rBQLN2IKx8quRrOw'
    _stats:
      coreVersion: '12.331'
    name: Artificial Intelligences
    sort: 500000
    text:
      content: >-
        <p>@Source[PZO9272;pages=58]</p><p>Among the most marvelous of
        technological wonders are artificial intelligences. Each artificial
        intelligence (or AI) has its own personality and interests, but its
        actual ability to interact with the world requires a being installed
        into a robot or structure (known as a host).</p><p>When an AI is
        installed in a technological structure with an integrated computer
        network, it can manipulate anything that is connected to that structure,
        such as traps, doors, or weapons. To destroy an AI in a host structure,
        either the physical device that contains the AI’s programming must be
        destroyed (the magnitude of this task can vary widely—the host may be
        merely a single computer with hardness 10 and 20 hit points or an entire
        building that must be destroyed over multiple encounters or adventures)
        or all three of its ability scores must be reduced to 0 via ability
        drain effects directed at the AI’s core processor (typically a large
        computer in the most well-defended part of a complex, but sometimes a
        processor housed in a robot). Ability damage dealt to an AI at best
        merely inconveniences it, and at worst renders it comatose for a limited
        time.</p><p>An AI that is installed in a robot enhances that robot with
        the aggregate template.</p><h2>Memory
        Facet</h2><p>@Source[PZO9086;pages=626]</p><p>A memory facet is a length
        of crystal about the size of a human thumb that's adorned at one end by
        a metal cap fitted with prongs and plugs. The crystal's interior
        contains sparkling veins of glittering light. A memory facet is a
        high-capacity storage device capable of containing a staggering amount
        of programming.</p><p>To use a memory facet, one needs simply to insert
        the crystal's connectors into an appropriate slot in an AI's core
        processor or a robot under the AI's control. A core processor or robot
        can hold as many memory facets as it has available slots, but at any one
        time, an AI can benefit from a maximum number of memory facets equal to
        its CR divided by 4 (minimum 1). The AI can gain the benefits of memory
        facets installed in any of the robots and processors it controls-where a
        memory facet is installed makes little difference, but most AIs prefer
        to keep their memory facets installed in their most secure
        locations.</p><p>Each memory facet contains a unique combination of
        emotions, knowledge, traits, and personality quirks designed to enhance
        and bolster an artificial intelligence's capabilities. In rare cases,
        destructive memory facets were created - items intended to disable or
        damage an artificial intelligence in case of emergencies. An AI
        immediately gains all of the advantages (and any disadvantages)
        associated with a particular memory facet as soon as it is installed
        (this is a full-round action). Memory facets can change an AI's
        personality or even its alignment. An AI can try to resist having a
        memory facet added to its code by attempting a DC 20 Will save. If the
        AI is successful, the memory facet ceases functioning for 1d4 rounds,
        and must be extracted and reinstalled to make a second attempt to
        changing the AI's code. Once installed, a memory facet is difficult to
        remove; removing one requires either a successful DC 25 Strength check
        to wrench free or a successful DC 30 Disable Device check. Both attempts
        are full-round actions.</p><section class="secret"
        id="secret-CaDWqvYKuhUwXX4V"><h2>Creating an AI</h2><p>An artificial
        intelligence’s stat block is similar to that of an intelligent magic
        item. Note that these statistics reflect only the intelligence itself,
        not the host. Building an AI stat block requires the following
        statistics.</p><p><strong>CR</strong>: An artificial intelligence’s CR
        sets its ability scores, saves, and skill checks. It also sets its XP
        value for defeating it—this award is typically granted as a story award
        once the AI has been overcome, but in some cases (such as when an AI is
        installed in a single robot), this XP award is replaced by the standard
        XP for defeating that robot.</p><p><strong>Alignment and Type</strong>:
        An AI can be any alignment. Its type is “artificial intelligence,” and
        it counts as a construct for all effects that target creature
        type.</p><p><strong>Initiative</strong>: The installed AI modifies its
        initiative check with its Intelligence modifier, not its Dexterity
        modifier.</p><p><strong>Senses</strong>: An AI needs access to a robot,
        cameras, microphones, or other mechanical sensory tools in order to be
        able to notice things in the outside world. These senses should be
        listed in the appropriate encounter areas, not in the AI’s stats, since
        it’s likely that an AI will have access to a variety of sensory devices
        over a large area.</p><p><strong>Defenses</strong>: An AI doesn’t have
        an AC, hit points, or other statistics related to a physical form; it
        relies on its host robot or structure for those scores. An AI uses its
        own saving throws only against attacks that target its mind—in most
        cases, this means it primarily uses its Will save. An AI uses its CR as
        its effective Hit Dice for determining its base saves—it has good Will
        saves and poor Fortitude and Reflex saves.</p><p><strong>Ability
        Scores</strong>: An AI’s base ability scores are 14, 12, and 10. Arrange
        them in any order desired. For every 2 points of CR it possesses, it
        gains a +2 bonus to an ability score (assigned by the
        GM).</p><p><strong>Skills</strong>: An AI has skill points equal to 6 +
        its Intelligence modifier per point of CR. An AI has a number of class
        skills equal to its Charisma modifier. These may be any skill, though
        the most common class skills for AIs are: Bluff, Diplomacy, Intimidate,
        Knowledge (all), Linguistics, Perception, and Sense
        Motive.</p><p><strong>Feats</strong>: An AI has a number of feats equal
        to half its CR (minimum 1). An AI must meet all prerequisites of its
        feats. AIs also gain
        @UUID[Compendium.pf1.feats.Item.1foaZv9wfqZVrZ90]{Technologist} as a
        bonus feat.</p><p><strong>Languages</strong>: AIs speak Androffan. An AI
        knows a number of additional languages equal to its Intelligence
        modifier.</p><p><strong>Special Abilities</strong>: Use this section to
        cover any unusual abilities the AI possesses.</p></section>
      format: 1
    title:
      level: 1
      show: true
    type: text
sort: 300000
