/**
 * The core API provided by the system, available via the global `pf1`.
 *
 * @module
 */

// Imports for side effects
import "./less/pf1.less";
import "./module/hmr.mjs";
import "./module/patch-core.mjs";
import "module/compendium-directory.mjs";
import "./module/chatlog.mjs";

// Import Modules
import { moduleToObject } from "./module/utils/internal.mjs";
import { initializeSocket } from "./module/socket.mjs";
import { SemanticVersion } from "./module/utils/semver.mjs";
import * as macros from "./module/documents/macros.mjs";
import * as chatUtils from "./module/utils/chat.mjs";
import { initializeModuleIntegration } from "./module/modules.mjs";
import { ActorPFProxy } from "@actor/actor-proxy.mjs";
import { ItemPFProxy } from "@item/item-proxy.mjs";
import * as tours from "module/tours.mjs";

// New API
import * as PF1 from "./module/config.mjs";
import * as PF1CONST from "./module/const.mjs";
import * as applications from "./module/applications/_module.mjs";
import * as documents from "./module/documents/_module.mjs";
import * as models from "./module/models/_module.mjs";
import * as actionUse from "./module/action-use/_module.mjs";
import * as chat from "./module/chat/_module.mjs";
import * as _canvas from "./module/canvas/_module.mjs";
import * as dice from "./module/dice/_module.mjs";
import * as components from "./module/components/_module.mjs";
import * as utils from "./module/utils/_module.mjs";
import * as registry from "./module/registry/_module.mjs";
import * as migrations from "./module/migration.mjs";

// ESM exports, to be kept in sync with globalThis.pf1
export {
  actionUse,
  applications,
  _canvas as canvas,
  components,
  PF1 as config,
  PF1CONST as const,
  dice,
  documents,
  models,
  migrations,
  registry,
  tours,
  utils,
  chat,
};

globalThis.pf1 = moduleToObject({
  actionUse,
  applications,
  canvas: _canvas,
  components,
  config: PF1,
  const: PF1CONST,
  dice,
  documents,
  models,
  migrations,
  registry,
  utils,
  chat,
  // Initialize skip confirm prompt value
  skipConfirmPrompt: false,
});

/* -------------------------------------------- */
/*  Foundry VTT Initialization                  */
/* -------------------------------------------- */
Hooks.once("init", function () {
  console.log(`PF1 | Initializing Pathfinder 1 System`);

  migrations.registerRedirects();

  // Temp store
  Object.defineProperty(pf1, "_temp", {
    value: {},
    enumerable: false,
    writable: false,
  });

  // Global exports
  globalThis.RollPF = dice.RollPF;

  // Record Configuration Values
  CONFIG.PF1 = pf1.config;

  // Canvas
  CONFIG.Canvas.layers.templates.layerClass = _canvas.TemplateLayerPF;

  // Measured Template
  CONFIG.MeasuredTemplate.objectClass = _canvas.MeasuredTemplatePF;
  CONFIG.MeasuredTemplate.defaults.originalAngle = CONFIG.MeasuredTemplate.defaults.angle;
  CONFIG.MeasuredTemplate.defaults.angle = 90; // PF1 uses 90 degree angles

  // Token
  CONFIG.Token.objectClass = _canvas.TokenPF;
  CONFIG.Token.hudClass = _canvas.TokenHUDPF;
  CONFIG.Token.documentClass = documents.TokenDocumentPF;

  // Document classes
  CONFIG.Actor.documentClass = ActorPFProxy;
  CONFIG.Actor.documentClasses = {
    character: documents.actor.ActorCharacterPF,
    npc: documents.actor.ActorNPCPF,
    haunt: documents.actor.ActorHauntPF,
    trap: documents.actor.ActorTrapPF,
    vehicle: documents.actor.ActorVehiclePF,
  };
  CONFIG.Actor.dataModels.haunt = models.actor.HauntModel;
  CONFIG.Actor.dataModels.trap = models.actor.TrapModel;
  CONFIG.Actor.dataModels.vehicle = models.actor.VehicleModel;

  CONFIG.Item.documentClass = ItemPFProxy;
  CONFIG.Item.documentClasses = {
    attack: documents.item.ItemAttackPF,
    buff: documents.item.ItemBuffPF,
    class: documents.item.ItemClassPF,
    consumable: documents.item.ItemConsumablePF,
    container: documents.item.ItemContainerPF,
    equipment: documents.item.ItemEquipmentPF,
    feat: documents.item.ItemFeatPF,
    loot: documents.item.ItemLootPF,
    race: documents.item.ItemRacePF,
    spell: documents.item.ItemSpellPF,
    weapon: documents.item.ItemWeaponPF,
    implant: documents.item.ItemImplantPF,
  };

  // Active Effects
  CONFIG.ActiveEffect.documentClass = documents.ActiveEffectPF;
  CONFIG.ActiveEffect.legacyTransferral = false; // TODO: Remove once legacy transferral is no longer default.
  CONFIG.ActiveEffect.dataModels.base = models.ae.AEBaseModel;
  CONFIG.ActiveEffect.dataModels.buff = models.ae.AEBuffModel;

  // Combat
  CONFIG.Combat.documentClass = documents.CombatPF;
  CONFIG.Combatant.documentClass = documents.CombatantPF;

  // Chat
  CONFIG.ChatMessage.documentClass = documents.ChatMessagePF;

  CONFIG.ChatMessage.dataModels.base = models.chat.BaseMessageModel;
  CONFIG.ChatMessage.dataModels.item = models.chat.ItemMessageModel;
  CONFIG.ChatMessage.dataModels.action = models.chat.ActionMessageModel;
  CONFIG.ChatMessage.dataModels.check = models.chat.CheckMessageModel;

  // UI classes
  CONFIG.ui.items = applications.ItemDirectoryPF;

  // Dice config
  CONFIG.Dice.rolls.unshift(dice.RollPF);

  CONFIG.Dice.rolls.push(dice.D20RollPF);
  CONFIG.Dice.rolls.push(dice.DamageRoll);

  // Replace core terms to provide custom functionality
  CONFIG.Dice.termTypes.FunctionTerm = pf1.dice.terms.FunctionTermPF;
  CONFIG.Dice.termTypes.StringTerm = pf1.dice.terms.StringTermPF;

  // Roll functions
  for (const [key, fn] of Object.entries(pf1.utils.roll.functions)) {
    CONFIG.Dice.functions[key] = fn;
  }

  // Combat time progression
  CONFIG.time.roundTime = 6;

  // Low-Light Vision mixin
  CONFIG.AmbientLight.objectClass = _canvas.lowLightVision.LLVMixin(CONFIG.AmbientLight.objectClass);
  CONFIG.Token.objectClass = _canvas.lowLightVision.LLVMixin(CONFIG.Token.objectClass);

  // Register System Settings
  documents.settings.registerSystemSettings();
  pf1.utils.internal.setDefaultSceneScaling();

  // Preload Handlebars Templates
  utils.handlebars.preload();
  // Register helpers
  utils.handlebars.helpers.register();

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("pf1", applications.actor.ActorSheetPFCharacter, {
    label: "PF1.Sheet.PC",
    types: ["character"],
    makeDefault: true,
  });
  Actors.registerSheet("pf1", applications.actor.ActorSheetPFNPC, {
    label: "PF1.Sheet.NPC",
    types: ["npc"],
    makeDefault: true,
  });
  Actors.registerSheet("pf1", applications.actor.ActorSheetPFNPCLite, {
    label: "PF1.Sheet.NPCLite",
    types: ["npc"],
    makeDefault: false,
  });
  Actors.registerSheet("pf1", applications.actor.ActorSheetPFNPCLoot, {
    label: "PF1.Sheet.NPCLoot",
    types: ["npc"],
    makeDefault: false,
  });
  Actors.registerSheet("pf1", applications.actor.ActorSheetPFHaunt, {
    label: "PF1.Sheet.Haunt",
    types: ["haunt"],
    makeDefault: true,
  });
  Actors.registerSheet("pf1", applications.actor.ActorSheetPFTrap, {
    label: "PF1.Sheet.Trap",
    types: ["trap"],
    makeDefault: true,
  });
  Actors.registerSheet("pf1", applications.actor.ActorSheetPFVehicle, {
    label: "PF1.Sheet.Vehicle",
    types: ["vehicle"],
    makeDefault: true,
  });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("pf1", applications.item.ItemSheetPF, {
    label: "PF1.Sheet.Item",
    types: ["class", "feat", "spell", "consumable", "equipment", "loot", "weapon", "buff", "attack", "race", "implant"],
    makeDefault: true,
  });
  Items.registerSheet("pf1", applications.item.ItemSheetPF_Container, {
    label: "PF1.Sheet.Container",
    types: ["container"],
    makeDefault: true,
  });

  DocumentSheetConfig.registerSheet(JournalEntryPage, "pf1", applications.journal.JournalTextPageSheetPF1, {
    types: ["text"],
    makeDefault: false,
    label: "PF1.Sheet.TextPage",
  });

  // Register detection modes
  for (const mode of Object.values(pf1.canvas.detectionModes)) {
    CONFIG.Canvas.detectionModes[mode.ID] = new mode({
      id: mode.ID,
      label: mode.LABEL,
      type: mode.DETECTION_TYPE ?? DetectionMode.DETECTION_TYPES.SIGHT,
    });
  }

  // Register vision modes
  CONFIG.Canvas.visionModes.darkvision = pf1.canvas.visionModes.darkvision;

  // Initialize socket listener
  initializeSocket();

  // Initialize module integrations
  initializeModuleIntegration();

  // Initialize registries with initial/built-in data
  const registries = /** @type {const} */ ([
    ["damageTypes", registry.DamageTypes],
    ["materials", registry.Materials],
    ["scriptCalls", registry.ScriptCalls],
    ["conditions", registry.Conditions],
    ["sources", registry.Sources],
  ]);

  for (const [registryName, registryClass] of registries) {
    pf1.registry[registryName] = new registryClass();
  }

  Object.defineProperty(pf1.documents, "customRolls", {
    value: function (message, speaker, rollData) {
      foundry.utils.logCompatibilityWarning(
        "pf1.documents.customRolls() is deprecated in favor of pf1.chat.command()",
        {
          since: "PF1 v11",
          until: "PF1 v12",
        }
      );

      const re = /^\/(?<command>h|heal|d|damage)\s+(?<formula>.*?)(\s*#\s*(?<comment>.*))?$/i.exec(message);
      if (!re) throw new Error(`Failed to parse message: ${message}`);

      const { command, formula, comment } = re.groups;
      return pf1.chat.command(command, formula, comment, { speaker, rollData });
    },
  });

  Object.defineProperty(pf1.registry, "materialTypes", {
    get() {
      foundry.utils.logCompatibilityWarning("pf1.registry.materialTypes has been moved to pf1.registry.materials.", {
        since: "PF1 v11",
        until: "PF1 v12",
      });
      return pf1.registry.materials;
    },
  });

  // @deprecated - pf1.config.measureTemplateTypes to CONFIG.MeasuredTemplate.types
  Object.defineProperty(pf1.config, "measureTemplateTypes", {
    get() {
      foundry.utils.logCompatibilityWarning(
        "pf1.config.measureTemplateTypes is deprecated in favor of CONFIG.MeasuredTemplate.types",
        {
          since: "PF1 v11",
          until: "PF1 v12",
        }
      );

      return pf1.utils.internal.getTemplateTypes();
    },
  });

  //Calculate conditions for world
  CONFIG.statusEffects = pf1.utils.init.getConditions();
  // Add swim as special condition
  CONFIG.specialStatusEffects.SWIM = "swim";

  // Register controls
  documents.controls.registerSystemControls();

  // Register compendium browsers
  pf1.applications.compendiumBrowser.CompendiumBrowser.registerDefault();

  // Delay pack preparation after babele is ready if it is in use
  if (game.modules.get("babele")?.active && game.settings.get("pf1", "integration").babele)
    Hooks.once("babele.ready", preparePacks);
  else Hooks.once("ready", preparePacks);

  // Call post-init hook
  Hooks.callAll("pf1PostInit");
});

/**
 * Prepare pack related data.
 */
function preparePacks() {
  // Pre-load class ID map for better UX
  console.debug("PF1 | Pre-caching class ID map");
  pf1.utils.packs.getClassIDMap();
}

// Load Quench test in development environment
if (import.meta.env.DEV) {
  await import("./module/test/index.mjs");
}

/* -------------------------------------------- */
/*  Foundry VTT Setup                           */
/* -------------------------------------------- */

// Pre-translation passes
Hooks.once("i18nInit", function () {
  // Localize pf1.config objects once up-front
  const toLocalize = [
    "abilities",
    "abilitiesShort",
    "alignments",
    "alignmentsShort",
    "currencies",
    "distanceUnits",
    "itemActionTypes",
    "senses",
    "skills",
    "timePeriods",
    "timePeriodsShort",
    "durationEndEvents",
    "savingThrows",
    "ac",
    "featTypes",
    "featTypesPlurals",
    "traitTypes",
    "racialTraitCategories",
    "raceTypes",
    "conditionTypes",
    "lootTypes",
    "flyManeuverabilities",
    "favouredClassBonuses",
    "abilityTypes",
    "weaponGroups",
    "weaponTypes",
    "weaponProperties",
    "spellComponents",
    "spellDescriptors",
    "spellSchools",
    "spellSubschools",
    "spellLevels",
    "spellcasting",
    "armorProficiencies",
    "weaponProficiencies",
    "actorSizes",
    "abilityActivationTypes",
    "abilityActivationTypesPlurals",
    "limitedUsePeriods",
    "equipmentTypes",
    "equipmentSlots",
    "implantSlots",
    "implantTypes",
    "consumableTypes",
    "attackTypes",
    "buffTypes",
    "divineFocus",
    "classSavingThrows",
    "classBAB",
    "classTypes",
    "creatureTypes",
    "creatureSubtypes",
    "measureUnits",
    "measureUnitsShort",
    "languages",
    "weaponHoldTypes",
    "auraStrengths",
    "conditionalTargets",
    "bonusTypes",
    "abilityActivationTypes_unchained",
    "abilityActivationTypesPlurals_unchained",
    "actorStatures",
    "ammoTypes",
    "damageResistances",
    "vehicles",
    "woundThresholdConditions",
    "changeFlags",
    "hauntTemplateLabels",
    "traps",
  ];

  // Localize pf1.const objects
  const toLocalizeConst = ["messageVisibility"];

  // Config (sub-)objects to be sorted
  const toSort = [
    "bonusTypes",
    "skills",
    "traitTypes",
    "racialTraitCategories",
    "conditionTypes",
    "consumableTypes",
    "creatureTypes",
    "creatureSubtypes",
    "featTypes",
    "weaponProperties",
    "spellSchools",
    "languages",
  ];

  /**
   * Helper function to recursively localize object entries
   *
   * @param {object} obj - The object to be localized
   * @param {string} cat - The object's name
   * @returns {object} The localized object
   */
  const doLocalize = (obj, cat) => {
    // Create tuples of (key, localized object/string)
    const localized = Object.entries(obj).reduce((arr, [key, value]) => {
      if (typeof value === "string") arr.push([key, game.i18n.localize(value)]);
      else if (typeof value === "object") arr.push([key, doLocalize(value, `${cat}.${key}`)]);
      return arr;
    }, []);

    if (toSort.includes(cat)) {
      // Sort simple strings, fall back to sorting by label for objects/categories
      localized.sort(([akey, aval], [bkey, bval]) => {
        // Move misc to bottom of every list
        if (akey === "misc") return 1;
        else if (bkey === "misc") return -1;

        // Regular sorting of localized strings
        const localA = typeof aval === "string" ? aval : aval._label;
        const localB = typeof bval === "string" ? bval : bval._label;
        return localA.localeCompare(localB);
      });
    }

    // Get the localized and sorted object out of tuple
    return localized.reduce((obj, [key, value]) => {
      obj[key] = value;
      return obj;
    }, {});
  };

  const doLocalizePaths = (obj, paths = []) => {
    for (const path of paths) {
      const value = foundry.utils.getProperty(obj, path);
      if (value) {
        foundry.utils.setProperty(obj, path, game.i18n.localize(value));
      }
    }
  };

  const doLocalizeKeys = (obj, keys = []) => {
    for (const path of Object.keys(foundry.utils.flattenObject(obj))) {
      const key = path.split(".").at(-1);
      if (keys.includes(key)) {
        const value = foundry.utils.getProperty(obj, path);
        if (value) {
          foundry.utils.setProperty(obj, path, game.i18n.localize(value));
        }
      }
    }
  };

  // Localize and sort CONFIG objects
  for (const o of toLocalize) {
    pf1.config[o] = doLocalize(pf1.config[o], o);
  }

  for (const o of toLocalizeConst) {
    pf1.const[o] = doLocalize(pf1.const[o], o);
  }

  // Localize buff targets
  const localizeLabels = [
    "buffTargets",
    "buffTargetCategories",
    "contextNoteTargets",
    "contextNoteCategories",
    "ageCategories",
    "vehicleMaterials",
  ];
  for (const l of localizeLabels) {
    for (const [k, v] of Object.entries(pf1.config[l])) {
      pf1.config[l][k].label = game.i18n.localize(v.label);
    }
  }

  // Extra attack structure
  doLocalizeKeys(pf1.config.extraAttacks, ["label", "flavor"]);

  // Level-up data
  doLocalizePaths(pf1.config.levelAbilityScoreFeature, ["name", "system.description.value"]);

  // Point buy data
  doLocalizeKeys(pf1.config.pointBuy, ["label"]);

  // Caster type labels
  doLocalizeKeys(pf1.config.caster.type, ["label"]);
  doLocalizeKeys(pf1.config.caster.progression, ["label", "hint"]);

  // Localize registry data
  for (const registry of Object.values(pf1.registry)) {
    if (registry instanceof pf1.registry.Registry) registry.localize();
  }
});

/**
 * This function runs after game data has been requested and loaded from the servers, so documents exist
 */
Hooks.once("setup", () => {
  Hooks.callAll("pf1PostSetup");
});

/* -------------------------------------------- */

/**
 * Once the entire VTT framework is initialized, check to see if we should perform a data migration
 */
Hooks.once("ready", async function () {
  // Migrate data
  const NEEDS_MIGRATION_VERSION = "11.1";
  let PREVIOUS_MIGRATION_VERSION = game.settings.get("pf1", "systemMigrationVersion");
  if (typeof PREVIOUS_MIGRATION_VERSION === "number") {
    PREVIOUS_MIGRATION_VERSION = PREVIOUS_MIGRATION_VERSION.toString() + ".0";
  } else if (
    typeof PREVIOUS_MIGRATION_VERSION === "string" &&
    PREVIOUS_MIGRATION_VERSION.match(/^([0-9]+)\.([0-9]+)$/)
  ) {
    PREVIOUS_MIGRATION_VERSION = `${PREVIOUS_MIGRATION_VERSION}.0`;
  }
  const needMigration = SemanticVersion.fromString(NEEDS_MIGRATION_VERSION).isHigherThan(
    SemanticVersion.fromString(PREVIOUS_MIGRATION_VERSION)
  );

  if (needMigration) {
    const options = {};
    // Omit dialog for new worlds with presumably nothing to migrate
    if (PREVIOUS_MIGRATION_VERSION === "0.0.0") options.dialog = false;

    await pf1.migrations.migrateWorld(options);
  }

  // Inform users who aren't running migration
  if (!game.user.isGM && game.settings.get("pf1", "migrating")) {
    ui.notifications.warn("PF1.Migration.InProgress", { localize: true });
  }

  // Migrate system settings
  await documents.settings.migrateSystemSettings();

  // Populate `pf1.applications.compendiums`
  pf1.applications.compendiumBrowser.CompendiumBrowser.initializeBrowsers();

  // Show changelog
  if (!game.settings.get("pf1", "dontShowChangelog")) {
    const v = game.settings.get("pf1", "changelogVersion");
    const changelogVersion = SemanticVersion.fromString(v);
    const curVersion = SemanticVersion.fromString(game.system.version);

    if (curVersion.isHigherThan(changelogVersion)) {
      const app = new pf1.applications.ChangeLogWindow(true);
      app.render(true, { focus: true });
      game.settings.set("pf1", "changelogVersion", curVersion.toString());
    }
  }

  Hooks.callAll("pf1PostReady");
});

/* -------------------------------------------- */
/*  Other Hooks                                 */
/* -------------------------------------------- */

Hooks.on(
  "renderChatMessage",
  /**
   * @param {ChatMessage} cm - Chat message instance
   * @param {JQuery<HTMLElement>} jq - JQuery instance
   * @param {object} options - Render options
   */
  (cm, jq, options) => {
    const html = jq[0];

    // Hide roll info
    chatUtils.hideRollInfo(cm, jq, options);

    // Hide GM sensitive info
    chatUtils.hideGMSensitiveInfo(cm, html, options);

    // Display placeholder if no description is present.
    const desc = html.querySelector(".card-content .item-description");
    if (desc && !desc.textContent.trim()) {
      desc.append(game.i18n.localize("PF1.NoDescription"));
    }

    // Hide non-visible targets for players
    if (!game.user.isGM) chatUtils.hideInvisibleTargets(cm, html);

    // Create target callbacks
    chatUtils.addTargetCallbacks(cm, jq);

    // Alter target defense options
    chatUtils.alterTargetDefense(cm, jq);

    // Optionally collapse the content
    if (game.settings.get("pf1", "autoCollapseItemCards")) {
      html.querySelector(".card-content")?.classList.add("hidden");
    }

    // Optionally hide chat buttons
    if (game.settings.get("pf1", "hideChatButtons")) {
      for (const button of html.querySelectorAll(".card-buttons")) button.remove();
    }

    // Alter ammo recovery options
    chatUtils.alterAmmoRecovery(cm, jq);
  }
);

Hooks.on("renderChatMessage", (cm, [html]) => pf1.utils.chat.addListeners(cm, html));
Hooks.on("renderChatMessage", (_, html) => _canvas.attackReach.addReachListeners(html));

Hooks.on("renderActorDirectory", (app, html) => {
  if (html instanceof jQuery) html = html[0]; // v12/v13 cross compatibility
  for (const li of html.querySelectorAll("li.actor")) {
    li.addEventListener(
      "drop",
      pf1.applications.CurrencyTransfer._directoryDrop.bind(undefined, li.getAttribute("data-document-id"))
    );
  }
});

Hooks.on("renderItemDirectory", (app, html) => {
  if (html instanceof jQuery) html = html[0]; // v12/v13 cross compatibility
  for (const li of html.querySelectorAll("li.item")) {
    li.addEventListener(
      "drop",
      applications.CurrencyTransfer._directoryDrop.bind(undefined, li.getAttribute("data-document-id"))
    );
  }
});

Hooks.on("dropActorSheetData", (act, sheet, data) => {
  if (data.type === "Currency") {
    sheet._onDropCurrency(event, data);
    return false;
  }
});

/* -------------------------------------------- */
/*  Hotbar Macros                               */
/* -------------------------------------------- */

// Delay hotbarDrop handler registration to allow modules to override it.
Hooks.once("ready", () => {
  Hooks.on("hotbarDrop", (bar, data, slot) => {
    let macro;
    const { type, uuid } = data;
    switch (type) {
      case "Item":
        macro = macros.createItemMacro(uuid, slot);
        break;
      case "pf1Action":
        macro = macros.createActionMacro(uuid, slot);
        break;
      case "skill":
        macro = macros.createSkillMacro(data.skill, uuid, slot);
        break;
      case "save":
        macro = macros.createSaveMacro(data.save, uuid, slot);
        break;
      case "defenses":
      case "cmb":
      case "concentration":
      case "cl":
      case "attack":
      case "abilityScore":
      case "initiative":
      case "bab":
        macro = macros.createMiscActorMacro(type, uuid, slot, data);
        break;
      default:
        return true;
    }

    if (macro == null || macro instanceof Promise) return false;
  });
});

// Render TokenConfig
Hooks.on(
  "renderTokenConfig",
  /**
   * @param {TokenConfig} app - Config application
   * @param {JQuery<HTMLElement>} html - HTML element
   */
  async (app, html) => {
    // Add vision inputs
    let token = app.document ?? app.object;
    // Prototype token
    if (token instanceof Actor) token = token.prototypeToken;

    const flags = token.flags?.pf1 ?? {};

    // Add static size checkbox
    const sizingTemplateData = { flags, appId: app.id };
    const sizeContent = await renderTemplate(
      "systems/pf1/templates/foundry/token/token-sizing.hbs",
      sizingTemplateData
    );

    const systemVision = game.settings.get("pf1", "systemVision");

    /** @type {HTMLElement} */
    html = html instanceof jQuery ? html[0] : html;

    html
      .querySelector('.tab[data-tab="appearance"] input[name="width"]')
      .closest(".form-group")
      .insertAdjacentHTML("afterEnd", sizeContent);

    const visionTab = html.querySelector(`.tab[data-tab="vision"]`);

    // Disable vision elements if custom vision is disabled
    const enableCustomVision = flags.customVisionRules === true || !systemVision;

    const toggleCustomVision = (enabled) => {
      // Disable vision mode selection
      visionTab.querySelector("select[name='sight.visionMode']").disabled = !enabled;

      // Disable detection mode tab
      let dmTab;
      if (game.release.generation >= 13)
        dmTab = visionTab.querySelector(".detection-mode").parentElement; // V12 and prior
      else dmTab = visionTab.querySelector(".tab[data-tab='detection']");

      for (const el of dmTab.querySelectorAll("input,select")) {
        if (el.name === "flags.pf1.customVisionRules") continue;
        el.disabled = !enabled;
      }

      // Disable detection mode tab buttons via CSS
      // TODO: Apply inert property instead
      dmTab.classList.toggle("disabled", !enabled);
    };

    if (!enableCustomVision) toggleCustomVision(enableCustomVision);

    const visionContent = await renderTemplate("systems/pf1/templates/foundry/token/custom-vision.hbs", {
      enabled: enableCustomVision || !systemVision,
      noSystemVision: !systemVision,
      appId: app.id,
    });

    visionTab.insertAdjacentHTML("beforeEnd", visionContent);

    // Add listener for custom vision rules checkbox
    // Soft toggle to work nicer with Foundry's preview behaviour
    visionTab.querySelector(`input[name="flags.pf1.customVisionRules"]`).addEventListener("change", async (event) => {
      toggleCustomVision(event.target.checked);
    });

    // Resize windows
    app.setPosition();
  }
);

// V13
Hooks.on("renderSettings", (app, html) => {
  if (!(game.release.generation >= 13)) return;
  if (!(app instanceof CONFIG.ui.settings)) return;

  const template = document.createElement("template");
  template.innerHTML = `<section class="flexcol" id="pf1-details">
    <h4 class="divider">${game.i18n.localize("PF1.Title")}</h4>
    <button type="button" data-action="pf1-changelog">${game.i18n.localize("PF1.Application.Changelog.Title")}</button>
    <button type="button" data-action="pf1-documentation">${game.i18n.localize("PF1.Help.Label")}</button>
    <button type="button" data-action="pf1-troubleshooter">${game.i18n.localize("PF1.Troubleshooter.Button")}</button>
  </section>`;

  Object.assign(app.options.actions, {
    "pf1-changelog": () => {
      const chlog =
        Object.values(ui.windows).find((app) => app instanceof applications.ChangeLogWindow) ??
        new applications.ChangeLogWindow();
      chlog.render(true, { focus: true });
    },
    "pf1-documentation": () => pf1.applications.helpBrowser.openUrl("Help/Home"),
    "pf1-troubleshooter": () => pf1.applications.Troubleshooter.open(),
  });

  const details = html.querySelector("section.info");
  details.after(template.content);
});

// Render Sidebar (V12 and prior)
Hooks.on("renderSidebarTab", (app, html) => {
  if (!(app instanceof Settings)) return;

  const details = document.getElementById("game-details");

  const template = document.createElement("template");
  template.innerHTML = `<h2>${game.i18n.localize("PF1.Title")}</h2>
  <div id='pf1-details'>
    <button type="button" data-action="pf1-changelog">${game.i18n.localize("PF1.Application.Changelog.Title")}</button>
    <button type="button" data-action="pf1-documentation">${game.i18n.localize("PF1.Help.Label")}</button>
    <button type="button" data-action="pf1-troubleshooter">${game.i18n.localize("PF1.Troubleshooter.Button")}</button>
  </div>`;

  for (const button of template.content.querySelectorAll("button")) {
    switch (button.dataset.action) {
      case "pf1-changelog":
        button.addEventListener("click", (ev) => {
          ev.preventDefault();
          const chlog =
            Object.values(ui.windows).find((app) => app instanceof applications.ChangeLogWindow) ??
            new applications.ChangeLogWindow();
          chlog.render(true, { focus: true });
        });
        break;
      case "pf1-documentation":
        button.addEventListener("click", (ev) => {
          ev.preventDefault();
          pf1.applications.helpBrowser.openUrl("Help/Home");
        });
        break;
      case "pf1-troubleshooter":
        button.addEventListener("click", (ev) => {
          ev.preventDefault();
          pf1.applications.Troubleshooter.open();
        });
        break;
    }
  }

  details.after(template.content);
});

Hooks.on("controlToken", () => {
  // Refresh lighting to (un)apply low-light vision parameters to them
  canvas.perception.update(
    {
      initializeLighting: true,
    },
    true
  );
});

/* ------------------------------- */
/* Expire active effects
/* ------------------------------- */

/**
 * Expire Active Effects from Tokens
 *
 * Triggers duration end handling of active effects on currently viewed scene.
 *
 * Called with `updateWorldTime` and `canvasReady` events.
 */
function expireFromTokens() {
  if (!game.users.activeGM?.isSelf) return;

  for (const t of canvas.tokens.placeables) {
    if (!t.actor) continue;

    // Skip tokens in combat to avoid too early expiration
    if (t.combatant?.combat?.started) continue;

    try {
      t.actor.expireActiveEffects();
    } catch (err) {
      console.error(`PF1 | Failed to expire active effects from token: ${t.name} [${t.document?.uuid}]\n`, err);
    }
  }
}

// On game time change
Hooks.on("updateWorldTime", () => expireFromTokens());
// On canvas render
Hooks.on("canvasReady", () => expireFromTokens());
Hooks.on("canvasReady", async (canvas) => {
  if (!game.user.isGM) return;
  /** @type {ActorPF[]} */
  const actors = canvas.scene?.tokens.map((t) => t.actor);
  if (!actors.length) return;
  console.debug("PF1 | Fixing token sizes");
  for (const actor of actors) actor?._updateTokenSize?.();
});

// Refresh skip state (alleviates sticky modifier issue #1572)
window.addEventListener("focus", () => (pf1.skipConfirmPrompt = false), { passive: true });
