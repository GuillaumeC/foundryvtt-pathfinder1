import { PF1ActorSheetTour } from "./tours/actor-sheet-tour.mjs";

Hooks.once("ready", async function registerPF1Tours() {
  // Actor Sheet Tour
  game.tours.register("pf1", "actorSheet", await PF1ActorSheetTour.fromJSON("systems/pf1/tours/actor-sheet-tour.json"));
});
