import { PF1Tour } from "./base-tours.mjs";

export class PF1ActorSheetTour extends PF1Tour {
  /**
   * Opens the sheet of the actor used for the tour and assigns it to `this.sheetInDisplay`.
   *
   * @param {object} [options={}] - Options to pass to the render function.
   *
   * @returns {Promise<ActorSheet>} The opened sheet.
   */
  async openSheet(options = {}) {
    const comp = game.packs.get(this.config.compendiumId, { strict: true });

    /** @type {pf1.documents.actor.ActorPF | undefined} */
    const actor = await comp.getDocument(this.config.actorId);
    if (!actor) {
      throw new Error(`Actor with ID ${this.config.actorId} not found in compendium ${this.config.compendiumId}`);
    }

    /** @type {ActorSheet | undefined} */
    const sheet = actor.sheet;
    await sheet._render(true, { focus: true, options });
    // Add sheet to apps
    this.apps.push(sheet);
    return sheet;
  }

  /* @override */
  async start() {
    ui.sidebar.tabs.compendium.activate();

    await super.start();
  }

  /* @override */
  async previous() {
    switch (this.previousStep?.id) {
      // Returning to the Compendium tab after opening the compendium
      case this.StepsEnum.GO_TO_BASIC_MONSTERS:
        this._debug(`(${this.previous.name}) Closing Basic Monsters compendium to return to the compendium tab`);
        await this.closeCompendium();
        // FIXME: Since the compendium is closed we need to remove manually the tooltips otherwise they will still be visible
        // and tour stuck
        // This will cause to `_postStep` to be called twice but it *should* be fine
        await this._postStep();
        break;
      case this.StepsEnum.HIGHLIGHT_STREET_MAGICIAN_IN_COMPENDIUM:
        this._debug(`(${this.previous.name}) Closing sheet to return to the compendium`);
        await this.sheetInDisplay?.close();
        break;
      default:
        if (!this.previousStep) {
          this._warn(`(${this.previous.name}) The previous step to ${this.currentStep?.id} doesn't seem exist.`);
        }
        break;
    }

    await super.previous();
  }

  /** @override */
  exit() {
    this.apps.forEach((app) => app.close());

    super.exit();
  }

  /* @override */
  async _preStep() {
    // Based on the current step index we'll know if the character sheet needs to be opened
    // From HIGHLIGHT_SHEET onwards we'll be overseeing the character sheet
    if (this.stepIndex >= this.getStepIndexById(this.StepsEnum.HIGHLIGHT_SHEET)) {
      this._debug(`(${this._preStep.name}) Opening character sheet for overview`);
      await this.openSheet(this.config.actorId);
    }

    // Actions for *specific* steps
    switch (this.currentStep?.id) {
      case this.StepsEnum.GO_TO_BASIC_MONSTERS:
        // We need to expand the Actors folder for the selector of this step to work
        this._debug(`(${this._preStep.name}) Expanding Actors folder`);
        this.expandCompendiumFolder(this.config.compendiumId);
        break;
      case this.StepsEnum.HIGHLIGHT_STREET_MAGICIAN_IN_COMPENDIUM:
        // We need to open the Basic Monsters compendium
        this._debug(`(${this._preStep.name}) Rendering Basic Monsters compendium`);
        await this.openCompendium(this.config.compendiumId);
        break;
      case this.StepsEnum.SPELLBOOKS_CONFIGURATION:
        // Expand the Configuration section
        this._debug(`(${this._preStep.name}) Opening primary spellbook configuration`);
        // At this point we don't have a targetElement so we need to find it by selector
        document.querySelector(this.currentStep?.selector)?.click();
        break;
      default:
        if (!this.currentStep?.id) {
          this._warn(`(${this._preStep.name}) The current step doesn't seem to have an ID.`);
          return;
        }
        break;
    }

    await super._preStep();
  }

  /** @override */
  async _postStep() {
    await super._postStep();

    switch (this.currentStep?.id) {
      // After highlighting the entry in the compendium, we need to open the sheet
      case this.StepsEnum.HIGHLIGHT_STREET_MAGICIAN_IN_COMPENDIUM:
        this._debug(`(${this._postStep.name}) Closing Basic Monsters compendium`);
        this.closeCompendium();
        break;
      case this.StepsEnum.SPELLBOOKS_CONFIGURATION:
        this._debug(`(${this._postStep.name}) Closing primary spellbook configuration`);
        this.targetElement?.click();
        break;
      default:
        if (!this.currentStep?.id) {
          this._warn(`(${this._postStep.name}) The current step doesn't seem to have an ID.`);
          return;
        }
        break;
    }
  }

  /** @override */
  async complete() {
    this._debug(`(${this.complete.name}) Completing tour`);
    this.apps.forEach((app) => app.close());

    await super.complete();
  }
}
