import { NoAutocomplete } from "@app/mixins/no-autocomplete.mjs";
import { Widget_CategorizedItemPicker } from "./categorized-item-picker.mjs";

const { DocumentSheetV2, HandlebarsApplicationMixin } = foundry.applications.api;

/**
 * Change Editor
 *
 * @since PF1 v10
 */
export class ChangeEditor extends HandlebarsApplicationMixin(NoAutocomplete(DocumentSheetV2)) {
  static DEFAULT_OPTIONS = {
    tag: "form",
    form: {
      handler: this._onSubmit,
      submitOnChange: true,
      submitOnClose: true,
      closeOnSubmit: false,
    },
    classes: ["pf1-v2", "change-editor", "standard-form"],
    window: {
      minimizable: false,
      resizable: true,
    },
    position: {
      width: 460,
    },
    actions: {
      copyUuid: { handler: this.#onCopyUuid, buttons: [0, 2] },
    },
    sheetConfig: false,
  };

  static PARTS = {
    form: {
      template: "systems/pf1/templates/apps/change-editor.hbs",
    },
  };

  /** @type {ItemChange} */
  change;

  /**
   * @internal
   * @param options
   */
  constructor(options) {
    super(options);
    this.change = options.change;
  }

  /**
   * Override to fix DocumentSheet ignoring get id
   *
   * @override
   * @param {object} options - App options
   */
  _initializeApplicationOptions(options) {
    options = super._initializeApplicationOptions(options);
    options.uniqueId = this.getID(options); // HACK: Document Sheet ignores the id getter
    return options;
  }

  /* -------------------------------------------- */

  /**
   * @type {ActorPF}
   */
  get actor() {
    return this.item.actor;
  }

  /* -------------------------------------------- */

  /**
   * @type {ItemPF}
   */
  get item() {
    return this.document;
  }

  /* -------------------------------------------- */

  /**
   * @override
   * @type {string}
   */
  get title() {
    let title = game.i18n.localize("PF1.Application.ChangeEditor.Label");
    title += ": " + this.item.name;
    if (this.actor) title += " – " + this.actor.name;
    return title;
  }

  /* -------------------------------------------- */

  /**
   * @override
   * @type {string}
   */
  get id() {
    return this.getID();
  }

  /**
   * HACK: To deal with AppV2 and DocumentSheetV2 behaviour
   *
   * @internal
   * @param {object} options
   * @returns {string}
   */
  getID(options) {
    options ??= this.options;
    return "change-editor-" + options.change.uuid.replaceAll(".", "-");
  }

  /* -------------------------------------------- */

  /**
   * @inheritDoc
   * @internal
   * @async
   */
  async _prepareContext() {
    const change = this.change,
      actor = this.actor,
      item = this.item;

    const buffTargets = pf1.utils.internal.getBuffTargets("buffs", { actor, item });
    const target = buffTargets[change.target];

    const editable = this.isEditable;

    const defaultPriority = change.priority == 0,
      earlyPriority = change.priority > 0,
      latePriority = change.priority < 0;

    return {
      editable,
      cssClass: editable ? "" : "locked",
      config: pf1.config,
      actor,
      item,
      change,
      // Which kind of priority does this have
      priority: {
        early: earlyPriority,
        late: latePriority,
        normal: defaultPriority,
      },
      // Alternative values
      values: {
        priority: {
          early: earlyPriority ? change.priority : defaultPriority ? 1 : -change.priority,
          late: latePriority ? change.priority : defaultPriority ? -1 : -change.priority,
        },
      },
      isAdd: change.operator === "add",
      isSet: change.operator === "set",
      isValid: !!target,
      isCustom: ["skill", "cl", "sl", "concn"].includes(change.target?.split(".")?.[0]),
      isValidType: !!pf1.config.bonusTypes[change.type],
      isValidOp: ["add", "set"].includes(change.operator),
      isSimple: change.isSimple,
      isDeferred: change.isDeferred,
      label: target?.label || change.target,
    };
  }

  /* -------------------------------------------- */

  /**
   * @internal
   * @param {Event} event
   */
  _onChangeTargetControl(event) {
    event.preventDefault();

    if (!this.isEditable) return;

    // Prepare categories and changes to display
    const categories = pf1.utils.internal.getBuffTargetDictionary("buffs", { actor: this.item.actor, item: this.item });

    // Sort specific categories
    const sortable = new Set(["skill"]);
    const lang = game.settings.get("core", "language");
    for (const category of categories) {
      if (!sortable.has(category.key)) continue;
      category.items.sort((a, b) => a.label.localeCompare(b.label, lang));
    }

    let part1 = this.change?.target?.split(".")[0];
    if (part1 === "concn") part1 = "concentration"; // HACK: Fixes spellbook concentration pointing wrong
    const category = pf1.config.buffTargets[part1]?.category ?? part1;

    // Show widget
    const w = new Widget_CategorizedItemPicker(
      { window: { title: "PF1.Application.ChangeTargetSelector.Title" }, classes: ["change-target-selector"] },
      categories,
      (key) => {
        if (key) {
          this.change.update({ target: key });
        }
      },
      { category, item: this.change?.target }
    );
    w.render(true);
  }

  /* -------------------------------------------- */

  /**
   * Show the help browser
   *
   * @internal
   * @param {Event} event
   */
  _openHelpBrowser(event) {
    event.preventDefault();
    const a = event.currentTarget;

    pf1.applications.helpBrowser.openUrl(a.dataset.url);
  }

  /* -------------------------------------------- */

  /**
   * Validate input formula for basic errors.
   *
   * @internal
   * @param {HTMLElement} el
   */
  async _validateFormula(el) {
    const formula = el.value;
    if (!formula) return;

    let roll;
    // Test if formula even works
    try {
      roll = Roll.create(formula);
      await roll.evaluate({ allowInteractive: false });
    } catch (e) {
      el.dataset.tooltip = e.message;
      el.setCustomValidity(e.message);
      return;
    }

    if (el.classList.contains("simple")) {
      if (!roll.isDeterministic || formula.includes("@")) {
        el.dataset.tooltip = "PF1.Warning.FormulaMustBeSimple";
        el.setCustomValidity(game.i18n.localize("PF1.Warning.FormulaMustBeSimple"));
      }
    }
    // Deterministic formulas must be deterministic
    else if (el.classList.contains("deterministic")) {
      if (!roll.isDeterministic) {
        el.dataset.tooltip = "PF1.Warning.FormulaMustBeDeterministic";
        el.setCustomValidity(game.i18n.localize("PF1.Warning.FormulaMustBeDeterministic"));
      }
    }
  }

  /* -------------------------------------------- */

  /**
   * Test if this Change is in its declared parent.
   *
   * @type {boolean}
   */
  get inParent() {
    return !!this.document.changes.get(this.change.id);
  }

  /**
   * HACK: Override to force close the dialog when the Change is deleted
   *
   * @override
   * @param {object} options - Options
   * @param {object} _options - Legacy options
   */
  async render(options, _options) {
    // Despite _options being deprecated, it is the only place for render context
    if (_options?.renderContext === "updateItem") {
      // Close instead of rendering if the Change no longer exists, ignoring anything else
      if (!this.inParent) return this.close();
    }
    return super.render(options, _options);
  }

  /**
   * Attach event listeners to the rendered application form.
   *
   * @param {ApplicationRenderContext} context      Prepared context data
   * @param {RenderOptions} options                 Provided render options
   * @protected
   */
  _onRender(context, options) {
    // Modify changes
    this.element
      .querySelector(".target .change-target")
      .addEventListener("click", this._onChangeTargetControl.bind(this));

    // Open help browser
    this.element.querySelector("a.help-browser[data-url]").addEventListener("click", this._openHelpBrowser.bind(this));

    // Add warning about formulas
    this.element.querySelectorAll("input.formula").forEach(async (el) => this._validateFormula(el));

    this.element.reportValidity();

    // Disable form if not editable
    if (!this.isEditable) {
      for (const el of this.element.querySelectorAll(".window-content :is(input,select)")) {
        el.disabled = true;
      }
    }
  }

  /**
   * Override document ID/UUID handling and redirect them to the Change
   *
   * @internal
   * @override
   * @param {PointerEvent} event - Triggering event
   * @this {ChangeEditor}
   */
  static #onCopyUuid(event) {
    event.preventDefault();
    event.stopPropagation();
    if (event.detail > 1) return; // Ignore repeated clicks
    const id = event.button === 2 ? this.change.id : this.change.uuid;
    const type = event.button === 2 ? "id" : "uuid";
    const label = game.i18n.localize("PF1.Change");
    game.clipboard.copyPlainText(id);
    ui.notifications.info(game.i18n.format("DOCUMENT.IdCopiedClipboard", { label, type, id }));
  }

  /* -------------------------------------------- */

  /**
   * @param {object} options - Application options
   * @param {ItemChange} options.change - Change to modify
   * @param {object} oldOptions - Deprecated application options
   * @returns {Promise<void|ChangeEditor>} - Promise that resolves when the app is closed. Returns application instance if no new instance was created.
   */
  static async wait(options, oldOptions = {}) {
    if (options instanceof pf1.components.ItemChange) {
      foundry.utils.logCompatibilityWarning(
        "ChangeEditor.wait() first parameter as change instance is deprecated. Please include it in app options instead.",
        {
          since: "PF1 v11",
          until: "PF1 v12",
        }
      );

      oldOptions.change = options;
      options = oldOptions;
    }

    const old = Object.values(foundry.applications.instances).find(
      (app) => app instanceof this && app.change === options.change
    );

    if (old) {
      old.render(true);
      old.bringToFront();
      return old;
    }

    return new Promise((resolve) => {
      options.document = options.change.parent;
      const app = new this(options);
      app.resolve = resolve;
      app.render(true);
    });
  }

  /* -------------------------------------------- */

  /**
   * Update the object with the new change data from the form.
   *
   * @this {ChangeEditor}
   * @param {SubmitEvent} event                   The originating form submission event
   * @param {HTMLFormElement} form                The form element that was submitted
   * @param {FormDataExtended} formData           Processed data for the submitted form
   * @returns {Promise<void>}
   * @private
   */
  static _onSubmit(event, form, formData) {
    formData = formData.object;
    const updateData = foundry.utils.expandObject(formData).change;
    this.change.update(updateData);
  }
}
