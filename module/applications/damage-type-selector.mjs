import { NoAutocomplete } from "@app/mixins/no-autocomplete.mjs";

const { ApplicationV2, HandlebarsApplicationMixin } = foundry.applications.api;

export class DamageTypeSelector extends HandlebarsApplicationMixin(NoAutocomplete(ApplicationV2)) {
  static DEFAULT_OPTIONS = {
    tag: "form",
    form: {
      handler: this._updateObject,
      closeOnSubmit: true,
    },
    classes: ["pf1-v2", "damage-type-selector"],
    window: {
      title: "PF1.DamageType",
      minimizable: true,
      resizable: false,
    },
    position: {
      width: 720,
    },
    actions: {
      toggleDamageType: this._toggleDamageType,
    },
    sheetConfig: false,
  };

  static PARTS = {
    form: {
      template: "systems/pf1/templates/apps/damage-type-selector.hbs",
      scrollable: [".damage-type-categories", ".damage-modifiers"],
    },
    footer: {
      template: "templates/generic/form-footer.hbs",
    },
  };

  /**
   * @internal
   * @type {DamageTypes}
   */
  damage;

  /**
   * @param {object} object - Parent object
   * @param {string} path - Path to damage data in object
   * @param {DamageTypes} data - Damage data
   * @param {object} options - Options
   * @param {Function} options.updateCallback - Update callback
   */
  constructor(object, path, data, options = {}) {
    options.object = object;
    options.path = path;
    super(options);

    // Prepare types
    const customTypes = new Set();
    const allTypes = data ? [...data] : [];
    const types = new Set();

    for (const type of allTypes) {
      if (!type) continue;

      const d = pf1.registry.damageTypes.get(type);
      if (d) types.add(type);
      else customTypes.add(type);
    }

    this.types = types;
    this.customTypes = customTypes;
  }

  /* -------------------------------------------- */

  /**
   * Initialize the configuration for this application. Override the default ID to be unique to this
   * entry selector instance based on document and attribute that is being edited.
   *
   * @override
   * @param {ApplicationConfiguration} options    The provided configuration options for the Application
   * @returns {ApplicationConfiguration}           The final configuration values for the application
   */
  _initializeApplicationOptions(options) {
    options = super._initializeApplicationOptions(options);
    options.id = `DamageTypeSelector-${options.object.id}-${options.path.replaceAll(".", "_")}`;
    return options;
  }

  /* -------------------------------------------- */

  get categoryOrder() {
    return ["physical", "energy", "misc"];
  }

  /* -------------------------------------------- */

  /**
   * @inheritDoc
   * @internal
   * @async
   */
  async _prepareContext() {
    const types = pf1.registry.damageTypes
      .filter((damageType) => !damageType.isModifier)
      .map((dt) => ({ ...dt, id: dt.id, enabled: this.types.has(dt.id) }));

    const sortOrder = this.categoryOrder;

    return {
      types: this.types,
      customTypes: [...this.customTypes].join(";"), // HACK
      modifiers: pf1.registry.damageTypes
        .filter((o) => o.isModifier)
        .map((dm) => ({ ...dm, id: dm.id, enabled: this.types.has(dm.id) })),
      // Damage type categories
      categories: types
        .reduce((cur, o) => {
          let categoryObj = cur.find((o2) => o2.key === o.category);
          if (!categoryObj) {
            categoryObj = { key: o.category, label: `PF1.DamageTypeCategory.${o.category}`, types: [] };
            cur.push(categoryObj);
          }
          categoryObj.types.push(o);
          return cur;
        }, [])
        .sort((a, b) => {
          const idxA = sortOrder.indexOf(a.key);
          const idxB = sortOrder.indexOf(b.key);
          if (idxA === -1 && idxB >= 0) return 1;
          if (idxB === -1 && idxA >= 0) return -1;
          if (idxA > idxB) return 1;
          if (idxA < idxB) return -1;
          return 0;
        }),
      buttons: [{ type: "submit", label: "PF1.Save", icon: "far fa-save" }],
    };
  }

  /* -------------------------------------------- */

  /**
   * Update internal data snapshot on form change
   *
   * @param formConfig
   * @param event
   * @override
   * @internal
   * @this {DamageTypeSelector}
   * @returns {Promise<void>}
   */
  async _onChangeForm(formConfig, event) {
    event.preventDefault();
    const elem = event.target;

    // Only handle custom types here
    if (elem.name !== "custom") return;

    const types = elem.value
      .split(";")
      .map((t) => t.trim())
      .filter((t) => !!t);

    this.customTypes = new Set(types);
  }

  /* -------------------------------------------- */

  /**
   * @internal
   * @param {Event} event
   */
  static _toggleDamageType(event) {
    event.preventDefault();
    const a = event.target.closest("[data-action]");
    const dt = a.dataset.id;

    if (this.types.has(dt)) this.types.delete(dt);
    else this.types.add(dt);

    this.render();
  }

  /* -------------------------------------------- */

  /**
   * @override
   * @param {Event} event
   * @param {object} formData
   */
  static async _updateObject(event, formData) {
    const allTypes = [...this.types.union(this.customTypes)];
    return this.options.updateCallback(allTypes);
  }
}

/**
 * @typedef {object} DamageTypes
 * @property {string[]} values - Damage type IDs
 * @property {string} custom - Semicolon deliminated list of custom damage type.
 */
