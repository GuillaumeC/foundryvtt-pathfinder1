import { MigrationState } from "../../migration/migration-state.mjs";
import { MigrationIssuesDialog } from "./migration-issues-dialog.mjs";

const { ApplicationV2, HandlebarsApplicationMixin } = foundry.applications.api;

/**
 * Migration progress tracking dialog
 */
export class MigrationDialog extends HandlebarsApplicationMixin(ApplicationV2) {
  /**
   * Migration state object.
   *
   * @type {MigrationState}
   */
  migrationState;
  /**
   * Autoclose migration dialog once done
   *
   * @type {boolean}
   */
  autoClose;
  /**
   * Ignore documents with appropriate migration marker.
   *
   * @type {boolean}
   */
  fastMigration;

  constructor(options) {
    super(options);

    this.migrationState = options.stateTracker;

    this.autoClose = options.autoClose ?? false;
    this.fastMigration = options.fast ?? true;

    this.migrationState.callbacks[this.appId] = this._onMigration.bind(this);
  }

  get title() {
    const label = game.i18n.localize("PF1.Migration.Dialog.Title");
    if (this.migrationState.label) return `${label}: ${this.migrationState.label}`;
    return label;
  }

  static DEFAULT_OPTIONS = {
    tag: "form",
    classes: ["pf1-v2", "migration"],
    window: {
      resizable: true,
    },
    position: {
      top: 200,
      width: "auto",
      height: "auto",
    },
    actions: {
      "show-details": this._showDetails,
      "reload-all": this._reloadClients,
    },
  };

  static PARTS = {
    form: {
      template: "systems/pf1/templates/apps/migration-progress.hbs",
    },
    status: {
      template: "systems/pf1/templates/apps/migration-status.hbs",
    },
  };

  /**
   * @override
   * @param {object} options
   * @returns {object}
   */
  async _prepareContext(options) {
    await super._prepareContext(options);

    return this.migrationState;
  }

  /**
   * @override
   * @param {object} context
   * @param {object} options
   */
  _onRender(context, options) {
    super._onRender(context, options);

    if (this.migrationState.completed && this.migrationState.typeChanges !== true && this.autoClose) {
      setTimeout(() => this.close(), 5_000);
    }
  }

  /**
   * @param {Event} event
   * @param {HTMLElement} target
   * @this {MigrationDialog}
   */
  static _reloadClients(event) {
    event.preventDefault();

    game.socket.emit("reload");
    foundry.utils.debouncedReload();
  }

  /**
   * @param { Event } event
   * @param { HTMLElement } target
   * @this {MigrationDialog}
   */
  static _showDetails(event) {
    event.preventDefault();

    const el = event.target.closest(".category");
    const categoryId = el.dataset.category;
    const category = this.migrationState.categories[categoryId];

    MigrationIssuesDialog.open(category);
  }

  _lastProcessUpdate = 0;
  /**
   * @param {MigrationState} state
   * @param {MigrationCategory} category
   * @param {object} info
   */
  _onMigration(state, category, info) {
    const parts = ["form"];
    switch (info.action) {
      case "finish":
        if (state.completed) parts.push("status");
        break;
      case "process": {
        // Ignore process updates if they come too frequently
        const t0 = performance.now();
        if (t0 - this._lastProcessUpdate < 200 && this.state !== this.constructor.RENDER_STATES.RENDERED) return;
        this._lastProcessUpdate = t0;
        break;
      }
    }
    this.render({ parts });
  }

  /**
   * @override
   * @param {object} options
   */
  async _preClose(options) {
    await super._preClose(options);

    delete this.migrationState.callbacks[this.appId];
  }

  /**
   * Initialize migration dialog and migration state tracker if necessary.
   *
   * @param {MigrationState} [state] - Existing state tracker if any
   * @param {string} [label] - Label associated with the tracker
   * @param {object} [dialogOptions={}] - Dialog options
   * @returns {MigrationDialog} - Active state tracker
   */
  static async initialize(state, label, dialogOptions = {}) {
    state ??= new MigrationState(label);
    state.label ||= label;

    const app = new this({ stateTracker: state, autoClose: dialogOptions.autoClose ?? false });
    await app.render({ dialogOptions, force: true });

    return app;
  }
}
