import { ActorSheetPFNPC } from "./npc-sheet.mjs";

/**
 * An Actor sheet for Vehicle type characters in the game system.
 * Extends the base ActorSheetPF class.
 */
export class ActorSheetPFHaunt extends ActorSheetPFNPC {
  /**
   * Define default rendering options for the NPC sheet
   *
   * @returns {object} The default rendering options
   */
  static get defaultOptions() {
    const options = super.defaultOptions;
    return {
      ...options,
      classes: [...options.classes, "haunt"],
      width: 880,
      height: 700,
      tabs: [{ navSelector: "nav.tabs", contentSelector: "section.primary-body", initial: "summary" }],
      scrollY: [".tab.summary"],
    };
  }

  /* -------------------------------------------- */
  /*  Rendering                                   */
  /* -------------------------------------------- */

  /**
   * Get the correct HTML template path to use for rendering this particular sheet
   *
   * @type {string}
   */
  get template() {
    if (this.actor.limited) return "systems/pf1/templates/actors/limited-sheet.hbs";
    return "systems/pf1/templates/actors/haunt-sheet.hbs";
  }

  /* -------------------------------------------- */

  /** @override */
  static EDIT_TRACKING = ["system.details.notes", "system.details.effect", "system.details.destruction"];

  /**
   * Add some extra data when rendering the sheet to reduce the amount of logic required within the template.
   *
   * @returns {Promise<object>} The data object to render the sheet
   */
  async getData() {
    const isOwner = this.document.isOwner;
    const context = {
      owner: isOwner,
      limited: this.document.limited,
      editable: this.isEditable,
      cssClass: isOwner ? "editable" : "locked",
      config: pf1.config,
      isGM: game.user.isGM,
      system: this.actor.system,
      measureTemplateTypes: pf1.utils.internal.getTemplateTypes(),
      hasHD: false,
      fields: this.actor.system.schema.fields,
      units: game.i18n.localize(
        pf1.utils.getDistanceSystem() === "imperial" ? "PF1.Distance.ftShort" : "PF1.Distance.mShort"
      ),
    };

    const rollData = this.actor.getRollData();
    context.rollData = rollData;

    // Labels
    context.labels = {
      cr: pf1.utils.CR.fromNumber(context.system.details.cr.total),
      alignment: context.config.alignments[context.system.details.alignment],
    };

    const auraStrength = context.rollData.auraStrength;
    if (auraStrength) {
      context.labels.auraStrength = pf1.config.auraStrengths[auraStrength];
    }

    // Enrich description and notes
    const enrichHTMLOptions = {
      secrets: isOwner,
      rollData: context.rollData,
      relativeTo: this.actor,
    };

    const noDesc = "<p class='placeholder'>" + game.i18n.localize("PF1.NoDescription") + "</p>";

    const destruction = context.system.details?.destruction;
    const pDestruction = destruction ? pf1.utils.enrichHTMLUnrolled(destruction, enrichHTMLOptions) : Promise.resolve();
    pDestruction.then((html) => (context.destructionHTML = html || noDesc));
    const effect = context.system.details?.effect;
    const pEffect = effect ? pf1.utils.enrichHTMLUnrolled(effect, enrichHTMLOptions) : Promise.resolve();
    pEffect.then((html) => (context.effectHTML = html || noDesc));
    const notes = context.system.details?.notes;
    const pNotes = notes ? pf1.utils.enrichHTMLUnrolled(notes, enrichHTMLOptions) : Promise.resolve();
    pNotes.then((html) => (context.notesHTML = html));
    await Promise.all([pDestruction, pEffect, pNotes]);

    // The Actor and its Items
    context.actor = this.actor;
    context.token = this.token;
    context.items = this.document.items
      .map((item) => this._prepareItem(item))
      .sort((a, b) => (a.sort || 0) - (b.sort || 0));

    // Prepare owned items
    this._prepareItems(context);

    // Prepare Templates
    const trait = context.system.templates;
    const values = trait.total;

    const pTemplates = values.reduce((arr, t) => {
      arr.push(
        context.config.hauntTemplates[t]
          ? TextEditor.enrichHTML(`@UUID[${context.config.hauntTemplates[t].uuid}]`)
          : TextEditor.enrichHTML(`@UUID[${t}]{${t}}`)
      );
      return arr;
    }, []);
    const templateLinks = await Promise.all(pTemplates);
    trait.selected = await values.reduce((obj, t, index) => {
      const link = templateLinks[index];
      obj[t] = {
        name: context.config.hauntTemplateLabels[t] || t,
        link,
        broken: link === t,
      };

      return obj;
    }, {});

    context._editorState = pf1.applications.utils.restoreEditState(this, context);

    return context;
  }

  /**
   * @private
   * @param {string} fullId - Target ID
   * @param {object} context - Context object to store data into
   * @throws {Error} - If provided ID is invalid.
   * @override
   * @returns {Promise<void>}
   */
  async _getTooltipContext(fullId, context) {
    const actor = this.actor;

    // Lazy roll data
    const lazy = {
      get rollData() {
        this._cache ??= actor.getRollData();
        return this._cache;
      },
    };

    let header, subHeader;
    const details = [];
    const paths = [];
    const sources = [];
    let notes;

    const re = /^(?<id>[\w-]+)(?:\.(?<detail>.*))?$/.exec(fullId);
    const { id } = re?.groups ?? {};

    switch (id) {
      case "cl":
        paths.push({ path: "@details.cl", value: lazy.rollData.details.cl }, { path: "@cl", value: lazy.rollData.cl });
        break;
      case "health":
        paths.push(
          { path: "@attributes.hp.value", value: lazy.rollData.attributes.hp.value },
          { path: "@attributes.hp.max", value: lazy.rollData.attributes.hp.max }
        );
        break;
      default:
        return super._getTooltipContext(fullId, context);
    }

    context.header = header;
    context.subHeader = subHeader;
    context.details = details;
    context.paths = paths;
    context.sources = sources;
    context.notes = notes ?? [];
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /**
   * Organize and classify Owned Items - We just need attacks
   *
   * @param {object} context - The data object to render the sheet
   * @private
   * @override
   * @returns {void}
   */
  _prepareItems(context) {
    const attacks = context.items.filter((i) => i.type === "attack");

    const attackSections = Object.values(pf1.config.sheetSections.combatlite)
      .map((data) => ({ ...data }))
      .sort((a, b) => a.sort - b.sort);
    for (const i of attacks) {
      const section = attackSections.find((section) => this._applySectionFilter(i, section));
      if (section) {
        section.items ??= [];
        section.items.push(i);
      } else {
        console.warn("Could not find a sheet section for", i.name);
      }
    }

    context.attacks = attackSections;
  }

  _updateObject(event, formData) {
    formData = foundry.utils.expandObject(formData);

    // Convert distances back to backend imperial format
    const value = formData.system.details.area?.size;
    if (Number.isFinite(value)) {
      formData.system.details.area.size = pf1.utils.convertDistanceBack(value)[0];
    }

    return super._updateObject(event, formData);
  }
}
