import { ActorSheetPF } from "./actor-sheet.mjs";

/**
 * An Actor sheet for Vehicle type characters in the game system.
 * Extends the base ActorSheetPF class.
 *
 * @type {ActorSheetPF}
 */
export class ActorSheetPFTrap extends ActorSheetPF {
  /**
   * Define default rendering options for the NPC sheet
   *
   * @returns {object} The default rendering options
   */
  static get defaultOptions() {
    const options = super.defaultOptions;
    return {
      ...options,
      classes: [...options.classes, "trap"],
      width: 600,
      height: 700,
      tabs: [{ navSelector: "nav.tabs", contentSelector: "section.primary-body", initial: "summary" }],
      scrollY: [".tab.summary"],
    };
  }

  /* -------------------------------------------- */
  /*  Rendering                                   */
  /* -------------------------------------------- */

  /**
   * Get the correct HTML template path to use for rendering this particular sheet
   *
   * @type {string}
   */
  get template() {
    if (this.actor.limited) return "systems/pf1/templates/actors/limited-sheet.hbs";
    return "systems/pf1/templates/actors/trap-sheet.hbs";
  }

  /* -------------------------------------------- */

  /** @override */
  static EDIT_TRACKING = ["system.notes", "system.effect"];

  /**
   * Add some extra data when rendering the sheet to reduce the amount of logic required within the template.
   *
   * @override
   * @returns {Promise<object>} The data object to render the sheet with
   */
  async getData() {
    const isOwner = this.document.isOwner;
    const context = {
      owner: isOwner,
      system: this.actor.system,
      limited: this.document.limited,
      editable: this.isEditable,
      cssClass: isOwner ? "editable" : "locked",
      config: pf1.config,
      isGM: game.user.isGM,
      hasHD: false,
      fields: this.actor.system.schema.fields,
      rollData: this.actor.getRollData(),
      perception: this.actor.getPerceptionModifier(),
    };

    // Trigger Types
    if (!(context.system.type in pf1.config.traps.types)) {
      context.trapTypes = foundry.utils.mergeObject(
        { [`${context.system.type}`]: context.system.type },
        pf1.config.traps.types
      );
    } else {
      context.trapTypes = pf1.config.traps.types;
    }

    // Challenge Rating
    context.labels = {
      cr: pf1.utils.CR.fromNumber(context.system.cr.total),
    };

    // Trigger info
    if (["proximity", "visual", "timed"].includes(context.system.trigger.type)) {
      context.hasTriggerRange = true;

      if (context.system.trigger.type === "timed") context.hasTriggerTime = true;
    }
    context.hasPerceptionTrigger = ["electricEyes", "genetic", "sound", "visual"].includes(context.system.trigger.type);

    // Enrich effects and notes
    const enrichHTMLOptions = {
      secrets: isOwner,
      rollData: context.rollData,
      relativeTo: this.actor,
    };

    const noDesc = "<p class='placeholder'>" + game.i18n.localize("PF1.NoDescription") + "</p>";

    const effect = context.system.effect;
    const pEffect = effect ? pf1.utils.enrichHTMLUnrolled(effect, enrichHTMLOptions) : Promise.resolve();
    pEffect.then((html) => (context.effectHTML = html || noDesc));
    const notes = context.system.notes;
    const pNotes = notes ? pf1.utils.enrichHTMLUnrolled(notes, enrichHTMLOptions) : Promise.resolve();
    pNotes.then((html) => (context.notesHTML = html));
    await Promise.all([pEffect, pNotes]);

    // The Actor and its Items
    context.actor = this.actor;
    context.token = this.token;
    context.items = this.document.items
      .map((item) => this._prepareItem(item))
      .sort((a, b) => (a.sort || 0) - (b.sort || 0));

    // Prepare owned items
    this._prepareItems(context);

    context._editorState = pf1.applications.utils.restoreEditState(this, context);

    return context;
  }

  /**
   * @private
   * @param {string} fullId - Target ID
   * @param {object} context - Context object to store data into
   * @throws {Error} - If provided ID is invalid.
   * @override
   * @returns {Promise<void>}
   */
  async _getTooltipContext(fullId, context) {
    /** @type {pf1.documents.actor.ActorTrapPF} */
    const actor = this.actor;

    // Lazy roll data
    const lazy = {
      get rollData() {
        this._cache ??= actor.getRollData();
        return this._cache;
      },
    };

    let header, subHeader;
    const details = [];
    const paths = [];
    const sources = [];
    let notes;

    const re = /^(?<id>[\w-]+)(?:\.(?<detail>.*))?$/.exec(fullId);
    const { id } = re?.groups ?? {};

    switch (id) {
      case "detect":
        paths.push({ path: "@perception", value: lazy.rollData.perception });
        break;
      case "disarm":
        paths.push({ path: "@disarm", value: lazy.rollData.disarm });
        break;
      default:
        return super._getTooltipContext(fullId, context);
    }

    context.header = header;
    context.subHeader = subHeader;
    context.details = details;
    context.paths = paths;
    context.sources = sources;
    context.notes = notes ?? [];
  }

  /* -------------------------------------------- */
  /*  Event Listeners and Handlers                */
  /* -------------------------------------------- */

  /**
   * Activate event listeners using the prepared sheet HTML
   *
   * @param {JQuery<HTMLElement>} jq   The prepared HTML object ready to be rendered into the DOM
   */
  activateListeners(jq) {
    super.activateListeners(jq);

    /** @type {Document} */
    const html = jq[0];

    // Adjust CR
    html.querySelector("span.cr-input")?.addEventListener("click", (event) => {
      this._onSpanTextInput(event, this._adjustCR.bind(this));
    });

    html.querySelector(".trigger .rollable[data-dtype='per']")?.addEventListener("click", (ev) => {
      ev.preventDefault();

      this.actor.rollPerception({ token: this.token });
    });

    html.querySelectorAll("input[name='system.trigger.value'], input[name='system.reset.value']").forEach((el) => {
      el.addEventListener("click", (ev) => {
        ev.target.select();
      });
    });
  }

  /**
   * Adjust the CR of the trap
   *
   * @param {Event} event   The originating click event
   */
  async _adjustCR(event) {
    event.preventDefault();
    const el = event.currentTarget;

    const value = pf1.utils.CR.fromString(el.tagName === "INPUT" ? el.value : el.innerText);
    const name = el.getAttribute("name");
    let updateData;
    if (name) {
      updateData = { [name]: value };
    }

    // Update on lose focus
    if (event.originalEvent instanceof MouseEvent) {
      el.addEventListener("pointerleave", async (ev) => this._updateObject(event, this._getSubmitData(updateData)), {
        //passive: true, // Causes Foundry to error
        once: true,
      });
    } else {
      this._updateObject(event, this._getSubmitData(updateData));
    }
  }

  /**
   * Organize and classify Owned Items - We just need attacks
   *
   * @param {object} context The rendering context
   * @private
   * @override
   */
  _prepareItems(context) {
    const attacks = context.items.filter((i) => i.type === "attack");

    const attackSections = Object.values(pf1.config.sheetSections.combatlite)
      .map((data) => ({ ...data }))
      .sort((a, b) => a.sort - b.sort);
    for (const i of attacks) {
      const section = attackSections.find((section) => this._applySectionFilter(i, section));
      if (section) {
        section.items ??= [];
        section.items.push(i);
      } else {
        console.warn("Could not find a sheet section for", i.name);
      }
    }

    context.attacks = attackSections;
  }

  _updateObject(event, formData) {
    formData = foundry.utils.expandObject(formData);

    // Convert distances back to backend imperial format
    if (formData.system.trigger.value && ["proximity", "visual"].includes(this.actor.system.trigger.type)) {
      const value = formData.system.trigger.value;
      if (Number.isFinite(value)) {
        formData.system.trigger.value = pf1.utils.convertDistanceBack(value)[0];
      }
    }

    return super._updateObject(event, formData);
  }
}
