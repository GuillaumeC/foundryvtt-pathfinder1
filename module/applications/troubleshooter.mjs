const { ApplicationV2, HandlebarsApplicationMixin } = foundry.applications.api;

export class Troubleshooter extends HandlebarsApplicationMixin(ApplicationV2) {
  static DEFAULT_OPTIONS = {
    id: "pf1-troubleshooter",
    tag: "form",
    classes: ["pf1-v2", "troubleshooter"],
    window: {
      title: "PF1.Troubleshooter.Title",
      minimizable: false,
      resizable: false,
    },
    position: {
      width: 460,
    },
    actions: {
      migrate: this._runMigration,
      help: this._openHelp,
    },
  };

  static PARTS = {
    form: {
      template: "systems/pf1/templates/apps/troubleshooter.hbs",
    },
  };

  // Are packs to be unlocked?
  unlock = false;

  // Are already migrated documents to be reprocessed?
  reprocess = false;

  // Migration state
  migrating = { world: false, modules: false };

  /* -------------------------------------------- */

  /**
   * @inheritDoc
   * @internal
   * @async
   */
  async _prepareContext() {
    return {
      isGM: game.user.isGM,
      unlockPacks: this.unlock,
      reprocess: this.reprocess,
      migrating: this.migrating,
      links: {
        help: `<a data-action='help'>${game.i18n.localize("PF1.Troubleshooter.Steps.HelpLink")}</a>`,
        report: `<a href="https://gitlab.com/foundryvtt_pathfinder1e/foundryvtt-pathfinder1/-/issues">${game.i18n.localize(
          "PF1.Troubleshooter.Steps.ReportLinkText"
        )}</a>`,
        foundry: {
          kb: `<a href="https://foundryvtt.com/kb/">${game.i18n.localize("PF1.Troubleshooter.Steps.FoundryLink")}</a>`,
          discord: `<a href="https://discord.gg/foundryvtt">Foundry VTT</a>`,
          channel: "#pf1",
        },
        faq: "https://gitlab.com/foundryvtt_pathfinder1e/foundryvtt-pathfinder1/-/wikis/FAQs",
        helpmodule: `<a href="https://foundryvtt.com/packages/find-the-culprit">Find the Culprit</a>`,
      },
    };
  }

  /* -------------------------------------------- */

  /**
   * @param {Event} event - Triggering event
   * @param {Element} button - Source button
   */
  static async _runMigration(event, button) {
    const unlock = this.unlock ?? false;
    const fast = !this.reprocess;

    /** @type {Element} */
    if (button.disabled) return;

    button.classList.remove("finished");
    button.disabled = true;
    button.classList.add("active");

    const target = button.dataset.target;
    const top = button.closest(".window-content").getBoundingClientRect().top + 20;

    switch (target) {
      case "world":
        this.migrating.world = true;
        await pf1.migrations.migrateWorld({ unlock, fast, dialog: { top } });
        this.migrating.world = false;
        break;

      case "modules":
        this.migrating.modules = true;
        await pf1.migrations.migrateModules({ unlock, fast, dialog: { top } });
        this.migrating.modules = false;
        break;

      default:
        throw new Error(`Unrecognized migration target: "${target}"`);
    }

    this.element?.querySelector(".form-body").classList.remove("migrating");
    button.disabled = false;
    button.classList.remove("active");
    button.classList.add("finished");
  }

  /* -------------------------------------------- */

  /**
   * @param {Event} _event - Triggering event
   */
  static _openHelp(_event) {
    pf1.applications.helpBrowser.openUrl("Help/Home");
  }
  /* -------------------------------------------- */

  /**
   * The event handler for changes to form input elements
   *
   * @internal
   * @override
   * @param {ApplicationFormConfiguration} formConfig - The configuration of the form being changed
   * @param {Event} event - The triggering event
   */
  _onChangeForm(formConfig, event) {
    const target = event.target;

    switch (target.name) {
      case "unlock":
        this.unlock = target.checked;
        break;
      case "reprocess":
        this.reprocess = target.checked;
        break;
    }
  }

  /* -------------------------------------------- */

  /**
   * Attach event listeners to the rendered application form.
   *
   * @param {ApplicationRenderContext} _context      Prepared context data
   * @param {RenderOptions} _options                 Provided render options
   * @protected
   */
  _onRender(_context, _options) {
    const migrationButtons = this.element.querySelectorAll("button[data-action='migrate']");

    // React to external migration to minimal degree
    if (pf1.migrations.isMigrating) {
      this.migrating.world = true;
      this.migrating.modules = true;
      for (const button of migrationButtons) {
        button.disabled = true;
        button.classList.add("active");
      }

      Hooks.once("pf1MigrationFinished", () => {
        for (const button of migrationButtons) {
          button.disabled = false;
          this.migrating.world = false;
          this.migrating.modules = false;
        }
      });
    }
  }

  /* -------------------------------------------- */

  static open() {
    new Troubleshooter().render(true);
  }
}
