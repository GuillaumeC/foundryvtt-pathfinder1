/**
 * Stack splitting dialog
 *
 * @example
 * ```js
 * const result = await pf1.applications.SplitStack.wait({ title: "My Stuff", initial: 50, total: 100 });
 * if (!result) throw new Error("Fsck!");
 * const [keep,split] = result;
 * console.log(`I keep ${keep} and you get ${split}`);
 * ```
 */
export class SplitStack extends foundry.applications.api.DialogV2 {
  /**
   * Wait for user interaction to finish.
   *
   * @param {object} options - Options
   * @param {string} [options.title] - Dialog title
   * @param {number} options.initial - Initial value
   * @param {number} options.total - Total value to split.
   * @param {number} [options.step] - Value stepping.
   * @param {string[]} [options.css] - Optional CSS selectors to add to the dialog.
   * @param {object} dialogOptions - Additional options to pass to DialogV2
   * @returns {number[]|null} - Number tuple, to keep and to split values. Null if cancelled.
   */
  static async wait({ title, initial = 1, step = 1, total, css = [] } = {}, dialogOptions = {}) {
    // Can't split
    if (total <= 1) return null;
    // Only one answer
    if (total == 2) return [1, 1];

    step ||= 1;
    initial = Math.clamp(initial || 0, 1, total);
    const max = total - 1;

    const content = await renderTemplate("systems/pf1/templates/apps/split-stack.hbs", {
      initial,
      keep: total - initial,
      max,
    });

    const options = {
      window: { title },
      content,
      classes: ["pf1-v2", "split-stack", ...css],
      buttons: [
        {
          icon: "fa-solid fa-people-arrows",
          label: game.i18n.localize("PF1.Split"),
          action: "split",
          default: true,
          callback: (event, target, html) => {
            const splitValue = Math.clamp(html.querySelector("form input.split").valueAsNumber, 1, max);
            if (Number.isNumeric(splitValue)) {
              return [Math.max(1, total - splitValue), splitValue];
            }
            return null;
          },
        },
      ],
      render: (event, html) => {
        html = html.querySelector(".dialog-content");
        const slider = html.querySelector("input.slider");
        const oldStack = html.querySelector("input.left");
        const newStack = html.querySelector("input.split");
        slider.addEventListener(
          "input",
          (ev) => {
            const newValue = ev.target.valueAsNumber;
            newStack.value = newValue;
            oldStack.value = total - newValue;
          },
          { passive: true }
        );
        newStack.addEventListener(
          "input",
          (ev) => {
            let newValue = ev.target.valueAsNumber;
            if (newValue > max) {
              newStack.value = max;
              newValue = max;
            } else if (newValue < 1) {
              newStack.value = 1;
              newValue = 1;
            }
            slider.value = newValue;
            oldStack.value = total - newValue;
          },
          { passive: true }
        );
        oldStack.addEventListener("input", (ev) => {
          let newValue = total - ev.target.valueAsNumber;
          if (newValue > total) {
            oldStack.value = max;
            newValue = 1;
          } else if (newValue < 1) {
            oldStack.value = 1;
            newValue = max;
          }
          newStack.value = newValue;
          slider.value = newValue;
        });
      },
      close: () => null,
      rejectClose: false,
    };

    return super.wait(foundry.utils.mergeObject(options, dialogOptions));
  }
}
