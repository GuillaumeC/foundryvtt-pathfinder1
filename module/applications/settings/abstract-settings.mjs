import { NoAutocomplete } from "@app/mixins/no-autocomplete.mjs";

const { ApplicationV2, HandlebarsApplicationMixin } = foundry.applications.api;

/**
 * A generic application to render a settings modal.
 *
 * @abstract
 */
export class AbstractSettingsApplication extends HandlebarsApplicationMixin(NoAutocomplete(ApplicationV2)) {
  static DEFAULT_OPTIONS = {
    tag: "form",
    form: {
      handler: this._save,
      submitOnChange: false,
      closeOnSubmit: true,
      submitOnClose: false,
    },
    classes: ["pf1-v2", "settings", "standard-form"],
    window: {
      minimizable: false,
      resizable: false,
    },
    actions: {
      resetForm: this._onFormReset,
    },
    position: {
      width: 520,
    },
    sheetConfig: false,
  };

  /* -------------------------------------------- */

  /** @override */
  get title() {
    return game.i18n.localize(this.options.phraseKey + ".Title");
  }

  /* -------------------------------------------- */

  /**
   * Initialize the configuration for this application. Override the default ID to be unique to this
   * settings app instance and tack on extra class.
   *
   * @override
   * @param {ApplicationConfiguration} options    The provided configuration options for the Application
   * @returns {ApplicationConfiguration}           The final configuration values for the application
   */
  _initializeApplicationOptions(options) {
    options = super._initializeApplicationOptions(options);

    options.id = options.configKey.replace("Config", "").toLowerCase() + "-config";
    options.classes = options.classes || [];
    options.classes.push(options.id);

    return options;
  }

  /* -------------------------------------------- */

  /** @override */
  async _prepareContext() {
    if (!this.settings) {
      const settings = game.settings.get("pf1", this.options.configKey);
      this.settings = new this.options.model(settings.toObject());
      this.fields = this.settings.fields;
    }

    return {};
  }

  /* -------------------------------------------- */

  /**
   * @override
   * @inheritDoc
   * @internal
   * @param {string} partId - Part ID
   * @param {ApplicationRenderContext} context - Context
   * @returns {Promise<ApplicationRenderContext>} - Context data
   */
  async _preparePartContext(partId, context) {
    switch (partId) {
      case "form":
        {
          Object.assign(context, {
            activeTab: this.tabGroups.primary,
            settings: this.settings,
            fields: this.settings.schema.fields,
            model: this.options.model,
            config: pf1.config,
            const: pf1.const,
          });
        }
        break;

      case "footer":
        {
          context.buttons = [
            { type: "button", label: "SETTINGS.Reset", icon: "fa-solid fa-sync", action: "resetForm" },
            { type: "submit", label: "SETTINGS.Save", icon: "fa-solid fa-save" },
          ];
        }
        break;

      default:
        break;
    }

    if (["form", "tabs"].includes(partId)) {
      context.tabs = this.tabs?.primary || {};
      for (const [id, tab] of Object.entries(context.tabs)) {
        const active = this.tabGroups.primary === id;
        Object.assign(tab, {
          id,
          group: "primary",
          active,
          cssClass: active ? "active" : "",
        });
      }
    }

    return context;
  }

  /* -------------------------------------------- */

  /**
   * The event handler for changes to form input elements
   *
   * @internal
   * @param {ApplicationFormConfiguration} formConfig   The configuration of the form being changed
   * @param {Event} event                               The triggering event
   * @returns {void}
   */
  _onChangeForm(formConfig, event) {
    const input = event.target.closest("[name]");
    this.settings.updateSource({ [input.name]: input.type === "checkbox" ? input.checked : input.value });
    this.render({ parts: ["form"] });
  }

  /* -------------------------------------------- */

  /**
   * The event handler that is fired when the form is reset to its original values
   *
   * @internal
   * @param {Event} event - The triggering event
   * @returns {Promise<void>}
   */
  static async _onFormReset(event) {
    this.settings = new this.options.model();
    ui.notifications.info(game.i18n.localize(this.options.phraseKey + ".Reset"));
    this.render({ parts: ["form"] });
  }

  /* -------------------------------------------- */

  /**
   * Update the game settings with the new configuration
   *
   * @internal
   * @this {ActorRestDialog}
   * @param {SubmitEvent} event                                     The originating form submission event
   * @param {HTMLFormElement} form                                  The form element that was submitted
   * @param {FormDataExtended} formData                             Processed data for the submitted form
   * @param {Partial<ActorRestOptions>} formData.object             The form data in object form
   * @returns {Promise<void>}
   */
  static async _save(event, form, formData) {
    formData = foundry.utils.expandObject(formData.object);
    await game.settings.set("pf1", this.options.configKey, formData);
  }
}

/**
 * A generic application to render a settings modal with a "Player" and a "World" tab.
 *
 * @abstract
 */
export class AbstractSplitSettingsApplication extends AbstractSettingsApplication {
  static DEFAULT_OPTIONS = {
    form: {
      handler: this._save,
    },
    actions: {
      resetForm: this._onFormReset,
    },
  };

  tabGroups = { primary: "player" };

  tabs = {
    primary: {
      player: {
        icon: "fas fa-user",
        label: "PF1.Application.Settings.Player",
      },
      world: {
        icon: "fas fa-globe",
        label: "PF1.Application.Settings.World",
      },
    },
  };

  /* -------------------------------------------- */

  /**
   * @inheritDoc
   * @internal
   */
  _configureRenderOptions(options) {
    super._configureRenderOptions(options);
    if (!game.user.isGM) {
      // If we're not a GM, remove the tabs part
      options.parts.shift();
    }
  }

  /* -------------------------------------------- */

  /**
   * @inheritDoc
   * @internal
   * @param {string} partId
   * @param {ApplicationRenderContext} context
   * @returns {Promise<ApplicationRenderContext>}
   */
  async _preparePartContext(partId, context) {
    context = await super._preparePartContext(partId, context);
    if (partId !== "form") return context;

    delete context.settings;
    delete context.model;
    delete context.fields;

    Object.assign(context, {
      player: this.playerSettings,
      playerModel: this.options.playerModel,
      playerSchema: this.options.playerModel.defineSchema(),
      world: this.worldSettings,
      worldModel: this.options.worldModel,
      worldSchema: this.options.worldModel.defineSchema(),
      isGM: game.user.isGM,
    });

    return context;
  }

  /* -------------------------------------------- */

  /**
   * @inheritDoc
   * @internal
   */
  async _prepareContext(options) {
    if (!this.settings) {
      const playerSettings = game.settings.get("pf1", this.options.configKey);
      this.playerSettings = new this.options.playerModel(playerSettings.toObject());
      if (game.user.isGM) {
        const worldSettings = game.settings.get("pf1", this.options.worldConfigKey);
        this.worldSettings = new this.options.worldModel(worldSettings.toObject());
      }
      this.settings = true;
    }

    return super._prepareContext(options);
  }

  /* -------------------------------------------- */

  /**
   * The event handler that is fired when the form is reset to its original values
   *
   * @internal
   * @param {Event} event - The triggering event
   * @returns {Promise<void>}
   */
  async _onFormReset(event) {
    this.playerSettings = new this.options.playerModel();
    if (game.user.isGM) {
      this.worldSettings = new this.options.worldModel();
    }
    ui.notifications.info(game.i18n.localize(this.options.phraseKey + ".Reset"));
    this.render({ parts: ["form"] });
  }

  /* -------------------------------------------- */

  /**
   * Update the game settings with the new configuration
   *
   * @internal
   * @this {ActorRestDialog}
   * @param {SubmitEvent} event                                     The originating form submission event
   * @param {HTMLFormElement} form                                  The form element that was submitted
   * @param {FormDataExtended} formData                             Processed data for the submitted form
   * @param {Partial<ActorRestOptions>} formData.object             The form data in object form
   * @returns {Promise<void>}
   */
  static async _save(event, form, formData) {
    formData = foundry.utils.expandObject(formData.object);

    await game.settings.set("pf1", this.options.configKey, formData.player);
    if (game.user.isGM) {
      await game.settings.set("pf1", this.options.worldConfigKey, formData.world);
    }
  }
}
