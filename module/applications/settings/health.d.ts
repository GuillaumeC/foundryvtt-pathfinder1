export {};

declare module "./health.mjs" {
  interface HealthConfigModel {
    pc: {
      classes: {
        base: {
          auto: boolean;
          rate: number;
          maximized: boolean;
        };
        racial: {
          auto: boolean;
          rate: number;
          maximized: boolean;
        };
        npc: {
          auto: boolean;
          rate: number;
          maximized: boolean;
        };
      };
      rules: {
        useWoundsAndVigor: boolean;
        useWoundThresholds: 0 | 1 | 2;
      };
    };
    npc: {
      classes: {
        base: {
          auto: boolean;
          rate: number;
          maximized: boolean;
        };
        racial: {
          auto: boolean;
          rate: number;
          maximized: boolean;
        };
        npc: {
          auto: boolean;
          rate: number;
          maximized: boolean;
        };
      };
      rules: {
        useWoundsAndVigor: boolean;
        useWoundThresholds: 0 | 1 | 2;
      };
    };
    maximized: number;
    /**
     * @defaultValue `up`
     */
    rounding: string;
    continuous: boolean;
  }
}
