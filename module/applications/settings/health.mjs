import { AbstractSettingsApplication } from "@app/settings/abstract-settings.mjs";

export class HealthConfigModel extends foundry.abstract.DataModel {
  static defineSchema() {
    const fields = foundry.data.fields;

    const classType = (max = false) =>
      new fields.SchemaField({
        auto: new fields.BooleanField({ initial: false }),
        rate: new fields.NumberField({ positive: true, initial: 0.5, max: 1, step: 0.01, min: 0.01 }),
        maximized: new fields.BooleanField({ initial: max }),
      });

    const variantRules = () =>
      new fields.SchemaField({
        useWoundsAndVigor: new fields.BooleanField({ initial: false }),
        useWoundThresholds: new fields.NumberField({ initial: 0, choices: () => this.woundThresholdOptions }),
      });

    return {
      pc: new fields.SchemaField({
        classes: new fields.SchemaField({
          base: classType(true),
          racial: classType(),
          npc: classType(),
        }),
        rules: variantRules(),
      }),
      npc: new fields.SchemaField({
        classes: new fields.SchemaField({
          base: classType(true),
          racial: classType(),
          npc: classType(),
        }),
        rules: variantRules(),
      }),
      maximized: new fields.NumberField({ integer: true, min: 0, initial: 1, step: 1 }),
      rounding: new fields.StringField({
        blank: false,
        nullable: false,
        initial: "up",
        choices: () => this.healthRoundingOptions,
      }),
      continuous: new fields.BooleanField({ initial: false }),
    };
  }

  /**
   * Retrieve hit die configuration relevant to given class.
   *
   * @param {ItemClassPF} item
   * @param {string} [actorType] - Actor type. If not provided, will be retrieved from the item parent or fall back to `character`.
   * @returns {object} -
   */
  getClassHD(item, actorType) {
    actorType ||= item.actor?.type || "character";

    const config = this.getActorConfig(actorType);

    switch (item.system.subType) {
      case "npc":
        return config.classes.npc;
      case "racial":
        return config.classes.racial;
      default:
        return config.classes.base;
    }
  }

  /**
   * Get actor type specific config
   *
   * @param {Actor|string} actor - Actor document or type string
   * @returns {object}
   */
  getActorConfig(actor) {
    const type = actor?.type || actor;

    switch (type) {
      case "character":
        return this.pc;
      case "npc":
        return this.npc;
      default:
        return {
          rules: { useWoundThresholds: false, useWoundsAndVigor: false },
          get useWoundThresholds() {
            foundry.utils.logCompatibilityWarning("useWoundThresholds is in rules.useWoundThresholds", {
              since: "PF1 v11",
              until: "PF1 v12",
            });
            return false;
          },
          get useWoundsAndVigor() {
            foundry.utils.logCompatibilityWarning("useWoundsAndVigor is in rules.useWoundsAndVigor", {
              since: "PF1 v11",
              until: "PF1 v12",
            });
            return false;
          },
        };
    }
  }

  static migrateData(source) {
    if (!source) return;

    if (source.continuity) {
      source.continuous ??= source.continuity === "continuous";
      delete source.continuity;
    }

    if (source.hitdice) {
      // PCs
      source.pc ??= {};
      source.pc.classes ??= {};
      source.pc.classes.base ??= source.hitdice.PC;
      source.pc.classes.npc ??= source.hitdice.NPC;
      source.pc.classes.racial ??= source.hitdice.Racial;

      // NPCs
      source.npc ??= {};
      source.npc.classes ??= {};
      source.npc.classes.base ??= source.hitdice.PC;
      source.npc.classes.npc ??= source.hitdice.NPC;
      source.npc.classes.racial ??= source.hitdice.Racial;

      delete source.hitdice;
    }

    if (source.variants) {
      source.pc ??= {};
      source.pc.rules ??= {};
      source.pc.rules.useWoundsAndVigor ??= source.variants.pc?.useWoundsAndVigor;
      source.pc.rules.useWoundThresholds ??= source.variants.pc?.useWoundThresholds;

      source.npc ??= {};
      source.npc.rules ??= {};
      source.npc.rules.useWoundsAndVigor ??= source.variants.npc?.useWoundsAndVigor;
      source.npc.rules.useWoundThresholds ??= source.variants.npc?.useWoundThresholds;

      delete source.variants;
    }
  }

  /**
   * @deprecated
   */
  get hitdice() {
    foundry.utils.logCompatibilityWarning("HealthConfigModel.hitdice is deprecated with no direct replacement.", {
      since: "PF1 v11",
      until: "PF1 v12",
    });

    // Hand over PC settings
    return {
      PC: this.pc.classes.base,
      Racial: this.pc.classes.racial,
      NPC: this.pc.classes.npc,
    };
  }

  /**
   * @deprecated
   */
  get variants() {
    foundry.utils.logCompatibilityWarning("HealthConfigModel.variants is deprecated with no direct replacement.", {
      since: "PF1 v11",
      until: "PF1 v12",
    });

    // Collect variant rules
    return {
      pc: {
        useWoundsAndVigor: this.pc.rules.useWoundsAndVigor,
        useWoundThresholds: this.pc.rules.useWoundThresholds,
      },
      npc: {
        useWoundsAndVigor: this.npc.rules.useWoundsAndVigor,
        useWoundThresholds: this.npc.rules.useWoundThresholds,
      },
    };
  }

  static get woundThresholdOptions() {
    return {
      0: game.i18n.localize("PF1.Application.Settings.Health.WoundThresholds.Disabled"),
      1: game.i18n.localize("PF1.Application.Settings.Health.WoundThresholds.Normal"),
      2: game.i18n.localize("PF1.Application.Settings.Health.WoundThresholds.Gritty"),
    };
  }

  static get healthRoundingOptions() {
    return {
      up: "PF1.Application.Settings.Health.RoundingUp",
      nearest: "PF1.Application.Settings.Health.RoundingNearest",
      down: "PF1.Application.Settings.Health.RoundingDown",
    };
  }

  static get healthContinuityOptions() {
    return {
      true: "PF1.Application.Settings.Health.Continuous",
      false: "PF1.Application.Settings.Health.Discrete",
    };
  }

  prepareDerivedData() {
    for (const config of [this.pc, this.npc]) {
      Object.defineProperties(config, {
        useWoundsAndVigor: {
          get() {
            foundry.utils.logCompatibilityWarning("useWoundsAndVigor is in rules.useWoundsAndVigor", {
              since: "PF1 v11",
              until: "PF1 v12",
            });
            return this.rules.useWoundsAndVigor;
          },
        },
        useWoundThresholds: {
          get() {
            foundry.utils.logCompatibilityWarning("useWoundThresholds is in rules.useWoundThresholds", {
              since: "PF1 v11",
              until: "PF1 v12",
            });
            return this.rules.useWoundThresholds;
          },
        },
      });
    }
  }
}

export class HealthConfig extends AbstractSettingsApplication {
  static DEFAULT_OPTIONS = {
    configKey: "healthConfig",
    phraseKey: "PF1.Application.Settings.Health",
    model: HealthConfigModel,
    window: {
      icon: "fas fa-heartbeat",
    },
  };

  static PARTS = {
    form: {
      template: "systems/pf1/templates/settings/health.hbs",
      templates: ["systems/pf1/templates/settings/health.hbs", "templates/generic/tab-navigation.hbs"],
    },
    footer: {
      template: "templates/generic/form-footer.hbs",
    },
  };

  tabGroups = { actor: "pc" };

  tabs = {
    actor: {
      pc: {
        icon: "fas fa-heartbeat",
        label: "PF1.PCs",
      },
      npc: {
        icon: "fas fa-prescription-bottle-alt",
        label: "PF1.NPCs",
      },
    },
  };

  /* -------------------------------------------- */

  /**
   * @inheritDoc
   * @internal
   * @param {string} partId
   * @param {ApplicationRenderContext} context
   * @returns {Promise<ApplicationRenderContext>}
   */
  async _preparePartContext(partId, context) {
    context = await super._preparePartContext(partId, context);
    if (partId !== "form") return context;

    Object.assign(context, {
      showWoundsVigorWarning: {
        pc: this.settings.pc.rules.useWoundsAndVigor && this.settings.pc.rules.useWoundThresholds !== 0,
        npc: this.settings.npc.rules.useWoundsAndVigor && this.settings.npc.rules.useWoundThresholds !== 0,
      },
    });

    for (const actorType of ["pc", "npc"]) {
      for (const [hdId, hdData] of Object.entries(context.settings[actorType].classes)) {
        hdData.label = `PF1.Application.Settings.Health.Class.${hdId}`;
        hdData.fields = context.fields[actorType].fields.classes.fields[hdId].fields;
      }
    }

    context.settings.pc.label = "PC";
    context.settings.npc.label = "NPC";

    // Prepare tabs
    context.activeTab = this.tabGroups.actor;
    context.tabs = this.tabs?.actor || {};
    for (const [id, tab] of Object.entries(context.tabs)) {
      const active = this.tabGroups.actor === id;
      Object.assign(tab, {
        id,
        group: "actor",
        active,
        cssClass: active ? "active" : "",
      });
    }

    return context;
  }
}
