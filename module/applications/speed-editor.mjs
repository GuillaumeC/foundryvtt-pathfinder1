import { NoAutocomplete } from "@app/mixins/no-autocomplete.mjs";

const { DocumentSheetV2, HandlebarsApplicationMixin } = foundry.applications.api;

/**
 * An application that renders the movement speed configuration of an item
 *
 * @param {ActorPF} actor     The Actor instance for which to configure resting
 */
export class SpeedEditor extends HandlebarsApplicationMixin(NoAutocomplete(DocumentSheetV2)) {
  static DEFAULT_OPTIONS = {
    tag: "form",
    form: {
      handler: SpeedEditor._save,
      submitOnChange: false,
      submitOnClose: false,
      closeOnSubmit: true,
    },
    classes: ["pf1-v2", "speed-editor", "standard-form"],
    window: {
      minimizable: false,
      resizable: false,
    },
    position: {
      width: 400,
    },
    sheetConfig: false,
  };

  static PARTS = {
    form: {
      template: "systems/pf1/templates/apps/speed-editor.hbs",
    },
    footer: {
      template: "templates/generic/form-footer.hbs",
    },
  };

  /* -------------------------------------------- */

  /**
   * @inheritDoc
   * @internal
   * @async
   */
  async _prepareContext() {
    const itemData = this.document.system;
    const speeds = {};

    for (const key of pf1.const.movementKeys) {
      let value = itemData.speeds?.[key];
      if (value > 0) value = pf1.utils.convertDistance(value)[0];
      speeds[key] = value;
    }

    speeds.flyManeuverability = itemData.speeds.flyManeuverability || "average";

    const isMetric = pf1.utils.getDistanceSystem() === "metric";

    return {
      speeds,
      item: this.document,
      system: itemData,
      units: game.i18n.localize(isMetric ? "PF1.Distance.mShort" : "PF1.Distance.ftShort"),
      step: isMetric ? 1.5 : 5,
      flyManeuverability: {
        clumsy: "PF1.Movement.FlyManeuverability.Quality.clumsy",
        poor: "PF1.Movement.FlyManeuverability.Quality.poor",
        average: "PF1.Movement.FlyManeuverability.Quality.average",
        good: "PF1.Movement.FlyManeuverability.Quality.good",
        perfect: "PF1.Movement.FlyManeuverability.Quality.perfect",
      },
      buttons: [{ type: "submit", label: "PF1.Save", icon: "fas fa-save" }],
    };
  }

  /* -------------------------------------------- */

  /**
   * Configure the title of the speed editor window to include document name, and optionally the actors name, if present
   *
   * @override
   * @type {string}
   */
  get title() {
    const actor = this.document.actor;
    let title = `${game.i18n.localize("PF1.Movement.Label")}: ${this.document.name}`;
    if (actor) title += ` — ${actor.name}`;
    return title;
  }

  /* -------------------------------------------- */

  /**
   * Save the movement speed data back to the item
   *
   * @internal
   * @this {SpeedEditor}
   * @param {SubmitEvent} event - The originating form submission event
   * @param {HTMLFormElement} form - The form element that was submitted
   * @param {FormDataExtended} formData - Processed data for the submitted form
   * @param {Record<string, number|string>} formData.object - The movement speed configuration
   * @returns {Promise<void>}
   */
  static async _save(event, form, formData) {
    formData = formData.object;

    // Convert data back
    for (const [key, value] of Object.entries(formData)) {
      if (Number.isNumeric(value)) {
        formData[key] = pf1.utils.convertDistanceBack(value)[0];
      }
    }

    this.document.update(formData);
  }
}
