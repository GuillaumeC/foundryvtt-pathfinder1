import { MeasuredTemplatePF } from "./measure.mjs";

/**
 * A helper class for building MeasuredTemplates for PF1 spells and abilities
 */
export class AbilityTemplate extends MeasuredTemplatePF {
  /**
   * Preview movement and rotation re-render throttle time in milliseconds.
   *
   * @private
   */
  static RENDER_THROTTLE = 30;

  /**
   * A factory method to create an AbilityTemplate instance using provided data
   *
   * @param {object} data - Data used to create the template
   * @param {"cone" | "circle" | "rect" | "ray"} data.type - The type of template
   * @param {number} data.distance - The distance/size of the template
   * @param {string} [data.texture=null] - Path to template texture
   * @param {string} [data.color=game.user.color] - Template color
   * @param {object} [data.flags] - Additional flags stored on the template
   * @returns {AbilityTemplate|null} - The template object, or null if the data does not produce a template
   */
  static fromData(data) {
    const { type, distance, flags } = data;
    if (!type || !distance || !canvas.scene) return null;
    if (!["cone", "circle", "rect", "ray"].includes(type)) return null;

    // Prepare template data
    const templateData = {
      t: type,
      distance: distance || game.system.grid.distance,
      direction: 0,
      x: 0,
      y: 0,
      flags,
      fillColor: data.color ? data.color : game.user.color,
      texture: data.texture ? data.texture : null,
    };

    // Additional type-specific data
    switch (type) {
      case "cone":
        templateData.angle = game.settings.get("pf1", "measureStyle")
          ? game.canvas.grid.isHexagonal
            ? 60
            : CONFIG.MeasuredTemplate.defaults.angle
          : CONFIG.MeasuredTemplate.defaults.originalAngle;
        break;

      case "rect":
        templateData.distance = Math.sqrt(Math.pow(distance, 2) + Math.pow(distance, 2));
        templateData.direction = 45;
        break;

      case "ray":
        templateData.width = CONFIG.MeasuredTemplate.defaults.width;
        break;
      default:
        break;
    }

    // Return the template constructed from the item data
    const cls = CONFIG.MeasuredTemplate.documentClass;
    const template = new cls(templateData, { parent: canvas.scene });
    const object = new this(template);
    return object;
  }

  /* -------------------------------------------- */

  /**
   * Creates a preview of the spell template
   *
   * @override
   * @param {Event} _event - The initiating click event
   * @returns {Promise<object>} - Result
   */
  async drawPreview(_event) {
    const initialLayer = canvas.activeLayer;
    await this.draw();
    this.layer.activate();
    this.layer.preview.addChild(this);
    return this.activatePreviewListeners(initialLayer);
  }

  /* -------------------------------------------- */

  // Use system style
  pfStyle = true;

  // Event handlers
  #events;

  // Initial layer
  #initialLayer;

  /**
   * Activate listeners for the template preview
   *
   * @param {CanvasLayer} initialLayer  The initially active CanvasLayer to re-activate after the workflow is complete
   * @returns {Promise<object>} Returns result object
   */
  activatePreviewListeners(initialLayer) {
    this.#initialLayer = initialLayer;
    this.pfStyle = game.settings.get("pf1", "measureStyle") === true;

    return new Promise((resolve, reject) => {
      // Prepare events
      this.#events = {
        confirm: this._onConfirm.bind(this),
        cancel: this._onCancel.bind(this),
        move: this._onMove.bind(this),
        rotate: this._onRotate.bind(this),
        resolve,
        reject,
      };

      // Prevent interactions with control icon
      // This also allows left and right click to work correctly
      if (this.controlIcon) this.controlIcon.removeAllListeners();

      // Activate listeners
      canvas.stage.on("pointermove", this.#events.move);
      canvas.stage.on("pointerdown", this.#events.confirm);
      canvas.app.view.addEventListener("contextmenu", this.#events.cancel);
      canvas.app.view.addEventListener("wheel", this.#events.rotate);
    });
  }

  #lastMove = 0;

  // Update placement (mouse-move)
  _onMove(event) {
    event.stopPropagation();

    // Throttle
    const now = performance.now();
    if (now - this.#lastMove <= this.constructor.RENDER_THROTTLE) return;

    const snapMode =
      CONST.GRID_SNAPPING_MODES.CENTER | CONST.GRID_SNAPPING_MODES.EDGE_MIDPOINT | CONST.GRID_SNAPPING_MODES.CORNER;

    const center = event.data.getLocalPosition(this.layer);
    const pos = canvas.grid.getSnappedPoint(center, { mode: snapMode });

    // TODO: Adjust template size if placing in middle of a square (especially if on a token)

    this.document.updateSource({
      x: pos.x,
      y: pos.y,
    });

    this.refresh();

    this.#lastMove = now;
  }

  /**
   * Cancel the workflow (right-click)
   *
   * @param {Event} event
   */
  _onCancel(event) {
    console.debug("PF1 | Cancelling template placement");

    this._onFinish(event);
    this.#events.reject();
  }

  // Confirm the workflow (left-click)
  _onConfirm(event) {
    console.debug("PF1 | Placing template");

    this._onFinish(event);

    // Reject if template size is zero
    if (!this.document.distance) return this.#events.reject();

    // Create the template
    // TODO: This should create the template directly and resolve with it.
    const result = {
      result: true,
      place: async () => {
        this.document = await MeasuredTemplateDocument.create(this.document.toObject(false), { parent: canvas.scene });
        return this.document;
      },
      delete: () => {
        return this.document.delete();
      },
    };

    this.#events.resolve(result);
  }

  /**
   * Rotate the template by 3 degree increments (mouse-wheel)
   *
   * @param {Event} event
   */
  _onRotate(event) {
    event.preventDefault(); // Prevent browser zoom
    event.stopPropagation(); // Prevent other handlers

    let { distance, direction } = this.document,
      delta;

    if (event.ctrlKey) {
      delta = canvas.dimensions.distance * -Math.sign(event.deltaY);
      distance += delta;
      if (distance < 0) distance = 0;
    } else {
      let snap;
      if (this.pfStyle && this.document.t === "cone") {
        delta = game.canvas.grid.isHexagonal ? 60 : 90;
        snap = event.shiftKey ? delta : game.canvas.grid.isHexagonal ? 30 : 45;
      } else {
        delta = canvas.grid.type > CONST.GRID_TYPES.SQUARE ? 30 : 15;
        snap = event.shiftKey ? delta : 5;
      }
      if (this.document.t === "rect") {
        snap = Math.sqrt(Math.pow(5, 2) + Math.pow(5, 2));
        distance += snap * -Math.sign(event.deltaY);
      } else {
        direction += snap * Math.sign(event.deltaY);
      }
    }

    this.document.updateSource({ distance, direction });

    this.refresh();
  }

  /** @override */
  _onClickRight(event) {
    event.stopPropagation(); // Prevent right click counting as left click
  }

  /**
   * @param {Event} event
   */
  _onFinish(event) {
    // Call Foundry's preview cleanup
    this.layer._onDragLeftCancel(event);

    // Remove listeners
    canvas.stage.off("pointermove", this.#events.move);
    canvas.stage.off("pointerdown", this.#events.confirm);
    canvas.app.view.removeEventListener("contextmenu", this.#events.cancel);
    canvas.app.view.removeEventListener("wheel", this.#events.rotate);

    this.#initialLayer.activate();
  }
}
