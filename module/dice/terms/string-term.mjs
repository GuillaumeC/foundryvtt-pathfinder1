/**
 * String Term override to support booleans as numbers
 *
 * Should be obsoleted by: https://github.com/foundryvtt/foundryvtt/issues/12139
 */
export class StringTermPF extends foundry.dice.terms.StringTerm {
  constructor({ term, options } = {}) {
    if (["true", "false"].includes(term))
      return new foundry.dice.terms.NumericTerm({ number: term === "true" ? 1 : 0, options });

    super({ term, options });
  }
}
