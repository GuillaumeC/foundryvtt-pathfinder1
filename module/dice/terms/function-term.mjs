/**
 * Function Term override to support sizeRoll
 */
export class FunctionTermPF extends CONFIG.Dice.termTypes.FunctionTerm {
  /** @override */
  get expression() {
    // Evaluated sizeRoll has extra term to store the roll in
    if (this._evaluated && this.fn === "sizeRoll") {
      const terms = [...this.terms];
      terms.pop(); // Remove the result
      return `sizeRoll(${terms.join(",")})`;
    }
    return super.expression;
  }

  /**
   * Simpler formula expression if possible
   *
   * @remarks
   * - Used mainly by {@link pf1.utils.formula.simplify()}
   * - Unevaluated rolls can't always be simplified.
   * @type {string}
   */
  get simplify() {
    switch (this.fn) {
      case "sizeRoll":
        if (!this._evaluated) {
          if (this.terms.length === 2) return this.terms.join("d");
          else return this.expression;
        }
        return this.terms.at(-1);
      case "if": {
        const cond = this.rolls[0];
        if (!cond.isDeterministic) return this.expression;
        if (cond.total !== 0) return this.terms[1];
        return "0";
      }
      case "ifelse": {
        const cond = this.rolls[0];
        if (!cond.isDeterministic) return this.expression;
        if (cond.total !== 0) return this.terms[1];
        return this.terms[2];
      }
      case "lookup": {
        const search = this.rolls[0];
        if (!search.isDeterministic) return this.expression;
        const offset = search.total;
        const result = this.terms[offset + 1] ?? this.terms[1];
        return result;
      }
    }
    return this.expression;
  }

  /**
   * Flavor text
   *
   * @type {string}
   */
  get flavor() {
    return this.options.flavor || "";
  }

  /** @override */
  get isDeterministic() {
    if (this.fn === "sizeRoll") return false; // sizeRoll is never deterministic
    return super.isDeterministic;
  }

  /** @override */
  _evaluateSync(options = {}) {
    super._evaluateSync(options);
    if (this.fn === "sizeRoll") {
      const result = this.rolls.at(-1);
      result.options.flavor ||= this.flavor;
      result.propagateFlavor(this.flavor);
      if (!result._evaluated) result.evaluateSync(options);
      this.result = result.total;
    }
    return this;
  }

  /** @override */
  async _evaluateAsync(options = {}) {
    await super._evaluateAsync(options);
    if (this.fn === "sizeRoll") {
      const result = this.rolls.at(-1);
      result.options.flavor ||= this.flavor;
      result.propagateFlavor(this.flavor);
      if (!result._evaluated) await result.evaluate(options);
      this.result = result.total;
    }
    return this;
  }

  /** @override */
  toJSON() {
    const json = super.toJSON();
    json.class = super.constructor.name; // Alias to original function term to allow de-serialization to work
    return json;
  }
}
