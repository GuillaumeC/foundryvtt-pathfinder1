export class RollPF extends Roll {
  static CHAT_TEMPLATE = "systems/pf1/templates/chat/roll.hbs";

  get totalHalved() {
    foundry.utils.logCompatibilityWarning("RollPF.totalHalved is deprecated with no replacement", {
      since: "PF1 v11",
      until: "PF1 v12",
    });
    return Math.floor(this.total / 2);
  }

  /**
   * Synchronous and thrown error consuming roll evaluation.
   *
   * @remarks
   * - Returned roll has `.err` set if an error occurred during evaluation.
   * - If error occurs, the returned roll will have its formula replaced.
   * @param {string} formula - Roll formula
   * @param {object} rollData - Data supplied to roll
   * @param {object} context - If error occurs, this will be included in the error message.
   * @param {object} [options] - Additional options
   * @param {boolean} [options.suppressError=false] - If true, no error will be printed even if one occurs.
   * @param {object} [evalOpts] - Additional options to pass to Roll.evaluate()
   * @returns {Promise<RollPF>} - Evaluated roll, or placeholder if error occurred.
   */
  static async safeRoll(formula, rollData = {}, context, { suppressError = false } = {}, evalOpts = {}) {
    let roll;
    try {
      roll = await this.create(formula, rollData).evaluate({ ...evalOpts });
    } catch (err) {
      roll = this.create("0", rollData).evaluateSync({ ...evalOpts });
      roll.err = err;
    }
    if (roll.warning) roll.err = Error("This formula had a value replaced with null.");
    if (roll.err) {
      if (context && !suppressError) console.error(context, roll.err);
      else if (CONFIG.debug.roll) console.error(roll.err);
    }
    return roll;
  }

  /**
   * Synchronous version of {@link safeRoll safeRoll()}
   *
   * {@inheritDoc safeRoll}
   *
   * @param {string} formula - Formula to evaluate
   * @param {object} [rollData] - Roll data
   * @param {*} [context] - Context data to log if error occurs
   * @param {object} [options] - Additional options
   * @param {boolean} [options.suppressError] - If true, no error will be printed
   * @param {object} [evalOpts] - Options to pass to Roll.evaluate()
   * @returns {RollPF} - Evaluated roll
   */
  static safeRollSync(formula, rollData = {}, context, { suppressError = false } = {}, evalOpts = {}) {
    let roll;
    try {
      roll = new this(formula, rollData).evaluateSync({ ...evalOpts });
    } catch (err) {
      roll = new this("0", rollData).evaluateSync({ ...evalOpts });
      roll.err = err;
    }
    if (roll.warning) roll.err = Error("This formula had a value replaced with null.");
    if (roll.err) {
      if (context && !suppressError) console.error(context, roll.err);
      else if (CONFIG.debug.roll) console.error(roll.err);
    }
    return roll;
  }

  static cleanFlavor(flavor) {
    return flavor.replace(/\[\];/g, "");
  }

  static getTermTooltipData(term) {
    if (typeof term.total !== "number") return null; // Ignore terms that don't result in numbers

    const ttdata = term.getTooltipData?.() ?? {
      formula: term.expression,
      total: term.total,
      flavor: term.flavor,
      css: term.flavor ? "" : "placeholder",
    };

    return ttdata;
  }

  /**
   * Render the tooltip HTML for a RollPF instance
   *
   * @returns {Promise<string>} The rendered HTML tooltip as a string
   */
  async getTooltip() {
    const parts = this.dice.filter((d) => d.results.some((r) => r.active)).map(this.constructor.getTermTooltipData);
    const numericParts = this.terms.reduce((cur, t, idx, arr) => {
      if (t instanceof foundry.dice.terms.DiceTerm) return cur; // Ignore dice already handled above
      if (t instanceof foundry.dice.terms.FunctionTerm && t.dice.length) return cur; // Ignore function terms with dice

      const ttdata = this.constructor.getTermTooltipData(t);
      if (!ttdata) return cur;

      const prior = arr[idx - 1];
      if (
        t instanceof foundry.dice.terms.NumericTerm &&
        prior &&
        prior instanceof foundry.dice.terms.OperatorTerm &&
        prior.operator === "-"
      ) {
        ttdata.total = -ttdata.total;
      }

      cur.push(ttdata);

      return cur;
    }, []);

    // Add in flavor for base check if missing
    if (this.dice[0]?.constructor.name === "Die") {
      parts[0].flavor ||= game.i18n.localize("PF1.Rolls.Check.Label");
    }

    return renderTemplate("systems/pf1/templates/dice/tooltip.hbs", { parts, numericParts });
  }

  /**
   * @override
   *
   * Synced with Foundry v12.331
   */
  async render({ flavor, template = this.constructor.CHAT_TEMPLATE, isPrivate = false } = {}) {
    if (!this._evaluated) await this.evaluate({ allowInteractive: !isPrivate });

    const templateData = {
      formula: isPrivate ? "???" : this.formula,
      flavor: isPrivate ? null : (flavor ?? this.options.flavor),
      user: game.user.id,
      details: isPrivate ? "" : await this.getTooltip(),
      get tooltip() {
        return this.details;
      },
      total: isPrivate ? "?" : pf1.utils.limitPrecision(this.total, 2),
      hasDetails: isPrivate ? false : true,
    };

    return renderTemplate(template, templateData);
  }
}
