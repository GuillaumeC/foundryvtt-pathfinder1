import { getSkipActionPrompt } from "module/documents/settings.mjs";
import { RollPF } from "./roll.mjs";

/**
 * A class adding additional functionality to `Roll` class for d20 based Pathfinder rolls.
 */
export class D20RollPF extends RollPF {
  /** @type {D20RollOptions} */
  options;

  /**
   * Standard roll used by the system (1d20).
   *
   * @readonly
   */
  static standardRoll = "1d20";

  /**
   * @param {string} formula - The roll formula to parse
   * @param {object} [data] - The data object against which to parse attributes within the formula
   * @param {Partial<D20RollConstructorOptions>} [options] - Options
   * @param {D20RollContext} [context] - Context
   */
  constructor(formula, data, options = {}, context = {}) {
    super(formula, data, options);
    this.options = foundry.utils.mergeObject(this.constructor.defaultOptions, options);

    // Cleanup redundant options
    if (this.options.check) delete this.options.check; // Keep only if set to false
    if (!this.options.flavor) delete this.options.flavor;
    if (!Number.isFinite(this.options.staticRoll)) delete this.options.staticRoll;
    if (!this.options.bonus) delete this.options.bonus;
    if (!Number.isFinite(this.options.dc)) delete this.options.dc;

    this.context = context;

    this._applyStaticRoll();
  }

  /**
   * Apply static roll, such as Take 10/20
   *
   * @param {boolean} [force] - Ignore evaluated state of the roll and apply static roll config anyway.
   * @protected
   */
  _applyStaticRoll(force = false) {
    // Spoof only if check isn't disabled
    if (this.options.check === false) return;
    // _evaluate() has _evaluated already set true and the override is needed
    if (this._evaluated && force !== true) return;

    const d20 = this.d20;
    const result = d20.total;

    const isNumeric = d20 instanceof foundry.dice.terms.NumericTerm;
    const isDie = d20 instanceof foundry.dice.terms.Die;

    if (isNumeric) {
      this.options.staticRoll = result;
    } else if (isDie) {
      /* NOP */
    } else {
      // Conflict between numeric term and static roll
      throw new Error(`Invalid D20RollPF formula provided: ${this._formula}`);
    }

    if (!this.isStatic) return;

    const die = isDie ? d20.toJSON() : {};

    // Configure static roll
    die.number = 1;
    die.faces = 20;
    die.results = [
      {
        result: this.options.staticRoll,
        active: true,
      },
    ];
    die.method = "manual"; // Spoof resolver for consistency
    die.evaluated = true;

    // Record spoofed term
    this.terms[0] = new foundry.dice.terms.Die(die);
  }

  /**
   * Default options for D20Rolls
   *
   * @type {Partial<D20RollOptions>}
   */
  static get defaultOptions() {
    return { critical: 20 };
  }

  /**
   * The default handlebars template used to render the roll's dialog
   *
   * @type {string}
   * @readonly
   */
  static DIALOG_TEMPLATE = "systems/pf1/templates/chat/roll-dialog.hbs";

  /**
   * The default handlebars template used to render the roll's chat message
   *
   * @type {string}
   * @readonly
   */
  static CHAT_TEMPLATE = "systems/pf1/templates/chat/roll-ext.hbs";

  /**
   * Static roll results
   *
   * @enum {number}
   * @readonly
   */
  static STATIC_ROLL = {
    TEN: 10,
    TWENTY: 20,
  };

  /**
   * The D20 die this roll is based on.
   *
   * @type {Die}
   */
  get d20() {
    // this.dice[0] returns wrong number if formula had, for example, a die roll inside parenthesis.
    return this.terms[0];
  }

  /**
   * Is this a proper check?
   *
   * @type {boolean}
   */
  get isCheck() {
    return this.options.check !== false;
  }

  /**
   * Is this roll a critical success? Returns undefined if roll isn't evaluated.
   *
   * @type {boolean|void}
   */
  get isCrit() {
    if (!this._evaluated) return undefined;
    if (!Number.isFinite(this.options.critical)) return false;
    return this.d20.total >= this.options.critical;
  }

  /**
   * Is this roll a natural 20? Returns undefined if roll isn't evaluated.
   *
   * @type {boolean|void}
   */
  get isNat20() {
    if (!this._evaluated) return undefined;
    return this.d20.total === 20;
  }

  /**
   * Is this roll a natural 1? Returns undefined if roll isn't evaluated.
   *
   * @type {boolean|void}
   */
  get isNat1() {
    if (!this._evaluated) return undefined;
    return this.d20.total === 1;
  }

  /**
   * Difficulty Class
   *
   * @type {number|undefined}
   */
  get dc() {
    return this.options.dc;
  }

  /**
   * Is this roll a success against its DC?
   *
   * @remarks
   * - Returns undefined if no DC is known or if the roll is not evaluated yet.
   *
   * @type {boolean|undefined}
   */
  get isSuccess() {
    if (!this._evaluated) return undefined;
    const dc = this.dc;
    if (!Number.isFinite(dc)) return undefined;
    if (dc <= this.total) return true;
    return false;
  }

  /**
   * Is this roll a failure against its DC?
   *
   * @remarks
   * - Returns undefined if no DC is known or if the roll is not evaluated yet.
   *
   * @type {boolean|undefined}
   */
  get isFailure() {
    if (!this._evaluated) return undefined;
    const dc = this.dc;
    if (!Number.isFinite(dc)) return undefined;
    if (dc > this.total) return true;
    return false;
  }

  /**
   * DC offset
   *
   * @remarks
   * - Returns NaN if no DC is defined.
   * - Returns NaN if the roll has not been evaluated.
   *
   * @type {number}
   */
  get offset() {
    if (!this._evaluated) return NaN;
    return this.total - this.dc;
  }

  /**
   * How many 5s over the DC is this?
   *
   * Primarily meant for things like Heal check bonus healing or similar.
   *
   * @remarks
   * - Returns NaN if no DC is defined.
   *
   * @type {number}
   */
  get overBy5() {
    return Math.floor(this.offset / 5);
  }

  /**
   * Is this roll a misfire.
   *
   * @type {boolean|void}
   */
  get isMisfire() {
    if (!this._evaluated) return undefined;
    return this.natural <= (this.options.misfire ?? 0);
  }

  /**
   * Natural roll value. Undefined if the roll isn't evaluated.
   *
   * @type {number|void}
   */
  get natural() {
    if (!this._evaluated) return undefined;
    return this.d20.total;
  }

  /**
   * Is static roll (e.g. Take 20)
   *
   * @type {boolean}
   */
  get isStatic() {
    return Number.isFinite(this.options.staticRoll) || this.d20.method === "manual";
  }

  /**
   * Is not an actual roll.
   *
   * @type {boolean}
   */
  get isNonRoll() {
    return this.d20.constructor.name !== "Die" && this.options.check === false;
  }

  /**
   * Is a normal d20
   *
   * @type {boolean}
   */
  get isNormal() {
    return this.d20.formula === this.constructor.standardRoll;
  }

  /**
   * Modifier on the roll besides natural roll. Undefined if the roll isn't evaluated.
   *
   * @type {number|void}
   */
  get bonus() {
    if (!this._evaluated) return undefined;
    return this.total - this.natural;
  }

  /**
   * Return a standardized representation for the displayed formula associated with this Roll.
   * This formula includes any {@link D20RollOptions.bonus bonus} that might not be part of this roll's {@link terms}.
   *
   * @type {string}
   */
  get formula() {
    let formula = this.constructor.getFormula(this.terms);
    const bonusTerms = this.constructor.parse(`${this.options.bonus}`, this.data);
    if (this.options.bonus && !this._evaluated) formula += ` + ${this.constructor.getFormula(bonusTerms)}`;
    return formula;
  }

  /**
   * The flavor this roll was created with.
   *
   * @type {string}
   */
  get flavor() {
    return this.options.flavor;
  }

  /**
   * Render a {@link Dialog} for the user to enter additional bonuses, set a static roll result, or take 10/20.
   *
   * @param {D20RollDialogOptions} [options] - Additional options determining what options to show in the dialog
   * @returns {Promise<this | null>} A promise that resolves when the dialog is closed
   */
  async promptDialog(options = {}) {
    const { rollMode = game.settings.get("core", "rollMode"), template = this.constructor.DIALOG_TEMPLATE } = options;
    const d20 = this.isStatic ? this.options.staticRoll : this.d20.formula;
    const hasDC = Number.isFinite(options.dc);
    const editDC = game.user.isGM || !hasDC || !game.settings.get("pf1", "obscureSaveDCs");
    const templateData = {
      check: true,
      data: this.data,
      rollMode: options.rollMode || rollMode,
      rollModes: CONFIG.Dice.rollModes,
      // TODO: Move this standard roll obfuscation to dialog handling
      d20: d20 === pf1.dice.D20RollPF.standardRoll ? "" : d20, // Do not show standard roll in the input field
      bonus: this.options.bonus || options.bonus, // DEPRECATED: this.options.bonus to be removed with PF1 v12
      dc: options.dc,
      editDC,
    };

    let title = options.title || game.i18n.localize("PF1.Roll");
    // Display DC, if allowed
    if (hasDC && editDC) title += " (" + game.i18n.format("PF1.DCThreshold", { threshold: options.dc }) + ")";

    const dialogData = {
      window: { title },
      position: { width: 420 },
      classes: ["pf1-v2", "roll-prompt"],
      content: await renderTemplate(template, templateData),
      buttons: [
        {
          action: "normal",
          default: true,
          icon: "fa-solid fa-dice-d20",
          label: game.i18n.localize("PF1.Normal"),
          callback: (event, button, html) => this._onDialogSubmit(html, null),
        },
        {
          action: "take10",
          label: game.i18n.format("PF1.TakeX", { number: this.constructor.STATIC_ROLL.TEN }),
          callback: (event, button, html) => this._onDialogSubmit(html, this.constructor.STATIC_ROLL.TEN),
        },
        {
          action: "take20",
          label: game.i18n.format("PF1.TakeX", { number: this.constructor.STATIC_ROLL.TWENTY }),
          callback: (event, button, html) => this._onDialogSubmit(html, this.constructor.STATIC_ROLL.TWENTY),
        },
      ],
      close: () => null,
      rejectClose: false,
      subject: options.subject,
      speaker: options.speaker,
      roll: this,
    };

    if (options.dialogOptions) {
      foundry.utils.mergeObject(dialogData, options.dialogOptions);
    }

    return foundry.applications.api.DialogV2.wait(dialogData);
  }

  /**
   * Converts form element to object
   *
   * @protected
   * @param {HTMLFormElement} html
   * @returns {object} - Expanded form data
   */
  _getFormData(html) {
    return foundry.utils.expandObject(new FormDataExtended(html).object);
  }

  /**
   * A callback applying the user's input from the dialog to the roll and its options.
   *
   * @protected
   * @param {HTMLElement} html - The dialog's submitted HTML
   * @param {number | null} [staticRoll] - A static roll result to use instead of rolling the dice
   * @returns {D20RollPF} This roll
   */
  _onDialogSubmit(html, staticRoll) {
    const form = html.querySelector("form");
    if (!form) return this;
    const formData = this._getFormData(form);

    if (formData.bonus) this.options.bonus = formData.bonus;

    if (formData.d20) {
      const baseDice = this.constructor.parse(formData.d20, this.data);
      // If a static roll is given as d20 input, Take X button clicks are ignored
      if (baseDice[0] instanceof foundry.dice.terms.NumericTerm) this.options.staticRoll = baseDice[0].total;
      else if (baseDice[0] instanceof foundry.dice.terms.Die) {
        this.terms = [...baseDice, ...this.terms.slice(1)];
        // d20 input is actual dice, so Take X buttons are respected
        this.options.staticRoll = staticRoll;
      }
    } else {
      // No d20 input, base die is default, so Take X buttons are respected
      this.options.staticRoll = staticRoll;
    }

    if (formData.rollMode) {
      this.options.rollMode = formData.rollMode;
    }

    if (Number.isFinite(formData.dc)) this.options.dc = formData.dc;

    this._formula = this.constructor.getFormula(this.terms);

    return this;
  }

  /**
   * Transform this roll into a {@link ChatMessage} displaying the result.
   * This function can either create a ChatMessage (by default) or return the data object that would be used to create one.
   *
   * @param {object} messageData - The data object to use when creating the message
   * @param {D20RollChatOptions} options - Additional options which configure how the message is created
   * @returns {Promise<ChatMessage | object>} The created ChatMessage document, or the object of data that would be used to create one
   */
  async toMessage(messageData = {}, options = {}) {
    if (!this._evaluated) await this.evaluate();

    const chatTemplate = options.chatTemplate || this.constructor.CHAT_TEMPLATE;
    const chatTemplateData = foundry.utils.mergeObject(
      {
        formula: this.formula,
        details: await this.getTooltip(),
        roll: this,
        d20: this.d20.total, // Natural d20 roll
        total: pf1.utils.limitPrecision(this.total, 2), // Total result
        bonus: this.bonus, // Total bonus to the roll
        precision: Math.floor((this.total - Math.floor(this.total)) * 100), // Extra precision
        isCrit: this.isCrit,
        isMisfire: this.isMisfire,
        isNat20: this.isNat20,
        isNat1: this.isNat1,
        dc: this.dc,
        hasDC: Number.isFinite(this.dc),
        isSuccess: this.isSuccess,
        isFailure: this.isFailure,
        natural: this.natural,
        options: this.options,
        isStatic: this.isStatic,
        isNormal: this.isNormal,
        isNonRoll: this.isNonRoll,
        get isAbnormal() {
          if (this.isNonRoll) return false; // Technically abnormal but not override
          return this.isStatic || !this.isNormal;
        },
        get abnormalTooltip() {
          if (this.isStatic) return game.i18n.format("PF1.TakeX", { number: this.d20 });
          else if (this.isAbnormal) return "PF1.CustomRollDesc";
          else return "";
        },
        flavor: this.options.flavor,
        compendiumEntry: options.compendium?.entry,
        compendiumEntryType: options.compendium?.type,
      },
      options.chatTemplateData || {}
    );

    const rollMode = options.rollMode || this.options.rollMode || game.settings.get("core", "rollMode");

    messageData = foundry.utils.mergeObject(
      {
        type: "check",
        style: CONST.CHAT_MESSAGE_STYLES.OTHER,
        sound: options.noSound ? undefined : CONFIG.sounds.dice,
        content: await renderTemplate(chatTemplate, chatTemplateData),
        system: {
          dc: this.dc,
          reference: options.reference,
        },
      },
      messageData
    );
    messageData.rolls = [this]; // merge/expandObject would otherwise destroy the `Roll` instance

    messageData.system ??= {};
    if (options.subject) messageData.system.subject = options.subject;

    // Add combat reference if such exists
    const actor = ChatMessage.getSpeakerActor(messageData.speaker);
    if (actor && game.combat?.combatants.some((c) => c.actor === actor)) {
      foundry.utils.setProperty(messageData, "system.combat", game.combat.id);
    }

    const message = new ChatMessage.implementation(messageData);
    if (rollMode) message.applyRollMode(rollMode);
    messageData = message.toObject();

    if (options.create ?? true) {
      return ChatMessage.implementation.create(messageData, { rollMode });
    } else {
      return messageData;
    }
  }

  /** @inheritDoc */
  async _evaluate(options) {
    this._applyBonus(); // deprecated
    this._applyStaticRoll(true);
    await super._evaluate(options);
    return this;
  }

  /** @inheritDoc */
  _evaluateSync(options) {
    this._applyBonus(); // deprecated
    this._applyStaticRoll(true);
    super._evaluateSync(options);
    return this;
  }

  /**
   * Apply the bonus the roll was created with or the user entered into the dialog.
   *
   * @deprecated
   * @private
   */
  _applyBonus() {
    if (this.options.bonus) {
      foundry.utils.logCompatibilityWarning(
        "D20RollPF bonus option is deprecated in favor of including it in the base formula itself",
        {
          since: "PF1 v11",
          until: "PF1 v12",
        }
      );

      const bonusTerms = this.constructor.parse(`${this.options.bonus}`, this.data);
      if (!(bonusTerms[0] instanceof foundry.dice.terms.OperatorTerm))
        bonusTerms.unshift(new foundry.dice.terms.OperatorTerm({ operator: "+" }));
      this.terms.push(...bonusTerms);
      this._formula = this.constructor.getFormula(this.terms);
    }
  }

  /**
   * @internal
   * @param {string} bonus
   */
  addBonus(bonus) {
    const bonusTerms = this.constructor.parse(`${bonus}`, this.data);
    if (bonusTerms[0].operator !== "+") bonusTerms.unshift(new foundry.dice.terms.OperatorTerm({ operator: "+" }));
    this.terms.push(...bonusTerms);
    this._formula = this.constructor.getFormula(this.terms);
  }
}

/**
 * Performs an actor based d20 roll.
 *
 * @param {Partial<D20ActorRollOptions>} [options] - Options
 * @example Rolling a 1d20 + an actor's BAB + 2 for good behavior
 * ```js
 * const actor = game.actors.getName("Righteous Paladin");
 * await pf1.dice.d20Roll({
 *   skipDialog: true, // Roll away without a dialog
 *   flavor: "BAB", // Add a flavor/title to the roll
 *   parts: [`${actor.system.attributes.bab.total}[BAB]`], // Use the actor's BAB
 *   dice: "2d20kh", // Roll 2 d20s and keep the highest
 *   bonus: "2[Good Behavior]", // Add a static bonus of 2
 *   rollMode: "gmroll", // Make roll only visible to user and GM
 * });
 * ```
 * @returns {Promise<ChatMessage | object | undefined>} The created ChatMessage document, the object of data that would be used to create one, or undefined if cancelled
 */
export async function d20Roll(options = {}) {
  const {
    skipDialog = getSkipActionPrompt(),
    staticRoll = null,
    chatTemplateData = {},
    chatMessage = true,
    compendium,
    noSound = false,
    flavor = "",
    parts = [],
    dice = pf1.dice.D20RollPF.standardRoll,
    rollData = {},
    subject,
    bonus = "",
    speaker,
    messageData = undefined,
    dc,
    reference,
  } = options;

  let rollMode = options.rollMode;

  const formula = [dice, ...parts].join(" + ");

  const roll = new pf1.dice.D20RollPF(formula, rollData, { flavor, staticRoll, bonus, dc }, { speaker });
  if (!skipDialog) {
    const title = speaker?.alias ? `${speaker.alias}: ${flavor}` : flavor;
    const dialogResult = await roll.promptDialog({ title, rollMode, bonus, subject, speaker, dc });
    if (dialogResult === null) return;

    // Move roll mode selection from roll data
    rollMode = roll.options.rollMode;
    delete roll.options.rollMode;
  }

  // Move deprecated roll.options.bonus out
  if (roll.options?.bonus) {
    roll.addBonus(roll.options.bonus);
    delete roll.options.bonus;
  }

  if (Hooks.call("pf1PreD20Roll", roll, options) === false) return;

  return roll.toMessage(
    { ...messageData, speaker },
    { create: chatMessage, noSound, chatTemplateData, compendium, subject, dc, reference, rollMode }
  );
}
