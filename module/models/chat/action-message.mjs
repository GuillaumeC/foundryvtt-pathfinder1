import { ItemMessageModel } from "./item-message.mjs";

/**
 * Data Model for action cards.
 */
export class ActionMessageModel extends ItemMessageModel {
  static defineSchema() {
    const fields = foundry.data.fields;
    const nonEmpty = { initial: undefined, blank: false, nullable: false }; // Either undefined or something, not something but nothing

    return {
      ...super.defineSchema(),
      config: new fields.SchemaField({
        critMult: new fields.NumberField({ initial: undefined, min: 1, step: 1, integer: true }),
        nonlethal: new fields.BooleanField({ initial: false, required: false }),
        cl: new fields.NumberField({ initial: undefined, required: false, integer: true, min: 0 }),
        sl: new fields.NumberField({ initial: undefined, required: false, integer: true, min: 0 }),
      }),
      rolls: new fields.ObjectField(),
      targets: new fields.ArrayField(new fields.StringField({ ...nonEmpty })),
      action: new fields.SchemaField({
        id: new fields.StringField(), // Action ID
        name: new fields.StringField(),
        description: new fields.HTMLField(),
        dc: new fields.NumberField({ initial: undefined, integer: true, nullable: false }),
      }),
    };
  }

  static migrateData(source) {
    if (typeof source.action === "string") {
      source.action = { id: source.action };
    }

    return super.migrateData(source);
  }

  static pruneData(data) {
    super.pruneData(data);

    if (data.rolls) {
      if (!(data.rolls.attacks?.length > 0)) delete data.rolls.attacks;
      else {
        for (const atk of data.rolls.attacks) {
          if (!atk.critConfirm) delete atk.critConfirm;
          if (!atk.critDamage?.length) delete atk.critDamage;
          if (!atk.damage?.length) delete atk.damage;
        }
      }
      if (foundry.utils.isEmpty(data.rolls)) delete data.rolls;
    }

    if (!data.targets?.length) delete data.targets;

    if (data.config?.nonlethal === false) delete data.config?.nonlethal;
    if (!Number.isFinite(data.config?.sl)) delete data.config?.sl;
    if (!Number.isFinite(data.config?.cl)) delete data.config?.cl;

    if (data.action) {
      if (!data.action.id) delete data.action.id;
      if (!data.action.description) delete data.action.description;
      if (!data.action.name) delete data.action.name;
      if (!Number.isFinite(data.action.dc)) delete data.action.dc;

      if (foundry.utils.isEmpty(data.action)) delete data.action;
    }
  }
}
