/**
 * @param {ChatMessage} cm - Chat message instance
 * @param {JQuery<HTMLElement>} jq - JQuery instance
 */
export function hideRollInfo(cm, jq) {
  const whisper = cm.whisper || [];
  const isBlind = whisper.length && cm.blind;
  const isVisible = whisper.length ? whisper.includes(game.user.id) || (cm.isAuthor && !isBlind) : true;
  if (!isVisible) {
    jq.find(".dice-formula").text("???");
    jq.find(".dice-total").text("?");
    jq.find(".dice").text("");
    jq.find(".success").removeClass("success");
    jq.find(".failure").removeClass("failure");
  }
}

/**
 * @deprecated
 * @typedef {object} ChatMessagePFIdentifiedInfo
 * @property {boolean} identified - True if item was identified when rolled.
 * @property {string} name - Name of the identified item.
 * @property {string} description - Description of the identified item.
 * @property {string} [actionName] - Name of the action that was used
 * @property {string} [actionDescription] - Description of the action that was used
 */

/**
 * Generates an info block containing an item's identified info for GMs
 *
 * @remarks This HTML has to be generated in a synchronous way, as adding to a rendered chat message's content
 *          will cause erratic scrolling behaviour.
 * @param {ChatMessagePFIdentifiedInfo} info - An object containing the item's identified info
 * @returns {string} HTML string containing the info block
 */
function getIdentifiedBlock(info) {
  const hasUniqueActionName = info.action?.name && info.item.name !== info.action?.name;
  return renderTemplate("systems/pf1/templates/chat/parts/gm-description.hbs", { ...info, hasUniqueActionName });
}

export async function addExtraGMInfo(cm, html) {
  if (!game.user.isGM) return;

  if (!["item", "action"].includes(cm.type)) return;

  // Show identified info box for GM if item was unidentified when rolled
  const metadata = cm.system;
  const { identified, id: itemId } = metadata.item;

  if (identified === false && itemId) {
    const cardContent = html.querySelector(".card-content");
    if (!cardContent) return;
    cardContent.insertAdjacentHTML("beforeEnd", await getIdentifiedBlock(metadata));
  }
}

/**
 * Add GM-sensitive info for GMs and hide GM-sensitive info for players
 *
 * @param {ChatMessagePF} cm - Chat message
 * @param {HTMLElement} html - Chat message content
 */
export function hideGMSensitiveInfo(cm, html) {
  // Handle adding of GM-sensitive info. No other processing for GM
  if (game.user.isGM) return addExtraGMInfo(cm, html);

  // Hide info about unowned tokens
  for (const elem of html.querySelectorAll("[data-gm-sensitive-uuid]")) {
    // Quickly hide element
    elem.classList.add("hidden");

    // Then check for stuff
    const uuid = elem.dataset.gmSensitiveUuid;
    if (!uuid) continue;

    let obj = fromUuidSync(uuid);
    // If token or token document, get actor for testing user permissions
    // TODO: This should no longer be necessary with Foundry v11, unlinked actors give actor directly.
    if (obj instanceof Token || obj instanceof TokenDocument) obj = obj.actor;

    //  Show element again, since we have permission
    if (obj?.testUserPermission && obj.testUserPermission(game.user, "OBSERVER")) {
      elem.classList.remove("hidden");
    }
    // Remove element completely, since we don't have permission
    else {
      elem.remove();
    }
  }

  const showDCs = !game.settings.get("pf1", "obscureSaveDCs");
  if (!showDCs) for (const elem of html.querySelectorAll(".difficulty-class .threshold.sensitive")) elem.remove();

  // Message author, regardless of actor ownership, sees the rest
  if (cm.isAuthor) return;

  const actor = ChatMessage.getSpeakerActor(cm.speaker);
  // Exit if allowed to see, followup is for hiding info
  if (actor?.testUserPermission(game.user, CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER)) return;

  // Hide text enricher DCs
  for (const elem of html.querySelectorAll(".pf1-link[data-dc][data-sensitive-label]")) {
    for (const node of elem.childNodes) if (node.nodeName === "#text") node.remove();
    if (elem.children.length) elem.append(" ");
    elem.append(elem.dataset.sensitiveLabel);
    delete elem.dataset.sensitiveLabel;
  }

  // Hide info
  for (const elem of html.querySelectorAll(".gm-sensitive")) elem.remove();

  // Alter GM inner texts
  for (const elem of html.querySelectorAll("[data-gm-sensitive-inner]")) {
    if (showDCs && elem.dataset.action === "save") continue;

    elem.textContent = elem.dataset.gmSensitiveInner;
    delete elem.dataset["gm-sensitive-inner"];
  }

  if (game.settings.get("pf1", "obscureInlineRolls")) {
    // Turn rolls into raw strings
    /** @type {HTMLElement[]} */
    const inlineEls = html.querySelectorAll(".inline-roll");
    for (const elem of inlineEls) {
      if (!elem.dataset.roll) continue;

      let roll;
      try {
        roll = Roll.fromJSON(unescape(elem.dataset.roll));
      } catch (err) {
        console.error(`Inline roll in chat message ${cm.id} had invalid data`, err);
        return;
      }

      const nroll = Roll.defaultImplementation.safeRollSync(`${roll.total}`);
      elem.dataset.roll = escape(JSON.stringify(nroll));
      delete elem.dataset.tooltip;
      elem.classList.add("obfuscated");
    }

    /** @type {HTMLElement[]} */
    const results = html.querySelectorAll(".dice-roll .dice-result");
    for (const result of results) {
      // Hide natural roll and bonus, leaving only total
      /** @type {HTMLElement[]} */
      const rollEls = result.querySelectorAll(".roll-total");
      for (const elem of rollEls) {
        elem.replaceChildren(
          ...[...elem.children].filter((el) => el.classList.contains("total") || el.tagName === "I")
        );
        elem.classList.add("obfuscated");
        elem.classList.remove("has-details");
      }

      /** @type {HTMLElement[]} */
      const detailEls = result.querySelectorAll(".dice-tooltip");
      for (const elem of detailEls) {
        elem.remove();
      }
    }
  }
}

/**
 * @param {ChatMessage} cm - Chat message instance
 * @param {JQuery<HTMLElement>} jq - JQuery instance
 */
export function alterAmmoRecovery(cm, jq) {
  const recoveryData = cm.getFlag("pf1", "ammoRecovery");
  if (!recoveryData) return;

  jq.find(".chat-attack .ammo[data-ammo-id]").each((a, el) => {
    const attackIndex = el.closest(".chat-attack").dataset.index;
    const ammoId = el.dataset.ammoId;
    const data = recoveryData[attackIndex]?.[ammoId];
    if (!data) return;
    const { recovered } = data;
    $(el)
      .find(".inline-action")
      .each((i, ia) => {
        // TODO: Disable button & track proper quantities
        // TODO: Mark partial recovery
        if (recovered === undefined) return;
        else if (recovered > 0) ia.classList.add("recovered");
        else ia.classList.add("recovery-failed");
      });
  });
}

/**
 * @param {ChatMessage} cm - Chat message instance
 * @param {JQuery<HTMLElement>} jq - JQuery instance
 */
export function alterTargetDefense(cm, jq) {
  const defenseData = cm.getFlag("pf1", "targetDefense");
  if (!defenseData) return;

  jq.find(".attack-targets .saving-throws div[data-saving-throw]").each((a, el) => {
    const actorUUID = el.closest(".target").dataset.uuid;
    const save = el.dataset.savingThrow;
    const value = foundry.utils.getProperty(defenseData, `${actorUUID}.save.${save}`);
    if (value == null) return;
    $(el).find(".value").text(value.toString());
  });
}

/**
 * @param {ChatMessage} cm - Chat message instance
 * @param {HTMLElement} html - HTML element
 * @param {boolean} recursive - Is this recursive call?
 */
export function hideInvisibleTargets(cm, html, recursive = false) {
  const targetsElem = html.querySelector(".pf1.chat-card .attack-targets");
  if (!targetsElem) return; // No targets

  // Delay until canvas ready if it's not yet so.
  if (!canvas.ready) {
    if (recursive) return;
    targetsElem.classList.add("hidden");
    if (!game.settings.get("core", "noCanvas")) {
      Hooks.once("canvasReady", () => hideInvisibleTargets(cm, html, true));
    } else {
      // Canvas disabled, remove targets
      targetsElem.remove();
    }
    return;
  }

  const targetElems = targetsElem.querySelectorAll(".target");
  const targets = Array.from(targetElems).map((elem) => ({ uuid: elem.dataset.uuid, elem }));

  let hasVisible = false;
  for (const t of targets) {
    /** @type {TokenDocumentPF} */
    const token = fromUuidSync(t.uuid);
    if (!token) continue;
    t.token = token.object;

    const isVisible = t.token?.isVisible;
    const isObserver = token.actor?.testUserPermission(game.user, CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER) ?? false;

    // Hide if token invisible and user isn't observer of token
    if ((!isVisible && !isObserver) || token.isSecret) t.elem.remove();
    else hasVisible = true;
  }

  // Hide targets if there's none visible to not reveal presence of invisible targets
  if (!hasVisible) targetsElem.remove();
  else targetsElem.classList.remove("hidden");
}

const getTokenByUuid = (uuid) => fromUuidSync(uuid)?.object;

/**
 * Pan to defined token
 *
 * Provided here to allow overriding the behaviour.
 *
 * @internal
 * @param {Token} token - Token to pan to
 * @param {number} [duration=250] - Animation duration
 */
export function panToToken(token, duration = 250) {
  canvas.animatePan({ ...token.center, duration });
}

/**
 * @param {ChatMessage} cm - Chat message instance
 * @param {JQuery<HTMLElement>} jq - JQuery instance
 */
export function addTargetCallbacks(cm, jq) {
  const targetElems = jq[0].querySelectorAll(".attack-targets .target[data-uuid]");

  const _mouseEnterCallback = (event, uuid) => getTokenByUuid(uuid)?._onHoverIn(event, { hoverOutOthers: false });

  const _mouseLeaveCallback = (event, uuid) => getTokenByUuid(uuid)?._onHoverOut(event);

  const _imageClickCallback = (event, uuid) => {
    event.preventDefault();

    const token = getTokenByUuid(uuid);
    if (!token?.actor.testUserPermission(game.user, CONST.DOCUMENT_OWNERSHIP_LEVELS.OBSERVER)) return;

    const toggle = event.shiftKey;

    if (!toggle || (!token.controlled && toggle)) pf1.utils.chat.panToToken(token);

    if (token.actor.isOwner) {
      if (token.controlled) {
        if (toggle) token.release();
      } else {
        token.control({ releaseOthers: !toggle });
      }
    }
  };

  // Add callbacks
  for (let elem of targetElems) {
    const uuid = elem.dataset.uuid;
    const t = fromUuidSync(uuid);
    if (!t) continue;

    // Image element events
    const imgElem = elem.querySelector(".target-image");
    imgElem.addEventListener("pointerenter", (ev) => _mouseEnterCallback(ev, uuid), { passive: true });
    imgElem.addEventListener("pointerleave", (ev) => _mouseLeaveCallback(ev, uuid), { passive: true });
    imgElem.addEventListener("click", (ev) => _imageClickCallback(ev, uuid));

    // Misc element events
    elem = $(elem);
    elem.find(".ac").on("click", (event) => {
      event.preventDefault();

      const t = fromUuidSync(uuid);
      if (!t?.actor) return;
      pf1.utils.chat.targetACClick(cm, jq, t.actor, event);
    });

    elem.find(".saving-throws .click").on("click", (event) => {
      event.preventDefault();

      const t = fromUuidSync(uuid);
      if (!t?.actor) return;
      pf1.utils.chat.targetSavingThrowClick(cm, jq, t.actor, event);
    });
  }
}

/**
 * @param {ChatMessage}  cm - Chat message instance
 * @param {JQuery<HTMLElement>} jq - JQuery instance
 * @param {Actor} actor - Actor instance
 * @param {Event} _event - Triggering event
 */
export async function targetACClick(cm, jq, actor, _event) {
  try {
    await actor.displayDefenseCard({ rollMode: "selfroll" });
  } catch (err) {
    ui.notifications.error("PF1.Error.NoDefenseCard", { localize: true, console: false });
    throw new Error("Could not display defense card", { cause: err });
  }
}

/**
 * @param {ChatMessage}  cm - Chat message instance
 * @param {JQuery<HTMLElement>} jq - JQuery instance
 * @param {Actor} actor - Actor instance
 * @param {Event} event - Triggering event
 */
export async function targetSavingThrowClick(cm, jq, actor, event) {
  const elem = event.currentTarget;
  const save = elem.dataset.save || elem.dataset.savingThrow;

  const dc = cm.system.action?.dc;

  let message;
  try {
    message = await actor.rollSavingThrow(save, { event, dc, reference: cm.uuid });
  } catch (err) {
    ui.notifications.error("PF1.Error.NoSave", { localize: true, console: false });
    throw new Error("Failed to roll saving throw", { cause: err });
  }
  const roll = message?.rolls[0];
  if (!roll) return;

  // Replace saving throw value on original chat card's target
  // TODO: Move this data to system?
  await cm.setFlag("pf1", "targetDefense", { [actor.uuid]: { save: { [save]: roll.total } } });
}

/* Card Listeners */

/**
 * @internal
 * @param {ChatMessagePF} cm
 * @param {HTMLElement} html
 */
export function addListeners(cm, html) {
  // Header click to expand description
  html.querySelector(".item-name")?.addEventListener("click", (event) => pf1.utils.chat.onToggleDescription(cm, event));

  for (const elem of html.querySelectorAll("button[data-action], a[data-action]")) {
    elem.addEventListener("click", (event) => pf1.utils.chat.onButton(cm, event));
  }
}

/**
 * Handle toggling the visibility of chat card content when the name is clicked
 *
 * @internal
 * @param {ChatMessagePF} cm - Chat message
 * @param {Event} event - Triggering event
 */
export function onToggleDescription(cm, event) {
  event.preventDefault();

  /** @type {HTMLElement} */
  const content = event.currentTarget.closest(".message-content");
  const desc = content.querySelector(".card-content");
  cm._collapsedDescription = desc.classList.toggle("hidden");

  // Update chat popout size
  const popout = content.closest(".chat-popout");
  ui.windows[popout?.dataset.appid]?.setPosition();
}

/**
 * Handle `<a data-action>` buttons.
 *
 * @internal
 * @param {ChatMessagePF} cm
 * @param {Event} event - Button event
 */
export async function onButton(cm, event) {
  event.preventDefault();

  // Extract card data
  const button = event.currentTarget;
  const buttonActionId = button.dataset.action;

  const getLinkedItem = () => {
    const item = cm.itemSource;
    if (!item) throw new Error("Associated item not found");
    return item;
  };

  switch (buttonActionId) {
    case "applyDamage": {
      let asNonlethal;
      if (cm.system.config?.nonlethal) asNonlethal = true;

      const value = parseInt(button.dataset.value);
      if (isNaN(value)) return void console.warn("Invalid damage value:", value, { button });

      const attackIndex = parseInt(button.closest("[data-index]")?.dataset.index);
      const attackType = button.dataset.type;

      const attack = cm.systemRolls?.attacks?.[attackIndex];
      const isCritical = attackType === "critical";

      const instances = [];
      const addInstances = (damageRolls) => {
        if (!damageRolls) return;
        for (const dmg of damageRolls) {
          const d = new pf1.models.action.DamagePartModel(dmg.damageType.toObject());
          d.value = dmg.total;
          instances.push(d);
        }
      };

      if (attack) {
        addInstances(attack.damage);
        if (isCritical) addInstances(attack.critDamage);
      }

      pf1.documents.actor.ActorPF.applyDamage(value, {
        asNonlethal,
        event,
        element: button,
        message: cm,
        reference: cm.uuid,
        isCritical,
        critMult: isCritical ? (cm.system.config.critMult ?? 0) : 0,
        instances,
      });
      button.disabled = false;
      break;
    }
    // Recover ammunition
    case "recoverAmmo":
    case "forceRecoverAmmo": {
      const item = cm.itemSource;
      if (!item?.isOwner) return;

      // Check for recovery state
      const attackIndex = button.closest(".chat-attack").dataset.index;
      if (!attackIndex) throw new Error("Attack index not found");

      // Find ammo item
      const ammoId = button.dataset.ammoId || button.closest(".ammo")?.dataset.ammoId;
      const ammoItem = item.actor.items.get(ammoId);
      if (!ammoItem) return void ui.notifications.error(game.i18n.format("PF1.Error.ItemNotFound", { id: ammoId }));
      if (ammoItem.getFlag("pf1", "abundant")) return; // Abundant is unrecoverable

      const ammoQuantity = button.dataset.ammoQuantity || button.closest(".ammo")?.dataset.ammoQuantity || 1;
      if (ammoQuantity == 0) return; // Zero ammo used
      const recoveryData = cm.getFlag("pf1", "ammoRecovery");
      const ammoRecovery = recoveryData?.[attackIndex]?.[ammoId];
      // Backwards compatibility (PF1 v10) with old messages
      if (ammoRecovery?.failed === true) return;
      // Recovered is the number recovered, the rest failed
      if (ammoRecovery?.recovered !== undefined) return;

      let chance = 100; // Force recover
      if (buttonActionId === "recoverAmmo") {
        chance = ammoItem.system.recoverChance ?? 50;
      }

      // (Try to) recover ammo
      let recovered = 0;
      if (chance > 0) {
        for (let i = 0; i < ammoQuantity; i++) {
          const rngResult = Math.random() * 100;
          if (rngResult <= chance) {
            recovered += 1;
          }
        }
      }

      const promises = [];

      // Update ammo if any were recovered
      if (recovered > 0) promises.push(ammoItem.addCharges(recovered));

      // Update chat card
      promises.push(cm.setFlag("pf1", "ammoRecovery", { [attackIndex]: { [ammoId]: { recovered } } }));

      await Promise.all(promises);

      ui.notifications.info(game.i18n.format("PF1.RecoveredAmmo", { count: recovered }));

      return true;
    }
    case "save": {
      // .dataset.type is deprecated
      const saveId = button.dataset.save || button.dataset.type;
      const as = button.dataset.as;
      const dc = cm.system.action?.dc ?? (button.dataset.dc ? parseInt(button.dataset.dc) : undefined);

      /** @type {ActorPF[]} */
      let actors;
      // Defense cards want only speaker
      if (as === "speaker") actors = [ChatMessage.getSpeakerActor(cm.speaker)];
      // Otherwise use controlled or configured actor
      else {
        actors = canvas.tokens.controlled.map((t) => t.actor);
        if (actors.length == 0 && game.user.character) actors = [game.user.character];
      }
      actors = actors.filter((t) => !!t);

      if (!actors.length) return void ui.notifications.warn("PF1.Warning.NoActorSelected", { localize: true });

      let noSound = false,
        errored = false;
      for (const actor of actors) {
        actor?.rollSavingThrow(saveId, { event, noSound, dc, reference: cm.uuid }).catch((err) => {
          if (!errored) {
            ui.notifications.error("PF1.Error.NoSave", { localize: true, console: false });
            errored = true;
          }
          throw new Error("Failed to roll saving throw", { cause: err });
        });
        // TODO: Update card targets if they exist
        noSound = true;
      }
      break;
    }
    case "concentration": {
      const item = getLinkedItem();
      // TODO: Apply bonuses on card if any
      item.actor.rollConcentration(item.system.spellbook, {
        item,
        reference: cm.uuid,
        dc: item.getConcentrationDC("defensive"),
      });
      break;
    }
    case "caster-level-check": {
      const item = getLinkedItem();
      // TODO: Apply bonuses on card (e.g. CL increase in dialog, conditional modifiers, etc.)
      item.actor.rollCL(item.system.spellbook, { item, reference: cm.uuid });
      break;
    }
    // Show journal entry
    case "journal":
    case "open-compendium-entry": {
      // .dataset.compendiumEntry is deprecated
      const uuid = button.dataset.uuid || button.dataset.compendiumEntry;
      // .dataset.type is defined sometimes, but we only use journals
      pf1.utils.openJournal(uuid);
      break;
    }
  }
}
