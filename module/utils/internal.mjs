/**
 * Adjusts a string to a number, allowing relative adjustments.
 *
 * @internal
 * @param {number} initialValue - The initial number to use for relative operations.
 * @param {string} cmdStr - The exact string inputted by the user.
 * @param {number} [maxValue=null] - The maximum allowed value for this variable.
 * @param {number} [clearValue=null] - What to change the variable to if the user simply erased the value.
 * @returns {number} The resulting new value.
 */
export const adjustNumberByStringCommand = (initialValue, cmdStr, maxValue = null, clearValue = null) => {
  let result = initialValue;
  const re = cmdStr.match(/(?<abs>=)?(?<op>[+-]+)?(?<value>\d+)/);
  if (re) {
    const { op: operator, abs, value: rawValue } = re.groups;
    const isAbsolute = abs == "=" || ["--", "++"].includes(operator) || (!abs && !operator);
    const isNegative = ["-", "--"].includes(operator);
    let value = parseInt(rawValue);
    if (isNegative) value = -value;
    result = isAbsolute ? value : initialValue + value;
  } else if (cmdStr === "" && clearValue !== null) {
    result = clearValue;
  } else {
    result = parseFloat(cmdStr || "0");
  }

  if (Number.isFinite(maxValue)) result = Math.min(result, maxValue);

  if (Number.isNaN(result)) {
    console.warn("Input resulted in NaN", { initial: initialValue, command: cmdStr });
    result = initialValue;
  }

  return result;
};

/**
 * Determine if buff target or buff target category is valid for the defined actor and item.
 *
 * @internal
 * @param {object} data - Buff target or category data
 * @param {object} options - Additional options
 * @param {Actor} [options.actor] - Actor to test
 * @param {Item} [options.item] - Item to test
 * @returns {{actor:boolean,item:boolean,valid:boolean}} - Validity statement
 */
export function isValidChangeTarget(data, { actor, item } = {}) {
  const { filters } = data;
  if (!filters) return { actor: true, item: true, valid: true };

  let ar = true;
  if (filters.actor && actor) {
    const { include, exclude, fn } = filters.actor;
    if (exclude && exclude.includes(actor.type)) ar = false;
    else if (include && !include.includes(actor.type)) ar = false;
    else if (typeof fn === "function") ar = fn(data, { actor, item });
  }

  let ir = true;
  if (filters.item && item) {
    const { include, exclude, fn } = filters.item;
    if (exclude && exclude.includes(item.type)) ir = false;
    else if (include && !include.includes(item.type)) ir = false;
    else if (typeof fn === "function") ir = fn(data, { actor, item });
  }

  return {
    actor: actor ? ar : undefined,
    item: item ? ir : undefined,
    valid: ar && ir,
  };
}

/**
 * @typedef {object} BuffTargetItem
 * @property {string} [label] - The buff target's label.
 * @property {string} category - The buff target's category.
 * @property {string} [icon] - The URL to an icon.
 */
/**
 * Assembles an array of all possible buff targets.
 *
 * @internal
 * @param {"buffs"|"contextNotes"} type - Type of targets to fetch
 * @param {object} context - Additional context
 * @param {Actor} [context.actor] - Actor for which to specifically get buff targets.
 * @param {Item} [context.item] - Item on which this change is on.
 * @returns {Record<string, BuffTargetItem>} The resulting array of buff targets.
 */
export function getBuffTargets(type, { actor, item } = {}) {
  const buffTargets = foundry.utils.deepClone(
    {
      buffs: pf1.config.buffTargets,
      contextNotes: pf1.config.contextNoteTargets,
    }[type]
  );

  // Append individual skills to buff targets
  const allowSkills = isValidChangeTarget(pf1.config.buffTargetCategories.skills, { actor, item }).valid;

  if (actor) {
    const skillTargets = actor._skillTargets ?? [];
    for (const s of skillTargets) {
      const skillId = s.split(".").slice(1).join(".");
      if (skillId.startsWith("~")) continue; // Skip secondary targets that affect only the parent skill
      const skill = actor.getSkillInfo(skillId);
      buffTargets[s] = { label: skill.fullName, category: "skill", valid: allowSkills };
    }
  } else {
    for (const [key, label] of Object.entries(pf1.config.skills)) {
      buffTargets[`skill.${key}`] = { label, category: "skill", valid: allowSkills };
    }
  }

  // Append spell targets
  const allowSpells = isValidChangeTarget(pf1.config.buffTargetCategories.spell, { actor, item }).valid;

  const books = actor?.system.attributes?.spells?.spellbooks ?? {
    primary: { label: game.i18n.localize("PF1.SpellBookPrimary") },
    secondary: { label: game.i18n.localize("PF1.SpellBookSecondary") },
    tertiary: { label: game.i18n.localize("PF1.SpellBookTertiary") },
    spelllike: { label: game.i18n.localize("PF1.SpellBookSpelllike") },
  };

  // Get actor specific spell targets
  const spellTargets = actor?._spellbookTargets ?? [];

  // Add spell school DCs and CLs
  for (const schoolId of Object.keys(pf1.config.spellSchools)) {
    spellTargets.push(`dc.school.${schoolId}`, `cl.school.${schoolId}`);
  }

  for (const s of spellTargets) {
    const re = /^(?<key>\w+)(?:\.(?<category>\w+))?\.(?<subKey>\w+)$/.exec(s);
    if (!re) continue;
    const { key, category, subKey } = re.groups;

    let subLabel;
    if (category === "school") subLabel = pf1.config.spellSchools[subKey];
    else subLabel = books[subKey]?.label || subKey;

    const fullKey = category ? `${key}.${category}` : key;
    const mainLabel = game.i18n.localize(
      {
        "dc.school": "PF1.DC",
        concn: "PF1.Concentration",
        "cl.book": "PF1.CasterLevel",
        "cl.school": "PF1.CasterLevelAbbr",
      }[fullKey]
    );

    buffTargets[s] = {
      label: `${mainLabel} (${subLabel})`,
      category: "spell",
      valid: allowSpells,
    };
  }

  return buffTargets;
}

/**
 * @typedef {object} BuffTargetCategory
 * @property {string} label - The category's label.
 */
/**
 * Assembles an array of buff targets and their categories, ready to be inserted into a Widget_CategorizedItemPicker.
 *
 * @internal
 * @param {"buffs"|"contextNotes"} type - Type of targets to retrieve
 * @param {object} context - Additional context
 * @param {Actor} [context.actor] - Actor for which to specifically get buff targets.
 * @param {Item} [context.item] - Item on which this change is on.
 * @returns {Widget_CategorizedItemPicker~Category[]}
 */
export function getBuffTargetDictionary(type = "buffs", { actor, item } = {}) {
  const buffTargets = getBuffTargets(type, { actor, item });

  // Assemble initial categories and items
  const targetCategories = foundry.utils.deepClone(
    {
      buffs: pf1.config.buffTargetCategories,
      contextNotes: pf1.config.contextNoteCategories,
    }[type]
  );

  const categories = Object.values(
    Object.entries(buffTargets).reduce((cur, [key, { label, category, icon, ...options }]) => {
      if (!key.startsWith("~")) {
        cur[category] ??= {
          key: category,
          label: targetCategories[category].label,
          items: [],
          validity: isValidChangeTarget(targetCategories[category], { actor, item }),
        };

        cur[category].items.push({
          key,
          label,
          icon,
          validity: isValidChangeTarget({ key, label, category, icon, ...options }, { actor, item }),
        });
      }
      return cur;
    }, {})
  );

  pf1.utils.naturalSort(categories, "label");

  // Return result
  return categories;
}

/**
 * A locale-safe insertion sort of an Array of Objects, not in place. Ignores punctuation and capitalization.
 * `name` properties in objects will be lowercased.
 *
 * @internal
 * @template T
 * @param {Array.<T & {name: string}>} inputArr - Array to be sorted. Each element must have a name property set
 * @returns {T[]} - New sorted Array
 */
export const sortArrayByName = (inputArr) => {
  inputArr = foundry.utils.deepClone(inputArr);
  for (const elem of inputArr) {
    elem.name = elem.name.toLocaleLowerCase();
  }
  return pf1.utils.naturalSort(inputArr, "name", { numeric: true, ignorePunctuation: true });
};

/**
 * Variant of TextEditor._createInlineRoll for creating unrolled inline rolls.
 *
 * Synchronized with Foundry VTT v12.331
 *
 * {@inheritDoc TextEditor._createInlineRoll}
 *
 * @internal
 * @experimental
 * @param {string[]} match - RegExp match
 * @param {object} rollData - Roll data
 * @param {object} _options - Parser options
 */
export function createInlineFormula(match, rollData, _options) {
  let [command, formula, closing, label] = match.slice(1, 5);

  if (command) return TextEditor._createInlineRoll(match, rollData);

  // Handle the possibility of the roll formula ending with a closing bracket
  if (closing.length === 3) formula += "]";

  const rollCls = Roll.defaultImplementation;

  // Extract components of the matched command
  const pFormula = rollCls.replaceFormulaData(formula.trim() || "0", rollData || {});

  let pLabel;
  if (!label) {
    /** @type {RollPF} */
    const roll = new rollCls(pFormula);
    if (roll.isDeterministic) {
      roll.evaluateSync();
      pLabel = roll.total;
    } else {
      pLabel = pf1.utils.formula.simplify(pFormula);
    }
  }

  // Construct the roll element
  const a = document.createElement("a");
  a.classList.add("inline-preroll", "inline-formula");
  a.dataset.formula = formula;
  a.dataset.tooltip = pFormula;
  a.innerHTML = `<i class="fas fa-dice-d20"></i>${label || pLabel || pFormula}`;

  return a;
}

/**
 * Recursively transforms an ES module to a regular, writable object.
 *
 * @internal
 * @template T
 * @param {T} module - The ES module to transform.
 * @returns {T} The transformed module.
 */
export function moduleToObject(module) {
  const result = {};
  for (const key in module) {
    if (Object.prototype.toString.call(module[key]) === "[object Module]") {
      result[key] = moduleToObject(module[key]);
    } else {
      result[key] = module[key];
    }
  }
  return result;
}

/**
 * Set default scene scaling and default template scaling in scenes.
 *
 * `imperial` sets scaling to 5 ft, `metric` sets scaling to 1.5 m
 *
 * @internal
 * @param {UnitSystem | undefined} [system] System of units. Pull current setting if undefined.
 */
export function setDefaultSceneScaling(system) {
  system ??= pf1.utils.getDistanceSystem();

  let scaling, units;
  if (system == "metric") {
    units = "m";
    scaling = 1.5;
  } else {
    units = "ft";
    scaling = 5;
  }

  game.system.grid.units = units;
  game.system.grid.distance = scaling;
  CONFIG.MeasuredTemplate.defaults.width = scaling;
}

/**
 * Turns dictionaries with numbered keys into arrays.
 *
 * @internal
 * @param {object} sourceObj The source object which contains the full array in the same path as targetObj.
 * @param {object} targetObj The target object to alter. The array doesn't have to be immediately in this object.
 * @param {string} keepPath A path to the array to keep, separated with dots. e.g. "system.damageParts".
 */
export function keepUpdateArray(sourceObj, targetObj, keepPath) {
  const newValue = foundry.utils.getProperty(targetObj, keepPath);
  if (newValue == null) return;
  if (Array.isArray(newValue)) return;

  const newArray = foundry.utils.deepClone(foundry.utils.getProperty(sourceObj, keepPath) || []);

  for (const [key, value] of Object.entries(newValue)) {
    if (foundry.utils.getType(value) === "Object") {
      const subData = foundry.utils.expandObject(value);
      newArray[key] = foundry.utils.mergeObject(newArray[key], subData);
    } else {
      newArray[key] = value;
    }
  }

  foundry.utils.setProperty(targetObj, keepPath, newArray);
}

/**
 * Resolve UUID by fastest means possible
 *
 * This is partial implementation to sidestep fromUuidSync() not supporting items embedded in compendium actors that you have document for.
 *
 * @remarks
 * - Does not support retrieval of other than Item documents
 * - May or may not return an actual document.
 *
 * @internal
 * @param {string} uuid - UUID
 * @param {Actor} [parent] - Parent actor
 * @returns {object|Item}
 */
export function fromUuid(uuid, parent) {
  if (!parent?.pack) return fromUuidSync(uuid, { relative: parent });
  const result = foundry.utils.parseUuid(uuid, { relative: parent });
  if (result.embedded.length) {
    const [type, itemId] = result.embedded;
    if (type === "Item") return parent.items.get(itemId);
    throw new Error(`Unsupported embedded document type "${type}" for UUID: ${uuid}`);
  }
  return fromUuidSync(uuid, { relative: parent });
}

/**
 * Transform UUID into its long form
 *
 * Converts relative UUIDs to full and old format UUIDs to current format.
 *
 * @param {string} uuid - UUID
 * @param {Actor|Item} [parent] - Parent document
 * @returns {string} - Long for UUID
 */
export function uniformUuid(uuid, parent) {
  return foundry.utils.parseUuid(uuid, { relative: parent }).uuid;
}

/**
 * Return map of template ID to label.
 *
 * @internal
 * @returns {Record<string,string>}
 */
export function getTemplateTypes() {
  if (game.release.generation >= 13)
    return Object.fromEntries(
      Object.values(CONST.MEASURED_TEMPLATE_TYPES).map((id) => [id, game.i18n.localize(`TEMPLATE.TYPES.${id}`)])
    );
  else return CONFIG.MeasuredTemplate.types;
}
