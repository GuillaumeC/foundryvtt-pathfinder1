/**
 * Active Effect extension for system properties.
 */
export class ActiveEffectPF extends ActiveEffect {
  /**
   * @internal
   * @override
   * @param {object} data - Creation data
   * @param {object} context - Creation context
   * @param {User} user - Triggering user
   */
  async _preCreate(data, context, user) {
    await super._preCreate(data, context, user);

    const actor = this.actor;
    if (!actor) return;

    // Record current initiative
    // But only if the current actor is in combat
    const combat = actor.getCombatants()[0]?.combat;
    if (combat) {
      // Set flag only if it doesn't exist in the data already
      const init = this.system.initiative ?? data.system?.initiative;
      if (init === undefined) {
        this.updateSource({ "system.initiative": combat.initiative });
      }
    }
  }

  /**
   * Parent actor
   *
   * @remarks
   * - Null if no parent exists.
   *
   * @type {Actor|null}
   */
  get actor() {
    const parent = this.parent;
    if (parent instanceof Actor) return parent;
    else return parent?.actor || null;
  }

  /**
   * @remarks BUG: Foundry v11 and older controls visibility through this.
   * @override
   * @type {boolean}
   */
  get isTemporary() {
    // Allow overlays to always show, no matter what else
    if (this.statuses.size && this.getFlag("core", "overlay")) return true;

    // Hide everything told to hide
    if (this.getFlag("pf1", "show") === false) return false;

    // Hide buffs if buff hiding option is enabled
    const isTracker = this.isTracker;
    if (isTracker) {
      // Hide based on parent item hide toggle
      if (this.parent?.system?.hideFromToken) return false;
      // Hide based on global setting
      if (game.settings.get("pf1", "hideTokenConditions")) return false;
    }

    return isTracker || super.isTemporary;
  }

  /**
   * Temporary solution until Foundry v12 to deal with problems of isTemporary.
   *
   * @internal
   * @returns {boolean} - Has duration
   */
  get _hasDuration() {
    const duration = this.duration.seconds ?? (this.duration.rounds || this.duration.turns);
    // Allow zero for single-turn duration effects to register correctly.
    return Number.isFinite(duration) && duration >= 0;
  }

  /**
   * Initiative counter
   *
   * Present only if the AE was started during combat
   *
   * @type {number|undefined}
   */
  get initiative() {
    return this.system.initiative;
  }

  /**
   * Tracking a buff
   *
   * @remarks
   * - There should be only one tracker per item.
   *
   * @type {boolean}
   */
  get isTracker() {
    return this.system.isTracker ?? false;
  }

  /** @override */
  get isSuppressed() {
    if (this.parent instanceof Item) return !this.parent.isActive;
    return false;
  }
}
