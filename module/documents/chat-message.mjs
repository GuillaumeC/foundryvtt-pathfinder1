/**
 * Chat Message document extension
 */
export class ChatMessagePF extends ChatMessage {
  /** @override */
  async _preCreate(data, context, user) {
    await super._preCreate(data, context, user);

    const flags = data.flags?.pf1;
    if (!flags) return;

    // Print warnings, leave changes to migration
    if (flags.metadata) {
      foundry.utils.logCompatibilityWarning(
        "message.flags.pf1.metadata has been deprecated in favor of message.system",
        {
          since: "PF1 v11",
          until: "PF1 v12",
        }
      );
      // Migration required
    }

    if (flags.identifiedInfo) {
      foundry.utils.logCompatibilityWarning(
        "message.flags.pf1.identifiedInfo has been deprecated in favor of message.system",
        {
          since: "PF1 v11",
          until: "PF1 v12",
        }
      );
      // Migration required
    }

    if (flags.subject) {
      foundry.utils.logCompatibilityWarning(
        "message.flags.pf1.subject has been deprecated in favor of message.system.subject",
        {
          since: "PF1 v11",
          until: "PF1 v12",
        }
      );
      // Move data to new location
      this.updateSource({
        flags: { pf1: { "-=subject": null } },
        system: { subject: flags.subject },
      });
    }
  }

  /**
   * Replaces all roll data object in a given object with {@link Roll} instances
   *
   * @param {object} maybeRollObject - The object to replace roll data objects with {@link Roll} instances
   * @returns {object} The object with all roll data objects replaced with {@link Roll} instances
   * @private
   */
  static _initRollObject(maybeRollObject) {
    // If object is an array, map to roll objects
    if (Array.isArray(maybeRollObject)) {
      return maybeRollObject.map((o) => this._initRollObject(o));
    }

    // If this is a roll object, initialize it
    if (maybeRollObject != null && typeof maybeRollObject === "object" && "class" in maybeRollObject) {
      return Roll.fromData(maybeRollObject);
    }

    // If object is a regular object, recurse into it to find roll to initialize
    if (typeof maybeRollObject === "object" && maybeRollObject != null) {
      for (const [k, v] of Object.entries(maybeRollObject)) {
        maybeRollObject[k] = this._initRollObject(v);
      }
    }
    // Return object in which all roll data has been replaced by Roll instances
    return maybeRollObject;
  }

  /**
   * Linked action.
   *
   * @remarks
   * - Null is returned if no action is linked.
   * - Undefined is returned if the linked action is not found.
   *
   * @type {ItemAction|undefined|null}
   */
  get actionSource() {
    const id = this.system.action;
    return id ? this.itemSource?.actions.get(id) : null;
  }

  /**
   * Linked item.
   *
   * @remarks
   * - Null is returned if no item is linked
   * - Undefined if item is not found
   *
   * @type {ItemPF|undefined|null}
   */
  get itemSource() {
    const itemId = this.system.item?.id;
    if (itemId) {
      // fromUuidSync(this.system.actor)
      const actor = this.constructor.getSpeakerActor(this.speaker);
      return actor?.items.get(itemId);
    }
    return null;
  }

  /**
   * Has linked item.
   *
   * @deprecated
   * @type {boolean}
   */
  get hasItemSource() {
    foundry.utils.logCompatibilityWarning(
      "ChatMessagePF.hasItemSource has been deprecated in favor of ChatMessagePF.itemSource",
      {
        since: "PF1 v11",
        until: "PF1 v12",
      }
    );
    return this.system.item !== undefined;
  }

  /**
   * Associated measured template
   *
   * @type {MeasuredTemplatePF|null}
   */
  get measureTemplate() {
    const templateId = this.system.template;
    if (!templateId) return null;

    return fromUuidSync(templateId) ?? canvas.templates.get(templateId) ?? null;
  }

  /**
   * Targeted tokens.
   *
   * @type {Array<TokenPF>}
   */
  get targets() {
    const targetIds = this.system.targets ?? [];
    if (targetIds.length === 0) return targetIds;

    // Legacy IDs from old messages
    if (/^\w{16}$/.test(targetIds[0])) return canvas.tokens.placeables.filter((o) => targetIds.includes(o.id));

    return targetIds.map((uuid) => fromUuidSync(uuid)?.object).filter((t) => !!t);
  }

  /** @inheritDoc */
  prepareDerivedData() {
    super.prepareDerivedData();

    /**
     * An object containing Pathfinder specific rolls for this chat message,
     * with a structure grouping them by their purpose.
     *
     * @type {object}
     */
    this.systemRolls = this.constructor._initRollObject(this.system.rolls ?? {});

    // Compatibility shims
    this.flags.pf1 ??= {};

    const msg = this;

    if (!("subject" in this.flags.pf1)) {
      Object.defineProperty(this.flags.pf1, "subject", {
        get() {
          foundry.utils.logCompatibilityWarning(
            "ChatMessagePF.flags.pf1.subject has been deprecated in favor of ChatMessagePF.system.subject",
            {
              since: "PF1 v11",
              until: "PF1 v12",
            }
          );

          return msg.system?.subject;
        },
      });
    }

    if (!("metadata" in this.flags.pf1)) {
      Object.defineProperty(this.flags.pf1, "metadata", {
        get() {
          foundry.utils.logCompatibilityWarning(
            "ChatMessagePF.flags.pf1.metadata has been deprecated in favor of ChatMessagePF.system",
            {
              since: "PF1 v11",
              until: "PF1 v12",
            }
          );

          return msg.system;
        },
      });
    }

    if (!["item", "action"].includes(this.type)) return;

    if (!("identifiedInfo" in this.flags.pf1)) {
      Object.defineProperty(this.flags.pf1, "identifiedInfo", {
        get() {
          foundry.utils.logCompatibilityWarning(
            "ChatMessagePF.flags.pf1.identifiedInfo has been deprecated in favor of ChatMessagePF.system",
            {
              since: "PF1 v11",
              until: "PF1 v12",
            }
          );

          return {
            identified: msg.system.item?.identified ?? true,
            name: msg.system.item?.name,
            description: msg.system.item?.description,
            actionName: msg.system.action?.name,
            actionDescription: msg.system.action?.description,
          };
        },
      });
    }
  }

  /**
   * Chat message frame context data.
   *
   * @override
   * @param {object} context
   *
   * Synced with Foundry v12.331
   */
  _renderRollContent(context) {
    switch (this.type) {
      case "check":
        context.cssClass += "basic-check pf1";
        break;
      case "item":
        context.cssClass += "item-card pf1";
        break;
      case "action":
        context.cssClass += "action-card pf1";
        break;
      default:
        if (this.isRoll) context.cssClass += "basic-roll pf1";
        break;
    }
    return super._renderRollContent(context);
  }

  /**
   * @override
   * @param {boolean} source - Return source data
   * @param {boolean} clean - Clean the source data of excess data
   * @returns {object} - Raw data
   */
  toObject(source = true, clean = true) {
    const data = super.toObject(source);

    if (clean && data.system) this.system.constructor.pruneData?.(data.system);

    return data;
  }
}
