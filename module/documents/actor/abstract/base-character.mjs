import { ActorPF } from "@actor/actor-pf.mjs";
import { getSourceInfo, getChangeFlat } from "@actor/utils/apply-changes.mjs";
import { RollPF } from "@dice/roll.mjs";

/**
 * Shared functionality for {@link pf1.documents.actor.ActorCharacterPF | PCs} and {@link pf1.documents.actor.ActorNPCPF | NPCs}.
 *
 * @todo Move PC/NPC functionality from ActorPF here.
 *
 * @abstract
 */
export class BaseCharacterPF extends ActorPF {
  /** @inheritDoc */
  _detectHealthChange(changed, context) {
    super._detectHealthChange(changed, context);

    const deltas = context.pf1?.deltas ?? {};

    const hpData = changed.system?.attributes;

    if (hpData) {
      const oldHpData = this.system.attributes;

      for (const type of ["hp", "wounds", "vigor"]) {
        const hp = hpData[type];
        if (!hp) continue;
        for (const key of ["offset", "temp", "nonlethal"]) {
          if (hp[key] === undefined) continue;
          const diff = hp[key] - (oldHpData?.[type]?.[key] || 0);
          if (diff && Number.isFinite(diff)) {
            deltas[type] ??= {};
            deltas[type][key] = diff;
          }
        }
      }
    }

    const abilities = changed.system.abilities;
    if (abilities) {
      const oldAblData = this.system.abilities;
      for (const [ablKey, ablData] of Object.entries(abilities)) {
        for (const key of ["damage", "drain", "userPenalty"]) {
          if (ablData[key] === undefined) continue;
          const diff = ablData[key] - (oldAblData?.[ablKey]?.[key] ?? 0);
          if (diff && Number.isFinite(diff)) {
            deltas[ablKey] ??= {};
            deltas[ablKey][key] = diff;
          }
        }
      }
    }

    if (changed.system.attributes?.energyDrain !== undefined) {
      const diff = changed.system.attributes?.energyDrain - this.system.attributes.energyDrain;
      if (diff && Number.isFinite(diff)) {
        deltas.energyDrain = diff;
      }
    }

    if (Object.keys(deltas).length > 0) {
      context.pf1 ??= {};
      context.pf1.deltas = deltas;
    }
  }

  /** @override */
  _onUpdate(changed, context, userId) {
    super._onUpdate(changed, context, userId);

    this._onDeltaUpdates(context.pf1?.deltas);
  }

  /**
   * Handle Deltas in update context
   *
   * Produces scrolling text for health changes.
   *
   * @param {object} deltas
   */
  async _onDeltaUpdates(deltas) {
    if (!deltas) return;

    // TODO: Maybe allow if the token has visible HP bar regardless?
    if (!game.user.isGM && !this.testUserPermission(game.user, game.settings.get("pf1", "healthPermission"))) return;

    // TODO: Support multiple tokens?
    let token = this.token ?? this.getActiveTokens(true, true)[0];

    if (!game.user.isGM && !token.object.isVisible) token = null;

    if (!token) return;

    const deltaLabels = {
      hp: {
        offset: {
          label: "PF1.ActiveFeedback.Deltas.Health",
          color: pf1.config.feedback.deltas.hp,
        },
        temp: {
          label: "PF1.ActiveFeedback.Deltas.TempHP",
          color: pf1.config.feedback.deltas.temp,
        },
        nonlethal: {
          label: "PF1.ActiveFeedback.Deltas.Nonlethal",
          color: pf1.config.feedback.deltas.nonlethal,
        },
      },
      wounds: {
        offset: {
          label: "PF1.ActiveFeedback.Deltas.Wounds",
          color: pf1.config.feedback.deltas.wounds,
        },
      },
      vigor: {
        offset: {
          label: "PF1.ActiveFeedback.Deltas.Vigor",
          color: pf1.config.feedback.deltas.vigor,
        },
        temp: {
          label: "PF1.ActiveFeedback.Deltas.TempVigor",
          color: pf1.config.feedback.deltas.temp,
        },
      },
      energyDrain: {
        label: "PF1.ActiveFeedback.Deltas.NegativeLevel",
        color: pf1.config.feedback.deltas.energyDrain,
      },
      ability: {
        damage: {
          label: "PF1.ActiveFeedback.Deltas.AbilityDamage",
          color: pf1.config.feedback.deltas.abilityScore,
        },
        drain: {
          label: "PF1.ActiveFeedback.Deltas.AbilityDrain",
          color: pf1.config.feedback.deltas.abilityScore,
        },
        userPenalty: {
          label: "PF1.ActiveFeedback.Deltas.AbilityPenalty",
          color: pf1.config.feedback.deltas.abilityScore,
        },
      },
    };

    const formatter = new Intl.NumberFormat(undefined, { signDisplay: "always" }).format;

    for (const key of Object.keys(deltas)) {
      const labels = [];
      switch (key) {
        case "hp":
        case "wounds":
        case "vigor":
          for (const [hpKey, value] of Object.entries(deltas[key])) {
            const conf = deltaLabels[key][hpKey];
            if (conf) {
              const positive = value > 0;
              const color = positive ? conf.color.positive : conf.color.negative;
              labels.push({
                label: game.i18n.format(conf.label, { value: formatter(value) }),
                positive,
                color,
                value,
                key: `${key}.${hpKey}`,
              });
            } else console.debug("Unrecognized delta:", key, hpKey, value);
          }
          break;
        case "energyDrain": {
          const diff = deltas[key];
          const conf = deltaLabels.energyDrain;
          const positive = diff < 0;
          const color = positive ? conf.color.positive : conf.color.negative;
          labels.push({
            label: game.i18n.format(conf.label, { value: formatter(diff) }),
            positive,
            color,
            value: diff,
            key,
          });
          break;
        }
        default:
          if (key in pf1.config.abilities) {
            for (const [dmgKey, diff] of Object.entries(deltas[key])) {
              const ablName = pf1.config.abilities[key];
              const conf = deltaLabels.ability[dmgKey];
              if (ablName && conf) {
                const positive = diff < 0;
                const color = positive ? conf.color.positive : conf.color.negative;
                labels.push({
                  label: game.i18n.format(conf.label, { value: formatter(diff), ability: ablName }),
                  positive,
                  color,
                  value: diff,
                  key: `${key}.${dmgKey}`,
                });
              } else {
                console.debug("Unrecognized ability score delta:", key, dmgKey, deltas[key]?.[dmgKey]);
              }
            }
          } else {
            console.warn("Unrecognized delta:", key, deltas[key]);
          }
          break;
      }

      // Print the actual message.
      let t0 = 0;
      for (const options of labels) {
        const { positive, color } = options;

        // Roughly scale based on canvas resolution with some lower end clamping
        // TODO: Adjust size based on how "large" the change was
        const fontSize = Math.max(canvas.grid.size / 3, 24);

        const textData = {
          anchor: CONST.TEXT_ANCHOR_POINTS.CENTER,
          direction: positive ? CONST.TEXT_ANCHOR_POINTS.TOP : CONST.TEXT_ANCHOR_POINTS.BOTTOM,
          // duration: 2000,
          fontSize,
          fill: color,
          stroke: 0x000000,
          strokeThickness: 2.5,
          jitter: 0.5,
        };

        if (Hooks.call("pf1HealthDeltaRender", this, options, textData) === false) continue;

        const t1 = performance.now();

        // Prevent scrolling text from overlapping too much
        const sinceLast = t1 - t0;
        if (sinceLast < 200) {
          await new Promise((resolve) => setTimeout(() => resolve(), Math.max(10, 200 - sinceLast)));
        }
        t0 = t1;

        console.debug("PF1 |", options.label, "|", this.uuid);
        canvas.interface.createScrollingText(token.object.center, options.label, textData);
      }
    }
  }

  /**
   * @protected
   * @override
   */
  prepareBaseData() {
    super.prepareBaseData();

    const system = this.system;

    system.traits ??= {};

    // Prepare size data
    const baseSize = system.traits.size || "med";
    const sizeValue = Object.keys(pf1.config.sizeChart).indexOf(baseSize);
    system.traits.size = {
      base: baseSize,
      value: sizeValue,
    };

    // Prepare age category data
    const baseAge = system.traits.ageCategory || "adult";
    const ageValue = Object.keys(pf1.config.ageCategories).indexOf(baseAge);
    system.traits.ageCategory = {
      base: baseAge,
      value: ageValue,
      physical: ageValue,
      mental: ageValue,
    };

    // Prepare skills
    const skills = system.skills;
    for (const skill of Object.values(skills)) {
      if (!skill) continue; // Shouldn't happen

      skill.rank ||= 0;
      skill.mod = 0;

      for (const subSkill of Object.values(skill.subSkills || {})) {
        if (!subSkill) continue; // Shouldn't happen
        subSkill.rank ||= 0;
        subSkill.mod = 0;
      }
    }
  }

  /** @inheritDoc  */
  _prepareTypeChanges(changes) {
    // Call hook
    const tempChanges = [];
    if (Hooks.events.pf1AddDefaultChanges?.length) {
      Hooks.callAll("pf1AddDefaultChanges", this, tempChanges);
    }
    changes.push(...tempChanges.filter((c) => c instanceof pf1.components.ItemChange));

    const allClasses = this.itemTypes.class.sort((a, b) => a.sort - b.sort);

    this._calculateMaxHealth(allClasses, changes);

    const system = this.system;

    // Add class data to saving throws
    const useFractional = game.settings.get("pf1", "useFractionalBaseBonuses") === true;
    for (const a of Object.keys(system.attributes.savingThrows)) {
      let hasGoodSave = false;
      system.attributes.savingThrows[a].total = system.attributes.savingThrows[a]?.base ?? 0;

      const total = allClasses.reduce((cur, cls) => {
        const base = cls.system.savingThrows[a].base;

        if (!useFractional) {
          // Add per class change
          changes.push(
            new pf1.components.ItemChange({
              formula: base,
              target: a,
              type: "untypedPerm",
              flavor: cls.name,
            })
          );
        } else {
          if (cls.system.savingThrows[a].good === true) hasGoodSave = true;
        }
        return cur + base;
      }, 0);

      if (useFractional) {
        // Add shared change with fractional
        changes.push(
          new pf1.components.ItemChange({
            formula: Math.floor(total),
            target: a,
            type: "untypedPerm",
            flavor: game.i18n.localize("PF1.Base"),
          })
        );
      }

      // Fractional bonus +2 when one class has good save
      if (useFractional && hasGoodSave) {
        const goodSaveFormula = pf1.config.classFractionalSavingThrowFormulas.goodSaveBonus;
        const total = RollPF.safeRollSync(goodSaveFormula).total;
        changes.push(
          new pf1.components.ItemChange({
            formula: total,
            target: a,
            type: "untypedPerm",
            flavor: game.i18n.localize("PF1.SavingThrowGoodFractionalBonus"),
          })
        );
      }
    }

    // Add Constitution to HP
    const hpAbility = system.attributes.hpAbility;
    if (hpAbility) {
      changes.push(
        new pf1.components.ItemChange({
          formula: "@attributes.hpAbility.mod * @attributes.hd.total",
          operator: "add",
          target: "mhp",
          type: "base",
          flavor: pf1.config.abilities[hpAbility],
        })
      );

      if (!system.attributes.wounds?.base) {
        // > a creature has a number of wound points equal to twice its Constitution score.
        changes.push(
          new pf1.components.ItemChange({
            formula: "@attributes.hpAbility.undrained * 2",
            operator: "add",
            target: "wounds",
            type: "base",
            flavor: pf1.config.abilities[hpAbility],
          })
        );
        // > It also has a wound threshold equal to its Constitution score.
        changes.push(
          new pf1.components.ItemChange({
            formula: "@attributes.hpAbility.undrained",
            operator: "add",
            target: "woundThreshold",
            type: "base",
            flavor: pf1.config.abilities[hpAbility],
          })
        );
        // https://www.aonprd.com/Rules.aspx?ID=1157
        // >  For each point of Constitution damage a creature takes, it loses 2 wound points
        changes.push(
          new pf1.components.ItemChange({
            formula: "-(@attributes.hpAbility.damage * 2)",
            operator: "add",
            target: "wounds",
            type: "untyped",
            flavor: game.i18n.localize("PF1.AbilityDamage"),
          })
        );
        // > When a creature takes a penalty to its Constitution score or its Constitution is drained,
        // > it loses 1 wound point per point of drain or per penalty
        changes.push(
          new pf1.components.ItemChange({
            formula: "@attributes.hpAbility.penalty", // no minus since penalty is negative inherently
            operator: "add",
            target: "wounds",
            type: "untyped",
            flavor: game.i18n.localize(`PF1.Ability${hpAbility.capitalize()}Pen`),
          })
        );
        changes.push(
          new pf1.components.ItemChange({
            formula: "-@attributes.hpAbility.drain",
            operator: "add",
            target: "wounds",
            type: "untyped",
            flavor: game.i18n.localize("PF1.AbilityDrain"),
          })
        );
      }
    }

    // Add skill changes
    const addSKillChanges = (skill, target) => {
      if (skill.rank > 0) {
        changes.push(
          new pf1.components.ItemChange({
            formula: skill.rank,
            value: skill.rank,
            target: `skill.~${target}`,
            type: "base",
            operator: "add",
            flavor: game.i18n.localize("PF1.SkillRankPlural"),
          })
        );
        if (skill.cs) {
          changes.push(
            new pf1.components.ItemChange({
              formula: pf1.config.classSkillBonus,
              value: pf1.config.classSkillBonus,
              target: `skill.~${target}`,
              type: "untyped",
              operator: "add",
              flavor: game.i18n.localize("PF1.CSTooltip"),
            })
          );
        }
      }

      if (skill.ability) {
        changes.push(
          new pf1.components.ItemChange({
            formula: `@abilities.${skill.ability}.mod`,
            target: `skill.~${target}`,
            operator: "add",
            type: "untyped",
            flavor: pf1.config.abilities[skill.ability] || skill.ability,
            priority: -10, // Stat buffing items don't work correctly without this
          })
        );

        if (skill.acp) {
          changes.push(
            new pf1.components.ItemChange({
              formula: "-@attributes.acp.skill",
              target: `skill.~${target}`,
              type: "untyped",
              operator: "add",
              flavor: game.i18n.localize("PF1.ACPLong"),
            })
          );
        }
      }
    };

    const skills = system.skills;
    for (const [skillId, skill] of Object.entries(skills)) {
      if (!skill) continue; // Shouldn't happen
      addSKillChanges(skill, skillId);

      for (const [subSkillId, subSkill] of Object.entries(skill.subSkills || {})) {
        if (!subSkill) continue; // Shouldn't happen
        addSKillChanges(subSkill, `${skillId}.${subSkillId}`);
      }
    }

    // Add movement speed(s)
    for (const [mode, speed] of Object.entries(system.attributes.speed)) {
      changes.push(
        new pf1.components.ItemChange({
          formula: speed.base || 0,
          target: `${mode}Speed`,
          type: "base",
          operator: "set",
          priority: 1001,
          flavor: game.i18n.localize("PF1.Base"),
        })
      );
    }

    // Add base attack modifiers shared by all attacks
    // BAB to attack
    changes.push(
      new pf1.components.ItemChange({
        _id: "_bab", // HACK: Force ID to be special
        formula: "@attributes.bab.total",
        operator: "add",
        target: "~attackCore",
        type: "untypedPerm",
        flavor: game.i18n.localize("PF1.BAB"),
      })
    );
    // Negative levels to attack
    changes.push(
      new pf1.components.ItemChange({
        formula: "-@attributes.energyDrain",
        operator: "add",
        target: "~attackCore",
        type: "untypedPerm",
        flavor: game.i18n.localize("PF1.NegativeLevels"),
      })
    );
    // ACP to attack
    changes.push(
      new pf1.components.ItemChange({
        formula: "-@attributes.acp.attackPenalty",
        operator: "add",
        target: "~attackCore",
        type: "untyped",
        flavor: game.i18n.localize("PF1.ArmorCheckPenalty"),
      })
    );

    // Add variables to CMD
    // BAB to CMD
    changes.push(
      new pf1.components.ItemChange({
        formula: "@attributes.bab.total",
        operator: "add",
        target: "cmd",
        type: "untypedPerm",
        flavor: game.i18n.localize("PF1.BAB"),
      })
    );
    // Strength or substitute to CMD
    const strAbl = system.attributes.cmd.strAbility;
    if (strAbl in pf1.config.abilities) {
      changes.push(
        new pf1.components.ItemChange({
          formula: `@abilities.${strAbl}.mod`,
          target: "cmd",
          type: "untypedPerm",
          flavor: pf1.config.abilities[strAbl],
        })
      );
    }
    // Negative levels to CMD
    changes.push(
      new pf1.components.ItemChange({
        formula: "-@attributes.energyDrain",
        operator: "add",
        target: "cmd",
        type: "untypedPerm",
        flavor: game.i18n.localize("PF1.NegativeLevels"),
      })
    );

    // Add Dexterity Modifier to Initiative
    const abl = system.attributes.init.ability;
    if (abl) {
      changes.push(
        new pf1.components.ItemChange({
          formula: `@abilities.${abl}.mod`,
          operator: "add",
          target: "init",
          type: "untypedPerm",
          priority: -100,
          flavor: pf1.config.abilities[abl],
        })
      );
    }

    // Add ACP penalty
    if (["str", "dex"].includes(abl)) {
      changes.push(
        new pf1.components.ItemChange({
          formula: "-@attributes.acp.attackPenalty",
          operator: "add",
          target: "init",
          type: "untyped",
          priority: -100,
          flavor: game.i18n.localize("PF1.ArmorCheckPenalty"),
        })
      );
    }

    // Add Ability modifiers and negative levels to saving throws
    // Ability Mod to Fortitude
    const fortAbl = system.attributes.savingThrows.fort.ability;
    if (fortAbl) {
      changes.push(
        new pf1.components.ItemChange({
          formula: `@abilities.${fortAbl}.mod`,
          operator: "add",
          target: "fort",
          type: "untypedPerm",
          flavor: pf1.config.abilities[fortAbl],
        })
      );
    }
    // Ability Mod to Reflex
    const refAbl = system.attributes.savingThrows.ref.ability;
    if (abl) {
      changes.push(
        new pf1.components.ItemChange({
          formula: `@abilities.${refAbl}.mod`,
          operator: "add",
          target: "ref",
          type: "untypedPerm",
          flavor: pf1.config.abilities[refAbl],
        })
      );
    }
    // Ability Mod to Will
    const willAbl = system.attributes.savingThrows.will.ability;
    if (willAbl) {
      changes.push(
        new pf1.components.ItemChange({
          formula: `@abilities.${willAbl}.mod`,
          operator: "add",
          target: "will",
          type: "untypedPerm",
          flavor: pf1.config.abilities[willAbl],
        })
      );
    }
    // Negative level to saves
    changes.push(
      new pf1.components.ItemChange({
        formula: "-@attributes.energyDrain",
        operator: "add",
        target: "allSavingThrows",
        type: "untyped",
        flavor: game.i18n.localize("PF1.NegativeLevels"),
      })
    );

    // Spell Resistance
    changes.push(
      new pf1.components.ItemChange({
        formula: system.attributes.sr.formula || 0,
        target: "spellResist",
        type: "untyped",
        priority: 1000,
        flavor: game.i18n.localize("PF1.CustomBonus"),
      })
    );

    // Carry capacity strength bonus
    changes.push(
      new pf1.components.ItemChange({
        formula: system.details.carryCapacity.bonus.user || 0,
        target: "carryStr",
        type: "untyped",
        priority: 1000,
        flavor: game.i18n.localize("PF1.Custom"),
      })
    );
    // Carry capacity multiplier
    changes.push(
      new pf1.components.ItemChange({
        formula: system.details.carryCapacity.multiplier.base ?? 1,
        target: "carryMult",
        type: "base",
        priority: 1000,
        flavor: game.i18n.localize("PF1.Base"),
      })
    );
    changes.push(
      new pf1.components.ItemChange({
        formula: system.details.carryCapacity.multiplier.user || 0,
        target: "carryMult",
        type: "untyped",
        priority: 1000,
        flavor: game.i18n.localize("PF1.Custom"),
      })
    );

    // NPC Lite Sheet Values for Init, CMD, BAB and AC
    const liteValues = {
      init: null,
      cmd: null,
      bab: null,
      ac: (data) => data.normal,
    };

    for (const [key, valfn] of Object.entries(liteValues)) {
      let value = system.attributes[key];
      if (typeof valfn === "function") value = valfn(value);
      value = value.value;

      if (value !== undefined) {
        changes.push(
          new pf1.components.ItemChange({
            formula: value,
            trget: key,
            type: "base",
            flavor: game.i18n.localize("PF1.Custom"),
            operator: "set",
          })
        );
      }
    }

    // Natural armor
    const ac = system.attributes.naturalAC || 0;
    changes.push(
      new pf1.components.ItemChange({
        formula: ac,
        target: "nac",
        type: "untyped",
        flavor: game.i18n.format("PF1.CustomBonusType", { type: game.i18n.localize("PF1.NaturalArmor") }),
      })
    );

    // Add armor bonuses from equipment
    for (const item of this.itemTypes.equipment) {
      if (!item.system.equipped) continue;
      // Ignore armor with zero value
      const normalAC = item.system.armor?.value || 0;
      const enhAC = item.system.armor?.enh || 0;
      if (!(normalAC + enhAC)) continue;

      const isBroken = item.isBroken;
      let armorTarget = "aac";
      if (item.system.subType === "shield") armorTarget = "sac";
      // Push base armor
      const baseAC = isBroken ? Math.floor(normalAC / 2) : normalAC;
      const enhACEffective = isBroken ? Math.floor(enhAC / 2) : enhAC;
      // Adjust broken AC to not cause extra reduction from halving both
      // Proof: 5 / 2 base + 3 / 2 enh = 3, when it should be 8 / 2 = 4
      const brokenAdjust = isBroken ? Math.floor(((normalAC % 2) + (enhAC % 2)) / 2) : 0;

      // Add changes
      changes.push(
        new pf1.components.ItemChange(
          {
            formula: baseAC,
            value: baseAC,
            target: armorTarget,
            type: "base",
          },
          { parent: item }
        )
      );
      changes.push(
        new pf1.components.ItemChange(
          {
            formula: enhACEffective + brokenAdjust,
            value: enhACEffective + brokenAdjust,
            target: armorTarget,
            type: "enhancement",
          },
          { parent: item }
        )
      );
    }

    // Add fly bonuses or penalties based on maneuverability

    const flyKey = system.attributes.speed.fly.maneuverability;
    let flyValue = 0;
    if (flyKey != null) flyValue = pf1.config.flyManeuverabilityValues[flyKey];
    if (flyValue !== 0) {
      changes.push(
        new pf1.components.ItemChange({
          formula: flyValue,
          target: "skill.fly",
          type: "racial",
          flavor: game.i18n.localize("PF1.Movement.FlyManeuverability.Label"),
        })
      );
    }

    // Add swim and climb skill bonuses based on having speeds for them
    changes.push(
      new pf1.components.ItemChange({
        formula: "(min(1, @attributes.speed.climb.total) * 8)",
        operator: "add",
        target: "skill.clm",
        type: "racial",
        priority: -1,
        flavor: game.i18n.localize("PF1.Movement.Mode.climb"),
      })
    );

    changes.push(
      new pf1.components.ItemChange({
        formula: "(min(1, @attributes.speed.swim.total) * 8)",
        operator: "add",
        target: "skill.swm",
        type: "racial",
        priority: -1,
        flavor: game.i18n.localize("PF1.Movement.Mode.swim"),
      })
    );

    // Negative level to skills
    changes.push(
      new pf1.components.ItemChange({
        formula: "-@attributes.energyDrain",
        operator: "add",
        target: "skills",
        type: "untypedPerm",
        flavor: game.i18n.localize("PF1.NegativeLevels"),
      })
    );

    // Wound Threshold to all skills
    // BUG: This does nothing since the penalty is calculated independent of the change system
    changes.push(
      new pf1.components.ItemChange({
        formula: "-@attributes.woundThresholds.penalty",
        operator: "add",
        target: "skills",
        type: "untyped",
        flavor: game.i18n.localize("PF1.WoundThreshold"),
      })
    );

    // Add size bonuses to various attributes
    // AC
    changes.push(
      new pf1.components.ItemChange({
        formula: "lookup(@size + 1, 0, " + Object.values(pf1.config.sizeMods).join(", ") + ")",
        target: "ac",
        type: "size",
        flavor: game.i18n.localize("PF1.ModifierType.size"),
        priority: -1000,
      })
    );
    // Stealth skill
    changes.push(
      new pf1.components.ItemChange({
        formula: "lookup(@size + 1, 0, " + Object.values(pf1.config.sizeStealthMods).join(", ") + ")",
        target: "skill.ste",
        type: "size",
        flavor: game.i18n.localize("PF1.ModifierType.size"),
        priority: -1000,
      })
    );
    // Fly skill
    changes.push(
      new pf1.components.ItemChange({
        formula: "lookup(@size + 1, 0, " + Object.values(pf1.config.sizeFlyMods).join(", ") + ")",
        target: "skill.fly",
        type: "size",
        flavor: game.i18n.localize("PF1.ModifierType.size"),
        priority: -1000,
      })
    );
    // CMD
    changes.push(
      new pf1.components.ItemChange({
        formula: "lookup(@size + 1, 0, " + Object.values(pf1.config.sizeSpecialMods).join(", ") + ")",
        target: "cmd",
        type: "size",
        flavor: game.i18n.localize("PF1.ModifierType.size"),
        priority: -1000,
      })
    );

    // Add age modifiers to attributes
    const ageCategories = Object.values(pf1.config.ageCategories);
    for (const key of ["str", "dex", "con", "int", "wis", "cha"]) {
      const ageCategoryIdentifier = ["str", "dex", "con"].includes(key) ? "physical" : "mental";
      const lookupStatement =
        `lookup(@ageCategory.${ageCategoryIdentifier} + 1, 0, ` +
        ageCategories.map((c) => c.modifiers[key]).join(", ") +
        ")";
      changes.push(
        new pf1.components.ItemChange({
          formula: `ifelse(gt(@abilities.${key}.base + ${lookupStatement}, 0), ${lookupStatement}, -@abilities.${key}.base + 1)`,
          target: key,
          type: "untyped",
          flavor: game.i18n.localize("PF1.Age"),
          priority: 0,
        })
      );
    }

    // Custom skill rank bonus from sheet
    if (system.details?.bonusSkillRankFormula) {
      changes.push(
        new pf1.components.ItemChange({
          formula: system.details.bonusSkillRankFormula,
          target: "bonusSkillRanks",
          type: "untyped",
          flavor: game.i18n.localize("PF1.SkillBonusRankFormula"),
        })
      );
    }

    // Add conditions
    for (const [con, v] of Object.entries(system.conditions)) {
      if (!v) continue;
      const condition = pf1.registry.conditions.get(con);
      const mechanics = condition?.mechanics;
      if (!mechanics) continue;

      // Add changes
      for (const change of mechanics.changes) {
        // Alter change data
        const changeData = { ...change, flavor: condition.name };

        // Create change object
        const changeObj = new pf1.components.ItemChange(changeData);
        changes.push(changeObj);
      }

      // Set flags
      for (const flag of mechanics.flags) {
        this.changeFlags[flag] = true;
      }
    }

    // Negative level to hit points and init
    if (system.attributes.energyDrain > 0) {
      changes.push(
        new pf1.components.ItemChange({
          formula: "-(@attributes.energyDrain * 5)",
          operator: "add",
          target: "mhp",
          type: "untyped",
          priority: -750,
          flavor: game.i18n.localize("PF1.NegativeLevels"),
        })
      );

      changes.push(
        new pf1.components.ItemChange({
          formula: "-(@attributes.energyDrain * 5)",
          operator: "add",
          target: "vigor",
          type: "untyped",
          priority: -750,
          flavor: game.i18n.localize("PF1.NegativeLevels"),
        })
      );
    }
  }

  /**
   * Calculate maximum health
   *
   * @internal
   * @param {ItemClassPF[]} allClasses - All classes, sorted in order to be applied
   * @param {ItemChange[]} changes - Changes to
   */
  _calculateMaxHealth(allClasses, changes) {
    // Categorize classes
    const pcClasses = [],
      npcClasses = [],
      racialHD = [];
    for (const cls of allClasses) {
      if (cls.subType === "racial") racialHD.push(cls);
      else if (cls.subType === "npc") npcClasses.push(cls);
      else pcClasses.push(cls);
    }

    /** @type {pf1.applications.settings.HealthConfigModel} */
    const healthConfig = game.settings.get("pf1", "healthConfig");
    const actorHealthConfig = healthConfig.getActorConfig(this);

    /**
     * @type {typeof Math.round}
     */
    const round = { up: Math.ceil, nearest: Math.round, down: Math.floor }[healthConfig.rounding];
    const { continuous } = healthConfig;

    /**
     * @param {number} value - Amount of health to add
     * @param {ItemPF} source - Source item
     */
    function pushHealth(value, source) {
      const fcb = pf1.config.favoredClassTypes.includes(source.subType) ? source.system.fc?.hp?.value || 0 : 0;

      changes.push(
        new pf1.components.ItemChange({
          formula: value,
          target: "mhp",
          type: "untypedPerm",
          flavor: source.name,
        }),
        new pf1.components.ItemChange({
          formula: value,
          target: "vigor",
          type: "untypedPerm",
          flavor: source.name,
        })
      );
      if (fcb != 0) {
        changes.push(
          new pf1.components.ItemChange({
            formula: fcb,
            target: "mhp",
            type: "untypedPerm",
            flavor: game.i18n.format("PF1.SourceInfoSkillRank_ClassFC", { className: source.name }),
          }),
          new pf1.components.ItemChange({
            formula: fcb,
            target: "vigor",
            type: "untypedPerm",
            flavor: game.i18n.format("PF1.SourceInfoSkillRank_ClassFC", { className: source.name }),
          })
        );
      }
    }

    /**
     * @param {ItemPF} source - Source item
     */
    function manualHealth(source) {
      let health = source.system.hp;
      if (!continuous) health = round(health);

      pushHealth(health, source);
    }

    /**
     * @param {ItemPF} source - Class granting health
     * @param {object} config - Class type configuration
     * @param {number} config.rate - Automatic HP rate
     * @param {boolean} config.maximized - Is this class allowed to grant maximized HP
     * @param {object} state - State tracking
     */
    function autoHealth(source, { rate, maximized } = {}, state) {
      const hpPerHD = source.system.hd ?? 0;
      if (hpPerHD === 0) return;

      let health = 0;

      // Mythic
      if (source.subType === "mythic") {
        const hpPerTier = hpPerHD ?? 0;
        if (hpPerTier === 0) return;
        const tiers = source.system.level ?? 0;
        if (tiers === 0) return;
        health = hpPerTier * tiers;
      }
      // Everything else
      else {
        let dieHealth = 1 + (hpPerHD - 1) * rate;
        if (!continuous) dieHealth = round(dieHealth);

        const hitDice = source.hitDice;

        let maxedHD = 0;
        if (maximized) {
          maxedHD = Math.min(hitDice, state.maximized.remaining);
          state.maximized.value += maxedHD;
        }
        const maxedHp = maxedHD * hpPerHD;
        const levelHp = Math.max(0, hitDice - maxedHD) * dieHealth;
        health = maxedHp + levelHp;
      }

      pushHealth(health, source);
    }

    /**
     * Compute and push health, tracking the remaining maximized levels.
     *
     * @param {ItemPF[]} sources - Health source classes
     * @param {object} config - Configuration for this class type
     * @param {boolean} config.auto - Automatic health enabled
     * @param {object} state - State tracker for auto health
     */
    function computeHealth(sources, config, state) {
      if (config.auto) {
        for (const cls of sources) autoHealth(cls, config, state);
      } else {
        for (const cls of sources) manualHealth(cls);
      }
    }

    // State tracking
    const state = {
      maximized: {
        value: 0,
        max: healthConfig.maximized,
        get remaining() {
          return this.max - this.value;
        },
      },
    };

    computeHealth(racialHD, actorHealthConfig.classes.racial, state);
    computeHealth(pcClasses, actorHealthConfig.classes.base, state);
    computeHealth(npcClasses, actorHealthConfig.classes.npc, state);
  }

  /** @inheritDoc */
  getSourceDetails(path) {
    const sources = super.getSourceDetails(path);

    const baseLabel = game.i18n.localize("PF1.Base");

    // Add base values to certain bonuses
    if (
      [
        "system.attributes.ac.normal.total",
        "system.attributes.ac.touch.total",
        "system.attributes.ac.flatFooted.total",
        "system.attributes.cmd.total",
        "system.attributes.cmd.flatFootedTotal",
      ].includes(path)
    ) {
      sources.push({ name: baseLabel, value: 10, enabled: true });
    }

    const ablRE = /^system\.abilities\.(?<key>\w+)\.(?<detail>\w+)$/.exec(path);
    if (ablRE) {
      const { key, detail } = ablRE.groups;
      const abl = this.system.abilities[key];
      switch (detail) {
        case "total":
          sources.push({ name: baseLabel, value: abl.value });
          if (abl.drain > 0) {
            sources.push({
              name: game.i18n.localize("PF1.AbilityDrain"),
              value: -Math.abs(abl.drain),
            });
          }
          break;
        case "mod":
          if (abl.damage > 0) {
            sources.push({
              name: game.i18n.localize("PF1.AbilityDamage"),
              value: -Math.floor(Math.abs(abl.damage) / 2),
            });
          }
          if (abl.userPenalty > 0) {
            sources.push({
              name: game.i18n.localize("PF1.AbilityPenalty"),
              value: -Math.floor(Math.abs(abl.userPenalty) / 2),
            });
          }
          break;
      }
    }

    // Add wound threshold data
    /** @type {pf1.applications.settings.HealthConfigModel} */
    const hpConfig = game.settings.get("pf1", "healthConfig");
    const actorHpConfig = hpConfig.getActorConfig(this);

    if (actorHpConfig.rules.useWoundThresholds != 0) {
      const wtData = this.getWoundThresholdData({ healthConfig: actorHpConfig });
      if (wtData.level > 0) {
        const penalty = -wtData.penalty;
        for (const fk of pf1.config.woundThresholdChangeTargets) {
          const paths = getChangeFlat(this, fk, "untyped", penalty);
          if (paths.includes(path)) {
            sources.push({ name: pf1.config.woundThresholdConditions[wtData.level], value: penalty });
          }
        }
      }
    }

    return sources;
  }

  /**
   * @internal
   */
  _prepareTraits() {
    this.system.traits ??= {};
    const traits = this.system.traits;

    const damageTypes = pf1.registry.damageTypes.getLabels();
    const traitsMap = {
      di: damageTypes,
      dv: damageTypes,
      ci: pf1.config.conditionTypes,
      languages: pf1.config.languages,
      armorProf: pf1.config.armorProficiencies,
      weaponProf: pf1.config.weaponProficiencies,
      creatureTypes: pf1.config.creatureTypes,
      creatureSubtypes: pf1.config.creatureSubtypes,
    };

    for (const [key, labels] of Object.entries(traitsMap)) {
      const trait = {
        base: traits[key] ?? [],
        custom: new Set(),
        standard: new Set(),
        get total() {
          return this.standard.union(this.custom);
        },
        get names() {
          return [...this.standard.map((t) => labels[t] || t), ...this.custom];
        },
      };

      traits[key] = trait;

      Object.defineProperty(trait, "value", {
        get() {
          foundry.utils.logCompatibilityWarning(`actor.system.traits.${key}.value is deprecated.`, {
            since: "PF1 v11",
            until: "PF1 v12",
          });
          return this.standard;
        },
      });
      if (Array.isArray(trait.base)) {
        const canon = traitsMap[key];
        for (const c of trait.base) {
          if (canon[c]) trait.standard.add(c);
          else trait.custom.add(c);
        }
      }
    }
  }

  /**
   * Finalize preparing armor, weapon, and language proficiencies and other traits.
   *
   * @inheritDoc
   * @protected
   */
  _finalizeTraits() {
    const traits = this.system.traits;

    const traitsMap = {
      armorProf: pf1.config.armorProficiencies,
      weaponProf: pf1.config.weaponProficiencies,
      languages: pf1.config.languages,
      creatureTypes: pf1.config.creatureTypes,
      creatureSubtypes: pf1.config.creatureSubtypes,
    };

    const validItems = this.items.filter((i) => i.isActive);

    for (const [traitKey, labels] of Object.entries(traitsMap)) {
      const trait = traits[traitKey];
      if (!trait) {
        console.error(this.name, "actor trait", traitKey, "is missing");
        continue;
      }

      // Iterate over all items to create one array of non-custom proficiencies
      for (const item of validItems) {
        const itemTrait = item.system[traitKey];
        // Check only items able to grant proficiencies
        if (!itemTrait) continue;
        // Lack of migration or prep failure elsewhere
        if (!itemTrait.total) continue;
        if (itemTrait.total.size == 0) continue;

        // Get existing sourceInfo for item with this name, create sourceInfo if none is found
        // Remember whether sourceInfo can be modified or has to be pushed at the end
        let sInfo = getSourceInfo(this.sourceInfo, `system.traits.${traitKey}`).positive.find(
          (o) => o.name === item.name
        );
        const hasInfo = !!sInfo;
        if (!sInfo) sInfo = { name: item.name, value: [] };
        else if (typeof sInfo.value === "string") sInfo.value = sInfo.value.split(", "); // BUG: this is not i18n friendly

        // Regular proficiencies
        for (const traitId of itemTrait.standard) {
          // Add localized source info if item's info does not have this proficiency already
          const label = labels[traitId];
          if (!sInfo.value.includes(label)) sInfo.value.push(label);
          // Add raw proficiency key
          trait.standard.add(traitId);
        }

        // Add readable custom profs to sources and overall collection
        for (const name of itemTrait.custom) {
          if (!sInfo.value.includes(name)) sInfo.value.push(name);
          trait.custom.add(name);
        }

        if (sInfo.value.length > 0) {
          // Dedupe if adding to existing sourceInfo
          if (hasInfo) sInfo.value = [...new Set(sInfo.value)];
          // Transform arrays into presentable strings
          sInfo.value = pf1.utils.i18n.join(sInfo.value);
          // If sourceInfo was not a reference to existing info, push it now
          if (!hasInfo) getSourceInfo(this.sourceInfo, `system.traits.${traitKey}`).positive.push(sInfo);
        }
      }
    }
  }
}
