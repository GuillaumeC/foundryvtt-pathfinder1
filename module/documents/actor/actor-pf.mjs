import { ActorBasePF } from "./actor-base.mjs";
import {
  applyChanges,
  getChangeFlat,
  getSourceInfo,
  setSourceInfoByName,
  getHighestChanges,
} from "./utils/apply-changes.mjs";
import { RollPF } from "@dice/roll.mjs";
import { Spellbook, SpellRanges, SpellbookMode, SpellbookSlots } from "./utils/spellbook.mjs";
import { VisionSharingSheet } from "module/applications/vision-sharing.mjs";
import { Resource } from "./components/resource.mjs";

/**
 * Extend the base Actor class to implement additional game system logic.
 */
export class ActorPF extends ActorBasePF {
  /**
   * Configure actor before data preparation.
   *
   * @override
   * @param {object} options - Document options
   */
  _configure(options = {}) {
    super._configure(options);

    /**
     * Stores all ItemChanges from carried items.
     *
     * @public
     * @type {Collection<ItemChange>}
     */
    this.changes ??= new Collection();

    /**
     * Cached roll data for this item.
     *
     * @internal
     * @type {object}
     */
    Object.defineProperties(this, {
      itemFlags: {
        value: { boolean: {}, dictionary: {} },
        writable: false,
      },
      _rollData: {
        value: null,
        enumerable: false,
        writable: true,
      },
      _visionSharingSheet: {
        value: null,
        enumerable: false,
        writable: true,
      },
    });
  }

  /* -------------------------------------------- */

  /**
   * Spell Failure Percentage as number from 0 to 100.
   *
   * @type {number}
   */
  get spellFailure() {
    return this.itemTypes.equipment
      .filter((o) => o.system.equipped === true)
      .reduce((cur, o) => cur + (o.system.spellFailure || 0), 0);
  }

  /**
   * Actor's current race item.
   *
   * @type {pf1.documents.item.ItemRacePF|null}
   */
  get race() {
    return this.itemTypes.race[0] ?? null;
  }

  /**
   * @internal
   * @param {SourceInfo} src - Source info
   * @returns {string} - Label for source
   */
  static _getSourceLabel(src) {
    /** @type {ItemPF} */
    const item = src.change?.parent;
    if (item) {
      const subtype = item.subType;
      let typeLabel;

      if (subtype && ((item.system.identified ?? true) || game.user.isGM) && !["weapon"].includes(item.type))
        typeLabel = game.i18n.localize(`PF1.Subtypes.Item.${item.type}.${subtype}.Single`);
      else typeLabel = game.i18n.localize(`TYPES.Item.${item.type}`);

      return `${src.name} (${typeLabel})`;
    }

    return src.name;
  }

  /**
   * Retrieve valid skill change targets for this actor.
   *
   * @internal
   * @returns {Array<string>} - Skill target array
   */
  get _skillTargets() {
    const skills = [];
    for (const [sklKey, skl] of Object.entries(this.system.skills)) {
      if (skl == null) continue;
      // Add main skill
      skills.push(`skill.${sklKey}`); // Affects this and sub-skills
      skills.push(`skill.~${sklKey}`); // Special extra target that does not affect sub-skills
      // Add subskills if present
      for (const subSklKey of Object.keys(skl.subSkills ?? [])) {
        skills.push(`skill.${sklKey}.${subSklKey}`);
      }
    }
    return skills;
  }

  /**
   * Change target paths for spellbooks on the actor.
   *
   * @internal
   * @type {Array<string>}
   */
  get _spellbookTargets() {
    const spellTargets = [];
    // Add caster level and concentration to targets
    for (const [bookId, bookData] of Object.entries(this._source.system.attributes?.spells?.spellbooks ?? {})) {
      if (bookData.inUse) {
        spellTargets.push(`cl.book.${bookId}`, `concn.${bookId}`);
      }
    }
    return spellTargets;
  }

  /**
   * Prepare boolean and dictionary flags.
   *
   * @internal
   */
  _prepareItemFlags() {
    const items = this.allItems;
    const bFlags = {};
    const dFlags = {};

    for (const item of items) {
      // Process boolean flags
      if (item.isActive) {
        const flags = item.system.flags?.boolean || {};
        for (const flag of Object.keys(flags)) {
          bFlags[flag] ??= { sources: [] };
          bFlags[flag].sources.push(item);
        }
      }

      // Process dictionary flags
      const tag = item.system.tag;
      if (tag) {
        const dEntries = Object.entries(item.system.flags?.dictionary || {});
        if (dEntries.length) {
          dFlags[tag] ||= {};

          for (const [key, value] of dEntries) {
            if (dFlags[tag][key] !== undefined && this.isOwner) {
              const msg = game.i18n.format("PF1.Warning.DuplicateDFlag", {
                actor: this.uuid,
                item: item.name,
                key,
                tag,
              });
              console.warn(msg, item);
              ui.notifications?.warn(msg, { console: false });
            }

            dFlags[tag][key] = item.isActive ? value : 0;
          }
        }
      }
    }

    this.itemFlags.boolean = bFlags;
    this.itemFlags.dictionary = dFlags;
  }

  /**
   * @internal
   */
  _prepareChanges() {
    /** @type {ItemChange[]} */
    const changes = [];
    for (const item of this.items) {
      if (item.isActive && item.hasChanges && item.changes.size) {
        changes.push(...item.changes);
      }
    }

    this._prepareTypeChanges(changes);

    const c = new Collection();
    for (const change of changes) {
      // Avoid ID conflicts
      const parentId = change.parent?.id ?? "Actor";
      const uniqueId = `${parentId}-${change._id}`;
      c.set(uniqueId, change);
    }

    this.changes = c;
  }

  /**
   * Add actor type specific changes.
   *
   * @internal
   * @abstract
   * @protected
   * @param {ItemChange[]} changes - Array of changes
   */
  // eslint-disable-next-line no-unused-vars
  _prepareTypeChanges(changes) {}

  /**
   * @internal
   * @override
   */
  applyActiveEffects() {
    // Apply active effects. Required for status effects in v11 and onward, such as blind and invisible.
    super.applyActiveEffects();

    this.prepareConditions();

    this._prepareItemFlags();
    this._prepareChanges();
    applyChanges(this, { simple: true });
  }

  /**
   * Deletes expired temporary active effects and disables linked expired buffs.
   *
   * @param {object} [options] Additional options
   * @param {Combat} [options.combat] Combat to expire data in, if relevant
   * @param {number} [options.worldTime] - World time
   * @param {number} [options.timeOffset=0] Time offset from world time
   * @param {string} [options.event] - Expiration event
   * @param {number} [options.initiative] - Initiative based expiration marker
   * @param {DocumentModificationContext} [context] Document update context
   * @throws {Error} - With insufficient permissions to control the actor.
   */
  async expireActiveEffects(
    { combat, timeOffset = 0, worldTime = null, event = null, initiative = null } = {},
    context = {}
  ) {
    if (!this.isOwner) throw new Error("Must be owner");

    // Canonical world time.
    // Due to async code in numerous places and no awaiting of time updates, this can go out of sync of actual time.
    worldTime ??= game.time.worldTime;
    worldTime += timeOffset;

    // Effects that have timed out
    const expiredEffects = this._effectsWithDuration.filter((ae) => {
      const { seconds, startTime } = ae.duration;
      const { rounds, startRound } = ae.duration;

      // Calculate remaining duration.
      // AE.duration.remaining is updated by Foundry only in combat and is unreliable.
      let remaining = Infinity;
      // Convert rounds to seconds
      if (Number.isFinite(seconds) && seconds >= 0) {
        const elapsed = worldTime - (startTime ?? 0);
        remaining = seconds - elapsed;
      } else if (rounds > 0 && combat) {
        // BUG: This will ignore which combat the round tracking started for
        const elapsed = combat.round - (startRound ?? 0);
        remaining = (rounds - elapsed) * CONFIG.time.roundTime;
      }

      // Time still remaining
      if (remaining > 0) return false;

      const aeInit = ae.system?.initiative;
      const endTiming = ae.system?.end || "turnStart";

      switch (endTiming) {
        // Initiative based ending
        case "initiative":
          if (initiative !== null) {
            return initiative <= aeInit;
          }
          // Anything not on initiative expires if they have negative time remaining
          return remaining < 0;
        // End on turn start, but we're not there yet
        case "turnStart":
          if (remaining === 0 && !["turnStart", "turnEnd"].includes(event)) return false;
          break;
        // End on turn end, but we're not quite there yet
        case "turnEnd":
          if (remaining === 0 && event !== "turnEnd") return false;
          break;
      }

      // Otherwise end when time is out
      return remaining <= 0;
    });

    const disableActiveEffects = [],
      deleteActiveEffects = [],
      disableBuffs = [];

    for (const ae of expiredEffects) {
      let item;
      // Use AE parent when available
      if (ae.parent instanceof Item) item = ae.parent;
      // Otherwise support older origin cases
      else item = ae.origin ? await fromUuid(ae.origin, { relative: this }) : null;

      if (item?.type === "buff") {
        disableBuffs.push({ _id: item.id, "system.active": false });
      } else {
        if (ae.getFlag("pf1", "autoDelete")) {
          deleteActiveEffects.push(ae.id);
        } else {
          disableActiveEffects.push({ _id: ae.id, disabled: true });
        }
      }
    }

    // Add context info for why this update happens to allow modules to understand the cause.
    context.pf1 ??= {};
    context.pf1.reason = "duration";

    if (deleteActiveEffects.length) {
      const deleteAEContext = foundry.utils.mergeObject(
        { render: !disableBuffs.length && !disableActiveEffects.length },
        context
      );
      await this.deleteEmbeddedDocuments("ActiveEffect", deleteActiveEffects, deleteAEContext);
    }

    if (disableActiveEffects.length) {
      const disableAEContext = foundry.utils.mergeObject({ render: !disableBuffs.length }, context);
      await this.updateEmbeddedDocuments("ActiveEffect", disableActiveEffects, disableAEContext);
    }

    if (disableBuffs.length) {
      await this.updateEmbeddedDocuments("Item", disableBuffs, context);
    }
  }

  /**
   * Prepare actor data before items are prepared.
   *
   * @override
   */
  prepareBaseData() {
    super.prepareBaseData();

    this.system.details ??= {};
    this.system.details.level ??= {};

    /** @type {Record<string, {negative:SourceInfo[], positive:SourceInfo[]}>} */
    this.sourceInfo = {};
    this.changeFlags = {};

    // Reset equipment info
    this.equipment = {
      shield: { type: pf1.config.shieldTypes.none, id: undefined },
      armor: { type: pf1.config.armorTypes.none, id: undefined },
    };

    // Reset class info
    this.classes = {};

    //  Init resources structure
    this.system.resources ??= {};

    this._resetInherentTotals();

    this._prepareTraits();

    if (Hooks.events.pf1PrepareBaseActorData?.length) {
      Hooks.callAll("pf1PrepareBaseActorData", this);
    }

    // Update total level and mythic tier
    const classes = this.itemTypes.class;
    /** @type {{hd:number,mythic:number,level:number}} */
    const levels = classes.reduce(
      (cur, o) => {
        o.reset(); // HACK: Out of order preparation for later.
        cur.hd += o.hitDice;
        if (!["mythic", "racial"].includes(o.subType)) {
          cur.level += o.system.level ?? 0;
        }
        cur.mythic += o.mythicTier;
        return cur;
      },
      { hd: 0, mythic: 0, level: 0 }
    );

    this.system.details.level.value = levels.level;
    this.system.details.mythicTier = levels.mythic;

    // Refresh ability scores
    for (const ability of Object.values(this.system.abilities)) {
      const value = ability.value;
      if (value === null) {
        ability.total = null;
        ability.base = null;
        ability.undrained = null;
      } else {
        ability.undrained = value;
        ability.total = value - ability.drain;
        ability.penalty = (ability.penalty || 0) - Math.abs(ability.userPenalty || 0);
        ability.base = ability.total;
      }
    }
    this.refreshAbilityModifiers();

    // Reset BAB
    {
      const k = "system.attributes.bab.total";
      const v = Math.floor(
        classes.reduce((cur, cls) => {
          // HACK: Depends on earlier out of order preparation
          const bab = cls.system.babBase;
          if (bab !== 0) {
            getSourceInfo(this.sourceInfo, k).positive.push({
              name: cls.name,
              value: pf1.utils.fractionalToString(bab),
            });
          }
          return cur + bab;
        }, 0)
      );
      this.system.attributes.bab.total = Math.floor(v);

      // Add .value for NPC lite sheet
      if (this.system.attributes.bab.value) this.system.attributes.bab.total += this.system.attributes.bab.value ?? 0;
    }

    this._prepareSenses();

    this._prepareClassSkills();

    // Reset HD
    foundry.utils.setProperty(this.system, "attributes.hd.total", levels.hd);
  }

  /**
   * Prepare traits.
   *
   * Called in prepareBaseData()
   *
   * @protected
   * @abstract
   */
  _prepareTraits() {}

  /**
   * Finalize preparing armor, weapon, and language proficiencies and other traits.
   *
   * Called in prepareDerivedData()
   *
   * @protected
   * @abstract
   */
  _finalizeTraits() {}

  /**
   * Prepare creature type info
   */
  _prepareCreatureType() {
    const race = this.race;
    if (race) {
      const creatureType = race?.system.creatureType || "humanoid";
      this.system.traits.type = creatureType;
      // TODO: Move quadruped to traits or system roots
      this.system.attributes.quadruped ??= race?.system.quadruped ?? false;
    }

    const types = this.system.traits.creatureTypes?.standard;
    if (!types?.size) return;

    // Add some booleans about the types
    this.system.traits.humanoid = types.has("humanoid");
    this.system.traits.undead = types.has("undead");
    this.system.traits.construct = types.has("construct");
    this.system.traits.living = !this.system.traits.undead && !this.system.traits.construct;

    // Add something from this.system.traits.creatureSubtypes ?
  }

  _prepareSenses() {
    // Refresh senses
    for (const [senseId, sense] of Object.entries(this.system.traits.senses)) {
      if (typeof sense !== "object") continue;
      if (!sense) continue;

      switch (senseId) {
        case "ll":
        case "si":
        case "sid":
          break;

        default:
          sense.total = sense.value;
          break;
      }
    }
  }

  /**
   * Prepare actor.system.conditions for use.
   *
   * @protected
   */
  prepareConditions() {
    this.system.conditions = {};
    const conditions = this.system.conditions;

    // Populate conditions
    // @deprecated - This should be only done in getRollData()
    for (const condition of pf1.registry.conditions.keys()) {
      conditions[condition] = this.statuses.has(condition);
    }
  }

  /**
   * Prepare natural reach for melee range and for reach weapons.
   *
   * @protected
   */
  _prepareNaturalReach() {
    // Prepare base natural reach
    this.system.traits.reach ??= {};
    const reach = this.system.traits.reach;

    reach.base = this.constructor.getReach(this.system.traits?.size?.value, this.system.traits?.stature);

    // Reset values
    reach.natural = reach.base;
    reach.total = { ...reach.base };

    // Add base natural values to the change sources
    getSourceInfo(this.sourceInfo, "system.traits.reach.total.melee").positive.push({
      name: game.i18n.localize("PF1.BuffTarReach"),
      modifier: "base",
      value: reach.base.melee,
    });
    getSourceInfo(this.sourceInfo, "system.traits.reach.total.reach").positive.push({
      name: game.i18n.localize("PF1.BuffTarReach"),
      modifier: "base",
      value: reach.base.reach,
    });
  }

  /**
   * Reset class skills.
   *
   * @protected
   */
  _prepareClassSkills() {
    /** @type {Record<string,SkillData>} */
    const skills = this.system.skills;
    if (!skills) return;

    const skillSet = new Set();
    const csSources = this.items.filter((item) => ["class", "race", "feat"].includes(item.type));
    for (const csItem of csSources) {
      for (const [classSkillName, isClassSkill] of Object.entries(csItem.system.classSkills || {})) {
        if (isClassSkill === true) skillSet.add(classSkillName);
      }
    }
    if (skillSet.size == 0) return;

    for (const [skillKey, skillData] of Object.entries(skills)) {
      if (!skillData) {
        console.warn(`Bad skill data for "${skillKey}"`, this);
        continue;
      }
      skills[skillKey].cs = skillSet.has(skillKey);
      for (const k2 of Object.keys(skillData.subSkills ?? {})) {
        foundry.utils.setProperty(skillData, `subSkills.${k2}.cs`, skillSet.has(skillKey));
      }
    }
  }

  /**
   * Checks if there's any matching proficiency
   *
   * @param {pf1.document.item.ItemEquipmentPF} item - The item to check for.
   * @returns {boolean} Whether the actor is proficient with that item.
   */
  hasArmorProficiency(item) {
    // Check for item type
    if (item.type !== "equipment" || !["armor", "shield"].includes(item.system.subType)) return true;

    /** @type {TraitData} */
    const aprof = this.system.traits?.armorProf;
    if (!aprof) return false;

    // Base proficiency
    if (aprof.total.has(item.baseArmorProficiency)) return true;

    // Base types with custom proficiencies
    if (aprof.custom.size == 0) return false;

    /** @type {string[]} */
    const baseTypes = item.system.baseTypes ?? [];
    return baseTypes.some((type) => aprof.custom.has(type));
  }

  /**
   * Test if actor is proficient with specified weapon.
   *
   * @remarks Natural attacks incorrectly do not count as proficient.
   *
   * @param {ItemPF} item - Item to test
   * @param {object} [options] - Additional options
   * @param {boolean} [options.override=true] - Allow item's proficiency override to influence the result.
   * @returns {boolean} - Proficiency state
   */
  hasWeaponProficiency(item, { override = true } = {}) {
    if (override && item.system.proficient) return true; // Explicitly marked as proficient

    /** @type {TraitData} */
    const wprof = this.system.traits?.weaponProf;
    if (!wprof) return false;

    // Match basic proficiencies, e.g. simple and martial (only present on weapons)
    // TODO: Make the item identify it's own weapon type
    let category;
    if (item.type === "weapon") {
      category = item.subType;
    } else if (item.type === "attack") {
      category = item.subType === "weapon" ? item.system.weapon?.category : null;
    }
    if (wprof.standard.has(category)) return true;

    // Match base types
    if (wprof.custom.size == 0) return false;
    const baseTypes = item.system.baseTypes ?? [];
    return baseTypes.some((type) => wprof.custom.has(type));
  }

  /**
   * Update specific spellbook.
   *
   * @internal
   * @param {string} bookId Spellbook identifier
   * @param {object} [rollData] Roll data instance
   * @param {object} cache Pre-calculated data for re-use from _generateSpellbookCache
   */
  _updateSpellBook(bookId, rollData, cache) {
    const actorData = this.system;
    const book = actorData.attributes.spells.spellbooks[bookId];
    if (!book) {
      console.error(`Spellbook data not found for "${bookId} on actor`, this);
      return;
    }

    book.isSchool = book.kind !== "divine";

    book.hasProgressionChoices =
      Object.keys(pf1.config.casterProgression.castsPerDay[book.spellPreparationMode] ?? {}).length > 1;

    // Set spellbook label
    book.label = book.name || game.i18n.localize(`PF1.SpellBook${bookId.capitalize()}`);

    // Do not process spellbooks that are not in use
    if (!book.inUse) return;

    // Use custom name if present
    if (book.name) book.label = book.name;
    // Get name from class if selected
    else if (book.class) {
      if (book.class === "_hd") book.label = book.name || game.i18n.localize("PF1.SpellBookSpelllike");
      else {
        const bookClassId = this.classes[book.class]?._id;
        const bookClass = this.items.get(bookClassId);
        if (bookClass) book.label = bookClass.name;
      }
    }

    rollData ??= this.getRollData({ refresh: true });
    cache ??= this._generateSpellbookCache();

    const bookInfo = cache.books[bookId];

    /** @type {AbilityScoreData} */
    const spellbookAbility = actorData.abilities[book.ability];

    // Add spell slots based on ability bonus slot formula
    const spellSlotAbilityScoreBonus = RollPF.safeRollSync(book.spellSlotAbilityBonusFormula || "0", rollData).total,
      spellSlotAbilityScore = (spellbookAbility?.total ?? 10) + spellSlotAbilityScoreBonus,
      spellSlotAbilityMod = pf1.utils.getAbilityModifier(spellSlotAbilityScore);

    // Set CL
    let clTotal = 0;
    {
      const key = `system.attributes.spells.spellbooks.${bookId}.cl.total`;
      const formula = book.cl.formula || "0";
      let total = 0;

      // Add NPC base
      if (this.type === "npc") {
        const value = book.cl.base || 0;
        total += value;
        clTotal += value;
        setSourceInfoByName(this.sourceInfo, key, game.i18n.localize("PF1.Base"), value);
      }
      // Add HD
      if (book.class === "_hd") {
        const value = actorData.attributes.hd.total;
        total += value;
        clTotal += value;
        setSourceInfoByName(this.sourceInfo, key, game.i18n.localize("PF1.HitDie"), value);
      }
      // Add class levels
      else if (book.class && rollData.classes[book.class]) {
        const value = rollData.classes[book.class].unlevel;
        total += value;
        clTotal += value;

        setSourceInfoByName(this.sourceInfo, key, rollData.classes[book.class].name, value);
      }

      // Set auto spell level calculation offset
      if (book.autoSpellLevelCalculation) {
        const autoFormula = book.cl.autoSpellLevelCalculationFormula || "0";
        const autoBonus = RollPF.safeRollSync(autoFormula, rollData).total ?? 0;
        const autoTotal = Math.clamp(total + autoBonus, 1, 20);
        book.cl.autoSpellLevelTotal = autoTotal;

        clTotal += autoBonus;
        if (autoBonus !== 0) {
          setSourceInfoByName(
            this.sourceInfo,
            key,
            game.i18n.localize("PF1.AutoSpellClassLevelOffset.Formula"),
            autoBonus
          );
        }
      }

      // Add from bonus formula
      const clBonus = RollPF.safeRollSync(formula, rollData).total;
      clTotal += clBonus;
      if (clBonus > 0) {
        setSourceInfoByName(this.sourceInfo, key, game.i18n.localize("PF1.CasterLevelBonusFormula"), clBonus);
      } else if (clBonus < 0) {
        setSourceInfoByName(this.sourceInfo, key, game.i18n.localize("PF1.CasterLevelBonusFormula"), clBonus, false);
      }

      // Apply negative levels
      if (rollData.attributes.energyDrain) {
        clTotal = Math.max(0, clTotal - rollData.attributes.energyDrain);
        setSourceInfoByName(
          this.sourceInfo,
          key,
          game.i18n.localize("PF1.NegativeLevels"),
          -Math.abs(rollData.attributes.energyDrain),
          false
        );
      }

      clTotal += book.cl.total ?? 0;
      clTotal += book.cl.bonus ?? 0;
      book.cl.total = clTotal;
    }

    // Set concentration bonus
    {
      // Temp fix for old actors that fail migration
      if (Number.isFinite(book.concentration)) {
        console.error(`Bad spellbook concentration value "${book.concentration}" in spellbook "${bookId}"`);
        book.concentration = {};
      }

      // Bonus formula
      const concFormula = book.concentrationFormula;
      const formulaRoll = concFormula.length
        ? RollPF.safeRollSync(concFormula, rollData, undefined, undefined, { minimize: true })
        : { total: 0, isDeterministic: true };
      const rollBonus = formulaRoll.isDeterministic ? formulaRoll.total : 0;

      // Add it all up
      const classAbilityMod = actorData.abilities[book.ability]?.mod ?? 0;
      const concentration = clTotal + classAbilityMod + rollBonus;
      const prevTotal = book.concentration.total ?? 0;

      // Set source info
      setSourceInfoByName(
        this.sourceInfo,
        `system.attributes.spells.spellbooks.${bookId}.concentration.total`,
        game.i18n.localize("PF1.CasterLevel"),
        clTotal,
        false
      );
      setSourceInfoByName(
        this.sourceInfo,
        `system.attributes.spells.spellbooks.${bookId}.concentration.total`,
        game.i18n.localize("PF1.SpellcastingAbility"),
        classAbilityMod,
        false
      );
      setSourceInfoByName(
        this.sourceInfo,
        `system.attributes.spells.spellbooks.${bookId}.concentration.total`,
        game.i18n.localize("PF1.ByBonus"),
        formulaRoll.isDeterministic ? formulaRoll.total : formulaRoll.formula,
        false
      );

      // Apply value
      book.concentration ??= {};
      book.concentration.total = prevTotal + concentration;
    }

    const getAbilityBonus = (a) => (a !== 0 ? ActorPF.getSpellSlotIncrease(spellSlotAbilityMod, a) : 0);

    const mode = new SpellbookMode(book);

    // Spell slots
    const useAuto = book.autoSpellLevelCalculation;

    // Turn off spell points with auto slots
    if (useAuto) book.spellPoints.useSystem = false;
    // Turn off bonus slots from ability score without auto slots
    else book.autoSpellLevels = false;

    const useSpellPoints = book.spellPoints.useSystem === true;

    // Set base "spontaneous" based on spell prep mode when using auto slots or spell points
    book.spontaneous = mode.isSemiSpontaneous;
    const isSpontaneous = book.spontaneous;

    if (useAuto) {
      let casterType = book.casterType;
      // Set caster type to sane default if configuration not found.
      if (!pf1.config.casterProgression.castsPerDay[mode.raw]?.[casterType]) {
        const keys = Object.keys(pf1.config.casterProgression.castsPerDay[mode.raw]);
        book.casterType = casterType = keys[0];
      }

      const castsForLevels =
        pf1.config.casterProgression[isSpontaneous ? "castsPerDay" : "spellsPreparedPerDay"][mode.raw][casterType];
      let classLevel = Math.clamp(book.cl.autoSpellLevelTotal, 1, 20);

      // Protect against invalid class level bricking actors
      if (!Number.isSafeInteger(classLevel)) {
        const msg = `Actor ${this.id} has invalid caster class level.`;
        console.error(msg, classLevel);
        ui.notifications?.error(msg, { console: false });
        classLevel = Math.floor(classLevel);
      }

      rollData.ablMod = spellSlotAbilityMod;

      const allLevelModFormula = book[isSpontaneous ? "castPerDayAllOffsetFormula" : "preparedAllOffsetFormula"] || "0";
      const allLevelMod = RollPF.safeRollSync(allLevelModFormula, rollData).total ?? 0;

      for (let level = 0; level < 10; level++) {
        const levelData = book.spells[`spell${level}`];
        // 0 is special because it doesn't get bonus preps and can cast them indefinitely so can't use the "cast per day" value
        const spellsForLevel =
          (level === 0 && isSpontaneous
            ? pf1.config.casterProgression.spellsPreparedPerDay[mode.raw][casterType][classLevel - 1][level]
            : castsForLevels[classLevel - 1][level]) ?? NaN;
        levelData.base = spellsForLevel || 0;

        const offsetFormula = levelData[isSpontaneous ? "castPerDayOffsetFormula" : "preparedOffsetFormula"] || "0";

        const max =
          (level === 0 && book.hasCantrips) || Number.isFinite(spellsForLevel)
            ? spellsForLevel +
              getAbilityBonus(level) +
              allLevelMod +
              (RollPF.safeRollSync(offsetFormula, rollData).total ?? 0)
            : NaN;

        levelData.max = max;
        if (!Number.isFinite(levelData.value)) levelData.value = max;
      }
    } else {
      for (let level = book.hasCantrips ? 0 : 1; level < 10; level++) {
        const levelData = book.spells[`spell${level}`];
        let base = levelData.base;
        if (Number.isNaN(base) || base === null) {
          levelData.base = null;
          levelData.max = 0;
        } else if (book.autoSpellLevels && base >= 0) {
          base += getAbilityBonus(level);
          levelData.max = base;
        } else {
          levelData.max = base || 0;
        }

        if (!Number.isFinite(levelData.value)) {
          levelData.value = levelData.max;
        }
      }
    }

    // Set spontaneous spell slots to something sane
    for (let a = 0; a < 10; a++) {
      book.spells[`spell${a}`].value ||= 0;
    }

    // Update spellbook slots
    {
      const slots = {};
      for (let spellLevel = 0; spellLevel < 10; spellLevel++) {
        slots[spellLevel] = new SpellbookSlots({
          level: spellLevel,
          max: book.spells[`spell${spellLevel}`].max || 0,
          domain: book.domainSlotValue || 0,
        });
      }

      // Slot usage
      for (let level = 0; level < 10; level++) {
        /** @type {pf1.documents.item.ItemSpellPF[]} */
        const levelSpells = bookInfo.level[level]?.spells ?? [];
        const lvlSlots = slots[level];
        const levelData = book.spells[`spell${level}`];
        levelData.slots = { used: 0, max: lvlSlots.max };

        if (isSpontaneous) continue;

        for (const spell of levelSpells) {
          if (Number.isFinite(spell.maxCharges)) {
            const slotCost = spell.slotCost;
            const slots = spell.maxCharges * slotCost;
            if (spell.isDomain) {
              lvlSlots.domain -= slots;
            } else {
              lvlSlots.used += slots;
            }
            lvlSlots.value -= slots;
          }
        }
        levelData.value = lvlSlots.value;

        // Add slot statistics
        levelData.slots.used = lvlSlots.used;
        levelData.slots.remaining = levelData.slots.max - levelData.slots.used;
        levelData.slots.excess = Math.max(0, -levelData.slots.remaining);
        levelData.domain = { max: lvlSlots.domainMax, remaining: lvlSlots.domain };
        levelData.domain.excess = Math.max(0, -levelData.domain.remaining);
        levelData.mismatchSlots = -(
          levelData.slots.excess +
          levelData.domain.excess -
          Math.max(0, levelData.slots.remaining)
        );
        if (levelData.mismatchSlots == 0) levelData.mismatchSlots = levelData.slots.remaining;
        levelData.invalidSlots = levelData.mismatchSlots != 0 || levelData.slots.remaining != 0;
      }

      // Spells available hint text if auto spell levels is enabled
      const maxLevelByAblScore = (spellbookAbility?.total ?? 0) - 10;

      const allLevelModFormula = book.preparedAllOffsetFormula || "0";
      const allLevelMod = RollPF.safeRollSync(allLevelModFormula, rollData).total ?? 0;

      const casterType = book.casterType || "high";
      const classLevel = Math.floor(Math.clamp(book.cl.autoSpellLevelTotal, 1, 20));

      for (let spellLevel = 0; spellLevel < 10; spellLevel++) {
        const spellLevelData = book.spells[`spell${spellLevel}`];
        // Insufficient ability score for the level
        if (maxLevelByAblScore < spellLevel) {
          const unlimit = bookInfo.data.noAbilityLimit ?? false;
          if (!unlimit) {
            spellLevelData.hasIssues = true;
            spellLevelData.lowAbilityScore = true;
          }
        }

        spellLevelData.known = { unused: 0, max: 0 };
        const domainSlotMax = spellLevel > 0 ? (slots[spellLevel].domainMax ?? 0) : 0;
        spellLevelData.preparation = { unused: 0, max: 0, domain: domainSlotMax };

        let remaining = 0;
        if (mode.isPrepared) {
          // for prepared casters, just use the 'value' calculated above
          remaining = spellLevelData.value;
          spellLevelData.preparation.max = spellLevelData.max + domainSlotMax;
        } else {
          // spontaneous or hybrid
          // if not prepared then base off of casts per day
          let available = useAuto
            ? pf1.config.casterProgression.spellsPreparedPerDay[mode.raw][casterType]?.[classLevel - 1][spellLevel]
            : spellLevelData.max;
          available += allLevelMod;

          const formula = spellLevelData.preparedOffsetFormula || "0";
          available += RollPF.safeRollSync(formula, rollData).total ?? 0;

          // Leave record of max known
          spellLevelData.known.max = available;

          if (Number.isNaN(available)) {
            spellLevelData.hasIssues = true;
            spellLevelData.lowLevel = true;
          }

          // Count spell slots used
          let dSlots = slots[spellLevel].domain;
          const used =
            bookInfo.level[spellLevel]?.spells.reduce((acc, /** @type {pf1.documents.item.ItemSpellPF} */ i) => {
              const { preparation, atWill, domain } = i.system;
              if (!atWill && preparation.value) {
                const slotCost = i.slotCost;
                if (domain && dSlots > 0) dSlots -= slotCost;
                else acc += slotCost;
              }
              return acc;
            }, 0) ?? 0;
          slots[spellLevel].domainUnused = dSlots;
          slots[spellLevel].used = used;

          remaining = available - used;
        }

        const lvlSlots = slots[spellLevel];
        // Detect domain slot problems
        const domainSlotsRemaining = spellLevel > 0 ? lvlSlots.domain : 0;

        spellLevelData.remaining = remaining;

        // No more processing needed
        if (remaining == 0 && domainSlotsRemaining <= 0) continue;

        spellLevelData.hasIssues = true;

        if (isSpontaneous) {
          spellLevelData.known.unused = Math.max(0, remaining);
          spellLevelData.known.excess = -Math.min(0, remaining);
          if (useAuto) {
            spellLevelData.invalidKnown = spellLevelData.known.unused != 0 || spellLevelData.known.excess != 0;
            spellLevelData.mismatchKnown = remaining;
          }
        } else {
          spellLevelData.preparation.unused = Math.max(0, remaining);
        }
      }
    }

    // Spell points
    if (useSpellPoints) {
      const formula = book.spellPoints.maxFormula || "0";
      rollData.cl = book.cl.total;
      rollData.ablMod = spellSlotAbilityMod;
      const spellClass = book.class ?? "";
      rollData.classLevel =
        spellClass === "_hd"
          ? (rollData.attributes.hd?.total ?? rollData.details.level.value)
          : rollData.classes[spellClass]?.level || 0;

      const roll = RollPF.safeRollSync(formula, rollData);
      book.spellPoints.max = roll.total;
    } else {
      book.spellPoints.max = 0;
    }

    // Set spellbook ranges
    book.range = new SpellRanges(book.cl.total);
  }

  /**
   * Collect some basic spellbook info so it doesn't need to be gathered again for each spellbook.
   *
   * @internal
   * @returns {object} Spellbook cache
   */
  _generateSpellbookCache() {
    const bookKeys = new Set(Object.keys(this.system.attributes.spells.spellbooks));

    const allSpells = this.itemTypes.spell;

    const cache = {
      spells: allSpells,
      /** @type {Record<string,Spellbook>} */
      books: {},
    };

    // Prepare spellbooks
    for (const bookKey of bookKeys) {
      cache.books[bookKey] ??= new Spellbook(bookKey, this);
    }

    // Spread out spells to books
    for (const spell of allSpells) {
      const bookKey = spell.system.spellbook;
      if (!bookKeys.has(bookKey)) return console.error("Spell has invalid book", spell);
      cache.books[bookKey].addSpell(spell);
    }

    return cache;
  }

  /**
   * Update all spellbooks
   *
   * @internal
   * @param {object} [rollData] Roll data instance
   * @param {object} [cache] Spellbook cache
   */
  updateSpellbookInfo(rollData, cache) {
    rollData ??= this.getRollData({ refresh: true });
    cache ??= this._generateSpellbookCache();

    const spellbooks = this.system.attributes.spells.spellbooks;

    // Set spellbook info
    for (const bookKey of Object.keys(spellbooks)) {
      this._updateSpellBook(bookKey, rollData, cache);
    }
  }

  /**
   * Called just before the first change is applied, and after every change is applied.
   * Sets additional variables (such as spellbook range)
   *
   * @internal
   */
  refreshDerivedData() {
    // Reset maximum dexterity bonus
    this.system.attributes.maxDexBonus = null;
    this.system.abilities.dex.maxBonus = this.system.abilities.dex.mod;

    // Compute encumbrance
    const encPen = this._computeEncumbrance();

    // Apply armor penalties
    const gearPen = this._applyArmorPenalties();

    // Set armor check penalty
    this.system.attributes.acp.encumbrance = encPen.acp;
    this.system.attributes.acp.gear = gearPen.acp;
    this.system.attributes.acp.total = Math.max(encPen.acp, gearPen.acp);
    // Broken gear affects only skills
    this.system.attributes.acp.skill = Math.max(encPen.acp, gearPen.acpSkill);

    // Set maximum dexterity bonus
    if (encPen.maxDexBonus != null || gearPen.maxDexBonus != null) {
      this.system.attributes.maxDexBonus = Math.min(
        encPen.maxDexBonus ?? Number.POSITIVE_INFINITY,
        gearPen.maxDexBonus ?? Number.POSITIVE_INFINITY
      );
      this.system.abilities.dex.maxBonus = Math.min(
        this.system.abilities.dex.maxBonus,
        this.system.attributes.maxDexBonus
      );
    }
  }

  /**
   * Augment the basic actor data with additional dynamic data.
   *
   * @override
   */
  prepareDerivedData() {
    super.prepareDerivedData();

    this._prepareNaturalReach();

    this.system.traits ??= {};
    this._finalizeTraits();

    // Add incorporeal status if they have incorporeal creature type
    if (this.system.traits?.creatureSubtypes?.standard?.has("incorporeal")) {
      this.statuses.add("incorporeal");
      this.system.conditions.incorporeal = true;
    }

    this.system.attributes ??= {};

    this._prepareCreatureType();

    // Reset roll data cache
    // Some changes act wonky without this
    // Example: `@skills.hea.rank >= 10 ? 6 : 3` doesn't work well without this
    this._rollData = null;

    // Setup links
    this.prepareItemLinks();

    // Update dependant data and resources
    for (const item of this.items) {
      item._prepareDependentData(false);
      this.updateItemResources(item);
    }

    applyChanges(this);

    const natReach = this.system.traits.reach.total;
    // Ensure reach never becomes negative value
    if (natReach.melee < 0) natReach.melee = 0;
    if (natReach.reach < 0) natReach.reach = 0;

    // Prepare specific derived data
    this.prepareSpecificDerivedData();

    // Prepare CMB total
    this.prepareCMB();

    this._prepareOverlandSpeeds();

    // Reset roll data cache again to include processed info
    this._rollData = null;

    // Update items
    for (const item of this.items) {
      item._prepareDependentData(true);
      // because the resources were already set up above, this is just updating from current roll data - so do not warn on duplicates
      this.updateItemResources(item, { warnOnDuplicate: false });
    }
  }

  /**
   * Calculate overland speeds.
   *
   * @protected
   */
  _prepareOverlandSpeeds() {
    for (const speed of Object.values(this.system.attributes?.speed ?? {})) {
      speed.overland = speed.total > 0 ? pf1.utils.overlandSpeed(speed.total).speed : 0;
    }
  }

  /**
   * Prepare total CMB value.
   *
   * @todo Move all the logic here to the Change system.
   *
   * @protected
   */
  prepareCMB() {
    const shrAtk = this.system.attributes.attack.shared ?? 0,
      genAtk = this.system.attributes.attack.general ?? 0,
      cmbAbl = this.system.attributes.cmbAbility,
      cmbAblMod = this.system.abilities[cmbAbl]?.mod ?? 0,
      size = this.system.traits.size.value,
      szCMBMod = Object.values(pf1.config.sizeSpecialMods)[size] ?? 0,
      cmbBonus = this.system.attributes.cmb.bonus ?? 0,
      cmb = shrAtk + genAtk + szCMBMod + cmbBonus + cmbAblMod;
    this.system.attributes.cmb.total = cmb;
  }

  /**
   * @protected
   */
  prepareSpecificDerivedData() {
    if (Hooks.events.pf1PrepareDerivedActorData?.length) {
      Hooks.callAll("pf1PrepareDerivedActorData", this);
    }

    this.refreshDerivedData();

    const attributes = this.system.attributes,
      /** @type {Record<string,AbilityScoreData>} */
      abilities = this.system.abilities;

    // Set base ability modifier
    for (const ablKey of Object.keys(abilities)) {
      const abl = abilities[ablKey];
      const base = abl.base;
      const penalty = abl.penalty || 0;
      const damage = abl.damage;
      abl.baseMod = pf1.utils.getAbilityModifier(base, { penalty, damage });
      // Save also unmodified modifier
      abl.unmod = pf1.utils.getAbilityModifier(abl.undrained);
    }

    const actorData = this.system;

    // Round health
    /** @type {pf1.applications.settings.HealthConfigModel} */
    const healthConfig = game.settings.get("pf1", "healthConfig");
    const round = { up: Math.ceil, nearest: Math.round, down: Math.floor }[healthConfig.rounding];
    for (const k of ["hp", "vigor"]) {
      attributes[k].max = round(attributes[k].max);
    }

    // Offset relative health
    for (const key of ["hp", "wounds", "vigor"]) {
      const hp = this.system.attributes[key];
      if (Number.isFinite(hp?.offset)) {
        hp.value = hp.max + hp.offset;
      }
    }

    // Shared attack bonuses
    {
      // Total
      const totalAtk = attributes.bab.total - attributes.acp.attackPenalty - (attributes.energyDrain ?? 0);
      attributes.attack.shared = totalAtk;
    }

    // Update wound threshold
    this.updateWoundThreshold();

    // Create arbitrary skill slots
    for (const skillId of pf1.config.arbitrarySkills) {
      if (actorData.skills[skillId] == null) continue;
      /** @type {SkillData} */
      const skill = actorData.skills[skillId];
      skill.subSkills = skill.subSkills || {};
      for (const subSkillId of Object.keys(skill.subSkills)) {
        if (skill.subSkills[subSkillId] == null) delete skill.subSkills[subSkillId];
      }
    }

    // Delete removed skills
    for (const skillId of Object.keys(actorData.skills)) {
      const skl = actorData.skills[skillId];
      if (skl == null) {
        delete actorData.skills[skillId];
      }
    }

    // Mark background skills
    for (const skillId of Object.keys(actorData.skills)) {
      if (pf1.config.backgroundSkills.includes(skillId)) {
        const skill = actorData.skills[skillId];
        skill.background = true;
        for (const subSkillId of Object.keys(skill.subSkills ?? {})) skill.subSkills[subSkillId].background = true;
      }
    }

    // Combine AC types
    for (const k of ["ac.normal.total", "ac.shield.total", "ac.natural.total"]) {
      const v = foundry.utils.getProperty(actorData, k);
      if (v) {
        for (const k2 of ["normal", "flatFooted"]) {
          attributes.ac[k2].total += v;
        }
      }
    }

    // Add Dexterity to AC
    {
      // get configured ability scores
      const acAbl = attributes.ac.normal.ability ?? "dex";
      const acTouchAbl = attributes.ac.touch.ability ?? "dex";
      const cmdDexAbl = attributes.cmd.dexAbility ?? "dex";
      let acAblMod = abilities[acAbl]?.mod ?? 0;
      let acTouchAblMod = abilities[acTouchAbl]?.mod ?? 0;
      const cmdDexAblMod = abilities[cmdDexAbl]?.mod ?? 0;
      if (this.changeFlags["loseDexToAC"]) {
        acAblMod = Math.min(acAblMod, 0);
        acTouchAblMod = Math.min(acTouchAblMod, 0);
      }
      const maxDex = attributes.maxDexBonus ?? null;
      const ac = {
        normal: maxDex !== null ? Math.min(maxDex, acAblMod) : acAblMod,
        touch: maxDex !== null ? Math.min(maxDex, acTouchAblMod) : acTouchAblMod,
        flatFooted: Math.min(0, acAblMod),
      };
      const acAblKey = {
        normal: acAbl,
        touch: acTouchAbl,
        flatFooted: acAbl,
      };
      const cmd = {
        total: cmdDexAblMod,
        flatFootedTotal: Math.min(0, cmdDexAblMod),
      };
      for (const [k, v] of Object.entries(ac)) {
        attributes.ac[k].total += v;
        getSourceInfo(this.sourceInfo, `system.attributes.ac.${k}.total`).positive.push({
          value: v,
          name: pf1.config.abilities[acAblKey[k]],
        });
      }
      for (const [k, v] of Object.entries(cmd)) {
        attributes.cmd[k] += v;
        getSourceInfo(this.sourceInfo, `system.attributes.cmd.${k}`).positive.push({
          value: v,
          name: pf1.config.abilities[cmdDexAbl],
        });
      }
    }

    // Reduce final speed under certain circumstances
    {
      let reducedSpeed = false;
      const sInfo = { name: "", value: game.i18n.localize("PF1.ReducedMovementSpeed") };

      // from encumbrance
      const encLevel = attributes.encumbrance.level;
      if (encLevel > 0) {
        const encLevels = pf1.config.encumbranceLevels;
        if (encLevel >= encLevels.heavy) {
          if (!this.changeFlags.noHeavyEncumbrance) {
            reducedSpeed = true;
            sInfo.name = game.i18n.localize("PF1.HeavyEncumbrance");
          }
        } else if (encLevel >= encLevels.medium) {
          if (!this.changeFlags.noMediumEncumbrance) {
            reducedSpeed = true;
            sInfo.name = game.i18n.localize("PF1.MediumEncumbrance");
          }
        }
      }

      const armor = { type: 0 };
      const eqData = this.equipment;
      if (eqData) this._prepareArmorData(eqData.armor, armor);

      // Wearing heavy armor
      if (armor.type == pf1.config.armorTypes.heavy && !this.changeFlags.heavyArmorFullSpeed) {
        reducedSpeed = true;
        sInfo.name = game.i18n.localize("PF1.Subtypes.Item.equipment.armor.Types.heavy");
      }
      // Wearing medium armor
      else if (armor.type == pf1.config.armorTypes.medium && !this.changeFlags.mediumArmorFullSpeed) {
        reducedSpeed = true;
        sInfo.name = game.i18n.localize("PF1.Subtypes.Item.equipment.armor.Types.medium");
      }

      for (const speedKey of Object.keys(this.system.attributes.speed)) {
        const speedValue = this.system.attributes.speed[speedKey].total;
        // Save speed unaffected by speed maluses here (not counting negative changes)
        // TODO: Somehow make this ignore additional set operators
        this.system.attributes.speed[speedKey].unhindered = speedValue; // @since PF1 v10

        if (reducedSpeed && speedValue > 0) {
          this.system.attributes.speed[speedKey].total = this.constructor.getReducedMovementSpeed(speedValue);
          getSourceInfo(this.sourceInfo, `system.attributes.speed.${speedKey}.total`).negative.push(sInfo);
        }
      }
    }

    // Add encumbrance source details
    let encACPPenalty = null,
      encMaxDex = null;
    switch (attributes.encumbrance.level) {
      case pf1.config.encumbranceLevels.medium: {
        encACPPenalty = 3;
        encMaxDex = 3;
        break;
      }
      case pf1.config.encumbranceLevels.heavy: {
        encACPPenalty = 6;
        encMaxDex = 1;
        break;
      }
    }
    const encLabel = game.i18n.localize("PF1.Encumbrance");
    if (encACPPenalty !== null) {
      getSourceInfo(this.sourceInfo, "system.attributes.acp.total").negative.push({
        name: encLabel,
        value: encACPPenalty,
      });
    }
    if (encMaxDex !== null) {
      getSourceInfo(this.sourceInfo, "system.attributes.maxDexBonus").negative.push({
        name: encLabel,
        value: encMaxDex,
      });
      let maxDexLabel = new Intl.NumberFormat(undefined, { signDisplay: "always" }).format(encMaxDex);
      maxDexLabel = `${game.i18n.localize("PF1.MaxDexShort")} ${maxDexLabel}`;
      getSourceInfo(this.sourceInfo, "system.attributes.ac.normal.total").negative.push({
        name: encLabel,
        value: maxDexLabel,
        valueAsNumber: encMaxDex,
      });
      getSourceInfo(this.sourceInfo, "system.attributes.ac.touch.total").negative.push({
        name: encLabel,
        value: maxDexLabel,
        valueAsNumber: encMaxDex,
      });
    }

    // Enable senses based on flags
    const senses = this.system.traits.senses;
    senses.ll.enabled ||= this.changeFlags.lowLightVision;
    senses.si ||= this.changeFlags.seeInvisibility;
    senses.sid ||= this.changeFlags.seeInDarkness;

    this.updateSpellbookInfo();
  }

  /**
   * Returns this actor's labels for use with sheets.
   *
   * @protected
   * @returns {Record<string, string>} - Label object
   */
  getLabels() {
    const labels = {};

    labels.alignment = pf1.config.alignments[this.system.details.alignment];

    // Speed
    labels.speed = {};
    for (const [key, obj] of Object.entries(this.system.attributes.speed ?? {})) {
      const dist = pf1.utils.convertDistance(obj.total);
      labels.speed[key] = `${dist[0]} ${pf1.config.measureUnitsShort[dist[1]]}`;
    }

    return labels;
  }

  /**
   * Computes armor penalties for this actor.
   *
   * @internal
   * @returns {MobilityPenaltyResult} The resulting penalties from armor.
   */
  _applyArmorPenalties() {
    let attackACPPenalty = 0; // ACP to attack penalty from lacking proficiency. Stacks infinitely.
    const acp = { armor: 0, shield: 0 };
    const broken = { armor: { value: 0, item: null }, shield: { value: 0, item: null } };
    const mdex = { armor: null, shield: null };

    const equipment = this.itemTypes.equipment;
    for (const item of equipment) {
      if (!item.system.equipped) continue;

      const eqType = item.system.subType;
      const isShieldOrArmor = ["armor", "shield"].includes(eqType);
      let itemACP = Math.abs(item.system.armor.acp);
      if (item.system.masterwork === true && isShieldOrArmor) itemACP = Math.max(0, itemACP - 1);

      if (isShieldOrArmor) {
        itemACP = Math.max(0, itemACP + (this.system.attributes?.acp?.[`${eqType}Bonus`] ?? 0));
      }

      if (itemACP) {
        if (item.isBroken) {
          broken[eqType].value = itemACP;
          broken[eqType].item = item;

          const bsInfo = getSourceInfo(this.sourceInfo, "system.attributes.acp.skill").negative.find(
            (o) => o.itemId === item.id
          );
          if (bsInfo) bsInfo.value = itemACP;
          else {
            getSourceInfo(this.sourceInfo, "system.attributes.acp.skill").negative.push({
              name: `${item.name} (${game.i18n.localize("PF1.Broken")})`,
              itemId: item.id,
              value: itemACP,
            });
          }
        }

        const sInfo = getSourceInfo(this.sourceInfo, "system.attributes.acp.total").negative.find(
          (o) => o.itemId === item.id
        );

        if (sInfo) sInfo.value = itemACP;
        else {
          getSourceInfo(this.sourceInfo, "system.attributes.acp.total").negative.push({
            name: item.name,
            itemId: item.id,
            value: itemACP,
          });
        }
      }

      if (isShieldOrArmor) {
        if (itemACP > acp[eqType]) acp[eqType] = itemACP;
        if (!item.getProficiency(false)) attackACPPenalty += itemACP;
      }

      if (item.system.armor.dex !== null && isShieldOrArmor) {
        const mDex = item.system.armor.dex;
        if (Number.isInteger(mDex)) {
          const mod = this.system.attributes?.mDex?.[`${eqType}Bonus`] ?? 0;
          const itemMDex = mDex + mod;
          mdex[eqType] = Math.min(itemMDex, mdex[eqType] ?? Number.POSITIVE_INFINITY);

          const sInfo = getSourceInfo(this.sourceInfo, "system.attributes.maxDexBonus").negative.find(
            (o) => o.itemId === item.id
          );
          if (sInfo) sInfo.value = itemMDex;
          else {
            getSourceInfo(this.sourceInfo, "system.attributes.maxDexBonus").negative.push({
              name: item.name,
              itemId: item.id,
              value: itemMDex,
              ignoreNull: false,
            });
          }

          // Add max dex to AC, too.
          let maxDexLabel = new Intl.NumberFormat(undefined, { signDisplay: "always" }).format(itemMDex);
          maxDexLabel = `${game.i18n.localize("PF1.MaxDexShort")} ${maxDexLabel}`;
          for (const p of ["system.attributes.ac.normal.total", "system.attributes.ac.touch.total"]) {
            // Use special maxDex id to ensure only the worst is shown
            const sInfoA = getSourceInfo(this.sourceInfo, p).negative.find((o) => o.id === "maxDexEq");
            if (sInfoA) {
              if (itemMDex < sInfoA.valueAsNumber) {
                sInfoA.value = maxDexLabel;
                sInfoA.valueAsNumber = itemMDex;
                sInfoA.itemId = item.id;
                sInfoA.name = item.name;
              } else if (sInfoA.itemId == item.id) {
                // Update existing (armor training or the like)
                sInfoA.value = maxDexLabel;
                sInfoA.valueAsNumber = itemMDex;
              }
            } else {
              getSourceInfo(this.sourceInfo, p).negative.push({
                name: item.name,
                value: maxDexLabel,
                valueAsNumber: itemMDex,
                itemId: item.id,
                id: "maxDexEq",
              });
            }
          }
        }
      }
    }

    // Add Broken to sources
    for (const eqType of Object.keys(broken)) {
      const value = broken[eqType].value;
      if (value == 0) continue;
      const brokenId = broken[eqType].item.id;
      const sInfo = getSourceInfo(this.sourceInfo, `system.attributes.acp.${eqType}Bonus`).negative.find(
        (o) => o.brokenId === brokenId
      );
      if (sInfo) sInfo.value = value;
      else
        getSourceInfo(this.sourceInfo, `system.attributes.acp.${eqType}Bonus`).negative.push({
          name: `${broken[eqType].item.name} (${game.i18n.localize("PF1.Broken")})`,
          brokenId,
          value,
        });
    }

    // Return result
    const totalACP = acp.armor + acp.shield;
    const result = {
      maxDexBonus: null,
      acp: totalACP,
      acpSkill: totalACP + broken.armor.value + broken.shield.value,
    };
    this.system.attributes.acp.gear = totalACP;
    if (mdex.armor !== null || mdex.shield !== null)
      result.maxDexBonus = Math.min(mdex.armor ?? Number.POSITIVE_INFINITY, mdex.shield ?? Number.POSITIVE_INFINITY);

    // Set armor penalty to attack rolls
    this.system.attributes.acp.attackPenalty = attackACPPenalty;

    return result;
  }

  /**
   * @internal
   */
  prepareItemLinks() {
    for (const item of this.items) {
      const links = item.system.links;
      if (!links) continue;

      for (const type of Object.keys(links)) {
        for (const link of links[type]) {
          // HACK: fromUuid[Sync]() causes recursive synth actor initialization at world launch, so we do this instead
          if (!link.uuid) {
            console.warn(`"${item.name}" on "${this.name}" has invalid link in "${type}"`, { link, item });
            continue;
          }
          const linkData = foundry.utils.parseUuid(link.uuid, { relative: this });
          const linkedItem = this.items.get(linkData?.id);
          if (!linkedItem) continue; // Ignore items not on current actor

          switch (type) {
            case "charges": {
              linkedItem.links.charges = item;
              linkedItem.prepareLinks();
              break;
            }
            case "children": {
              linkedItem.links.parent = item;
              break;
            }
          }
        }
      }
    }
  }

  /**
   * @deprecated - Use {@link getSourceDetails()} instead.
   * @type {object}
   */
  get sourceDetails() {
    foundry.utils.logCompatibilityWarning(
      "ActorPF.sourceDetails is deprecated in favor of ActorPF.getSourceDetails()",
      {
        since: "PF1 v11",
        until: "PF1 v12",
      }
    );

    const actor = this;

    // Redirect to getSourceDetails()
    const sourceDetailsHandler = {
      get(_, prop) {
        return actor.getSourceDetails(prop);
      },
    };

    return new Proxy({}, sourceDetailsHandler);
  }

  /**
   * Retrieve source details regarding a change target.
   *
   * @example
   * ```js
   * actor.getSourceDetails("system.attributes.hp.max");
   * ```
   * @todo - Merge `sourceInfo` into the change system to reduce underlying system complexity
   * @param {string} path - Change target path
   * @returns {SourceInfo[]}
   */
  getSourceDetails(path) {
    const sources = [];

    const dexDenied = this.changeFlags.loseDexToAC === true;

    // Add extra data
    const rollData = this.getRollData();
    const changeGrp = this.sourceInfo[path] ?? {};
    const sourceGroups = Object.values(changeGrp);

    const typeBonuses = {};

    for (const grp of sourceGroups) {
      for (const src of grp) {
        src.operator ||= "add";
        let srcValue =
          src.value != null
            ? src.value
            : RollPF.safeRollSync(src.formula || "0", rollData, [path, src, this], {
                suppressError: !this.isOwner,
              }).total;
        if (src.operator === "set") {
          let displayValue = srcValue;
          if (src.change?.isDistance) displayValue = pf1.utils.convertDistance(displayValue)[0];
          srcValue = game.i18n.format("PF1.SetTo", { value: displayValue });
        }

        // Add sources only if they actually add something else than zero
        if (!(src.operator === "add" && srcValue === 0) || src.ignoreNull === false) {
          // Account for dex denied denying dodge bonuses
          if (dexDenied && srcValue > 0 && src.modifier === "dodge" && src.operator === "add" && src.change?.isAC)
            continue;

          // TODO: Separate source name from item type label
          const label = this.constructor._getSourceLabel(src);
          const info = { name: label.replace(/[[\]]/g, ""), value: srcValue, modifier: src.modifier || null };
          typeBonuses[src.modifier || "untyped"] ??= [];
          typeBonuses[src.modifier || "untyped"].push(info);
          sources.push(info);
        }
      }
    }

    // Sort and disable entries
    const stacking = new Set(pf1.config.stackingBonusTypes);
    for (const [type, entries] of Object.entries(typeBonuses)) {
      if (stacking.has(type)) continue;
      entries.sort((a, b) => b.value - a.value);
      for (const entry of entries) {
        entry.disabled = entry.value >= 0 || typeof entry.value !== "number";
      }
      entries[0].disabled = false;
    }

    return sources;
  }

  /**
   * @internal
   * @returns {Record<string,*>} - Path to value mapping
   */
  _getInherentTotalsKeys() {
    // Determine base keys
    const keys = {
      "attributes.ac.normal.total": 10,
      "attributes.ac.touch.total": 10,
      "attributes.ac.flatFooted.total": 10,
      "attributes.bab.total": 0,
      "attributes.cmd.total": 10,
      "attributes.cmd.flatFootedTotal": 10,
      "attributes.acp.armorBonus": 0,
      "attributes.acp.shieldBonus": 0,
      "attributes.acp.gear": 0,
      "attributes.acp.encumbrance": 0,
      "attributes.acp.total": 0,
      "attributes.acp.skill": 0,
      "attributes.acp.attackPenalty": 0,
      "attributes.maxDexBonus": null,
      "ac.normal.total": 0,
      "ac.normal.base": 0,
      "ac.normal.enh": 0,
      "ac.normal.misc": 0,
      "ac.natural.total": 0,
      "ac.natural.base": 0,
      "ac.natural.misc": 0,
      "ac.natural.enh": 0,
      "ac.shield.total": 0,
      "ac.shield.base": 0,
      "ac.shield.enh": 0,
      "ac.shield.misc": 0,
      "attributes.sr.total": 0,
      "attributes.init.bonus": 0,
      "attributes.init.total": this.system.attributes.init.value ?? 0,
      "attributes.cmb.bonus": 0,
      "attributes.cmb.total": 0,
      "attributes.cmb.value": 0,
      "attributes.hp.max": this.system.attributes.hp.base ?? 0,
      "attributes.vigor.max": this.system.attributes.vigor.base ?? 0,
      "attributes.wounds.max": this.system.attributes.wounds.base ?? 0,
      "attributes.wounds.threshold": 0,
      "attributes.attack.general": 0,
      "attributes.attack.melee": 0,
      "attributes.attack.natural": 0,
      "attributes.attack.ranged": 0,
      "attributes.attack.thrown": 0,
      "attributes.attack.shared": 0,
      "attributes.attack.critConfirm": 0,
      "attributes.mDex": { armorBonus: 0, shieldBonus: 0 },
      "attributes.damage.general": 0,
      "attributes.damage.weapon": 0,
      "attributes.damage.natural": 0,
      "attributes.damage.melee": 0, // Melee weapon
      "attributes.damage.meleeAll": 0,
      "attributes.damage.ranged": 0, // Ranged weapon
      "attributes.damage.rangedAll": 0,
      "attributes.damage.thrown": 0, // Thrown weapon
      "attributes.damage.spell": 0,
      "attributes.damage.shared": 0,
      "attributes.woundThresholds.level": 0,
      "attributes.woundThresholds.mod": 0,
      "attributes.woundThresholds.penaltyBase": 0,
      "attributes.woundThresholds.penalty": 0,
      "abilities.str.checkMod": 0,
      "abilities.str.total": 0,
      "abilities.str.undrained": 0,
      "abilities.dex.checkMod": 0,
      "abilities.dex.total": 0,
      "abilities.dex.undrained": 0,
      "abilities.con.checkMod": 0,
      "abilities.con.total": 0,
      "abilities.con.undrained": 0,
      "abilities.int.checkMod": 0,
      "abilities.int.total": 0,
      "abilities.int.undrained": 0,
      "abilities.wis.checkMod": 0,
      "abilities.wis.total": 0,
      "abilities.wis.undrained": 0,
      "abilities.cha.checkMod": 0,
      "abilities.cha.total": 0,
      "abilities.cha.undrained": 0,
      "attributes.spells.spellbooks.primary.concentration.total": 0,
      "attributes.spells.spellbooks.secondary.concentration.total": 0,
      "attributes.spells.spellbooks.tertiary.concentration.total": 0,
      "attributes.spells.spellbooks.spelllike.concentration.total": 0,
      "attributes.spells.spellbooks.primary.cl.total": 0,
      "attributes.spells.spellbooks.secondary.cl.total": 0,
      "attributes.spells.spellbooks.tertiary.cl.total": 0,
      "attributes.spells.spellbooks.spelllike.cl.total": 0,
      "details.carryCapacity.bonus.total": 0,
      "details.carryCapacity.multiplier.total": 0,
      "details.feats.bonus": 0,
      "details.skills.bonus": 0,
      "attributes.speed.land.add": 0,
      "attributes.speed.swim.add": 0,
      "attributes.speed.fly.add": 0,
      "attributes.speed.climb.add": 0,
      "attributes.speed.burrow.add": 0,
      "attributes.savingThrows.fort.total": this.system.attributes.savingThrows.fort.base ?? 0,
      "attributes.savingThrows.ref.total": this.system.attributes.savingThrows.ref.base ?? 0,
      "attributes.savingThrows.will.total": this.system.attributes.savingThrows.will.base ?? 0,
    };

    // Determine skill keys
    try {
      const skillKeys = getChangeFlat(this, "skills");
      for (const k of skillKeys) {
        keys[k.replace(/^system\./, "")] = 0;
      }
    } catch (err) {
      console.error("Could not determine skills for an actor", this, err);
    }

    return keys;
  }

  /**
   * Data to reset base value of, but only if missing.
   *
   * @private
   * @see {@link _resetInherentTotals}
   * @returns {Record<string,number>} - Base fill keys
   */
  _getBaseValueFillKeys() {
    return [
      { parent: "abilities.str", key: "base", value: 0 },
      { parent: "abilities.dex", key: "base", value: 0 },
      { parent: "abilities.con", key: "base", value: 0 },
      { parent: "abilities.int", key: "base", value: 0 },
      { parent: "abilities.wis", key: "base", value: 0 },
      { parent: "abilities.cha", key: "base", value: 0 },
    ];
  }

  /**
   * @protected
   */
  _resetInherentTotals() {
    const keys = this._getInherentTotalsKeys();

    // Reset totals
    for (const [k, v] of Object.entries(keys)) {
      try {
        foundry.utils.setProperty(this.system, k, v);
      } catch (err) {
        console.error(err, k);
      }
    }

    for (const data of this._getBaseValueFillKeys()) {
      const { parent, key, value } = data;
      const o = foundry.utils.getProperty(this.system, parent);
      if (!o) continue; // Not all actor types have these
      o[key] ??= value;
    }
  }

  /**
   * Return reduced movement speed.
   *
   * @example
   * ```js
   * pf1.documents.actor.ActorPF.getReducedMovementSpeed(30); // => 20
   * ```
   *
   * @param {number} value - The non-reduced movement speed.
   * @returns {number} The reduced movement speed.
   */
  static getReducedMovementSpeed(value) {
    return value - Math.floor(value / 5 / 3) * 5;
  }

  /**
   * Return increased amount of spell slots by ability score modifier.
   *
   * @example
   * ```js
   * pf1.documents.actor.ActorPF.getSpellSlotIncrease(2, 1); // => 1
   * pf1.documents.actor.ActorPF.getSpellSlotIncrease(6, 1); // => 2
   * pf1.documents.actor.ActorPF.getSpellSlotIncrease(6, 7); // => 0
   * ```
   *
   * @param {number} mod - The associated ability modifier.
   * @param {number} level - Spell level.
   * @returns {number} Amount of spell levels to increase.
   */
  static getSpellSlotIncrease(mod, level) {
    if (level === 0) return 0;
    if (mod <= 0) return 0;
    return Math.max(0, Math.ceil((mod + 1 - level) / 4));
  }

  /**
   * Return the amount of experience required to gain a certain character level.
   *
   * @abstract
   * @param {number} level - The desired level
   * @returns {number} - The XP required
   */
  // eslint-disable-next-line no-unused-vars
  getLevelExp(level) {
    return 0; // Only used by PCs
  }

  /**
   * Create a new Token document, not yet saved to the database, which represents the Actor, and apply actor size to it.
   *
   * @override
   * @param {object} [data={}]            Additional data, such as x, y, rotation, etc. for the created token data
   * @param {object} [options={}]         The options passed to the TokenDocument constructor
   * @returns {Promise<TokenDocumentPF>}  The created TokenDocument instance
   */
  async getTokenDocument(data = {}, options = {}) {
    if (!this.prototypeToken.flags?.pf1?.staticSize) {
      const size = Object.values(pf1.config.tokenSizes)[this.getRollData({ refresh: true }).size];
      if (size) {
        Object.assign(data, {
          width: size.w,
          height: size.h,
          texture: {
            scaleX: size.scale * (this.prototypeToken.texture.scaleX || 1),
            scaleY: size.scale * (this.prototypeToken.texture.scaleY || 1),
          },
        });
      }
    }

    return super.getTokenDocument(data, options);
  }

  /* -------------------------------------------- */

  /* -------------------------------------------- */
  /*  Socket Listeners and Handlers
  /* -------------------------------------------- */

  /**
   * @internal
   * @override
   * @param {object} changed - Changed data
   * @param {object} context - Context
   * @param {User} user - Triggering user
   */
  async _preUpdate(changed, context, user) {
    await super._preUpdate(changed, context, user);
    if (!changed.system) return; // No system updates.
    if (context.diff === false || context.recursive === false) return; // Don't diff if we were told not to diff

    const oldData = this.system;

    // Offset HP values
    const attributes = changed.system.attributes;
    if (attributes) {
      for (const key of ["hp", "wounds", "vigor"]) {
        const hp = attributes[key];
        if (!hp) continue;
        if (hp.value !== undefined && hp.offset === undefined) {
          const max = hp.max ?? oldData.attributes[key]?.max ?? 0;
          hp.offset = hp.value - max;
        }
        // Do not ever keep .value
        delete hp.value;
      }

      // Convert excess vigor damage to wounds
      const vigor = attributes.vigor;
      if (vigor?.offset < 0) {
        const maxVigor = oldData.attributes.vigor.max;
        const excessVigorDamage = -(maxVigor + vigor.offset);
        if (excessVigorDamage > 0) {
          attributes.wounds ??= {};
          attributes.wounds.offset ??= oldData.attributes?.wounds?.offset ?? 0;
          attributes.wounds.offset -= excessVigorDamage;
          vigor.offset = -maxVigor;
        }
      }
    }

    if (changed.system.attributes?.quadruped !== undefined) {
      const quad = changed.system.attributes.quadruped;
      const quadRace = this.race?.system.quadruped ?? false;
      // Null if setting quadruped to same as race (no override)
      if (quad === quadRace) changed.system.attributes.quadruped = null;
    }

    // Make certain variables absolute
    const abilities = changed.system.abilities;
    if (abilities) {
      const absoluteKeys = ["userPenalty", "damage", "drain"];
      const keys = Object.keys(abilities);
      for (const abl of keys) {
        const ablData = abilities[abl];
        if (!ablData) continue; // e.g. if null from being deleted for homebrew
        for (const absKey of absoluteKeys) {
          if (ablData[absKey] !== undefined) {
            ablData[absKey] = Math.abs(ablData[absKey]);
          }
        }
      }
    }

    const energyDrain = changed.system.attributes?.energyDrain;
    if (energyDrain !== undefined) {
      changed.system.attributes.energyDrain = Math.abs(energyDrain);
    }

    // Never allow updates to the new conditions location
    if (changed.system.conditions !== undefined) {
      delete changed.system.conditions;
    }

    // Adjust spellbook data
    const books = changed.system.attributes?.spells?.spellbooks;
    if (books) {
      const cbooks = this.system.attributes?.spells?.spellbooks;
      for (const [bookId, bookData] of Object.entries(books)) {
        const prepMode = bookData.spellPreparationMode;
        if (prepMode !== cbooks[bookId].spellPreparationMode) {
          const prog = bookData.casterType || cbooks[bookId].casterType;
          const progs = pf1.config.casterProgression.castsPerDay[prepMode] ?? {};
          // Reset invalid progression to first choice
          if (!progs[prog]) bookData.casterType = Object.keys(progs)[0];
        }
      }
    }

    this._detectHealthChange(changed, context);
  }

  /**
   * Detect Health Changes
   *
   * Builds data for post-update handling of deltas, stored in `context.pf1.deltas`.
   *
   * @internal
   * @abstract
   * @param {object} changed
   * @param {object} context
   */
  _detectHealthChange(changed, context) {}

  /**
   * Synchronize actor and token vision
   *
   * @internal
   * @param {boolean} initializeVision - Initialize vision
   * @param {boolean} refreshLighting - Refresh lightning
   */
  updateVision(initializeVision = false, refreshLighting = false) {
    if (!this.testUserPermission(game.user, "OBSERVER")) return;

    const visionUpdate = {
      refreshLighting: true,
      refreshVision: true,
    };

    let tokens = this.getActiveTokens(false, true).filter((t) => t.sight.enabled);
    const needSelection = game.settings.get("pf1", "lowLightVisionMode");
    if (needSelection) tokens = tokens.filter((t) => t.object?.controlled ?? false);

    // Skip refresh if no tokens with vision present on scene
    if (tokens.length == 0) return;

    // Ensure vision immediately updates
    if (initializeVision) {
      for (const token of tokens) {
        token._syncSenses();
      }
      visionUpdate.initializeVision = true;
    }

    // Ensure LLV functions correctly
    if (refreshLighting) {
      visionUpdate.initializeLighting = true;
    }

    canvas.perception.update(visionUpdate);
  }

  /**
   * @override
   * @param {object} changed - Changed data
   * @param {object} context - Context
   * @param {string} userId - User ID
   */
  _onUpdate(changed, context, userId) {
    super._onUpdate(changed, context, userId);

    // No system data updated
    if (!changed.system) return;

    const sourceUser = game.user.id === userId;

    let initializeVision = false,
      refreshLighting = false;

    if (foundry.utils.hasProperty(changed.system, "traits.senses")) {
      initializeVision = true;
      if (changed.system.traits.senses.ll) {
        refreshLighting = true;
      }
    } else if (changed.flags?.pf1?.visionSharing) {
      initializeVision = true;
      refreshLighting = true;
    }

    if (initializeVision || refreshLighting) {
      this.updateVision(initializeVision, refreshLighting);
    }

    // Handle base size change
    // TODO: Combine this test with testing updated items to reduce updates
    if (sourceUser && changed?.system?.traits?.size) {
      this._updateTokenSize();
    }
  }

  /**
   * Resize token sizes based on actor size.
   *
   * Ignores tokens with static size set.
   *
   * @todo Add option to update token size on all scenes.
   *
   * @internal
   * @param {string} [sizeKey] - Size key to update to. If not provided, will use actor's current size.
   * @returns {Promise<TokenDocument[]>|null} - Updated token documents, or null if no update was performed.
   * @throws {Error} - On invalid parameters
   */
  async _updateTokenSize(sizeKey = undefined) {
    if (!sizeKey) {
      const sizes = Object.keys(pf1.config.tokenSizes);
      sizeKey = sizes[this.getRollData({ refresh: true }).size];
      if (sizeKey === undefined) return;
    }

    const size = pf1.config.tokenSizes[sizeKey];
    if (!size) throw new Error(`Size key "${sizeKey}" is invalid`);
    const scene = canvas.scene;
    if (!scene) return null;

    // Get relevant tokens
    const tokens = this.token
      ? [this.token]
      : this.getActiveTokens(false, true).filter((token) => !token.getFlag("pf1", "staticSize"));

    const protoTexture = this.prototypeToken?.texture ?? {};

    const updates = tokens.map((t) => ({
      _id: t.id,
      width: size.w,
      height: size.h,
      texture: {
        scaleX: size.scale * (protoTexture.scaleX || 1),
        scaleY: size.scale * (protoTexture.scaleY || 1),
      },
    }));

    return TokenDocument.implementation.updateDocuments(updates, { parent: scene });
  }

  /**
   * @internal
   * @override
   * @param {Item|Actor} parent - Parent document
   * @param {"items"|"effects"} collection - Collection name
   * @param {Item[]|ActiveEffect[]} documents - Created documents
   * @param {object[]} result - Creation data for the documents
   * @param {object} context - Create context options
   * @param {string} userId - Triggering user's ID
   */
  _onCreateDescendantDocuments(parent, collection, documents, result, context, userId) {
    super._onCreateDescendantDocuments(...arguments);

    if (userId !== game.user.id) return;

    if (collection === "items") {
      // Apply race size to actor
      const race = documents.find((d) => d.type === "race");
      if (race?.system.size) {
        if (this.system.traits.size !== race.system.size) this.update({ "system.traits.size": race.system.size });
      }
    }

    if (collection === "effects") {
      if (context.pf1?.updateConditionTracks !== false) {
        this._handleConditionTracks(documents, context);
      }
    }
  }

  /**
   * Handle condition track toggling post active effect creation if there's still some issues.
   *
   * @internal
   * @param {ActiveEffect[]} documents - Updated active effect documents
   * @returns {Promise<object>} - Result from {@link ActorPF.setConditions}
   */
  async _handleConditionTracks(documents) {
    // Record of previously update conditions that didn't get notified about
    const previousConditions = {};

    const conditions = {};
    const tracks = pf1.registry.conditions.trackedConditions();
    for (const ae of documents) {
      for (const statusId of ae.statuses ?? []) {
        // Skip non-conditions
        if (!pf1.registry.conditions.has(statusId)) continue;

        // Mark this condition for notification
        previousConditions[statusId] = true;

        // Process condition tracks
        for (const conditionGroup of tracks) {
          if (!conditionGroup.includes(statusId)) continue;
          // Disable other conditions in the track
          for (const disableConditionId of conditionGroup) {
            if (disableConditionId === statusId) continue;
            conditions[disableConditionId] = false;
          }
        }
      }
    }

    this._conditionToggleNotify(previousConditions);

    if (!foundry.utils.isEmpty(conditions)) {
      return this.setConditions(conditions);
    }
  }

  /**
   * @internal
   * @override
   * @param {*} parent - Parent document
   * @param {"items"|"effects"} collection - Collection name
   * @param {Item|ActiveEffect[]} documents - Document array
   * @param {string[]} ids - Document ID array
   * @param {object} context - Delete context
   * @param {string} userId - User ID
   */
  _onDeleteDescendantDocuments(parent, collection, documents, ids, context, userId) {
    super._onDeleteDescendantDocuments(parent, collection, documents, ids, context, userId);

    if (collection === "effects") {
      const updatedConditions = {};
      for (const ae of documents) {
        for (const statusId of ae.statuses ?? []) {
          // Toggle off only if it's valid ID and there isn't any other AEs that have same condition still
          if (pf1.registry.conditions.has(statusId) && !this.statuses.has(statusId)) {
            updatedConditions[statusId] = false;
          }
        }
      }

      if (context?.pf1?.updateConditionTracks !== false) {
        this._conditionToggleNotify(updatedConditions);
      }
    }

    // Following process is done only on triggering user
    if (game.user.id !== userId) return;

    if (collection === "items") {
      this._cleanItemLinksTo(documents);

      // Delete child linked items
      const toRemove = new Set();

      // Remove linked children with item
      const _enumChildren = (item) => {
        toRemove.add(item.id);

        const links = item.getLinkedItemsSync("children");
        for (const link of links) {
          if (toRemove.has(link.id)) continue;
          const child = item.actor.items.get(link.id);
          if (child) _enumChildren(child);
        }
      };

      // Find children
      for (const item of documents) _enumChildren(item);
      // Remove already deleted items
      for (const id of ids) toRemove.delete(id);

      if (toRemove.size > 0) {
        this.deleteEmbeddedDocuments("Item", Array.from(toRemove));
      }
    }
  }

  /**
   * @internal
   * @param {pf1.documents.item.ItemPF[]} items - Item documents to clean links to.
   * @returns {Promise<Document[]>} - Updated documents
   */
  async _cleanItemLinksTo(items) {
    const updates = [];
    // Clean up references to this item
    for (const deleted of items) {
      const uuid = deleted.getRelativeUUID(this);
      for (const item of this.items) {
        const updateData = await item.removeItemLink(uuid, { commit: false });
        if (updateData) {
          updateData._id = item.id;
          updates.push(updateData);
        }
      }
    }

    if (updates.length) {
      return this.updateEmbeddedDocuments("Item", updates);
    }
  }

  /**
   * @todo - The condition notification needs to be smarter.
   *
   * @internal
   * @param {Record<string,boolean>} conditions - Condition toggle state
   */
  _conditionToggleNotify(conditions = {}) {
    for (const [conditionId, state] of Object.entries(conditions)) {
      Hooks.callAll("pf1ToggleActorCondition", this, conditionId, state);
    }
  }

  /**
   * @internal
   * @param {ItemPF} item - the item to add to the actor's resources
   * @param {object} [options] - extra options
   * @param {boolean} [options.warnOnDuplicate] - Skips warning if item tag already exists in dictionary flags
   * @returns {boolean} True if resources were set
   */

  updateItemResources(item, { warnOnDuplicate = true } = {}) {
    if (item.type === "spell") return false;
    if (item.links?.charges) return false; // Don't create resource for items that are inheriting charges
    if (!item.isCharged) return false;
    if (item.isSingleUse) return false;
    if (item.isPhysical) return false;

    const tag = item.system.tag;
    if (!tag) console.error("Attempting create resource on tagless item", item);

    if (warnOnDuplicate && this.system.resources[tag] && this.isOwner) {
      const msg = game.i18n.format("PF1.Warning.DuplicateTag", {
        actor: this.uuid,
        item: item.name,
        tag,
      });
      console.warn(msg, item);
      ui.notifications?.warn(msg, { console: false });
    }

    const res = new Resource(item);
    this.system.resources[tag] = res;

    return true;
  }

  /* -------------------------------------------- */
  /*  Rolls                                       */
  /* -------------------------------------------- */

  /**
   * Enable and configure a new spellbook.
   *
   * @example
   * Create spellbook for inquisitor
   * ```js
   * actor.createSpellbook({
   *   type: "spontaneous",
   *   progression: "med",
   *   ability: "wis",
   *   spells: "divine",
   *   class: "inquisitor",
   *   cantrips: true,
   *   domain: 0
   * });
   * ```
   *
   * @param {object} [casting] - Book casting configuration
   * @param {"prepared"|"spontaneous"|"hybrid"} [casting.type="prepared"] - Spellbook type
   * @param {"high"|"med"|"low"} [casting.progression="high"] - Casting progression type
   * @param {string} [casting.ability="int"] - Spellcasting ability score ID
   * @param {"arcane"|"divine"|"psychic"|"alchemy"} [casting.spells="arcane"] - Spell/spellcasting type
   * @param {string} [casting.class="_hd"] - Class tag
   * @param {boolean} [casting.cantrips=true] - Has cantrips?
   * @param {number} [casting.domain=1] - Domain/School slots
   * @param {number} [casting.offset] - Level offset
   * @param {object} [options] - Additional options
   * @param {boolean} [options.commit=true] - Commit modifications. If false, update data is returned instead of committing.
   * @returns {Promise<this>} - Promise to updated document
   */
  createSpellbook(casting = {}, { commit = true } = {}) {
    const books = this.system.attributes.spells.spellbooks ?? {};

    const oldBook = casting.class
      ? Object.entries(books).find(([_, book]) => !!book.class && book.class === casting.class)
      : null;

    let bookId;
    if (oldBook) {
      if (oldBook[1].inUse) return void ui.notifications.warn(game.i18n.localize("PF1.Error.SpellbookExists"));
      bookId = oldBook[0]; // Reuse old book
    } else {
      const available = Object.entries(books).find(([_, bookData]) => bookData.inUse !== true);
      if (available === undefined) return void ui.notifications.warn(game.i18n.localize("PF1.Error.NoFreeSpellbooks"));
      bookId = available[0];
    }

    // Add defaults when unconfigured
    // `class` causes problems if destructured, hence why it is here.
    casting.type ??= "prepared";
    casting.class ??= "_hd";
    casting.progression ??= "high";
    casting.spells ??= "arcane";
    casting.ability ??= "int";
    casting.cantrips ??= true;
    casting.domain ??= 1;
    casting.offset ??= 0;
    if (casting.offset !== 0) casting.offset = `${casting.offset}`;

    const updateData = {
      [`system.attributes.spells.spellbooks.${bookId}`]: {
        inUse: true,
        kind: casting.spells,
        class: casting.class,
        spellPreparationMode: casting.type,
        casterType: casting.progression,
        ability: casting.ability,
        psychic: casting.spells === "psychic",
        arcaneSpellFailure: casting.spells === "arcane",
        hasCantrips: casting.cantrips,
        domainSlotValue: casting.domain,
        "cl.formula": casting.offset ? `${casting.offset}` : "",
      },
    };

    if (commit) return this.update(updateData);
    else return updateData;
  }

  /* -------------------------------------------- */

  /**
   * Retrieve information about a skill.
   *
   * @example
   * ```js
   * actor.getSkillInfo("per"); // Perception skill info
   * actor.getSkillInfo("crf.alchemy"); // Craft (Alchemy) subskill info
   * ```
   *
   * @param {string} skillId - Skill ID
   * @param {object} [options] - Additional options
   * @param {{ skills: Record<string, SkillData>}} [options.rollData] - Roll data instance to use.
   * @throws {Error} - If defined skill is not found.
   * @returns {SkillInfo} - Skill information
   */
  getSkillInfo(skillId, { rollData } = {}) {
    const skillIdParts = skillId.split(".");
    if (skillIdParts.length > 2) skillIdParts.splice(1, 1);

    const mainSkillId = skillIdParts.shift(),
      subSkillId = skillIdParts.pop(),
      isSubSkill = !!subSkillId;

    // Reconstruct skillId with new shorter version to ensure format
    skillId = [mainSkillId, subSkillId].filterJoin(".");

    rollData ??= this.getRollData();
    const parentSkill = isSubSkill ? this.getSkillInfo(mainSkillId, { rollData }) : null;

    /** @type {SkillInfo} */
    const skill = subSkillId
      ? parentSkill.subSkills?.[subSkillId]
      : foundry.utils.deepClone(rollData.skills[mainSkillId]);

    if (!skill) throw new Error(`Invalid skill ID '${skillId}'`);

    skill.journal ||= pf1.config.skillCompendiumEntries[isSubSkill ? mainSkillId : skillId];
    skill.name ||= pf1.config.skills[skillId] || skillId;
    skill.id = skillId;

    if (isSubSkill) {
      skill.fullName = `${parentSkill.name} (${skill.name})`;
      skill.parentSkill = parentSkill;
    } else {
      skill.fullName = skill.name;
    }

    return skill;
  }

  /**
   * Roll a Skill Check
   *
   * @example
   * ```js
   * await actor.rollSkill("per", { skipDialog: true, bonus: "1d6", dice: "2d20kh" });
   * ```
   *
   * @param {string} skillId - The skill id (e.g. "per", "prf.prf1", or "crf.alchemy")
   * @param {ActorRollOptions} [options={}] - Options which configure how the skill check is rolled
   * @returns {Promise<ChatMessage|object|void>} - The chat message if one was created, or its data if not. `void` if the roll was cancelled.
   */
  async rollSkill(skillId, options = {}) {
    if (!this.isOwner) {
      return void ui.notifications.warn(game.i18n.format("PF1.Error.NoActorPermissionAlt", { name: this.name }));
    }

    const skillIdParts = skillId.split(".");
    const mainSkillId = skillIdParts[0],
      subSkillId = skillIdParts.length > 1 ? skillIdParts.at(-1) : null;
    // Reconstruct skill ID to ensure it is valid for everything else.
    skillId = subSkillId ? `${mainSkillId}.${subSkillId}` : mainSkillId;

    const skl = this.getSkillInfo(skillId);
    const haveParentSkill = !!subSkillId;

    // Add context notes
    const rollData = this.getRollData();
    const notes = await this.getContextNotesParsed(`skill.${skillId}`, { rollData });
    if (haveParentSkill)
      notes.push(...(await this.getContextNotesParsed(`skill.${mainSkillId}`, { rollData, all: false })));

    // Add untrained note
    if (skl.rt && !skl.rank) {
      notes.push({ text: game.i18n.localize("PF1.Untrained") });
    }

    // Add parts
    const parts = [];

    // Base parts
    // Ability damage and penalty are not part of change system
    const details = this.getSourceDetails(`system.abilities.${skl.ability}.mod`);
    for (const { value, name } of details) {
      parts.push(`${value}[${name}]`);
    }

    // Add Wound Thresholds info
    if (rollData.attributes.woundThresholds?.penalty > 0) {
      const label = pf1.config.woundThresholdConditions[rollData.attributes.woundThresholds.level];
      notes.push({ text: label });
      parts.push(`- @attributes.woundThresholds.penalty[${label}]`);
    }

    // Gather changes
    const skillDataPathPart = subSkillId ? `${mainSkillId}.subSkills.${subSkillId}` : mainSkillId;

    const validChanges = this.changes.filter((/** @type {ItemChange} */ c) =>
      c.getTargets(this).includes(`system.skills.${skillDataPathPart}.mod`)
    );
    const changes = getHighestChanges(validChanges, { ignoreTarget: true });

    // Add changes
    for (const c of changes) {
      if (!c.value) continue;
      // Hide complex change formulas in parenthesis.
      if (typeof c.value === "string" && RollPF.parse(c.value).length > 1) {
        parts.push(`(${c.value})[${c.flavor}]`);
      } else {
        parts.push(`${c.value}[${c.flavor}]`);
      }
    }

    // Get relevant conditions if any
    const conds = this._getContextConditions(`skill.${skillId}`);
    for (const cond of conds) notes.push({ text: cond.name });

    const props = [];
    if (notes.length > 0) props.push({ header: game.i18n.localize("PF1.Notes"), value: notes });

    const token = options.token ?? this.token;

    // Add metadata about the skill
    const metadata = { rank: skl.rank ?? 0 };
    if (["acr", "swm", "clm"].includes(skillId)) {
      const speeds = this.system.attributes?.speed ?? {};
      metadata.speed = { base: speeds.land?.total ?? 0 };
      if (skillId === "swm") metadata.speed.swim = speeds.swim?.total ?? 0;
      if (skillId === "clm") metadata.speed.climb = speeds.climb?.total ?? 0;
    }

    const rollOptions = {
      ...options,
      parts,
      rollData,
      flavor: game.i18n.format("PF1.Check", { type: skl.fullName }),
      chatTemplateData: { properties: props },
      compendium: { entry: pf1.config.skillCompendiumEntries[skillId] ?? skl.journal, type: "JournalEntry" },
      subject: { skill: skillId },
      speaker: ChatMessage.implementation.getSpeaker({ actor: this, token }),
      messageData: {
        system: {
          subject: { skill: skillId },
          config: metadata,
        },
      },
    };
    if (Hooks.call("pf1PreActorRollSkill", this, rollOptions, skillId) === false) return;
    const result = await pf1.dice.d20Roll(rollOptions);
    if (result) Hooks.callAll("pf1ActorRollSkill", this, result, skillId);
    return result;
  }

  /* -------------------------------------------- */

  /**
   * Roll basic BAB check
   *
   * @param {ActorRollOptions} [options] - Additional options
   * @returns {Promise<ChatMessage|object|void>} The chat message if one was created, or its data if not. `void` if the roll was cancelled.
   */
  async rollBAB(options = {}) {
    if (!this.isOwner) {
      return void ui.notifications.warn(game.i18n.format("PF1.Error.NoActorPermissionAlt", { name: this.name }));
    }

    const token = options.token ?? this.token;

    const rollOptions = {
      ...options,
      parts: [`${this.system.attributes.bab.total}[${game.i18n.localize("PF1.BABAbbr")}]`],
      subject: { core: "bab" },
      flavor: game.i18n.format("PF1.Check", { type: game.i18n.localize("PF1.BABAbbr") }),
      speaker: ChatMessage.implementation.getSpeaker({ actor: this, token }),
    };
    if (Hooks.call("pf1PreActorRollBab", this, rollOptions) === false) return;
    const result = await pf1.dice.d20Roll(rollOptions);
    Hooks.callAll("pf1ActorRollBab", this, result);
    return result;
  }

  /**
   * Roll a generic attack
   *
   * @example
   * Basic ranged attack
   * ```js
   * await actor.rollAttack({ ranged: true });
   * ```
   * Basic melee maneuver
   * ```js
   * await actor.rollAttack({ maneuver: true });
   * ```
   *
   * @param {ActorRollOptions} [options={}] - Roll options
   * @param {boolean} [options.maneuver=false] - Whether this is weapon or maneuver check.
   * @param {boolean} [options.ranged=false] - Melee or ranged.
   * @param {boolean} [options.ability=null] - Attack ability. If not defined, appropriate one is chosen based on the ranged option.
   * @returns {Promise<ChatMessage|object|void>} The chat message if one was created, or its data if not. `void` if the roll was cancelled.
   */
  async rollAttack({ maneuver = false, ranged = false, ability = null, ...options } = {}) {
    if (!this.isOwner) {
      return void ui.notifications.warn(game.i18n.format("PF1.Error.NoActorPermissionAlt", { name: this.name }));
    }

    if (maneuver && this.system.config?.noManeuvers) throw new Error("Manuevers are disabled for this actor.");

    let actionType;
    if (!maneuver) actionType = ranged ? "rwak" : "mwak";
    else actionType = ranged ? "rcman" : "mcman";

    const atkData = {
      name: !ranged ? game.i18n.localize("PF1.Melee") : game.i18n.localize("PF1.Ranged"),
      actionType,
    };

    // Alter attack ability
    const atkAbl = this.system.attributes?.attack?.[`${ranged ? "ranged" : "melee"}Ability`];
    atkData.ability ??= {};
    if (maneuver) atkData.ability.attack = ability || this.system.attributes?.cmbAbility;
    atkData.ability.attack ||= ability || atkAbl || (ranged ? "dex" : "str");

    // Alter activation type
    atkData.activation ??= {};
    atkData.activation.type = "attack";
    atkData.activation.unchained ??= {};
    atkData.activation.unchained.type = "attack";

    // Generate temporary item
    /** @type {pf1.documents.item.ItemAttackPF} */
    const atk = new Item.implementation(
      {
        type: "attack",
        name: !maneuver ? game.i18n.localize("TYPES.Item.weapon") : game.i18n.localize("PF1.CMBAbbr"),
        system: {
          actions: [new pf1.components.ItemAction(atkData).toObject()],
        },
      },
      { parent: this }
    );

    return atk.use(options);
  }

  /**
   * Roll a Caster Level check using a particular spellbook of this actor
   *
   * @example
   * Roll caster level check for primary spellbook.
   * ```js
   * await actor.rollCL("primary");
   * ```
   *
   * @param {string} bookId Spellbook identifier
   * @param {ActorRollOptions} [options={}] Roll options
   * @returns {Promise<ChatMessage|object|void>} The chat message if one was created, or its data if not. `void` if the roll was cancelled.
   */
  async rollCL(bookId, options = {}) {
    const spellbook = this.system.attributes.spells.spellbooks[bookId];
    const rollData = options.rollData ?? this.getRollData();
    rollData.cl = spellbook.cl.total;

    // Set up roll parts
    const parts = [];

    const describePart = (value, label) => parts.push(`${value}[${label}]`);
    const srcDetails = (s) => s?.reverse().forEach((d) => describePart(d.value, d.name, -10));
    srcDetails(this.getSourceDetails(`system.attributes.spells.spellbooks.${bookId}.cl.total`));

    // Add contextual caster level string
    const notes = await this.getContextNotesParsed(`spell.cl.${bookId}`, { rollData });

    // Wound Threshold penalty
    const wT = this.getWoundThresholdData();
    if (wT.valid) notes.push({ text: pf1.config.woundThresholdConditions[wT.level] });

    const props = [];
    if (notes.length > 0) props.push({ header: game.i18n.localize("PF1.Notes"), value: notes });

    const token = options.token ?? this.token;

    const rollOptions = {
      ...options,
      parts,
      rollData,
      subject: { core: "cl", spellbook: bookId },
      flavor: game.i18n.localize("PF1.CasterLevelCheck"),
      chatTemplateData: { properties: props },
      speaker: ChatMessage.implementation.getSpeaker({ actor: this, token }),
    };
    if (Hooks.call("pf1PreActorRollCl", this, rollOptions, bookId) === false) return;
    const result = await pf1.dice.d20Roll(rollOptions);
    Hooks.callAll("pf1ActorRollCl", this, result, bookId);
    return result;
  }

  /**
   * Roll a concentration check using a particular spellbook of this actor
   *
   * @todo Add support for concentration check type, e.g. defensive casting
   *
   * @param {string} bookId Spellbook identifier
   * @param {ActorRollOptions} [options={}] Roll options
   * @returns {Promise<ChatMessage|object|void>} The chat message if one was created, or its data if not. `void` if the roll was cancelled.
   */
  async rollConcentration(bookId, options = {}) {
    const spellbook = this.system.attributes.spells.spellbooks[bookId];
    const rollData = options.rollData ?? this.getRollData();
    rollData.cl = spellbook.cl.total;
    rollData.mod = this.system.abilities[spellbook.ability]?.mod ?? 0;

    if (
      Hooks.call("actorRoll", "pf1PreActorRollConcentration", undefined, this, "concentration", bookId, options) ===
      false
    )
      return;

    // Set up roll parts
    const parts = [];

    const describePart = (value, label) => parts.push(`${value}[${label}]`);
    const srcDetails = (s) => s?.reverse().forEach((d) => describePart(d.value, d.name, -10));
    srcDetails(this.getSourceDetails(`system.attributes.spells.spellbooks.${bookId}.concentration.total`));

    // Add contextual concentration string
    const notes = await this.getContextNotesParsed(`spell.concentration.${bookId}`, { rollData });

    // Wound Threshold penalty
    const wT = this.getWoundThresholdData();
    if (wT.valid) notes.push({ text: game.i18n.localize(pf1.config.woundThresholdConditions[wT.level]) });
    // TODO: Make the penalty show separate of the CL.total.

    const props = [];
    if (notes.length > 0) props.push({ header: game.i18n.localize("PF1.Notes"), value: notes });

    const token = options.token ?? this.token;

    const rollOptions = {
      ...options,
      parts,
      rollData,
      subject: { core: "concentration", spellbook: bookId },
      flavor: game.i18n.localize("PF1.ConcentrationCheck"),
      chatTemplateData: { properties: props },
      speaker: ChatMessage.implementation.getSpeaker({ actor: this, token }),
    };
    if (Hooks.call("pf1PreActorRollConcentration", this, rollOptions, bookId) === false) return;
    const result = await pf1.dice.d20Roll(rollOptions);
    Hooks.callAll("pf1ActorRollConcentration", this, result, bookId);
    return result;
  }

  /**
   * @protected
   * @param {object} [options] Additional options
   * @param {boolean} [options.damageResistances=true] If false, damage resistances (DR, ER) are omitted.
   * @param {boolean} [options.damageVulnerabilities=true] If false, damage vulnerabilities are omitted.
   * @returns {*} - Header data
   */
  getDefenseHeaders({ damageResistances = true, damageVulnerabilities = true } = {}) {
    const system = this.system;
    const headers = [];

    const reSplit = pf1.config.re.traitSeparator;
    const misc = [];

    if (damageResistances) {
      // Damage reduction
      if (system.traits.dr.length) {
        headers.push({
          header: game.i18n.localize("PF1.DamRed"),
          value: system.traits.dr.split(reSplit).map((text) => ({ text })),
        });
      }
      // Energy resistance
      if (system.traits.eres.length) {
        headers.push({
          header: game.i18n.localize("PF1.EnRes"),
          value: system.traits.eres.split(reSplit).map((text) => ({ text })),
        });
      }
    }
    if (damageVulnerabilities) {
      // Damage vulnerabilities
      if (system.traits.dv.total.size) {
        const value = system.traits.dv.names.map((text) => ({ text }));
        headers.push({ header: game.i18n.localize("PF1.DamVuln"), value });
      }
    }
    // Condition resistance
    if (system.traits.cres.length) {
      headers.push({
        header: game.i18n.localize("PF1.ConRes"),
        value: system.traits.cres.split(reSplit).map((text) => ({ text })),
      });
    }
    // Immunities
    if (system.traits.di.total.size || system.traits.ci.total.size) {
      const value = [...system.traits.di.names, ...system.traits.ci.names].map((text) => ({ text }));
      headers.push({ header: game.i18n.localize("PF1.ImmunityPlural"), value });
    }
    // Spell Resistance
    if (system.attributes.sr.total > 0) {
      misc.push({ text: game.i18n.format("PF1.SpellResistanceNote", { value: system.attributes.sr.total }) });
    }

    if (misc.length > 0) {
      headers.push({ header: game.i18n.localize("PF1.MiscShort"), value: misc });
    }

    return headers;
  }

  /**
   * Roll initiative for one or multiple Combatants associated with this actor.
   * If no combat exists, GMs have the option to create one.
   * If viewing a full Actor document, all Tokens which map to that actor will be targeted for initiative rolls.
   * If viewing a synthetic Token actor, only that particular Token will be targeted for an initiative roll.
   *
   * @example
   * ```js
   * await actor.rollInitiative({ dice: "2d20kh", createCombatants: true, skipDialog: true });
   * ```
   *
   * @override
   * @see {@link pf1.documents.CombatPF#rollInitiative}
   * @param {object} [options={}] Options which configure how initiative is rolled
   * @param {boolean} [options.createCombatants=false] - Create new Combatant entries for tokens associated with this actor.
   * @param {boolean} [options.rerollInitiative=false] - Reroll initiative for existing Combatants
   * @param {object} [options.initiativeOptions] - Options to pass to {@link CombatPF.rollInitiative()}
   * @param {string|null} [options.dice=null] - Formula override for dice to roll
   * @param {string|null} [options.bonus=null] - Formula for bonus to initiative
   * @param {boolean} [options.skipDialog] - Skip roll dialog
   * @param {string} [options.rollMode] - Roll mode override
   * @param {TokenDocumentPF} [options.token=this.token] - For which token this initiative roll is for
   * @returns {Promise<pf1.documents.CombatPF|null>} The updated Combat document in which initiative was rolled, or null if no initiative was rolled
   */
  async rollInitiative({
    createCombatants = false,
    rerollInitiative = false,
    initiativeOptions = {},
    dice = null,
    bonus = null,
    rollMode = null,
    skipDialog,
    token,
  } = {}) {
    token ||= this.token;

    // Obtain (or create) a combat encounter
    let combat = game.combat;
    if (!combat) {
      if (game.user.isGM) {
        const cls = getDocumentClass("Combat");
        combat = await cls.create({ scene: canvas.scene?.id, active: true });
      } else {
        ui.notifications.warn("COMBAT.NoneActive", { localize: true });
        return null;
      }
    }

    // Create new combatants
    if (createCombatants) {
      const tokens = this.isToken ? [this.token] : this.getActiveTokens().map((t) => t.document);
      const toCreate = [];
      if (tokens.length) {
        for (const t of tokens) {
          if (t.inCombat) continue;
          toCreate.push({ tokenId: t.id, sceneId: t.parent.id, actorId: this.id, hidden: t.hidden });
        }
      }
      // No tokens on scene
      else {
        const existing = combat.combatants.filter((t) => t.actor == this && !t.token);
        if (!existing.length) {
          toCreate.push({ actorId: this.id, hidden: false });
        }
      }

      if (toCreate.length) await combat.createEmbeddedDocuments("Combatant", toCreate);
    }

    let untokened = 0;
    // Roll initiative for combatants
    let combatants = combat.combatants.filter((c) => {
      if (c.actor?.id !== this.id) return false;
      if (token && c.token?.id !== token.id) return false;
      if (!c.token) untokened += 1;
      return rerollInitiative || c.initiative === null;
    });

    // If more than one relevant combatants with no token present, prune list of valid combatants.
    if (untokened > 1) {
      combatants = combatants.filter((c) => !!c.token || c.initiative === null);
      if (combatants.length == 0) ui.notifications.warn(game.i18n.localize("PF1.Error.NoInitOnDuplicateCombatant"));
    }

    // No combatants. Possibly from reroll being disabled.
    if (combatants.length == 0) return combat;

    foundry.utils.mergeObject(initiativeOptions, { d20: dice, bonus, rollMode, skipDialog });

    return combat.rollInitiative(
      combatants.map((c) => c.id),
      initiativeOptions
    );
  }

  /**
   * Roll a specific saving throw
   *
   * @example
   * ```js
   * await actor.rollSavingThrow("ref", { skipDialog: true, dice: "2d20kh", bonus: "4" });
   * ```
   *
   * @param {SaveId} savingThrowId Identifier for saving throw type.
   * @param {ActorRollOptions} [options={}] Roll options.
   * @returns {Promise<ChatMessage|object|void>} The chat message if one was created, or its data if not. `void` if the roll was cancelled.
   */
  async rollSavingThrow(savingThrowId, options = {}) {
    if (!this.isOwner) {
      return void ui.notifications.warn(game.i18n.format("PF1.Error.NoActorPermissionAlt", { name: this.name }));
    }

    // Add contextual notes
    const rollData = this.getRollData();
    const notes = await this.getContextNotesParsed(`savingThrow.${savingThrowId}`, { rollData });

    const parts = [];

    // Get base
    const base = this.system.attributes.savingThrows[savingThrowId]?.base;
    if (base) parts.push(`${base}[${game.i18n.localize("PF1.Base")}]`);

    // Add changes
    let changeBonus = [];
    const changes = this.changes.filter((c) => ["allSavingThrows", savingThrowId].includes(c.target));
    {
      // Get damage bonus
      changeBonus = getHighestChanges(
        changes.filter((c) => c.operator !== "set"),
        { ignoreTarget: true }
      ).reduce((cur, c) => {
        if (c.value)
          cur.push({
            value: c.value,
            source: c.flavor,
          });
        return cur;
      }, []);
    }
    for (const c of changeBonus) {
      parts.push(`${c.value}[${c.source}]`);
    }

    // Wound Threshold penalty
    if (rollData.attributes.woundThresholds.penalty > 0) {
      const label = pf1.config.woundThresholdConditions[rollData.attributes.woundThresholds.level];
      notes.push({ text: label });
      parts.push(`- @attributes.woundThresholds.penalty[${label}]`);
    }

    // Get relevant conditions if any
    const conds = this._getContextConditions(`savingThrow.${savingThrowId}`);
    for (const cond of conds) notes.push({ text: cond.name });

    const props = this.getDefenseHeaders({ damageResistances: false, damageVulnerabilities: false });
    if (notes.length > 0) props.push({ header: game.i18n.localize("PF1.Notes"), value: notes });
    const label = pf1.config.savingThrows[savingThrowId];

    const token = options.token ?? this.token;

    const rollOptions = {
      ...options,
      parts,
      rollData,
      flavor: game.i18n.format("PF1.SavingThrowRoll", { save: label }),
      subject: { save: savingThrowId },
      chatTemplateData: { properties: props },
      speaker: ChatMessage.implementation.getSpeaker({ actor: this, token }),
    };

    if (Hooks.call("pf1PreActorRollSave", this, rollOptions, savingThrowId) === false) return;

    // Roll saving throw
    const result = await pf1.dice.d20Roll(rollOptions);
    Hooks.callAll("pf1ActorRollSave", this, result, savingThrowId);

    return result;
  }

  /* -------------------------------------------- */

  /**
   * Roll an Ability Test
   * Prompt the user for input regarding Advantage/Disadvantage and any Situational Bonus
   *
   * @example
   * ```js
   * await actor.rollAbilityTest("str");
   * ```
   *
   * @param {AbilityId} abilityId - The ability ID (e.g. "str")
   * @param {ActorRollOptions} [options={}] - Additional options
   * @returns {Promise<ChatMessage|object|void>} The chat message if one was created, or its data if not. `void` if the roll was cancelled.
   */
  async rollAbilityTest(abilityId, options = {}) {
    if (!this.isOwner) {
      return void ui.notifications.warn(game.i18n.format("PF1.Error.NoActorPermissionAlt", { name: this.name }));
    }

    // Add contextual notes
    const rollData = options.rollData || this.getRollData();
    const notes = await this.getContextNotesParsed(`abilityChecks.${abilityId}`, { rollData });

    const label = pf1.config.abilities[abilityId] ?? abilityId;

    const parts = [`@abilities.${abilityId}.mod[${label}]`];

    // Base parts
    // Ability damage and penalty are not part of change system
    const details = this.getSourceDetails(`system.abilities.${abilityId}.mod`);
    for (const { value, name } of details) {
      parts.push(`${value}[${name}]`);
    }

    // Gather changes
    const dataPathCheck = `system.abilities.${abilityId}.checkMod`;

    const changes = getHighestChanges(
      this.changes.filter((/** @type {ItemChange} */ c) => {
        const cf = c.getTargets(this);

        return cf.includes(dataPathCheck);
      }),
      { ignoreTarget: true }
    );

    // Add changes
    for (const c of changes) {
      if (!c.value) continue;
      // Hide complex change formulas in parenthesis.
      if (typeof c.value === "string" && RollPF.parse(c.value).length > 1) {
        parts.push(`(${c.value})[${c.flavor}]`);
      } else {
        parts.push(`${c.value}[${c.flavor}]`);
      }
    }

    // Add negative levels
    if (this.system.attributes.energyDrain) {
      const label = game.i18n.localize("PF1.NegativeLevels");
      parts.push(`-@attributes.energyDrain[${label}]`);
    }

    // Wound Threshold penalty
    if (rollData.attributes.woundThresholds.penalty > 0) {
      const label = pf1.config.woundThresholdConditions[rollData.attributes.woundThresholds.level];
      notes.push({ text: label });
      parts.push(`-@attributes.woundThresholds.penalty[${label}]`);
    }

    const props = [];
    if (notes.length > 0) props.push({ header: game.i18n.localize("PF1.Notes"), value: notes });

    const token = options.token ?? this.token;

    const rollOptions = {
      ...options,
      parts,
      rollData,
      flavor: game.i18n.format("PF1.Check", { type: label }),
      subject: { ability: abilityId },
      chatTemplateData: { properties: props },
      speaker: ChatMessage.implementation.getSpeaker({ actor: this, token }),
    };

    if (Hooks.call("pf1PreActorRollAbility", this, rollOptions, abilityId) === false) return;

    const result = await pf1.dice.d20Roll(rollOptions);

    Hooks.callAll("pf1ActorRollAbility", this, result, abilityId);

    return result;
  }

  /**
   * Show defenses in chat
   *
   * @param {object} [options={}] Additional options
   * @param {string | null} [options.rollMode=null]   The roll mode to use for the roll; defaults to the user's current preference when `null`.
   * @param {TokenDocument} [options.token] Relevant token if any.
   * @returns {ChatMessage|undefined} - Created message
   */
  async displayDefenseCard({ rollMode = null, token } = {}) {
    if (!this.isOwner) {
      return void ui.notifications.warn(game.i18n.format("PF1.Error.NoActorPermissionAlt", { name: this.name }));
    }
    const rollData = this.getRollData();

    const formatTextNotes = (notes) => notes?.split(/[\n\r]+/).map((text) => ({ text })) ?? [];

    // Add contextual AC notes
    const acNotes = await this.getContextNotesParsed("ac", { rollData });
    if (this.system.attributes.acNotes) acNotes.push(...formatTextNotes(this.system.attributes.acNotes));

    // Add contextual CMD notes
    const cmdNotes = await this.getContextNotesParsed("cmd", { rollData });
    if (this.system.attributes.cmdNotes) cmdNotes.push(...formatTextNotes(this.system.attributes.cmdNotes));

    // Add contextual SR notes
    const srNotes = await this.getContextNotesParsed("sr", { rollData });
    if (this.system.attributes.srNotes) srNotes.push(...formatTextNotes(this.system.attributes.srNotes));

    // BUG: No specific saving throw notes are included
    const saveNotes = await this.getContextNotesParsed("allSavingThrows", { rollData });
    if (this.system.attributes.saveNotes) saveNotes.push(...formatTextNotes(this.system.attributes.saveNotes));

    // Add misc data

    // Damage Reduction
    const drNotes = Object.values(this.parseResistances("dr")).map((text) => ({ text }));

    // Energy Resistance
    const erNotes = Object.values(this.parseResistances("eres")).map((text) => ({ text }));

    // Damage Immunity
    // BUG: All incorrectly clumped with ER
    if (this.system.traits.di.total.size) {
      const values = [...this.system.traits.di.names];
      erNotes.push(...values.map((o) => ({ text: game.i18n.format("PF1.ImmuneTo", { immunity: o }) })));
    }
    // Damage Vulnerability
    if (this.system.traits.dv.total.size) {
      const values = [...this.system.traits.dv.names];
      erNotes.push(...values.map((o) => ({ text: game.i18n.format("PF1.VulnerableTo", { vulnerability: o }) })));
    }

    // Conditions
    const conditions = Object.entries(this.system.conditions ?? {})
      .filter(([_, enabled]) => enabled)
      .map(([id]) => pf1.registry.conditions.get(id))
      .filter((c) => c?.showInDefense)
      .map((c) => ({ text: c.name }));

    // Wound Threshold penalty
    const wT = this.getWoundThresholdData();
    if (wT.valid) {
      const wTlabel = pf1.config.woundThresholdConditions[wT.level];
      acNotes.push({ text: wTlabel });
      cmdNotes.push({ text: wTlabel });
    }

    // Get actor's token
    token ??= this.token;

    // Create message
    const actorData = this.system;
    const templateData = {
      actor: this,
      name: token?.name ?? this.name,
      tokenUuid: token?.uuid ?? null,
      ac: {
        normal: actorData.attributes.ac.normal.total,
        touch: actorData.attributes.ac.touch.total,
        flatFooted: actorData.attributes.ac.flatFooted.total,
        notes: acNotes,
      },
      cmd: {
        normal: actorData.attributes.cmd.total,
        flatFooted: actorData.attributes.cmd.flatFootedTotal,
        notes: cmdNotes,
      },
      misc: {
        hardness: actorData.traits.hardness,
        sr: actorData.attributes.sr.total,
        srNotes,
        drNotes,
        erNotes,
        conditions,
      },
      saves: {
        fort: rollData.attributes?.savingThrows?.fort?.total,
        ref: rollData.attributes?.savingThrows?.ref?.total,
        will: rollData.attributes?.savingThrows?.will?.total,
        notes: saveNotes,
      },
    };

    if (this.system.config?.noManeuvers) {
      delete templateData.cmd;
    }

    // Add regeneration and fast healing
    if ((actorData.traits?.fastHealing || "").length || (actorData.traits?.regen || "").length) {
      templateData.regen = {
        regen: actorData.traits.regen,
        fastHealing: actorData.traits.fastHealing,
      };
    }

    rollMode ||= game.settings.get("core", "rollMode");

    const chatData = {
      content: await renderTemplate("systems/pf1/templates/chat/defenses.hbs", templateData),
      speaker: ChatMessage.implementation.getSpeaker({ actor: this, token }),
      rollMode,
      system: {
        subject: { info: "defenses" },
      },
      flags: {
        core: {
          canPopout: true,
        },
      },
    };

    // Apply roll mode
    ChatMessage.implementation.applyRollMode(chatData, rollMode);

    return ChatMessage.implementation.create(chatData);
  }

  /**
   * Easy way to toggle a condition.
   *
   * @example
   * Toggle Dazzled
   * ```js
   * await actor.toggleCondition("dazzled");
   * ```
   * Toggle Blinded, set duration for 3 rounds if it's being enabled.
   * ```js
   * await actor.toggleCondition("blinded", { duration: { seconds: 18 } });
   * ```
   *
   * @param {boolean} conditionId - A direct condition key, as per {@link pf1.registry.conditions}, such as `shaken` or `dazed`.
   * @param {object} [aeData] - Extra data to add to the AE if it's being enabled
   * @returns {object} Condition ID to boolean mapping of actual updates.
   */
  async toggleCondition(conditionId, aeData) {
    let active = !this.statuses.has(conditionId);
    if (active && aeData) active = aeData;
    return this.setCondition(conditionId, active);
  }

  /**
   * Easy way to set a condition.
   *
   * @example
   * Enable Dazzled
   * ```js
   * await actor.setCondition("dazzled", true);
   * ```
   * Enable Sleep for 10 rounds
   * ```js
   * await actor.setCondition("sleep", { duration: { seconds: 60 } });
   * ```
   *
   * @param {string} conditionId - A direct condition key, as per {@link pf1.registry.conditions}, such as `shaken` or `dazed`.
   * @param {object|boolean} enabled - Whether to enable (true) the condition, or disable (false) it. Or object for merging into the active effect as part of enabling.
   * @param {object} [context] Update context
   * @returns {object} Condition ID to boolean mapping of actual updates.
   */
  async setCondition(conditionId, enabled, context) {
    if (typeof enabled !== "boolean" && foundry.utils.getType(enabled) !== "Object")
      throw new TypeError("Actor.setCondition() enabled state must be a boolean or plain object");
    return this.setConditions({ [conditionId]: enabled }, context);
  }

  /**
   * Set state of multiple conditions.
   * Also handles condition tracks to minimize number of updates.
   *
   * @example
   * Enable Blinded and Shaken conditions but disable Sleeping
   * ```js
   * await actor.setConditions({ blind: true, sleep: false, shaken:true });
   * ```
   *
   * @param {object} conditions Condition ID to boolean (or update data) mapping of new condition states. See {@link setCondition()}
   * @param {object} [context] Update context
   * @returns {Record<string,boolean>} Condition ID to boolean mapping of actual updates.
   */
  async setConditions(conditions = {}, context = {}) {
    conditions = foundry.utils.deepClone(conditions);

    // Handle Condition tracks
    const tracks = pf1.registry.conditions.trackedConditions();
    for (const conditionGroup of tracks) {
      const newTrackState = conditionGroup.find((c) => conditions[c] === true);
      if (!newTrackState) continue;
      const disableTrackEntries = conditionGroup.filter((c) => c !== newTrackState);
      for (const key of disableTrackEntries) {
        conditions[key] = false;
      }
    }

    // Create update data
    const toDelete = [],
      toCreate = [];

    const immunities = this.getConditionImmunities();

    for (const [conditionId, value] of Object.entries(conditions)) {
      const currentCondition = pf1.registry.conditions.get(conditionId);
      if (currentCondition === undefined) {
        console.error("Unrecognized condition:", conditionId);
        delete conditions[conditionId];
        continue;
      }

      if (value === true && immunities.has(conditionId)) {
        console.warn("Actor is immune to condition:", conditionId, this);
        delete conditions[conditionId];
        continue;
      }

      // BUG: Only finds conditions directly on actor, not on items
      const oldAe = this.statuses.has(conditionId) ? this.effects.find((ae) => ae.statuses.has(conditionId)) : null;

      // Create
      if (value) {
        if (!oldAe) {
          const aeData = {
            flags: {
              pf1: {
                autoDelete: true,
              },
            },
            statuses: [conditionId],
            name: currentCondition.name,
            img: currentCondition.texture,
          };

          // Special boolean for easy overlay
          if (value?.overlay) {
            delete value.overlay;
            foundry.utils.setProperty(aeData.flags, "core.overlay", true);
          }

          if (typeof value !== "boolean") {
            foundry.utils.mergeObject(aeData, value);
          }

          toCreate.push(aeData);
        } else {
          delete conditions[conditionId];
        }
      }
      // Delete
      else {
        if (oldAe) {
          toDelete.push(oldAe.id);
        } else {
          delete conditions[conditionId];
        }
      }
    }

    // Perform updates
    // Inform update handlers they don't need to do work
    context.pf1 ??= {};
    context.pf1.updateConditionTracks = false;

    if (toDelete.length) {
      const deleteContext = foundry.utils.deepClone(context);
      // Prevent double render
      if (context.trender && toCreate.length) deleteContext.render = false;
      // Without await the deletions may not happen at all, presumably due to race condition, if AEs are also created.
      await this.deleteEmbeddedDocuments("ActiveEffect", toDelete, context);
    }
    if (toCreate.length) {
      const createContext = foundry.utils.deepClone(context);
      await this.createEmbeddedDocuments("ActiveEffect", toCreate, createContext);
    }

    this._conditionToggleNotify(conditions);

    return conditions;
  }

  /**
   * Easy way to determine whether this actor has a condition.
   *
   * @example
   * Test if user is grappled
   * ```js
   * actor.hasCondition("grappled");
   * ```
   *
   * @deprecated This is identical to `actor.statuses.has("conditionId")`
   * @param {string} conditionId - A direct condition key, as per pf1.registry.conditions, such as `shaken` or `dazed`.
   * @returns {boolean} Condition state
   */
  hasCondition(conditionId) {
    foundry.utils.logCompatibilityWarning(
      "ActorPF.hasCondition() is deprecated in favor of actor.statuses.has(conditionId)",
      {
        since: "PF1 v11",
        until: "PF1 v13",
      }
    );
    return this.statuses.has(conditionId);
  }

  /**
   * Get active conditions marked for context
   *
   * @internal
   * @param {string} context - Context ID
   * @returns {Array<pf1.registry.conditions.model>} - Array of conditions
   */
  _getContextConditions(context) {
    const conditions = [];
    for (const conditionId of this.statuses) {
      const condition = pf1.registry.conditions.get(conditionId);
      if (!condition) continue;
      if (condition.mechanics.contexts.has(context)) conditions.push(condition);
    }
    return conditions;
  }

  /* -------------------------------------------- */

  /**
   * Helper function for actor energy resistance and damage reduction feedback.
   *
   * @protected
   * @param {"dr"|"eres"} resistanceType Value to check resistances for.
   * @returns {Record<string,string>} Entry to label mapping of resistances or reductions.
   */
  parseResistances(resistanceType) {
    /** @type {{custom:string, value: {amount:number,operator:string,types:string[]}[]}} */
    const res = this.system.traits?.[resistanceType];
    if (!res) return {}; // Either actor is malformed, or it's a type that doesn't have this info

    const result = {};
    if (Array.isArray(res.value)) {
      res.value.forEach((entry, counter) => {
        const res = this._parseResistanceEntry(entry, resistanceType);
        result[`${counter + 1}`] = res.label;
      });
    }

    if (typeof res.custom === "string") {
      res.custom.split(pf1.config.re.traitSeparator).forEach((entry, counter) => {
        entry = entry?.trim();
        if (!entry?.length) return;
        const res = this._parseResistanceEntry(entry, resistanceType);
        result[`custom${counter + 1}`] = res.label;
      });
    }

    return result;
  }

  /**
   * Parse resistance entry
   *
   * Primarily called from {@link parseResistances}
   *
   * @internal
   * @experimental
   * @param {object|string} entry
   * @param {"dr"|"er"} [resistanceType]
   * @returns {ParsedResistanceEntry}
   */
  _parseResistanceEntry(entry, resistanceType = "dr") {
    const format = (amount, type1, operator = null, type2 = null) => {
      let label = type1;
      if (type2) {
        switch (operator) {
          case false: {
            // Combine with AND
            label = game.i18n.format("PF1.Application.DamageResistanceSelector.CombinationFormattedAnd", {
              type1,
              type2,
            });
            break;
          }
          default:
          case true: {
            // Combine with OR
            label = game.i18n.format("PF1.Application.DamageResistanceSelector.CombinationFormattedOr", {
              type1,
              type2,
            });
            break;
          }
        }
      }

      return resistanceType === "dr" ? `${amount}/${label}` : `${label} ${amount}`;
    };

    const getType = (typeId) =>
      pf1.registry.damageTypes.get(typeId) ??
      pf1.registry.materials.get(typeId) ?? { name: pf1.config.damageResistances[typeId] ?? typeId };

    const isDR = resistanceType == "dr";
    const isER = !isDR;

    // Parse custom entry
    if (typeof entry === "string") {
      const re = /(?<value>\d+)/.exec(entry);
      const amount = parseInt(re?.groups.value || "0");
      const type = entry.replace(/\d+\s*\/?/, "").trim();

      return {
        type0: { name: type },
        amount,
        label: format(amount, type || "–"),
        isDR,
        isER,
        custom: true,
      };
    }
    // Parse normal entry
    else {
      const { amount, operator } = entry;
      const [typeId0, typeId1] = entry.types;

      const type0 = getType(typeId0);
      const type0name = type0?.shortName || type0?.name;
      const type1 = getType(typeId1);
      const type1name = type1?.shortName || type1?.name;

      return {
        type0: { id: typeId0, name: type0name, data: type0 },
        type1: { id: typeId1, name: type1name, data: type1 },
        operator,
        amount,
        label: format(amount, type0name || "-", operator, type1name),
        isDR,
        isER,
      };
    }
  }

  /**
   * Wrapper for the static function, taking this actor as the only target.
   *
   * @see {@link ActorPF.applyDamage}
   *
   * @example
   * Cause 10 damage
   * ```js
   * await actor.applyDamage(10);
   * ```
   * Heal 10 damage
   * ```js
   * await actor.applyDamage(-10);
   * ```
   * Apply 3 damage directly to Wounds instead of Vigor
   * ```js
   * await actor.applyDamage(3, { asWounds: true });
   * ```
   *
   * @param {number} value - Value to adjust health by. Positive values deal damage, negative values heal.
   * @param {ApplyDamageOptions} [options] Additional options. This object will be transformed in-place as the damage application is processed.
   * @returns {Promise<Actor|false>} - Updated actor or false.
   */
  async applyDamage(value, options = {}) {
    const {
      asWounds = false,
      asNonlethal = false,
      critMult,
      dualHeal = game.settings.get("pf1", "dualHeal"),
      reduction = 0,
      ratio = 1,
    } = options;
    const isHealing = value < 0;

    // Apply ratio and reduction
    value = Math.floor(value * ratio);
    if (value < 0) value += Math.min(-value, reduction);
    else value -= Math.min(value, reduction);
    value = Math.floor(value); // Round again just for sake of sanity

    console.debug("PF1 | Apply Damage |", value, "to", this.name, `[${this.uuid}]`);

    /** @type {pf1.applications.settings.HealthConfigModel} */
    const healthConfig = game.settings.get("pf1", "healthConfig");

    if (value == 0) return void console.warn("Attempting to apply 0 damage."); // Early exit

    if (!this.isOwner) {
      ui.notifications.warn(game.i18n.format("PF1.Error.NoActorPermissionAlt", { name: this.name }));
      return;
    }

    const useWoundsAndVigor = healthConfig.getActorConfig(this).rules?.useWoundsAndVigor ?? false,
      hp = !useWoundsAndVigor ? this.system.attributes.hp : this.system.attributes.vigor;
    // Cancel if no health data is found
    if (!hp) return false;

    const tmp = hp.temp || 0;

    const updateData = {};

    // Wounds & Vigor
    if (useWoundsAndVigor) {
      const currentHealth = hp.value;
      let woundAdjust = 0;

      if (asWounds) {
        woundAdjust -= value;
        value = 0;
      }

      // Temp HP adjustment
      const dt = value > 0 ? Math.min(tmp, value) : 0;
      value -= dt;

      // Nonlethal damage
      if (asNonlethal && value > 0) {
        if (currentHealth > 0) {
          value = Math.min(currentHealth, value);
        } else {
          woundAdjust -= critMult > 1 ? critMult : 1;
          value = 0; // No other bleedover to wounds
        }
      }

      // Create update data
      if (dt != 0) updateData["system.attributes.vigor.temp"] = tmp - dt;
      if (value != 0) {
        let newHP = Math.min(currentHealth - value, hp.max);
        if (value > 0) {
          if (newHP < 0) {
            woundAdjust -= -newHP;
            if (critMult > 0) woundAdjust -= critMult;
            newHP = 0;
          }
        }

        if (newHP != hp.value) updateData["system.attributes.vigor.value"] = newHP;
      }

      if (woundAdjust != 0) {
        const wounds = this.system.attributes.wounds;
        updateData["system.attributes.wounds.value"] = Math.clamp(wounds.value + woundAdjust, 0, wounds.max);
      }
    }
    // Normal Hit Points
    else {
      // Nonlethal damage
      let nld = 0;
      if (asNonlethal) {
        if (value > 0) {
          nld = Math.min(hp.max - hp.nonlethal, value);
          value -= nld;
        }
        // Nonlethal healing
        else if (value < 0) {
          nld = value;
          value = 0;
        }
      }
      // Dual healing heals also nonlethal
      else if (isHealing && dualHeal) {
        nld = value;
      }

      // Temp HP adjustment
      const dt = !isHealing ? Math.min(tmp, value) : 0;

      // Create update data
      if (nld != 0) updateData["system.attributes.hp.nonlethal"] = Math.max(0, hp.nonlethal + nld);
      if (dt != 0) updateData["system.attributes.hp.temp"] = tmp - dt;
      const newHp = Math.min(hp.value - (value - dt), hp.max);
      if (newHp != hp.value) updateData["system.attributes.hp.value"] = newHp;
    }

    return this.update(updateData);
  }

  /**
   * Apply damage to the token or tokens which are currently controlled.
   *
   * If Shift is held, will prompt for adjustments based on damage reduction and energy resistances.
   *
   * @param {number} value - The amount of damage to deal. Negative values heal instead.
   * @param {TargetedApplyDamageOptions} [options] - Object containing default settings for overriding
   * @throws {Error} - If no valid targets are provided.
   * @returns {Promise<false|Actor[]>} - False if cancelled or array of updated actors.
   */
  static async applyDamage(value = 0, options = {}) {
    // Get targets if none are provided
    if (!options.targets?.length) options.targets = canvas.tokens.controlled;
    if (!options.targets?.length && game.user.character) options.targets = [game.user.character];

    // Make sure targets are all actors
    options.targets = options.targets.map((t) => t.actor || t).filter((t) => t instanceof Actor);

    console.debug("PF1 | Apply Damage |", value, "to", options.targets.length, "target(s)");

    if (options.targets.length === 0) throw new Error("No valid targets");

    if (value == 0 || !Number.isFinite(value)) {
      console.warn("Attempting to apply 0 damage.");
      return false;
    }

    // HACK: Infer nonlethal
    if (options.instances?.length && options.asNonlethal === undefined) {
      options.asNonlethal = options.instances.every((t) => t.types.has("nonlethal"));
      if (options.asNonlethal) console.debug("PF1 | Damage inferred as nonlethal");
    }

    // Apply defaults
    options.asNonlethal ??= false;
    options.critMult ??= 0;
    options.asWounds ??= false;
    options.forceDialog ??= false;

    if (options.reductionDefault !== undefined) {
      foundry.utils.logCompatibilityWarning(
        "ActorPF.applyDamage() reductionDefault option is deprecated in favor of reduction.",
        {
          since: "PF1 v11",
          until: "PF1 v12",
        }
      );

      options.reduction = options.reductionDefault;
    }

    // Dialog skipped
    if (pf1.skipConfirmPrompt ? options.forceDialog : !options.forceDialog) {
      const promises = options.targets.map((t) => t.applyDamage(value, options));
      return Promise.all(promises);
    }

    // Open dialog
    options.value = value;

    // TODO: This does not return updated documents
    const rv = await pf1.applications.ApplyDamage.wait(options);
    if (!rv) return false;
    return rv.updated;
  }

  /**
   * Adjust temporary hit points.
   *
   * @example
   * Gain 50 THP
   * ```js
   * actor.addTempHP(50);
   * ```
   * Lose 10 THP
   * ```js
   * actor.addTempHP(-10);
   * ```
   * Set THP to zero
   * ```js
   * actor.addTempHP(0, { set: true });
   * ```
   *
   * @param {number} value - Value to add to temp HP
   * @param {object} [options] - Additional options
   * @param {boolean} [options.set] - If true, the temporary hit points are set to the provide value instead of added to existing.
   * @returns {Promise<this|undefined>} - Updated document or undefined if no update occurred
   */
  async addTempHP(value, { set = false } = {}) {
    /** @type {pf1.applications.settings.HealthConfigModel} */
    const healthConfig = game.settings.get("pf1", "healthConfig");
    const hpActorConfig = healthConfig.getActorConfig(this);
    const vigor = hpActorConfig.rules.useWoundsAndVigor;

    const curTHP = (vigor ? this.system.attributes.vigor.temp : this.system.attributes.hp.temp) || 0;
    const newTHP = Math.max(0, !set ? curTHP + value : value);

    return this.update({ system: { attributes: { [vigor ? "vigor" : "hp"]: { temp: newTHP } } } });
  }

  /**
   * Returns effective Wound Threshold multiplier with rules and overrides applied.
   *
   * @protected
   * @param {object} [options] - Additional options
   * @param {object} [options.healthConfig] - PC/NPC health config variant data
   * @returns {number} Multiplier
   */
  getWoundThresholdMultiplier({ healthConfig } = {}) {
    healthConfig ??= game.settings.get("pf1", "healthConfig").getActorConfig(this);

    return healthConfig.rules.useWoundThresholds;
  }

  /**
   * Returns Wound Threshold relevant data.
   *
   * @protected
   * @param {object} [options] - Additional options
   * @param {object} [options.healthConfig] - PC/NPC health config variant data
   * @returns {{level:number,penalty:number,multiplier:number,valid:boolean}} - Wound threshold info
   */
  getWoundThresholdData({ healthConfig } = {}) {
    healthConfig ??= game.settings.get("pf1", "healthConfig").getActorConfig(this);

    /** @type {{level:number,mod:number}} */
    const wt = this.system.attributes?.woundThresholds ?? {};

    const woundMult = this.getWoundThresholdMultiplier({ healthConfig }),
      woundLevel = wt.level || 0,
      woundPenalty = woundLevel * woundMult + (wt.mod || 0);

    return {
      level: woundLevel,
      penalty: woundPenalty,
      multiplier: woundMult,
      valid: woundLevel > 0 && woundMult > 0,
    };
  }

  /**
   * Updates attributes.woundThresholds.level variable.
   *
   * @protected
   */
  updateWoundThreshold() {
    /** @type {pf1.applications.settings.HealthConfigModel} */
    const hpConfig = game.settings.get("pf1", "healthConfig");
    const hpActorConfig = hpConfig.getActorConfig(this);
    const usage = hpActorConfig.rules.useWoundThresholds;
    const vigor = hpActorConfig.rules.useWoundsAndVigor;

    /** @type {{mod:number,penalty:number}} */
    const wt = this.system.attributes.woundThresholds;
    // Null if WT is not in use, or it is combined with Wounds & Vigor
    if (!usage || vigor) {
      wt.level = 0;
      wt.penaltyBase = 0;
      wt.penalty = 0;
      wt.mod = 0;
      return;
    }
    /** @type {{value:number,temp:number,max:number}} */
    const hp = this.system.attributes.hp,
      curHP = hp.value,
      tempHP = hp.temp ?? 0,
      maxHP = hp.max;

    let level = usage > 0 ? Math.clamp(4 - Math.ceil(((curHP + tempHP) / maxHP) * 4), 0, 3) : 0;
    if (Number.isNaN(level)) level = 0; // Division by 0 due to max HP on new actors.

    const wtMult = this.getWoundThresholdMultiplier({ healthConfig: hpActorConfig });
    const wtMod = wt.mod ?? 0;

    wt.level = level;
    wt.penaltyBase = level * wtMult; // To aid relevant formulas
    wt.penalty = level * wtMult + wtMod;

    const penalty = wt.penalty;
    // TODO: Convert to changes
    if (penalty != 0) {
      const changeFlatKeys = pf1.config.woundThresholdChangeTargets;
      for (const fk of changeFlatKeys) {
        const flats = getChangeFlat(this, fk, "untyped", -penalty);
        for (const k of flats) {
          if (!k) continue;
          const curValue = foundry.utils.getProperty(this, k) ?? 0;
          foundry.utils.setProperty(this, k, curValue - penalty);
        }
      }

      // Soft add change for attacks
      const ch = new pf1.components.ItemChange({
        _id: "woundThreshold",
        formula: `-${penalty}`,
        flavor: pf1.config.woundThresholdConditions[wt.level],
        target: "attack",
        type: "untyped",
        value: -penalty,
      });
      this.changes.set(ch.id, ch);
    } else {
      this.changes.delete("woundThreshold");
    }
  }

  /**
   * All skill IDs relevant to this actor
   *
   * @type {Array<string>}
   */
  get allSkills() {
    const result = [];
    for (const [key, skillData] of Object.entries(this.system.skills)) {
      if (!skillData) continue;
      result.push(key);
      for (const subKey of Object.keys(skillData.subSkills ?? {})) {
        result.push(`${key}.${subKey}`);
      }
    }
    return result;
  }

  /**
   * An array of all context note data for this actor.
   *
   * @type {{notes: Array<pf1.components.ContextNote>, item: ItemPF}[]}
   */
  get allNotes() {
    return this.items
      .filter((item) => item.isActive && item.system.contextNotes?.length > 0)
      .map((item) => ({ notes: item.system.contextNotes, item }));
  }

  /**
   * @returns {ItemPF[]} All items on this actor, including those in containers.
   */
  get allItems() {
    return [...this.containerItems, ...this.items];
  }

  /**
   * All items this actor is holding in containers.
   *
   * @internal
   * @type {ItemPF[]}
   */
  get containerItems() {
    /** @type {Array<ItemPF>} */
    const items = [];

    /**
     * @param {pf1.documents.item.ItemContainerPF} container - Item doc
     */
    function getContainerContents(container) {
      if (container.type !== "container") return;

      for (const item of container.items) {
        items.push(item);
        getContainerContents(item);
      }
    }

    for (const container of this.itemTypes.container) {
      getContainerContents(container);
    }

    return items;
  }

  /**
   * Generates an array with all the active context-sensitive notes for the given context on this actor.
   *
   * @param {string} context - The context to draw from.
   * @param {boolean} [all=true] - Retrieve notes meant for all, such as notes targeting all skills.
   * @returns {Array<ItemContextNotes>} - Context notes
   */
  getContextNotes(context, all = true) {
    if (context.string) {
      foundry.utils.logCompatibilityWarning(
        "ActorPF.getcontextNotes() first parameter must be a string, support for anything else is deprecated.",
        {
          since: "PF1 v11",
          until: "PF1 v12",
        }
      );
      context = context.string;
    }

    const result = this.allNotes;

    const parts = context.split(".");
    const mainId = parts.shift();

    // Special contexts that retrieve additional targets.
    switch (mainId) {
      // skill.*
      case "skill": {
        const skillKey = parts.shift();
        const skill = this.getSkillInfo(skillKey);
        const ability = skill.ability;
        for (const noteSource of result) {
          noteSource.notes = noteSource.notes
            .filter((n) => [context, `${ability}Skills`].includes(n.target) || (all && n.target === "skills"))
            .map((n) => n.text);
        }

        return result;
      }
      // savingThrow.*
      case "savingThrow": {
        const saveKey = parts.shift();
        for (const noteSource of result) {
          noteSource.notes = noteSource.notes
            .filter((n) => [saveKey, "allSavingThrows"].includes(n.target))
            .map((n) => n.text);
        }

        if (this.system.attributes?.saveNotes) {
          result.push({ notes: [this.system.attributes.saveNotes], item: null });
        }

        return result;
      }
      // abilityChecks.*
      case "abilityChecks": {
        const ablKey = parts.shift();
        for (const noteSource of result) {
          noteSource.notes = noteSource.notes
            .filter((n) => [`${ablKey}Checks`, "allChecks"].includes(n.target))
            .map((n) => n.text);
        }

        return result;
      }
      // spell.*
      case "spell": {
        const subId = parts.shift();
        // spell.concentration.*
        if (subId === "concentration") {
          const bookId = parts.shift();
          for (const noteSource of result) {
            noteSource.notes = noteSource.notes.filter((n) => n.target === "concentration").map((n) => n.text);
          }

          const spellbookNotes = this.system.attributes?.spells?.spellbooks?.[bookId]?.concentrationNotes;
          if (spellbookNotes?.length) {
            result.push({ notes: spellbookNotes.split(/[\n\r]+/), item: null });
          }

          return result;
        }
        // spell.cl.*
        if (subId == "cl") {
          const bookId = parts.shift();
          for (const noteSource of result) {
            noteSource.notes = noteSource.notes.filter((n) => n.target === "cl").map((n) => n.text);
          }

          /** @type {string} */
          const spellbookNotes = this.system.attributes?.spells?.spellbooks?.[bookId]?.clNotes;
          if (spellbookNotes?.length) {
            result.push({ notes: spellbookNotes.split(/[\n\r]+/), item: null });
          }

          return result;
        }

        return [];
      }
    }

    // Otherwise return notes if they directly match context
    for (const note of result) {
      note.notes = note.notes.filter((o) => o.target === context).map((o) => o.text);
    }

    return result.filter((n) => n.notes.length);
  }

  /**
   * Returns a list of already parsed context notes.
   *
   * @param {string} context - The context to draw notes from.
   * @param {object} [options] Additional options
   * @param {boolean} [options.roll=true] Whether to roll inline rolls or not.
   * @param {boolean} [options.all] - Option to pass to {@link getContextNotes}
   * @returns {Promise<Array<ParsedContextNoteEntry>>} The resulting notes, already parsed.
   */
  async getContextNotesParsed(context, { all, roll = true, rollData } = {}) {
    rollData ??= this.getRollData();

    const noteObjects = this.getContextNotes(context, all);
    await this.enrichContextNotes(noteObjects, rollData, { roll });

    return noteObjects.reduce((all, o) => {
      all.push(...o.enriched.map((text) => ({ text, source: o.item?.name })));
      return all;
    }, []);
  }

  /**
   * Enrich context notes with item specific roll data.
   *
   * Adds `enriched` array to each note object.
   *
   * @param {ItemContextNotes} notes - Context notes
   * @param {object} [rollData] - Roll data instance
   * @param {object} [options] - Additional options
   * @param {boolean} [options.roll=true] - Handle rolls
   */
  async enrichContextNotes(notes, rollData, { roll = true } = {}) {
    rollData ??= this.getRollData();
    for (const noteObj of notes) {
      rollData.item = {};
      if (noteObj.item) rollData = noteObj.item.getRollData();

      const enriched = [];
      for (const note of noteObj.notes) {
        enriched.push(
          ...note
            .split(/[\n\r]+/)
            .map((subnote) => pf1.utils.enrichHTMLUnrolled(subnote, { rollData, rolls: roll, relativeTo: this }))
        );
      }

      noteObj.enriched = await Promise.all(enriched);
    }
  }

  /**
   * @typedef {object} MobilityPenaltyResult
   * @property {number|null} maxDexBonus - The maximum dexterity bonus allowed for this result.
   * @property {number} acp - The armor check penalty of this result.
   */

  /**
   * Computes encumbrance values for this actor.
   *
   * @internal
   * @returns {MobilityPenaltyResult} The resulting penalties from encumbrance.
   */
  _computeEncumbrance() {
    // Init base data
    this.system.attributes ??= {};
    const attributes = this.system.attributes;
    attributes.encumbrance ??= {};
    const encumbrance = attributes.encumbrance;

    const carry = this.getCarryCapacity();
    // Set levels
    encumbrance.levels = carry;
    encumbrance.levels.carry = carry.heavy * 2;
    encumbrance.levels.drag = carry.heavy * 5;

    const carriedWeight = Math.max(0, this.getCarriedWeight());
    encumbrance.carriedWeight = Math.round(carriedWeight * 10) / 10;

    // Determine load level
    let encLevel = pf1.config.encumbranceLevels.light;
    if (carriedWeight > 0) {
      if (carriedWeight > encumbrance.levels.medium) encLevel = pf1.config.encumbranceLevels.heavy;
      else if (carriedWeight > encumbrance.levels.light) encLevel = pf1.config.encumbranceLevels.medium;
    }
    encumbrance.level = encLevel;

    const result = {
      maxDexBonus: null,
      acp: 0,
    };

    switch (encumbrance.level) {
      case pf1.config.encumbranceLevels.medium:
        result.acp = 3;
        result.maxDexBonus = 3;
        break;
      case pf1.config.encumbranceLevels.heavy:
        result.acp = 6;
        result.maxDexBonus = 1;
        break;
    }

    encumbrance.acp = result.acp;
    encumbrance.maxDex = result.maxDexBonus;

    return result;
  }

  /**
   * @internal
   * @returns {number} - Total coin weight in lbs
   */
  _calculateCoinWeight() {
    const divisor = game.settings.get("pf1", "coinWeight");
    if (!divisor) return 0;
    return Object.values(this.system.currency || {}).reduce((total, coins) => total + (coins || 0), 0) / divisor;
  }

  /**
   * Calculate current carry capacity limits.
   *
   * @returns {{light:number,medium:number,heavy:number}} - Capacity info
   */
  getCarryCapacity() {
    // Determine carrying capacity
    const carryCapacity = this.system.details?.carryCapacity ?? {};
    const carryStr = this.system.abilities.str.total + carryCapacity.bonus?.total;
    let carryMultiplier = carryCapacity.multiplier?.total;
    const size = Object.keys(pf1.config.sizeChart)[this.system.traits.size.value];
    if (this.system.attributes.quadruped) carryMultiplier *= pf1.config.encumbranceMultipliers.quadruped[size];
    else carryMultiplier *= pf1.config.encumbranceMultipliers.normal[size];
    const table = pf1.config.encumbranceLoads;

    let heavy = Math.floor(table[carryStr] * carryMultiplier);
    if (carryStr >= table.length) {
      const multiplierCount = (carryStr - (table.length - 1)) / 10;
      heavy = Math.floor(table[table.length - 1] * Math.pow(4, multiplierCount) * carryMultiplier);
    }
    // Convert to world unit system
    heavy = pf1.utils.convertWeight(heavy);

    return {
      light: Math.floor(heavy / 3),
      medium: Math.floor((heavy / 3) * 2),
      heavy,
    };
  }

  /**
   * Determines carried weight.
   *
   * @returns {number} - kg or lbs of all carried things, including currency
   */
  getCarriedWeight() {
    const weight = this.items
      .filter((i) => i.isPhysical && i.system.carried !== false)
      .reduce((cur, o) => cur + o.system.weight.total, this._calculateCoinWeight());

    return pf1.utils.convertWeight(weight);
  }

  /**
   * Get total currency in category.
   *
   * @param {"currency"|"altCurrency"} [category="currency"] - Currency category.
   * @param {object} [options] - Additional options
   * @param {boolean} [options.inLowestDenomination=true] - Return result in lowest denomination (default copper). If false, returns standard currency (default gold) instead.
   * @returns {number} - Total currency in category.
   */
  getCurrency(category = "currency", { inLowestDenomination = true } = {}) {
    const currencies = this.system[category];
    if (!currencies) {
      console.error(`Currency type "${category}" not found.`);
      return NaN;
    }

    const cc = pf1.config.currency;

    let total = 0;
    for (let [unit, value] of Object.entries(currencies)) {
      value = Math.max(0, value || 0);
      if (value == 0) continue;
      total += value * (cc.rate[unit] || 1);
    }

    return inLowestDenomination ? total : total / cc.standardRate;
  }

  /**
   * Total coinage in both weighted and weightless.
   *
   * @param {object} [options] - Additional options
   * @param {boolean} [options.inLowestDenomination=true] - Use copper for calculations and return.
   * @returns {number} - The total amount of currency, in copper pieces.
   */
  getTotalCurrency({ inLowestDenomination = true } = {}) {
    const total =
      this.getCurrency("currency", { inLowestDenomination: true }) +
      this.getCurrency("altCurrency", { inLowestDenomination: true });

    return inLowestDenomination ? total : total / pf1.config.currency.standardRate;
  }

  /**
   * Converts currencies of the given category to the given currency type
   *
   * @see {@link pf1.utils.currency.convert}
   *
   * @param {"currency"|"altCurrency"} [category="currency"] - Currency category, altCurrency is for weightless
   * @param {CoinType} [type="pp"] - Target currency.
   * @returns {Promise<this>|undefined} Updated document or undefined if no update occurred.
   */
  convertCurrency(category = "currency", type = "pp") {
    const cp = this.getCurrency(category, { inLowestDenomination: true });
    if (!Number.isFinite(cp)) {
      console.error(`Invalid total currency "${cp}" in "${category}" category`);
      return;
    }

    const currency = pf1.utils.currency.convert(cp, type, { pad: true });

    return this.update({ system: { [category]: currency } });
  }

  /**
   * Prepare armor/shield data for roll data
   *
   * @internal
   * @param {object} equipment Equipment info
   * @param {string} equipment.id Item ID
   * @param {string} equipment.type Armor/Shield type
   * @param {object} armorData Armor data object
   */
  _prepareArmorData({ id, type } = {}, armorData) {
    armorData.type = type ?? null;

    const itemData = this.items.get(id)?.system;
    if (!itemData) return;

    armorData.ac = itemData.armor.value ?? 0;
    armorData.enh = itemData.armor.enh ?? 0;
    armorData.total = armorData.ac + armorData.enh;
    if (!Number.isFinite(armorData.total)) armorData.total = 0;
  }

  /**
   * Retrieve data used to fill in roll variables.
   *
   * @example
   * ```js
   * await new Roll("1d20 + @abilities.wis.mod[Wis]", actor.getRollData()).toMessage();
   * ```
   *
   * @override
   * @param {object} [options] - Additional options
   * @param {boolean} [options.refresh] - Refresh cache
   * @param {boolean} [options.cache] - Use cache. If set to false, new fresh copy is returned without caching it.
   * @returns {object} - Roll data object
   */
  getRollData(options = { refresh: false, cache: true }) {
    // Return cached data, if applicable
    const skipRefresh = !options.refresh && this._rollData && options.cache;

    const result = { ...(skipRefresh ? this._rollData : foundry.utils.deepClone(this.system)) };

    pf1.utils.rollData.addStatic(result);

    // Clear certain fields if not refreshing
    if (skipRefresh) {
      for (const key of pf1.config.temporaryRollDataFields.actor) {
        // Cleanup has no deep paths, so we can directly delete them
        // TODO: Move this cleanup to whatever adds the data
        delete result[key];
      }
    }

    /* ----------------------------- */
    /* Always add the following data
    /* ----------------------------- */

    // Add combat round, if in combat
    if (game.combats?.viewed) {
      result.combat = {
        round: game.combat.round || 0,
      };
    }

    // Add denied Dex to AC
    result.conditions ??= {};
    result.conditions.loseDexToAC = this.changeFlags?.loseDexToAC ?? false;

    // Return cached data, if applicable
    if (skipRefresh) return result;

    /* ----------------------------- */
    /* Set the following data on a refresh
    /* ----------------------------- */

    // Sync health values
    for (const hpKey of ["hp", "wounds", "vigor"]) {
      const hp = result.attributes[hpKey];
      hp.value = hp.max + hp.offset;
      /*
      // Supporting values
      const thp = hp.temp ?? 0;
      hp.effective = hp.value + thp;
      hp.ratio = hp.effective / (hp.max + thp);
      */
    }

    // Set size index
    const sizes = Object.values(pf1.config.sizeChart);
    result.size = Math.clamp(result.traits.size.value, 0, sizes.length - 1);

    // Set age category index
    const ageCategories = Object.keys(pf1.config.ageCategories);
    const maxAgeOffset = ageCategories.length - 1;
    result.ageCategory = {
      value: Math.clamp(result.traits?.ageCategory?.value, 0, maxAgeOffset),
      physical: Math.clamp(result.traits?.ageCategory?.physical, 0, maxAgeOffset),
      mental: Math.clamp(result.traits?.ageCategory?.mental, 0, maxAgeOffset),
    };

    // Add more info for formulas
    result.armor = { type: 0, total: 0, ac: 0, enh: 0 };
    result.shield = { type: 0, total: 0, ac: 0, enh: 0 };

    // Determine equipped armor type
    const eqData = this.equipment;
    if (eqData) {
      this._prepareArmorData(eqData.armor, result.armor);
      this._prepareArmorData(eqData.shield, result.shield);
    }

    // Add spellbook info
    result.spells = result.attributes.spells.spellbooks;
    for (const book of Object.values(result.spells)) {
      book.abilityMod = result.abilities[book.ability]?.mod ?? 0;
      // Add alias
      if (book.class && book.class !== "_hd") result.spells[book.class] ??= book;
    }

    // Add item dictionary flags
    result.dFlags = this.itemFlags.dictionary ?? {};
    result.bFlags = Object.fromEntries(
      Object.entries(this.itemFlags.boolean ?? {}).map(([key, { sources }]) => [key, sources.length > 0 ? 1 : 0])
    );

    result.range = this.system.traits?.reach?.total ?? { melee: NaN, reach: NaN };

    // Add class info
    result.classes = this.classes;
    const negLevels = result.attributes.energyDrain ?? 0;
    if (negLevels > 0 && result.classes) {
      for (const cls of Object.values(result.classes)) {
        if (cls.isMythic) continue;
        cls.level = Math.max(0, cls.unlevel - negLevels);
      }
    }

    // Map HP ability
    const hpAbility = result.abilities[result.attributes.hpAbility];
    Object.defineProperty(result.attributes, "hpAbility", {
      get() {
        return hpAbility;
      },
    });

    // @since PF1 v10
    result.alignment = pf1.utils.parseAlignment(this.system.details?.alignment || "tn");

    if (options.cache) {
      this._rollData = result;
    }

    // Call hook
    if (Hooks.events["pf1GetRollData"]?.length > 0) Hooks.callAll("pf1GetRollData", this, result);

    return result;
  }

  /**
   * Get melee and reach maximum ranges.
   *
   * @param {ActorSize|number} size - Actor size as size key or number
   * @param {ActorStature} stature - Actor stature
   * @returns {{melee:number,reach:number}} - Ranges
   */
  static getReach(size = "med", stature = "tall") {
    let effectiveSize = size >= 0 ? size : Object.keys(pf1.config.sizeChart).indexOf(size);
    // Long creatures larger than medium count as one size smaller
    // https://www.aonprd.com/Rules.aspx?ID=179
    if (stature !== "tall" && effectiveSize > 4) effectiveSize -= 1;

    const reachStruct = (melee, reach) => ({ melee, reach });

    switch (effectiveSize) {
      case 0: // Fine
      case 1: // Diminutive
        return reachStruct(0, 0);
      case 2: // Tiny
        return reachStruct(0, 5);
      default:
      case 3: // Small
      case 4: // Medium
        return reachStruct(5, 10);
      case 5: // Large
        return reachStruct(10, 20);
      case 6: // Huge
        return reachStruct(15, 30);
      case 7: // Gargantuan
        return reachStruct(20, 40);
      case 8: // Colossal
        return reachStruct(30, 60);
    }
  }

  /**
   * @protected
   * @returns {Array<object>} - Array of item defining objects
   */
  getQuickActions() {
    return this.items
      .filter((o) => o.isActive && o.system.showInQuickbar === true && o.showUnidentifiedData !== true)
      .sort((a, b) => a.sort - b.sort)
      .map((item) => {
        const qi = {
          item,
          name: item.name,
          id: item.id,
          type: item.type,
          img: item.img,
          get isSingleUse() {
            return item.isSingleUse;
          },
          get haveAnyCharges() {
            return this.item.isCharged && this.maxCharge > 0;
          },
          get maxCharge() {
            return item.maxCharges;
          },
          get charges() {
            return this.item.charges;
          },
        };

        // Fill in charge details
        qi.isCharged = qi.haveAnyCharges;
        if (qi.isCharged) {
          let chargeCost =
            item.defaultAction?.getChargeCostSync({ maximize: true })?.total ?? item.getDefaultChargeCost();
          //if (chargeCost == 0) qi.isCharged = false;

          qi.recharging = chargeCost < 0;
          chargeCost = Math.abs(chargeCost);

          qi.uses = qi.charges;
          qi.max = qi.maxCharge;

          if (!Number.isFinite(qi.max)) {
            delete qi.max; // Infinite
          } else if (chargeCost != 0) {
            // Maximum charging
            if (qi.recharging) {
              qi.uses = Math.ceil((qi.max - qi.uses) / chargeCost);
              qi.max = Math.ceil(qi.max / chargeCost);
            }
            // Actual uses
            else {
              qi.uses = Math.floor(qi.uses / chargeCost);
              qi.max = Math.floor(qi.max / chargeCost);
            }
          }
        } else {
          const action = item.defaultAction;
          // Add fake charges for ammo using items
          if (action?.ammo.type) {
            const ammo = item.defaultAmmo;
            if (ammo) {
              qi.isCharged = true;
              qi.uses = ammo.system.quantity || 0;
            }
          }
        }

        return qi;
      });
  }

  /**
   * @internal
   */
  refreshAbilityModifiers() {
    for (const abl of Object.values(this.system.abilities)) {
      const penalty = Math.abs(abl.penalty || 0);
      const damage = abl.damage || 0;
      const newMod = pf1.utils.getAbilityModifier(abl.total, { penalty, damage });
      abl.mod = newMod;
    }
  }

  /**
   * Return feat counts.
   *
   * @typedef FeatCounts
   * @type {object}
   * @property {number} max - The maximum allowed feats.
   * @property {number} active - The current number of active feats.
   * @property {number} owned - The current number of feats, active or not.
   * @property {number} levels - Feats gained by levels specifically
   * @property {number} mythic - Mythic feats
   * @property {number} formula - Feats gained by custom formula on the feats tab
   * @property {number} changes - Feats gained via Changes
   * @property {number} disabled - Disabled feats
   * @property {number} excess - Feats over maximum allowed
   * @property {number} missing - Feats under maximum allowed
   * @returns {FeatCounts} An object with a property `value` which refers to the current used feats, and `max` which refers to the maximum available feats.
   */
  getFeatCount() {
    const feats = this.itemTypes.feat.filter((o) => o.subType === "feat");

    const active = feats.filter((o) => o.isActive).length;
    const owned = feats.length;

    const result = {
      max: 0,
      active,
      owned,
      disabled: owned - active,
      levels: 0,
      mythic: 0,
      formula: 0,
      changes: 0,
      // Count totals
      get discrepancy() {
        return this.max - this.active;
      },
      get missing() {
        return Math.max(0, this.discrepancy);
      },
      get excess() {
        return Math.max(0, -this.discrepancy);
      },
    };

    const isMindless = this.system.abilities?.int?.value === null;

    // Ignore classes for feats with mindless
    // Mindless gets other bonuses to feats beyond these...
    // ... since they can be explicit "gains X feat", homebrew, or other impossible to account for.
    if (!isMindless) {
      // Add feat count by level
      result.levels = Math.ceil(this.system.attributes.hd.total / 2);
      result.max += result.levels;

      // Mythic feats
      // https://aonprd.com/Rules.aspx?Name=Mythic%20Heroes&Category=Mythic%20Rules
      // Gained at 1, 3, 5, etc.
      result.mythic = Math.ceil(this.system.details.mythicTier / 2);
      result.max += result.mythic;
    }

    // Bonus feat formula
    const bonusRoll = RollPF.safeRollSync(this.system.details?.bonusFeatFormula || "0", this.getRollData());
    result.formula = bonusRoll.total;
    result.max += result.formula;
    if (bonusRoll.err) {
      console.error(
        `An error occurred in the Bonus Feat Formula of actor "${this.name}" [${this.id}].`,
        {
          formula: this.system.details?.bonusFeatFormula,
          actor: this,
        },
        bonusRoll.err
      );
    }

    // Bonuses from changes
    result.changes = getHighestChanges(
      this.changes.filter((c) => {
        if (c.target !== "bonusFeats") return false;
        return c.operator !== "set";
      }),
      { ignoreTarget: true }
    ).reduce((cur, c) => cur + c.value, 0);
    result.max += result.changes;

    return result;
  }

  /**
   * Check if actor has item with specified boolean flag.
   *
   * @param {string} flagName - The name/key of the flag to search for.
   * @returns {boolean} Whether this actor has any owned item with the given flag.
   */
  hasItemBooleanFlag(flagName) {
    return this.itemFlags.boolean[flagName] != null;
  }

  /**
   * Restore spellbook used slots and spellpoints.
   *
   * @param {object} [options] Additional options
   * @param {boolean} [options.commit=true] If false, return update data object instead of directly updating the actor.
   * @param {object} [options.rollData] Roll data
   * @returns {Promise<this|object>} Result of update or the update data.
   */
  async resetSpellbookUsage({ commit = true, rollData } = {}) {
    const actorData = this.system;
    const updateData = {};

    rollData ??= this.getRollData();

    // Update spellbooks
    for (const [bookId, spellbook] of Object.entries(actorData.attributes.spells.spellbooks)) {
      if (!spellbook.inUse) continue;

      // Restore spellbooks using spell points
      if (spellbook.spellPoints.useSystem) {
        // Try to roll restoreFormula, fall back to restoring max spell points
        let restorePoints = spellbook.spellPoints.max;
        if (spellbook.spellPoints.restoreFormula) {
          const restoreRoll = await RollPF.safeRoll(
            spellbook.spellPoints.restoreFormula,
            rollData,
            undefined,
            undefined,
            { allowInteractive: false }
          );
          if (restoreRoll.err) console.error(restoreRoll.err, spellbook.spellPoints.restoreFormula);
          else restorePoints = Math.min(spellbook.spellPoints.value + restoreRoll.total, spellbook.spellPoints.max);
        }
        updateData[`system.attributes.spells.spellbooks.${bookId}.spellPoints.value`] = restorePoints;
      }
      // Restore spell slots
      else {
        for (let level = 0; level < 10; level++) {
          updateData[`system.attributes.spells.spellbooks.${bookId}.spells.spell${level}.value`] =
            spellbook.spells[`spell${level}`]?.max ?? 0;
        }
      }
    }

    if (commit) return this.update(updateData);
    return updateData;
  }

  /**
   * Recharge all owned items.
   *
   * @see {@link pf1.documents.item.ItemPF.recharge}
   *
   * @example
   * Recharge items with default settings.
   * ```js
   * await actor.rechargeItems();
   * ```
   * Recharge items as if week had passed.
   * ```js
   * await actor.rechargeItems({ period: "week" });
   * ```
   *
   * @param {RechargeActorItemsOptions} [options] - Additional options
   * @returns {Promise<Item[]|object[]>} - Result of an update or the update data.
   */
  async rechargeItems({ commit = true, ...rechargeOptions } = {}) {
    const itemUpdates = [];

    // Update charged items
    // TODO: Await all item recharges in one go.
    for (const item of this.items) {
      const itemUpdate = await item.recharge({ ...rechargeOptions, commit: false });

      // Append update to queue
      if (itemUpdate?.system && !foundry.utils.isEmpty(itemUpdate.system)) {
        itemUpdate._id = item.id;
        itemUpdates.push(itemUpdate);
      }
    }

    if (commit) {
      if (itemUpdates.length) return this.updateEmbeddedDocuments("Item", itemUpdates);
    } else return itemUpdates;
    return [];
  }

  /**
   * Handler for character healing during rest.
   *
   * @protected
   * @param {object} options Resting options.
   * @returns {object} Update data object
   */
  _restingHeal(options = {}) {
    const actorData = this.system,
      hp = actorData.attributes.hp,
      wounds = actorData.attributes?.wounds,
      vigor = actorData.attributes?.vigor;

    const { hours, longTermCare } = options;
    const updateData = {};

    /** @type {number} */
    const hd = actorData.attributes.hd.total;

    // Base healing
    const heal = {
      hp: hd,
      abl: 1,
      nonlethal: hours * hd,
      vigor: vigor?.max ?? 0,
      wounds: wounds?.max > 0 ? 1 : 0,
    };

    // -- Normal Hit Points ---

    // Full day of resting
    if (hours >= 24) {
      heal.hp += 1;
      heal.wounds += Math.floor(hd / 2);
      heal.abl += 1;
    }
    // Long term care
    if (longTermCare === true) {
      heal.hp *= 2;
      heal.abl *= 2;
      heal.wounds *= 2;
    }

    updateData["system.attributes.hp.value"] = Math.min(hp.value + heal.hp, hp.max);
    updateData["system.attributes.hp.nonlethal"] = Math.max(0, (hp.nonlethal || 0) - heal.nonlethal);
    for (const [key, abl] of Object.entries(actorData.abilities)) {
      const dmg = Math.abs(abl.damage);
      updateData[`system.abilities.${key}.damage`] = Math.max(0, dmg - heal.abl);
    }

    // --- Wounds & Vigor ---

    // Secondary actors don't use W&V rules
    if (wounds?.max && vigor?.max) {
      updateData["system.attributes.wounds.value"] = Math.min(wounds.value + heal.wounds, wounds.max);
      updateData["system.attributes.vigor.value"] = Math.min(vigor.value + heal.vigor, vigor.max);
    }

    return updateData;
  }

  /**
   * Perform all changes related to an actor resting, including restoring HP, ability scores, item uses, etc.
   *
   * @example
   * Rest with default settings
   * ```js
   * await actor.performRest();
   * ```
   *
   * @see {@link hookEvents!pf1PreActorRest pf1PreActorRest hook}
   * @see {@link hookEvents!pf1ActorRest pf1ActorRest hook}
   * @param {Partial<ActorRestOptions>} options - Options affecting an actor's resting
   * @returns {Promise<ActorRestData | void>} Updates applied to the actor, if resting was completed
   */
  async performRest(options = {}) {
    const { restoreHealth = true, longTermCare = false, restoreDailyUses = true, hours = 8, verbose = false } = options;

    const updateData = {};
    // Restore health and ability damage
    if (restoreHealth === true) {
      const healUpdate = this._restingHeal(options);
      foundry.utils.mergeObject(updateData, healUpdate);
    }

    let itemUpdates = [];
    // Restore daily uses of spells, feats, etc.
    if (restoreDailyUses === true) {
      const spellbookUpdates = await this.resetSpellbookUsage({ commit: false });
      foundry.utils.mergeObject(updateData, spellbookUpdates);

      // Recharge all items (including spells for prepared spellbooks)
      itemUpdates = await this.rechargeItems({ commit: false, updateData, period: "day" });
    }

    options = { restoreHealth, restoreDailyUses, longTermCare, hours };
    const allowed = Hooks.call("pf1PreActorRest", this, options, updateData, itemUpdates);
    if (allowed === false) return;

    const context = { pf1: { action: "rest", restOptions: options } };

    if (itemUpdates.length) await this.updateEmbeddedDocuments("Item", itemUpdates, foundry.utils.deepClone(context));
    if (!foundry.utils.isEmpty(updateData.system)) await this.update(updateData, foundry.utils.deepClone(context));

    Hooks.callAll("pf1ActorRest", this, options, updateData, itemUpdates);

    if (verbose) {
      const message = restoreDailyUses ? "PF1.FullRestMessage" : "PF1.RestMessage";
      ui.notifications.info(game.i18n.format(message, { name: this.token?.name ?? this.name, hours }));
    }

    return { options, updateData, itemUpdates };
  }

  /**
   * @protected
   * @override
   */
  async modifyTokenAttribute(attribute, value, isDelta = false, isBar = true) {
    let doc = this;
    const current = foundry.utils.getProperty(this.system, attribute),
      updates = {};

    const isResource = current instanceof Resource;
    if (isResource) doc = current.item;

    if (!doc) return;

    // Hit points
    if (attribute === "attributes.hp") {
      if (!isDelta) value = (current.temp + current.value - value) * -1;
      let dt = value;
      if (current.temp > 0 && value < 0) {
        dt = Math.min(0, current.temp + value);
        updates["system.attributes.hp.temp"] = Math.max(0, current.temp + value);
      }
      updates["system.attributes.hp.value"] = Math.min(current.value + dt, current.max);
    }
    // Wounds & Vigor
    else if (attribute === "attributes.vigor") {
      if (!isDelta) value = (current.temp + current.value - value) * -1;
      let dt = value;
      if (current.temp > 0 && value < 0) {
        dt = Math.min(0, current.temp + value);
        updates["system.attributes.vigor.temp"] = Math.max(0, current.temp + value);
      }
      updates["system.attributes.vigor.value"] = Math.min(current.value + dt, current.max);
    }
    // Relative
    else if (isDelta) {
      if (isResource) {
        updates["system.uses.value"] = Math.min(current.value + value, current.max);
      } else {
        if (isBar)
          updates[`system.${attribute}.value`] = Math.clamp(current.value + value, current.min || 0, current.max);
        else updates[`system.${attribute}`] = current + value;
      }
    }
    // Absolute
    else {
      if (isResource) {
        updates["system.uses.value"] = Math.clamp(value, 0, current.max);
      } else {
        if (isBar) updates[`system.${attribute}.value`] = Math.min(value, current.max);
        else updates[`system.${attribute}`] = value;
      }
    }

    const allowed = Hooks.call("modifyTokenAttribute", { attribute, value, isDelta, isBar }, updates);
    return allowed !== false ? doc.update(updates) : this;
  }

  /**
   * The VisionSharingSheet instance for this actor
   *
   * @type {VisionSharingSheet}
   */
  get visionSharingSheet() {
    this._visionSharingSheet ??= new VisionSharingSheet({ document: this });
    return this._visionSharingSheet;
  }
}

/**
 * @typedef {object} ActorRestOptions
 * Options given to {@link ActorPF.performRest} affecting an actor's resting.
 * @property {boolean} [restoreHealth=true] - Whether the actor's health should be restored.
 * @property {boolean} [restoreDailyUses=true] - Whether daily uses of spells and abilities should be restored.
 * @property {boolean} [longTermCare=false] - Whether additional hit and ability score points should be restored through the Heal skill.
 * @property {number} [hours=8] - The number of hours the actor will rest.
 * @property {boolean} [verbose=false] - Display notification once rest processing finishes.
 */

/**
 * @typedef {object} ActorRestData
 * @property {ActorRestOptions} options - Options for resting
 * @property {object} updateData - Updates applied to the actor
 * @property {object[]} itemUpdates - Updates applied to the actor's items
 */

/**
 * @typedef {object} DamageInstance
 * @property {number} value - Total damage in this instance
 * @property {object} types - Damage type data
 * @property {string} types.custom - Custom damage types
 * @property {string[]} types.values - Standard damage types
 */

/**
 * @typedef {object} ItemContextNotes
 * @property {Item} item - Item from which the notes are from
 * @property {Array<string>} notes - Note strings
 * @property {Array<string>} enriched - Enriched note things
 */

/**
 * @typedef {object} ParsedContextNoteEntry
 * @property {string} text - Enriched note text
 * @property {string|undefined} source - Source label if any
 */
