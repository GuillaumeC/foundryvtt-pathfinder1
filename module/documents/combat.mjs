import { getSkipActionPrompt } from "./settings.mjs";

/* -------------------------------------------- */

/**
 * @internal
 * @param {string} combatantId - Combatant ID
 * @returns {Promise<Combatant>} - Result of {@link CombatantPF.duplicateWithData}
 */
async function duplicateCombatantInitiativeDialog(combatantId) {
  /** @type {CombatantPF} */
  const combatant = game.combat.combatants.get(combatantId);
  if (!combatant) return void ui.notifications.warn(game.i18n.localize("PF1.Warning.NoCombatantFound"));

  const offset = await pf1.utils.dialog.getNumber({
    title: `${game.i18n.localize("PF1.DuplicateInitiative")}: ${combatant.name}`,
    label: game.i18n.localize("PF1.InitiativeOffset"),
    initial: 0,
    classes: ["duplicate-initiative"],
  });

  if (!Number.isFinite(offset)) return; // Cancelled

  return combatant.duplicateWithData({ initiative: (combatant.initiative ?? 0) + offset });
}

Hooks.on("getCombatTrackerEntryContext", function addCombatTrackerContextOptions(html, menuItems) {
  menuItems.push({
    name: "PF1.DuplicateInitiative",
    icon: '<i class="fas fa-dice-d20"></i>',
    callback: ([li]) => duplicateCombatantInitiativeDialog(li.dataset.combatantId),
  });
});

/**
 * Combat manager extension
 */
export class CombatPF extends Combat {
  /**
   * @override
   * @param {string[]} ids Combatant IDs to roll initiative for.
   * @param {object} [options={}] - Additional options
   * @param {string} [options.formula] - Formula override. Passed to {@link Combatant.getInitiativeRoll}
   * @param {string} [options.d20] - d20 override. Passed to {@link Combatant.getInitiativeRoll}
   * @param {string} [options.bonus=null] - Formula for bonus to initiative
   * @param {string} [options.rollMode] - Roll mode override.
   * @param {boolean} [options.updateTurn] - Adjust current turn if the new init would shift it
   * @param {object} [options.messageOptions={}] - Additional data for created chat message
   * @param {boolean} [options.skipDialog=null] - Skip roll dialog
   * Synced with Foundry v12.331
   */
  async rollInitiative(
    ids,
    { formula = null, d20, bonus = null, rollMode, updateTurn = true, messageOptions = {}, skipDialog = null } = {}
  ) {
    skipDialog ??= getSkipActionPrompt();
    // Structure input data
    ids = Array.isArray(ids) ? ids : [ids];

    const currentId = this.combatant?.id;

    // Shift rollmode to foundry standard parameter
    if (rollMode) messageOptions.rollMode = rollMode;
    rollMode = messageOptions.rollMode || game.settings.get("core", "rollMode");

    const roller = ids.length == 1 ? this.combatants.get(ids[0]) : null;
    const rollerName = roller?.name ?? null;

    // Show initiative dialog
    if (!skipDialog) {
      let check = true;
      if (roller) {
        const opts = roller.actor?.getInitiativeOptions?.();
        if (opts.check === false) check = false;
      }

      const dialogData = await this.constructor.showInitiativeDialog({
        d20,
        check,
        bonus,
        rollMode,
        name: rollerName,
      });
      if (!dialogData) return this;

      rollMode = dialogData.rollMode;
      messageOptions.rollMode = rollMode;
      bonus = dialogData.bonus || "";
      d20 = dialogData.d20;
    }

    // Iterate over Combatants, performing an initiative roll for each
    const updates = [];
    const messages = [];

    for (const [i, id] of ids.entries()) {
      // Get Combatant data (non-strictly)
      /** @type {CombatantPF} */
      const combatant = this.combatants.get(id);
      if (!combatant?.isOwner) continue;

      // Produce an initiative roll for the Combatant
      const roll = combatant.getInitiativeRoll(formula, d20, bonus);
      roll.options.flavor = game.i18n.format("PF1.Check", { type: game.i18n.localize("PF1.Initiative") });

      // Produce an initiative roll for the Combatant
      await roll.evaluate();
      if (roll.err) ui.notifications.warn(roll.err.message);

      updates.push({ _id: id, initiative: roll.total });

      // Per-combatant roll mode
      let cRollMode = rollMode;

      // Hide initiative rolls for hidden combatants, unless explicit roll mode was requested
      const isHidden = combatant.token?.hidden || combatant.hidden;
      if (isHidden && !messageOptions.rollMode && ["roll", CONST.DICE_ROLL_MODES.PUBLIC].includes(cRollMode)) {
        cRollMode = messageOptions.rollMode || CONST.DICE_ROLL_MODES.PRIVATE;
      }

      const properties = [];
      const notes = (await combatant.actor?.getContextNotesParsed?.("init")) ?? [];
      if (notes.length) {
        properties.push({ header: game.i18n.localize("PF1.Notes"), value: notes });
      }

      // Create card template data
      const templateData = {
        formula: roll.formula,
        tooltip: await roll.getTooltip(),
        total: roll.total,
        properties,
      };

      // Create base chat card data
      let chatData = {
        speaker: ChatMessage.implementation.getSpeakerActor({
          actor: combatant.actor,
          token: combatant.token,
          alias: combatant.name, // Combatant name can differ from token name, so alias is needed here
        }),
        ...messageOptions,
      };

      chatData.rollMode ||= cRollMode;

      // Mimic core Foundry data
      foundry.utils.setProperty(chatData, "flags.core.initiativeRoll", true);

      // Generate message proper via D20RollPF
      chatData = await roll.toMessage(chatData, {
        create: false,
        rollMode: cRollMode,
        subject: { core: "init" },
        chatTemplateData: templateData,
      });

      if (i > 0) chatData.sound = null; // Only play 1 sound for the whole set
      messages.push(chatData);
    }

    if (!updates.length) return this;

    // Update multiple combatants
    await this.updateEmbeddedDocuments("Combatant", updates);

    // Ensure the turn order remains with the same combatant
    if (updateTurn && currentId) {
      await this.update({ turn: this.turns.findIndex((t) => t.id === currentId) });
    }

    // Create multiple chat messages
    await ChatMessage.implementation.create(messages);

    return this;
  }

  /**
   * @param {object} [options] - Additional options
   * @param {string} [options.d20] Formula override
   * @param {boolean} [options.check=true] - If false, no base d20 check is provided
   * @param {string} [options.bonus] Bonus formula override
   * @param {string} options.name Name of the roller
   * @returns {object} - Result
   */
  static async showInitiativeDialog({ d20 = null, check = true, bonus = null, name } = {}) {
    const rollMode = game.settings.get("core", "rollMode");

    const template = "systems/pf1/templates/chat/roll-dialog.hbs";
    const dialogData = { d20, check, bonus, rollMode, rollModes: CONFIG.Dice.rollModes };

    // Show dialog
    // TODO: Use D20RollPF's prompt instead
    return foundry.applications.api.DialogV2.wait({
      window: {
        title:
          (name ? name + " – " : "") + game.i18n.format("PF1.Check", { type: game.i18n.localize("PF1.Initiative") }),
      },
      position: { width: 420 },
      content: await renderTemplate(template, dialogData),
      buttons: [
        {
          action: "roll",
          default: true,
          icon: "fa-solid fa-dice-d20",
          label: game.i18n.localize("PF1.Roll"),
          callback: (event, button, html) => new FormDataExtended(html.querySelector("form")).object,
        },
      ],
      close: () => null,
      classes: ["pf1-v2", "roll-prompt", "roll-initiative"],
      subject: { core: "init" },
      rejectClose: false,
    });
  }

  /**
   * @override
   * @param {object} changed - Update data
   * @param {options} context - Context options
   * @param {string} userId - Triggering user ID
   */
  _onUpdate(changed, context, userId) {
    super._onUpdate(changed, context, userId);

    if (changed.turn !== undefined || changed.round !== undefined) {
      // Cache current world time here since actual time update can happen at random time in the future due to async code.
      context.pf1 ??= {};
      context.pf1.worldTime = game.time.worldTime;
      this._onNewTurn(changed, context, userId);
    }
  }

  /**
   * @internal
   * @override
   * @param {object} changed - Changed data
   * @param {object} context - Context
   * @param {User} user - Triggering user
   */
  async _preUpdate(changed, context, user) {
    await super._preUpdate(changed, context, user);
    if (context.diff === false || context.recursive === false) return; // Don't diff if we were told not to diff

    if ("turn" in changed || "round" in changed) {
      // Record origin turn and round
      context.pf1 ??= {};
      context.pf1.from = { turn: this.turn, round: this.round };
    }
  }

  /**
   * New turn handling.
   *
   * @param {object} changed - Changed data
   * @param {object} context - Context
   * @param {string} userId - Triggering user ID
   * @private
   */
  async _onNewTurn(changed, context, userId) {
    if (!this._isForwardTime(changed, context)) return;

    if (context.pf1?.from) {
      const skipped = this._detectSkippedTurns(context.pf1.from, context);

      if (game.users.activeGM?.isSelf) {
        this._handleSkippedTurns(skipped, context);
      }

      const previous = this.turns.at(this.turn - 1);
      if (!skipped.has(previous)) this._processEndTurn(context.pf1?.from, context);
    }

    this._processTurnStart(changed, context, userId);

    this._processInitiative(context);
  }

  _isForwardTime(changed, context) {
    // Non-UI turn progression does not have context.direction present to detect this otherwise
    const t0 = context.pf1.from.turn,
      r0 = context.pf1.from.round,
      t1 = changed.turn ?? t0,
      r1 = changed.round ?? r0,
      rd = r1 - r0, // round delta
      td = t1 - t0; // turn delta

    if (rd < 0) return false;
    else if (rd == 0 && td <= 0) return false;
    return true;
  }

  /**
   * Determine skipped turns
   *
   * @internal
   * @param {object} from - Origin combat time frame
   * @param {number} from.turn - From turn
   * @param {number} from.round - From round
   * @param {object} context - Update context
   * @returns {Set<Combatant>} - Set of combatant IDs whose turn was skipped
   */
  _detectSkippedTurns({ turn, round } = {}, context) {
    const roundChange = this.round !== round;

    const skipped = new Set();

    // No combatants skipped
    if (!roundChange && turn + 1 === this.turn) return skipped;

    // Determine skipped combatants
    for (const [index, combatant] of this.turns.entries()) {
      // Seeking first, not actually skipped
      if (!roundChange && index <= turn) continue;
      // Skipped
      else if (index < this.turn) skipped.add(combatant);
      // Skipped (usually via nextRound)
      else if (roundChange && index > turn) skipped.add(combatant);
    }

    Hooks.callAll("pf1CombatTurnSkip", this, skipped, context);

    return skipped;
  }

  /**
   * Handle effects of skipped turns.
   *
   * @internal
   * @param {Set<Combatant>} skipped - Combatant IDs of those whose turn was skipped.
   * @param {object} context - Combat update context
   */
  _handleSkippedTurns(skipped, context) {
    const currentTurn = this.turn;
    const event = "turnEnd";

    const timeOffset = context.advanceTime ?? 0;
    const worldTime = context.pf1?.worldTime ?? game.time.worldTime;

    // Expire effects for skipped combatants
    for (const combatant of skipped) {
      const actor = combatant.actor;
      if (!actor) continue;

      // Adjust expiration time for those who come after in initiative (their expiration was for previous round)
      const turn = this.turns.findIndex((c) => c === combatant);
      const turnTimeOffset = timeOffset + (turn > currentTurn) ? -CONFIG.time.roundTime : 0;

      actor.expireActiveEffects?.({ timeOffset: timeOffset + turnTimeOffset, worldTime, combat: this, event });
    }
  }

  /**
   * Handle end of turn
   *
   * @internal
   * @param {object} originTime - Origin time frame
   * @param {number} originTime.turn - Turn that ended
   * @param {number} originTime.round - Round on which the turn ended
   * @param {object} context - Context
   */
  async _processEndTurn({ turn, round } = {}, context = {}) {
    const previous = this.turns.at(turn);
    const actor = previous.actor;
    if (!actor) return;

    const owner = actor.activeOwner;
    if (!owner?.isSelf) return;

    const timeOffset = context.advanceTime ?? 0;
    const worldTime = context.pf1?.worldTime ?? game.time.worldTime;

    try {
      await actor.expireActiveEffects?.({
        combat: this,
        worldTime,
        timeOffset,
        event: "turnEnd",
      });
    } catch (error) {
      console.error(error, actor);
    }
  }

  /**
   * Process current combatant: expire active effects & buffs.
   *
   * @param {object} changed Update data
   * @param {options} context Context options
   */
  async _processTurnStart(changed, context) {
    const actor = this.combatant?.actor;
    if (!actor) return;

    // Attempt to perform expiration on owning active user
    const owner = actor.activeOwner;
    if (!owner?.isSelf) return;

    const timeOffset = context.advanceTime ?? 0;
    const worldTime = context.pf1?.worldTime ?? game.time.worldTime;

    try {
      await actor.expireActiveEffects?.({
        combat: this,
        worldTime,
        timeOffset,
        event: "turnStart",
      });
    } catch (error) {
      console.error(error, actor);
    }

    try {
      await actor.rechargeItems?.({ period: "round", exact: true });
    } catch (error) {
      console.error(error, actor);
    }
  }

  /**
   * Process end of durations based on initiative.
   *
   * Only active GM processes these to avoid conflicts and logic bloat.
   *
   * @internal
   * @param {object} [context] - Update context
   */
  _processInitiative(context = {}) {
    if (!game.users.activeGM?.isSelf) return;

    const worldTime = context.pf1?.worldTime ?? game.time.worldTime;
    const timeOffset = context.advanceTime ?? 0;

    const initiative = this.initiative;
    for (const combatant of this.combatants) {
      if (combatant.isDefeated) continue;
      const actor = combatant.actor;
      if (!actor) continue;

      actor.expireActiveEffects?.({ combat: this, initiative, timeOffset, worldTime });
    }
  }

  _onDelete(options, userId) {
    super._onDelete(options, userId);

    if (game.user.id !== userId) return;

    // Show experience distributor after combat
    if (!this.started) return;
    const xpCfg = game.settings.get("pf1", "experienceConfig");
    if (xpCfg.disable) return;

    const openUI = xpCfg.openDistributor;
    const skipPrompt = pf1.documents.settings.getSkipActionPrompt();
    if (openUI ^ skipPrompt) {
      pf1.applications.ExperienceDistributor.fromCombat(this);
    }
  }

  /**
   * Get current initiative.
   *
   * @type {number|undefined}
   */
  get initiative() {
    return this.combatant?.initiative;
  }
}
